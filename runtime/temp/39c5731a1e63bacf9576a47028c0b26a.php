<?php /*a:1:{s:66:"D:\wwwroot\jlh_php_code\application\index\view\order\pro_list.html";i:1602825047;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
<style>
input[placeholder], [placeholder], *[placeholder],textarea[placeholder]{ color:rgba(255,255,255,0.5);}
input::-webkit-input-placeholder{ color:rgba(255,255,255,0.5);} 
input:-moz-placeholder{ color:rgba(255,255,255,0.5);} 
input::-moz-placeholder{ color:rgba(255,255,255,0.5);} 
input:-ms-input-placeholder{ color:rgba(255,255,255,0.5);}
.jui_top_left{ width:10%;}
.search_con{ width:90%;}
</style>
</head>
<body class="jui_bg_grey">
<!-- 搜索 -->
<div class="jui_top_bar search_bar">
     <a class="jui_top_left" href="<?php echo url('Order/shop'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_flex_row_center search_con">
          <input class="jui_flex1 bg_none jui_fc_fff searchs" type="search" placeholder="输入关键字">
          <img src=" /static/index/icons/search.png" style="width:.4rem;" id="search">
     </div>
</div>
<!-- 搜索end -->
<!-- 主体 -->
<div class="jui_main">
    <!-- 产品分类 -->
    <div class="pro_bar jui_flex jui_flex_wrap">
        <?php if(is_array($goods) || $goods instanceof \think\Collection || $goods instanceof \think\Paginator): $i = 0; $__LIST__ = $goods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?>
            <div class="pro_list jui_grid_w50">
                <a href="<?php echo url('Order/pro_con',['id'=>$row['id']]); ?>" class="pro_con">
                    <div class="pro_img"><img src="<?php echo htmlentities($row['g_img']); ?>"></div>
                    <div class="pro_text">
                        <div class="pro_tit jui_ellipsis_1"><?php echo htmlentities($row['g_title']); ?></div>
                        <div class="jui_fc_red">
                            <?php if($row['g_type']==1): if($row['g_credit']>0): ?>
                                    ¥ <?php echo htmlentities($row['g_price']); ?>+<?php echo htmlentities($row['g_credit']); ?> 积分
                                <?php else: ?>
                                    ¥ <?php echo htmlentities($row['g_price']); ?>
                                <?php endif; else: ?>
                                <?php echo htmlentities($row['g_credit']); ?>积分
                            <?php endif; ?>
                            </div>
                    </div>
                </a>
            </div>
       <?php endforeach; endif; else: echo "" ;endif; ?>

    </div>
    <!-- 产品分类end -->
</div>
<!-- 主体end -->
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>
    $("#search").click(function(){
        var search_key=$('.searchs').val();
        var key= encodeURI(search_key);
        window.location.href="<?php echo url('Order/pro_list'); ?>?key="+key;
    })
</script>
</body>
</html>
