<?php /*a:1:{s:71:"D:\wwwroot\jlh_php_code\application\index\view\center\bounsdetails.html";i:1602825048;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>奖励明细</title>
    <meta name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
</head>

<body class="jui_bg_grey">
    <!-- 头部 -->
    <div class="jui_top_bar">
        <a class="jui_top_left" href="<?php echo url('Center/center'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
        <div class="jui_top_middle">奖励明细</div>
    </div>
    <!-- 头部end -->
    <!-- 主体 -->
    <div class="jui_main">
        <!-- 明细列表
        <div class="jui_public_tit jui_bg_fff">
            <div class="jui_grid_w50 jui_font_weight">奖励明细</div>
            <div class="jui_grid_w50 jui_text_right jui_font_weight"></div>
        </div>-->
        <div class="jui_h12"></div>
        <div class="jui_bg_fff" id="buy">
            <?php if(!empty($list)): foreach($list as $key=>$vo): ?>
            <div class="jui_public_list2 jui_flex_justify_between">
                <div class="jui_flex_col">
                    <!--<a href='<?php echo url("Center/suboundetails"); ?>?fromType=<?php echo htmlentities($vo['fromType']); ?>&addDate=<?php echo htmlentities($vo['addDate']); ?>'>-->
                    <a href='#'>
                        <?php if($vo['fromDesc'] != ''): ?>
                            <p><?php echo htmlentities($vo['fromDesc']); ?>：<?php echo htmlentities($vo['fromTypeCounts']); ?>票</p>
                            <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['addDate']); ?></p>
                        <?php endif; ?>
                    </a>
                </div>
            </div>
            <?php endforeach; else: ?>
            <!-- 没有数据 -->
            <div class="jui_none_bar ">
                <img src=" /static/index/icons/none_icon.png">
                <P>暂无数据</P>
            </div>
            <!-- 没有数据end -->
            <?php endif; ?>
            <div class="jui_h12"></div>
        </div>
    </div>
    <!-- 主体end -->
</body>
<script>
    /*可用多个tab*/
    $(document).ready(function () {
        $(".jui_tab_tit li").click(function () {
            $(this).siblings().removeClass("jui_tab_on");
            $(this).addClass("jui_tab_on");
        });
    });
    $("#payAndBuy").change(function () {
        var type = $("#payAndBuy option:selected").val();
        if (type == 0) {
            $("#buy").show();
            $("#pay").hide();
        } else {
            $("#buy").hide();
            $("#pay").show();
        }
    });
</script>

</html>