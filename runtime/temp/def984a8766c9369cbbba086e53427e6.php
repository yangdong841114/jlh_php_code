<?php /*a:1:{s:62:"D:\wwwroot\jlh_php_code\application\index\view\order\shop.html";i:1602825047;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/public.js"></script>
<style>
.pro_con{ box-shadow:0px 0px 5px #eee;}
</style>
</head>
<body class="jui_bg_grey">
<!-- 主体 -->
<div class="jui_main">
    <!-- 轮播图 -->
    <div class="banner_con">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php if(is_array($banner) || $banner instanceof \think\Collection || $banner instanceof \think\Paginator): $i = 0; $__LIST__ = $banner;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <a class="swiper-slide" href="<?php echo htmlentities($v['b_link']); ?>"><img src="<?php echo htmlentities($v['b_img']); ?>"></a>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
            <div class="swiper-pagination"></div>			
        </div>
    </div>
    <script src=" /static/index/js/swiper.min.js"></script>
    <script type="text/javascript">
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2500,
                pagination: '.swiper-pagination',
                autoplayDisableOnInteraction: false,
                loop: true,
                paginationType: 'bullets',
                paginationClickable: true,
                observer: true,
            })
    </script>
    <!-- 轮播图end -->
    <!-- 快捷入口 -->
    <div class="jui_flex jui_flex_wrap jui_bg_fff">
        <?php if(is_array($cate) || $cate instanceof \think\Collection || $cate instanceof \think\Paginator): $i = 0; $__LIST__ = $cate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
            <a href="<?php echo url('Order/pro_list',['id'=>$item['id']]); ?>" class="jui_grid_w25 jui_grid_list">
                <img class="jui_grid_icon" src="<?php echo htmlentities($item['c_img']); ?>" alt="<?php echo htmlentities($item['c_name']); ?>">
                <p><?php echo htmlentities($item['c_name']); ?></p>
            </a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
    <div class="jui_public_list jui_bg_fff">
         <span class="sy_tit_icon"></span>
         <p class="jui_flex1 jui_fc_000 jui_font_weight">已释放<?php echo htmlentities($user['m_py_wot_sf']); ?></p>
         <p class="jui_flex1 jui_fc_000 jui_font_weight">分享积分<?php echo htmlentities($user['m_py_wot']); ?></p>
    </div>
    <!-- 快捷入口end -->
    <div class="jui_h12"></div>
    <!-- 积分专区 -->
    <div class="jui_public_list jui_bg_fff">
         <span class="sy_tit_icon"></span>
         <p class="jui_flex1 jui_fc_000 jui_font_weight">积分专区</p>
         <a class="jui_flex_no jui_fc_999 jui_fs13" href="<?php echo url('Order/pro_list',['type'=>2]); ?>">更多</a>
    </div>
    <div class="pro_bar jui_flex jui_flex_wrap">
        <?php if(is_array($integral_goods) || $integral_goods instanceof \think\Collection || $integral_goods instanceof \think\Paginator): $i = 0; $__LIST__ = $integral_goods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?>
            <div class="pro_list jui_grid_w50">
                <a href="<?php echo url('Order/pro_con',['id'=>$row['id']]); ?>" class="pro_con">
                    <div class="pro_img"><img src="<?php echo htmlentities($row['g_img']); ?>"></div>
                    <div class="pro_text">
                        <div class="pro_tit jui_ellipsis_1"><?php echo htmlentities($row['g_title']); ?></div>
                        <div class="jui_fc_red"><?php echo htmlentities($row['g_credit']); ?>积分</div>
                    </div>
                </a>
            </div>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>

    <!-- 积分专区end -->
    <div class="jui_h12"></div>
    <!-- 购物专区 -->
    <div class="jui_public_list jui_bg_fff">
         <span class="sy_tit_icon"></span>
         <p class="jui_flex1 jui_fc_000 jui_font_weight">购物专区</p>
         <a class="jui_flex_no jui_fc_999 jui_fs13" href="<?php echo url('Order/pro_list',['type'=>1]); ?>">更多</a>
    </div>
    <div class="pro_bar jui_flex jui_flex_wrap">
        <?php if(is_array($goods) || $goods instanceof \think\Collection || $goods instanceof \think\Paginator): $i = 0; $__LIST__ = $goods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$rows): $mod = ($i % 2 );++$i;?>
            <div class="pro_list jui_grid_w50">
                <a href="<?php echo url('Order/pro_con',['id'=>$rows['id']]); ?>" class="pro_con">
                    <div class="pro_img"><img src="<?php echo htmlentities($rows['g_img']); ?>"></div>
                    <div class="pro_text">
                        <div class="pro_tit jui_ellipsis_1"><?php echo htmlentities($rows['g_title']); ?></div>
                        <?php if($rows['g_credit']>0): ?>
                            <div class="jui_fc_red"><?php echo "¥ ".$rows['g_price']."+".$rows['g_credit']." 积分"; ?></div>
                        <?php else: ?>
                            <div class="jui_fc_red"><?php echo "¥ ".$rows['g_price'];?></div>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
    <!-- 购物专区end -->
</div>
<!-- 主体end -->
<!-- 固定底部 -->
<div class="jui_footer">
    <a href="<?php echo url('Index/index'); ?>" class="jui_foot_list">
        <b class="foot_index"></b>
        <p>交易</p>
    </a>
    <a href="<?php echo url('Index/quotations'); ?>" class="jui_foot_list">
        <b class="foot_hq"></b>
        <p>行情</p>
    </a>
    <a href="<?php echo url('Order/shop'); ?>" class="jui_foot_list jui_hover">
        <b class="foot_shop"></b>
        <p>置换仓库</p>
    </a>
    <a href="<?php echo url('Center/notice_list'); ?>" class="jui_foot_list">
        <b class="foot_notice"></b>
        <p>公告</p>
    </a>
    <a href="<?php echo url('Center/center'); ?>" class="jui_foot_list">
        <b class="foot_my"></b>
        <p>我的</p>
    </a>
</div>
<!-- 固定底部end -->
</body>
</html>
<script src=" /static/index/layer/layer.js"></script>
<script>
    $.get('<?php echo url("index/is_min_exchange_num"); ?>', function(res){

            if(res.code != 200){
                var layIndex = layer.load();
                layer.msg(res.msg,function(){ window.location.href = '<?php echo url("index/index"); ?>';});
                
               
            }
        }, 'json');

        
</script>
