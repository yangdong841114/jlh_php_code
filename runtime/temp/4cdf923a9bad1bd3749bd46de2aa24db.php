<?php /*a:1:{s:78:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/withdrawal.html";i:1589875383;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/key.css">        <!-- 安全密码css -->
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">提现</div>
    <a style="color: #fff;" href="<?php echo url('Index/goldRecord'); ?>">出金记录</a>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
     <div class="jui_pad_12">您的可提现余额：<span class="jui_fs16 jui_font_weight"><?php echo htmlentities($user['m_balance']); ?></span></div>
     <div class="jui_bg_fff">
         <div class="jui_public_list">
              <p class="dlmm_left_text jui_pad_r8">提现金额：</p>
              <input class="jui_flex1 jui_fc_000" type="number" value="" placeholder="请输入提现金额" autofocus name="t_money" id="t_money">
         </div>
         <div class="jui_public_list">
             <p class="jui_flex_no jui_pad_r8">备&nbsp;&nbsp;&nbsp;注：</p>
             <input class="jui_flex1 jui_fc_000" id="desc" type="text" value="" placeholder="    备注（选填）">
         </div>
         <div class="jui_public_list">
              <p class="dlmm_left_text jui_pad_r8">姓名：</p>
              <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($user['m_name']); ?>" readonly>
         </div>
         <div class="jui_public_list jui_none kaihu">
              <p class="dlmm_left_text jui_pad_r8">所属银行：</p>
              <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($user['m_bank_name']); ?>" readonly>
         </div>
         <div class="jui_public_list">
              <p class="dlmm_left_text jui_pad_r8">银行卡号：</p>
              <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($user['m_bank_carid']); ?>" readonly>
         </div>
     </div>
     <div class="jui_h16"></div>
     <div class="jui_public_btn" id="J_OpenKeyBoard"><input type="button" value="提现"></div>
</div>
<!-- 主体end -->
<!-- 安全密码 -->
<div class="m-keyboard" id="J_KeyBoard">
    <!-- 自定义内容区域 -->
    <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>
    <!-- 自定义内容区域 -->
</div>
<!-- 安全密码end -->
</body>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/box_psw.js"></script>
<script>
    //弹框输入安全密码
    !function ($, ydui) {
        var dialog      = ydui.dialog;
        var $keyboard   = $('#J_KeyBoard');
        var type        = 0;
        var order_id    = 0;
        // 初始化参数
        $keyboard.keyBoard({
            disorder: true,   // 是否打乱数字顺序
            title: '安全键盘' // 显示标题
        });
        // 打开键盘
        $('#J_OpenKeyBoard').on('click', function () {
            $keyboard.keyBoard('open');
            type        =   $(this).attr('data-type');
            order_id    =   $(this).parents('.order_list').find('.order_id').val();
        });
        // 六位密码输入完毕后执行
        $keyboard.on('done.ydui.keyboard', function (ret) {
            var pass    =   ret.password;
            $.post("<?php echo url('Center/checking_pay_pwd'); ?>",{pass:pass},function(res){
                layer.msg(res.msg);
                if(res.code==1){
                    click_withdrawal();
                    $keyboard.keyBoard('close');
                }
            },'json');
            // 弹出请求中提示框
            dialog.loading.open('验证支付密码');
            // 模拟AJAX校验密码
            setTimeout(function () {
                // 关闭请求中提示框
                dialog.loading.close();
                // 显示错误信息
            }, 1500);
        });
    }(jQuery, YDUI);
    function click_withdrawal(){
       var t_money      =$('#t_money').val();
       var user_balance =<?php echo htmlentities($user['m_balance']); ?>;
       var desc         =$('#desc').val();
       if(t_money==''){
           layer.msg('请输入提现金额');
           return;
       }
       if(parseInt(t_money)>user_balance){
           layer.msg('您的提现金额不足');
           return;
       }
       $.post("<?php echo url('Index/withdrawal'); ?>",{t_money:t_money,t_desc:desc},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function(){
                    window.location.href="<?php echo url('Index/index'); ?>";
                },1000)
            }
       },'json');

    }
</script>


</html>
