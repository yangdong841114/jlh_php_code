<?php /*a:1:{s:65:"D:\wwwroot\jlh_php_code\application\index\view\center\center.html";i:1602825049;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 主体 -->
<div class="jui_main">
    <!-- 头部 -->
    <div class="jui_pad_12 jui_bg_fff jui_flex_row_center">
              <a href="<?php echo url('Center/avatar'); ?>" class="my_img jui_bor_rad_50"><img src="<?php echo htmlentities($user['m_avatar']); ?>" alt="头像"></a>
              <div class="jui_flex1">
                   <p class="jui_fs15 jui_pad_b5"><?php echo htmlentities($user['m_nickname']); ?></p>
                   <p><?php echo htmlentities($user['m_account']); ?></p>
              </div>
    </div>
    <!-- 头部end -->
    <div class="jui_h12"></div>
    <div class="jui_bg_fff">     
         <a href="<?php echo url('Center/info'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/my_icon12.png">
               <p class="jui_fc_000 jui_flex1">个人资料</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <?php if($user['m_isFenGongSi'] == 1): ?>
         <a href="<?php echo url('Center/fengongsi'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/nav_icon03_hover.png">
               <p class="jui_fc_000 jui_flex1">服务补贴</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <?php endif; ?>
         <a href="<?php echo url('Center/bounsdetails'); ?>" class="jui_public_list">
                <img class="my_icon" src=" /static/index/icons/nav_icon03_hover.png">
                <p class="jui_fc_000 jui_flex1">奖励明细</p>
                <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
        </a>
         <a href="<?php echo url('Center/integral'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/nav_icon03_hover.png">
               <p class="jui_fc_000 jui_flex1">我的积分</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <a href="<?php echo url('Center/balance'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/my_icon19.png">
               <p class="jui_fc_000 jui_flex1">我的余额</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <a href="<?php echo url('Center/dlmm_edit'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/my_icon01.png">
               <p class="jui_fc_000 jui_flex1">登录密码</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
        <?php if($user['m_pay_pwd'] == ''): ?>
            <a href="<?php echo url('Center/jymm_set'); ?>" class="jui_public_list">
                <img class="my_icon" src=" /static/index/icons/my_icon10.png">
                <p class="jui_fc_000 jui_flex1">交易密码</p>
                <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
            </a>
        <?php else: ?>
            <a href="<?php echo url('Center/jymm_edit'); ?>" class="jui_public_list">
                <img class="my_icon" src=" /static/index/icons/my_icon10.png">
                <p class="jui_fc_000 jui_flex1">交易密码</p>
                <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
            </a>
        <?php endif; ?>
         <a href="<?php echo url('Center/address_list',['type'=>2]); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/my_icon09.png">
               <p class="jui_fc_000 jui_flex1">地址管理</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <a href="<?php echo url('Center/about'); ?>" class="jui_public_list">
               <img class="my_icon" src=" /static/index/icons/my_icon05.png">
               <p class="jui_fc_000 jui_flex1">关于我们</p>
               <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
         </a>
         <div class="jui_public_list" id="clear">
               <img class="my_icon" src=" /static/index/icons/my_icon04.png">
               <p class="jui_fc_000 jui_flex1">清空缓存</p>
         </div>
         <div class="jui_public_list" id="sign_out">
               <img class="my_icon" src=" /static/index/icons/my_icon07.png">
               <p class="jui_fc_000 jui_flex1">安全退出</p>
         </div>
     </div>
     <div class="jui_h12"></div>
</div>
<!-- 主体end -->

<!-- 固定底部 -->
<div class="jui_footer">
    <a href="<?php echo url('Index/index'); ?>" class="jui_foot_list">
        <b class="foot_index"></b>
        <p>交易</p>
    </a>
    <a href="<?php echo url('Index/quotations'); ?>" class="jui_foot_list">
        <b class="foot_hq"></b>
        <p>行情</p>
    </a>
    <?php if($configMsg == '1'): ?>
        <a href="#" class="jui_foot_list no_exchange">
    <?php else: ?>
        <a href="<?php echo url('Order/shop'); ?>" class="jui_foot_list">
    <?php endif; ?>

        <b class="foot_shop"></b>

        <p>置换仓库</p>

    </a>
    <a href="<?php echo url('Center/notice_list'); ?>" class="jui_foot_list">
        <b class="foot_notice"></b>
        <p>公告</p>
    </a>
    <a href="<?php echo url('Center/center'); ?>" class="jui_foot_list jui_hover">
        <b class="foot_my"></b>
        <p>我的</p>
    </a>
</div>
<!-- 固定底部end -->

<!-- 提示 -->
<div class="jui_box_bar jui_none">
     <div class="jui_box_conbar">
           <div class="jui_box_con jui_bg_ztqian">
                 <div class="jui_h20"></div>
                 <div class="jui_box_pub_middle">确定要退出吗？</div>
                 <div class="jui_h20"></div>
                 <div class="jui_box_btnbar jui_flex_row_center jui_flex_justify_center">
                       <div class="jui_box_btn jui_bor_rad_5 jui_fc_zhuse" id="close">取消</div>
                       <div class="jui_box_btn jui_bor_rad_5 jui_bg_zhuse jui_bor_none jui_fc_fff" id="submit">确定</div>
                 </div>
                 <div class="jui_h20"></div>
           </div>
     </div>
</div>
<!-- 提示end -->
</body>
<script>	
$(document).ready(function(){	
    $("#sign_out").on('click',function(){
        $('.jui_box_bar').removeClass('jui_none');
    });
    $("#close").on('click',function(){
        $('.jui_box_bar').addClass('jui_none');
    });
    $("#submit").on('click',function(){
        $('.jui_box_bar').addClass('jui_none');
    });
});
</script>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>
    
    $('.no_exchange').click(function(){
        layer.msg('非签约时间不可兑换');
    });
    
    $('#submit').click(function(){
        $.post("<?php echo url('Center/login_out'); ?>",{data:1},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function () {
                    window.location.href=res.url
                },1000);
            }
        },'json')
    });
    $('#clear').click(function(){
        $.post("<?php echo url('Center/clear_cache'); ?>",{data:1},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function(){
                    window.location.href="?m=index&c=index"
                },1000)
            }
        },'json')
    })
</script>
</html>
