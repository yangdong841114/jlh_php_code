<?php /*a:1:{s:74:"D:\wwwroot\jlh_php_code\application\index\view\order\get_express_info.html";i:1602825047;}*/ ?>
<!doctype html>

<html>

<head>

  <meta charset="utf-8">

  <title><?php echo htmlentities($config['w_name']); ?></title>

  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

  <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">

  <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">

  <script src=" /static/index/js/flexible.js"></script>

  <script src=" /static/index/js/jquery-3.3.1.min.js"></script>

  <script src=" /static/index/js/public.js"></script>

  <script src=" /static/index/js/clipboard.js"></script>
  <style>
    ul,
    li {
      padding: 0;
      margin: 0;
      list-style: none;
      font-family: tahoma, arial, 'Hiragino Sans GB', '\5b8b\4f53', sans-serif
    }

    .box-main {
      margin: 0 auto;
      width: 99%;
      padding: 10px;
      border: 1px solid #e8e8e8
    }

    .box-main h3.title {
      height: 40px;
      font-size: 18px;
      line-height: 40px;
      text-indent: 18px;
      color: #1c81e5;
      text-align: left;
      background: #F7F7F7;
      margin-bottom: 18px
    }

    .package-box {
      overflow: hidden;
      margin: 0 auto;
      width: 100%;
    }

    .package-box:before {
      content: " ";
      background-color: #fff;
      display: block;
      position: absolute;
      top: -4px;
      left: 20px;
      width: 10px;
      height: 4px
    }

    .package-box .supply {
      margin-top: 20px;
      padding-right: 10px;
      border-top: 1px solid #e8e8e8;
      line-height: 28px;
      color: #959595;
      text-align: right
    }

    .package-detail {
      margin: -9px 0 0 12px;
      padding: 0 0 0 10px;
      list-style: none;
      line-height: 30px;
      font-size: 12px;
      overflow: hidden
    }

    .package-detail li {
      border-left: 1px solid #d9d9d9
    }

    .package-detail li:before {
      content: '';
      border: 3px solid #fff;
      background: #d9d9d9;
      display: inline-block;
      width: 5px;
      height: 5px;
      border-radius: 5px;
      margin-left: -6px;
      margin-right: 10px
    }

    .package-detail .date {
      font-weight: 700;
      margin-right: 8px;
      font-family: arial;
      display: inline-block
    }

    .package-detail .text {
      width: 580px;
      display: inline-block;
      vertical-align: text-top;
      line-height: 1.3em
    }

    .package-detail .latest:before {
      background: #fe4300;
      border-color: #f8e9e4
    }
  </style>
</head>

<body class="jui_bg_grey">

  <!-- 头部 -->

  <div class="jui_top_bar">

    <a class="jui_top_left" href="<?php echo url('Order/order_list'); ?>"><img src=" /static/index/icons/back_icon.png"></a>

    <div class="jui_top_middle">物流查询</div>

  </div>

  <!-- 头部end -->

  <!-- 主体 -->

  <div class="jui_main jui_pad_16">

    <?php if(!(empty($allInfo) || (($allInfo instanceof \think\Collection || $allInfo instanceof \think\Paginator ) && $allInfo->isEmpty()))): if(is_array($allInfo) || $allInfo instanceof \think\Collection || $allInfo instanceof \think\Paginator): $i = 0; $__LIST__ = $allInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>

    <div class="jui_bg_fff jui_bor_rad_5 jui_flex_col_center yqm_bar">
      <div class="box-main">
        <h3 class="title"><span>物流跟踪信息 </span></h3>
        <h3 class="title"><span>物流公司：<?php echo htmlentities($v['expressName']); ?> </span></h3>
        <h3 class="title"><span>物流单号：<?php echo htmlentities($v['expressNum']); ?></span></h3>

        <div class="package-box">
          <ul class="package-detail">

            <?php if(is_array($v['data']) || $v['data'] instanceof \think\Collection || $v['data'] instanceof \think\Paginator): $place = 0; $__LIST__ = $v['data'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub): $mod = ($place % 2 );++$place;if($place==0): ?>
                <li class="latest"><span class="date"><?php echo htmlentities($sub['time']); ?></span> <span class="text"><?php echo htmlentities($sub['context']); ?></span>
            </li>
            <?php else: ?>
             <li><span class="date"><?php echo htmlentities($sub['time']); ?></span> <span class="text"><?php echo htmlentities($sub['context']); ?></span></li>
            <?php endif; ?>

            <?php endforeach; endif; else: echo "" ;endif; ?>

          </ul>
        </div>
      </div>
    </div>
    <?php endforeach; endif; else: echo "" ;endif; else: ?>
    <div class="box-main">
      <h3 class="title"><span>无待收货订单</span></h3>
      <div class="package-box">
        <ul class="package-detail">


        </ul>
      </div>
    </div>
    <?php endif; ?>
  </div>

  <!-- 主体end -->


</body>

<script src=" /static/index/layer/layer.js"></script>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>


</html>