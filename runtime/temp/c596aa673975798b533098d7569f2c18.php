<?php /*a:1:{s:70:"D:\wwwroot\jlh_php_code\application\index\view\center\notice_list.html";i:1602825048;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
    <style>
        .notice_list{ background:#fff; margin-top: .4266rem;}
        .notice_img{ width:100%; height:4rem; overflow: hidden;
            display: box;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-align-items:center;
            -moz-align-items:center;
            -ms-align-items:center;
            align-items:center;
            -webkit-justify-content:center;
            -moz-justify-content:center;
            -ms-justify-content:center;
            justify-content:center;
        }/* 图片比例3:2 */
        .notice_img img{ width: 100%; height:100%;}
    </style>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <div class="jui_top_left"></div>
     <div class="jui_top_middle">公告</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">

    <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
        <a class="notice_list jui_flex_col" href="<?php echo url('Center/notice_con',['id'=>$v['id']]); ?>">
            <div class="notice_img"><img src="<?php echo htmlentities($v['n_img']); ?>" alt="<?php echo htmlentities($config['w_name']); ?>"></div>
            <div class="jui_flex_col jui_pad_1216">
                <div class="jui_fc_000 jui_fs15"><?php echo htmlentities($v['n_title']); ?></div>
                <div class="jui_flex_row_center jui_flex_justify_between">
                    <p class="jui_fc_999 jui_fs12">发布时间:<?php echo htmlentities($v['n_time']); ?></p>
                    <p class="jui_fc_999 jui_fs12">阅读:<?php echo htmlentities($v['n_auth']); ?></p>
                </div>
            </div>
        </a>
       
   <?php endforeach; endif; else: echo "" ;endif; ?>
</div>
<!-- 主体end -->
<!-- 固定底部 -->
<div class="jui_footer">
    <a href="<?php echo url('Index/index'); ?>" class="jui_foot_list">
        <b class="foot_index"></b>
        <p>交易</p>
    </a>
    <a href="<?php echo url('Index/quotations'); ?>" class="jui_foot_list">
        <b class="foot_hq"></b>
        <p>行情</p>
    </a>
    <?php if($configMsg == '1'): ?>
        <a href="#" class="jui_foot_list no_exchange">
    <?php else: ?>
        <a href="<?php echo url('Order/shop'); ?>" class="jui_foot_list">
    <?php endif; ?>

        <b class="foot_shop"></b>

        <p>置换仓库</p>

    </a>
    <a href="<?php echo url('Center/notice_list'); ?>" class="jui_foot_list jui_hover">
        <b class="foot_notice"></b>
        <p>公告</p>
    </a>
    <a href="<?php echo url('Center/center'); ?>" class="jui_foot_list">
        <b class="foot_my"></b>
        <p>我的</p>
    </a>
</div>
<!-- 固定底部end -->
</body>
</html>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>
    $('.no_exchange').click(function(){
        layer.msg('非签约时间不可兑换');
    });
</script>
