<?php /*a:1:{s:67:"D:\wwwroot\jlh_php_code\application\index\view\center\integral.html";i:1602825048;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>积分明细</title>
    <meta name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
</head>

<body class="jui_bg_grey">
    <!-- 头部 -->
    <div class="jui_top_bar">
        <a class="jui_top_left" href="<?php echo url('Center/center'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
        <div class="jui_top_middle">积分明细</div>
    </div>
    <!-- 头部end -->
    <!-- 主体 -->
    <div class="jui_main">
        <!-- 明细列表 -->
        <div class="jui_public_tit jui_bg_fff">
            <div class="jui_grid_w50 jui_font_weight">我的积分</div>
            <div class="jui_grid_w50 jui_text_right jui_font_weight"><?php echo htmlentities($user['m_integral']); ?></div>
        </div>
        <div class="jui_public_tit jui_bg_fff">
            <div class="jui_grid_w50 jui_font_weight">昨日积分</div>
            <div class="jui_grid_w50 jui_text_right jui_font_weight"><?php echo htmlentities($user['m_zintegral']); ?></div>
        </div>
        <div class="jui_h12"></div>
        <div class="jui_flex_row_center buy_select">
            <select class="jui_flex1" id="payAndBuy">
                <option value="0">收入</option>
                <option value="1">支出</option>
            </select>
            <img class="jui_arrow_rimg jui_mar_r12" src="/static/index/icons/jt_right.png">
        </div>
        <div class="jui_bg_fff">
            <div class="jui_public_list2 jui_flex_justify_between">
                <p>收入汇总:<?php echo htmlentities($buyAmounts); ?></p>
            </div>
        </div>
        <div class="jui_bg_fff">
            <div class="jui_public_list2 jui_flex_justify_between">
                <p>支出汇总:<?php echo htmlentities($payAmounts); ?></p>
            </div>
        </div>
        <div class="jui_bg_fff" id="buy">
            <?php if(!empty($deal)): foreach($deal as $key=>$vo): ?>
            <div class="jui_public_list2 jui_flex_justify_between">
                <?php if($vo['d_zljl'] == 0): ?>
                <div class="jui_flex_col">
                    <p>卖出批发票<?php echo htmlentities($vo['d_total']); ?>票，获得积分<?php echo htmlentities($vo['d_total']*93); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                </div>
                <?php else: ?>
                <div class="jui_flex_col">
                    <?php if($vo['d_credit_2'] > 0): ?>
                    <p>卖出批发票<?php echo htmlentities($vo['d_total']); ?>票，获得积分<?php echo htmlentities($vo['d_zljl']); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                    <?php else: ?>
                    <p>卖出奖励票票<?php echo htmlentities($vo['d_total']); ?>票，获得比例积分<?php echo htmlentities($vo['d_zljl']); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
            <?php endif; if(!empty($dealHistory)): foreach($dealHistory as $key=>$vo): ?>
            <div class="jui_public_list2 jui_flex_justify_between">
                <?php if($vo['d_zljl'] == 0): ?>
                <div class="jui_flex_col">
                    <p>卖出批发票<?php echo htmlentities($vo['d_total']); ?>票，获得积分<?php echo htmlentities($vo['d_total']*93); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                </div>
                <?php else: ?>
                <div class="jui_flex_col">
                    <?php if($vo['d_credit_2'] > 0): ?>
                    <p>卖出批发票<?php echo htmlentities($vo['d_total']); ?>票，获得积分<?php echo htmlentities($vo['d_zljl']); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                    <?php else: ?>
                    <p>卖出奖励票票<?php echo htmlentities($vo['d_total']); ?>票，获得比例积分<?php echo htmlentities($vo['d_zljl']); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['d_date']); ?></p>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php endforeach; else: ?>
            <!-- 没有数据 -->
            <div class="jui_none_bar ">
                <img src=" /static/index/icons/none_icon.png">
                <P>暂无数据</P>
            </div>
            <!-- 没有数据end -->
            <?php endif; ?>
        </div>
        <!-- 明细列表end -->
        <div class="jui_bg_fff" id="pay" style="display:none;">
            <?php if(!empty($order)): foreach($order as $key=>$vo): ?>
            <div class="jui_public_list2 jui_flex_justify_between">
                <div class="jui_flex_col">
                    <p>积分兑换产品，支出积分<?php echo htmlentities($vo['o_buy_num'] * 186); ?></p>
                    <p class="jui_fs12 jui_fc_999"><?php echo htmlentities($vo['o_addtime']); ?></p>
                </div>
            </div>
            <?php endforeach; else: ?>
            <!-- 没有数据 -->
            <div class="jui_none_bar ">
                <img src=" /static/index/icons/none_icon.png">
                <P>暂无数据</P>
            </div>
            <!-- 没有数据end -->
            <?php endif; ?>
        </div>
    </div>
    <!-- 主体end -->
</body>
<script>
    /*可用多个tab*/
    $(document).ready(function () {
        $(".jui_tab_tit li").click(function () {
            $(this).siblings().removeClass("jui_tab_on");
            $(this).addClass("jui_tab_on");
        });
    });
    $("#payAndBuy").change(function () {
        var type = $("#payAndBuy option:selected").val();
        if (type == 0) {
            $("#buy").show();
            $("#pay").hide();
        } else {
            $("#buy").hide();
            $("#pay").show();
        }
    });
</script>

</html>