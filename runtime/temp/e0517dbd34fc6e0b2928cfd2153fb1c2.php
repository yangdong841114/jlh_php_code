<?php /*a:1:{s:77:"/www/wwwroot/www.jiulonghu123.com/application/index/view/order/order_con.html";i:1600227438;}*/ ?>
<!doctype html>

<html>

<head>

<meta charset="utf-8">

<title><?php echo htmlentities($config['w_name']); ?></title>

<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">

<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">

<link rel="stylesheet" type="text/css" href=" /static/index/css/key.css">        <!-- 安全密码css -->

<script src=" /static/index/js/flexible.js"></script>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>

<script src=" /static/index/js/public.js"></script>

</head>

<body class="jui_bg_grey">

<!-- 头部 -->

<div class="jui_top_bar">

     <a class="jui_top_left" href="<?php echo url('Order/order_list'); ?>"><img src=" /static/index/icons/back_icon.png"></a>

     <div class="jui_top_middle">等待付款</div>

</div>

<!-- 头部end -->

<!-- 主体 -->

<div class="jui_main">

     <!-- 收货地址 -->

     <div class="jui_flex_row_center jui_bg_fff jui_pad_16">

             <img class="order_address_icon" src=" /static/index/icons/address_icon.png">

             <div class="jui_flex1 jui_flex_col jui_pad_l16">

                  <p class="jui_fs15 jui_fc_000 jui_pad_b5"><?php echo htmlentities($address['a_name']); ?><span class="jui_pad_l12"><?php echo htmlentities($address['a_phone']); ?></span></p>

                  <p class="jui_fs13 jui_line_h13"><?php echo htmlentities($address['a_city']); ?><?php echo htmlentities($address['a_detail']); ?></p>

             </div>

     </div>

     <!-- 收货地址end -->

     <div class="jui_h12"></div>

     <!-- 产品列表 -->

     <a href="<?php echo url('Order/pro_con',['id'=>$goods['id']]); ?>" class="jui_flex jui_pad_1216 jui_bg_fff">

        <div class="order_img"><img src="<?php echo htmlentities($goods['g_img']); ?>"></div>

        <div class="jui_flex1 jui_flex_col">

             <div class="jui_flex_row_center jui_pad_b5">

                 <?php if($goods['g_type'] == 1): ?>

                    <div class="jui_tag jui_bg_red">购物专区</div>

                 <?php else: ?>

                    <div class="jui_tag jui_bg_red">积分专区</div>

                 <?php endif; ?>

                   <p class="jui_fc_000 jui_pad_l5"><?php echo htmlentities($goods['g_title']); ?></p>

             </div>

             <div class="jui_flex_row_center jui_flex_justify_between">

                 <?php if($goods['g_type'] == 1): if($goods['g_credit']>0): ?>

                        <p class="jui_fc_red">¥<?php echo htmlentities($order['o_price']); ?>+<?php echo htmlentities($order['o_credit']); ?>积分</p>

                    <?php else: ?>

                        <p class="jui_fc_red">¥<?php echo htmlentities($order['o_price']); ?></p>

                    <?php endif; else: ?>

                     <p class="jui_fc_red">积分<?php echo htmlentities($order['o_credit']); ?></p>

                 <?php endif; ?>

                  <p>×<?php echo htmlentities($order['o_buy_num']); ?></p>

             </div>

        </div>

    </a>

    <!-- 产品列表end -->

    <div class="jui_bg_fff jui_pad_1216 jui_line_h30">

         <div class="jui_fs12 jui_flex_row_center jui_flex_justify_between jui_fc_999">

               <p>商品总价</p>

             <?php if($goods['g_type'] == 1): if($order['o_credit_2']>0): ?>

                    <p>¥ <?php echo htmlentities($order['o_credit_1']); ?>+<?php echo htmlentities($order['o_credit_2']); ?> 积分</p>

                <?php else: ?>

                    <p>¥<?php echo htmlentities($order['o_credit_1']); ?></p>

                <?php endif; else: ?>

                 <p>积分<?php echo htmlentities($order['o_credit_2']); ?></p>

             <?php endif; ?>

         </div>

         <div class="jui_fs12 jui_flex_row_center jui_flex_justify_between jui_fc_999">

               <p>运费</p>

               <p>¥<?php echo htmlentities($order['o_company_price']); ?></p>

         </div>



    </div>

    <div class="jui_bg_fff jui_pad_16 jui_bor_top">

         <div class="jui_fs15 jui_flex_row_center jui_flex_justify_between">

               <p class="jui_fc_000">实付款</p>

             <?php if($goods['g_type'] == 1): if($order['o_credit_2']>0): ?>

                    <p class="jui_fc_red">¥ <?php echo htmlentities($order['o_credit_1']); ?>+<?php echo htmlentities($order['o_credit_2']); ?> 积分</p>

                 <?php else: ?>

                    <p class="jui_fc_red">¥ <?php echo htmlentities($order['o_credit_1']); ?></p>

                 <?php endif; else: ?>

                <p class="jui_fc_red">¥ <?php echo htmlentities($order['o_credit_2']); ?></p>

             <?php endif; ?>

         </div>

    </div>

    <div class="jui_h12"></div>

    <!-- 订单信息 -->

    <div class="jui_bg_fff">

         <div class="jui_public_tit jui_fs12 jui_fc_000 jui_bor_bottom">订单信息</div>

         <div class="ddcon_text_bar">

              <div class="ddcon_text_list">

                   <p>订单编号</p>

                   <p><?php echo htmlentities($order['o_code']); ?></p>

              </div>

             <?php if($order['o_status']==2 || $order['o_status']==3): ?>

                 <div class="ddcon_text_list">

                     <p>快递公司</p>

                     <p><?php echo htmlentities($order['o_company']); ?></p>

                 </div>

                 <div class="ddcon_text_list">

                     <p>快递编号</p>

                     <p><?php echo htmlentities($order['o_company_num']); ?></p>

                 </div>

             <?php endif; ?>

              <div class="ddcon_text_list">

                   <p>创建时间</p>

                   <p><?php echo htmlentities($order['o_addtime']); ?></p>

              </div>

             <?php if($order['o_status']==1): ?>

                 <div class="ddcon_text_list">

                     <p>付款时间</p>

                     <p><?php echo htmlentities($order['o_pay_time']); ?></p>

                 </div>

             <?php endif; if($order['o_status']==2): ?>

                 <div class="ddcon_text_list">

                     <p>发货时间</p>

                     <p><?php echo htmlentities($order['o_send_time']); ?></p>

                 </div>

             <?php endif; if($order['o_status']==3): ?>

                 <div class="ddcon_text_list">

                     <p>完成时间</p>

                     <p><?php echo htmlentities($order['o_take_time']); ?></p>

                 </div>

             <?php endif; ?>

         </div>

    </div>

    <!-- 订单信息end -->

    <div class="jui_h12"></div>



</div>

<!-- 主体end -->

<div class="jui_footer jui_flex_justify_end jui_pad_16">

    <?php if($order['o_status']==2): ?>

        <div class="order_btn order_btn2 J_OpenKeyBoard" data-type="1">确认收货</div>

    <?php elseif($order['o_status']==3): ?>

        <div class="order_btn order_btn2 J_OpenKeyBoard" data-type="2">删除</div>

    <?php endif; ?>

</div>



<!-- 安全密码 -->

<div class="m-keyboard" id="J_KeyBoard">

    <!-- 自定义内容区域 -->

    <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>

    <!-- 自定义内容区域 -->

</div>

<!-- 安全密码end -->



<script src=" /static/index/layer/layer.js"></script>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>

<script src=" /static/index/js/box_psw.js"></script>

<script>

    //弹框输入安全密码

    !function ($, ydui) {

        var dialog      = ydui.dialog;

        var $keyboard   = $('#J_KeyBoard');

        var type        = 0;

        var order_id    = 0;

        // 初始化参数

        $keyboard.keyBoard({

            disorder: true,   // 是否打乱数字顺序

            title: '安全键盘' // 显示标题

        });

        // 打开键盘

        $('.J_OpenKeyBoard').on('click', function () {

            $keyboard.keyBoard('open');

            type        =   $(this).attr('data-type');

            order_id    =   $(this).parents('.order_list').find('.order_id').val();

        });

        // 六位密码输入完毕后执行

        $keyboard.on('done.ydui.keyboard', function (ret) {

            var pass    =   ret.password;

            $.post("<?php echo url('Center/checking_pay_pwd'); ?>",{pass:pass},function(res){

                layer.msg(res.msg);

                if(res.code==1){

                    if(type==1 && order_id !=0){

                        taken(order_id);

                    }else if(type==2){

                        del_order(order_id);

                    }

                }

            },'json');

            // 弹出请求中提示框

            dialog.loading.open('验证支付密码');

            // 模拟AJAX校验密码

            setTimeout(function () {

                // 关闭请求中提示框

                dialog.loading.close();

                // 显示错误信息

            }, 1500);

        });

    }(jQuery, YDUI)



   function taken(){

        var order_id =<?php echo htmlentities($order['id']); ?>;

        $.post("<?php echo url('Order/taken'); ?>",{order_id:order_id},function(res){

            layer.msg(res.msg);

            if(res.code==1){

                setTimeout(function(){

                    window.location.reload();

                },1000)

            }

        },'json')

    }

    function del_order(){

        var order_id=<?php echo htmlentities($order['id']); ?>;

        $.post("<?php echo url('Order/del_order'); ?>",{order_id:order_id},function(res){

            layer.msg(res.msg);

            if(res.code==1){

                setTimeout(function(){

                    var type=$('.jui_tab_on').find('.types').val();

                    window.location.href="<?php echo url('Order/order_list'); ?>?type="+type;

                },1000)

            }

        },'json')

    }

</script>

</body>

</html>

