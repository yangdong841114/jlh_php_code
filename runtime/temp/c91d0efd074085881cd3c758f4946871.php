<?php /*a:1:{s:79:"/www/wwwroot/www.jiulonghu123.com/application/index/view/center/notice_con.html";i:1586753587;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_fff">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Center/notice_list'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">公告详情</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
    <div class="newscon_tit">
         <h2><?php echo htmlentities($detail['n_title']); ?></h2>
         <p class="jui_fs12 jui_fc_999">发表时间：<?php echo htmlentities($detail['n_time']); ?></p>
    </div>
    <div class="newscon_zt">
        <?php echo $detail['n_content']; ?>
    </div>
</div>
<!-- 主体end -->
</body>
<script>
$(document).ready(function(){
	/*如果富文本上传的图片，去掉一些行内样式*/
	$(".newscon_zt img").removeAttr("width");	
	$(".newscon_zt img").removeAttr("height");	
	$(".newscon_zt img").removeAttr("alt");	
	$(".newscon_zt img").removeAttr("width");	
	$(".newscon_zt img").removeAttr("style");	
	$(".newscon_zt img").removeAttr("title");
});
</script>
</html>
