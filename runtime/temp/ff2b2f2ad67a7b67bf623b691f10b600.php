<?php /*a:1:{s:78:"/www/wwwroot/www.jiulonghu123.com/application/index/view/order/order_list.html";i:1586753587;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/key.css">        <!-- 安全密码css -->
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">我的订单</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
    <!-- 标题 -->
    <ul class="jui_tab_tit order_tit_bar">
        <li class="<?php echo $type==0?'jui_tab_on':'';?>"><input type="hidden" class="types" value="0"/>全部</li>
        <li class="<?php echo $type==1?'jui_tab_on':'';?>"><input type="hidden" class="types" value="1"/>待发货</li>
        <li class="<?php echo $type==2?'jui_tab_on':'';?>"><input type="hidden" class="types" value="2"/>待收货</li>
        <li class="<?php echo $type==3?'jui_tab_on':'';?>"><input type="hidden" class="types" value="3"/>已完成</li>
    </ul>
    <!-- 标题end -->
    <div class="jui_h40"></div>

    <?php if($status==0): ?>
        <!-- 没有订单 -->
        <div class="jui_none_bar ">
            <img src=" /static/index/icons/none_icon.png">
            <P>暂无数据</P>
        </div>
        <!-- 没有订单end -->
    <?php else: if(is_array($order) || $order instanceof \think\Collection || $order instanceof \think\Paginator): $i = 0; $__LIST__ = $order;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <div class="order_list">
                    <input type="hidden" class="order_id" value="<?php echo htmlentities($v['id']); ?>">
                    <div class="jui_pad_1216 jui_flex_row_center jui_flex_justify_between jui_bor_bottom">
                        <p class="jui_fs12"><?php echo htmlentities($v['o_pay_time']); ?></p>
                        <?php if($v['o_status']==0): ?>
                            <p class="jui_fs12 jui_fc_red">待付款</p>
                        <?php elseif($v['o_status']==1): ?>
                            <p class="jui_fs12 jui_fc_red">待发货</p>
                        <?php elseif($v['o_status']==2): ?>
                            <p class="jui_fs12 jui_fc_red">待收货</p>
                        <?php elseif($v['o_status']==3): ?>
                            <p class="jui_fs12 jui_fc_red">已完成</p>
                        <?php endif; ?>
                    </div>
                    <a href="<?php echo url('Order/order_con',['id'=>$v['id']]); ?>">
                        <div class="jui_pad_1216 jui_flex">
                            <div class="order_img"><img src="<?php echo htmlentities($v['g_img']); ?>"></div>
                            <div class="jui_flex1 jui_flex_col">
                                <div class="jui_flex_row_center jui_pad_b5">
                                    <?php if($v['g_type']==1): ?>
                                        <div class="jui_tag jui_bg_red">购物专区</div>
                                    <?php elseif($v['g_type']==2): ?>
                                        <div class="jui_tag jui_bg_red">积分专区</div>
                                    <?php endif; ?>
                                    <p class="jui_fc_000 jui_pad_l5"><?php echo htmlentities($v['g_title']); ?></p>
                                </div>
                                <div class="jui_flex_row_center jui_flex_justify_between">
                                    <p class="jui_fc_red">
                                        <?php if($v['g_type']==1): if($v['g_credit']>0): ?>
                                                ¥ <?php echo htmlentities($v['o_price']); ?>+ <?php echo htmlentities($v['o_credit']); ?>积分
                                            <?php else: ?>
                                                ¥ <?php echo htmlentities($v['o_price']); ?>
                                            <?php endif; elseif($v['g_type']==2): ?>
                                            <?php echo htmlentities($v['o_credit']); ?>积分
                                        <?php endif; ?>
                                    </p>
                                    <p>×<?php echo htmlentities($v['o_buy_num']); ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="jui_flex_row_center jui_flex_justify_end order_shop_bottom">
                        <?php if($v['o_status']==2): ?>
                            <div class="order_btn J_OpenKeyBoard" data-type="1">确认收货</div>
                        <?php elseif($v['o_status']==4): ?>
                            <div class="order_btn  J_OpenKeyBoard" data-type="2">删除订单</div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
    <?php endif; ?>
    <div class="jui_h16"></div>
</div>
<!-- 安全密码 -->
<div class="m-keyboard" id="J_KeyBoard">
    <!-- 自定义内容区域 -->
    <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>
    <!-- 自定义内容区域 -->
</div>
<!-- 安全密码end -->

<!-- 主体end -->
</body>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/box_psw.js"></script>
<script>
    //弹框输入安全密码
    !function ($, ydui) {
        var dialog      = ydui.dialog;
        var $keyboard   = $('#J_KeyBoard');
        var type        = 0;
        var order_id    = 0;
        // 初始化参数
        $keyboard.keyBoard({
            disorder: true,   // 是否打乱数字顺序
            title: '安全键盘' // 显示标题
        });
        // 打开键盘
        $('.J_OpenKeyBoard').on('click', function () {
            $keyboard.keyBoard('open');
            type        =   $(this).attr('data-type');
            order_id    =   $(this).parents('.order_list').find('.order_id').val();
        });
        // 六位密码输入完毕后执行
        $keyboard.on('done.ydui.keyboard', function (ret) {
            var pass    =   ret.password;
            $.post("<?php echo url('Center/checking_pay_pwd'); ?>",{pass:pass},function(res){
                layer.msg(res.msg);
                if(res.code==1){
                    if(type==1 && order_id !=0){
                        taken(order_id);
                    }else if(type==2){
                        del_order(order_id);
                    }
                }
            },'json');
            // 弹出请求中提示框
            dialog.loading.open('验证支付密码');
            // 模拟AJAX校验密码
            setTimeout(function () {
                // 关闭请求中提示框
                dialog.loading.close();
                // 显示错误信息
            }, 1500);
        });
    }(jQuery, YDUI);


	/*可用多个tab*/
    $(document).ready(function(){
        $(".jui_tab_tit li").click(function(){
            $(this).siblings().removeClass("jui_tab_on");
            var type=$(this).find('.types').val();
            window.location.href="<?php echo url('Order/order_list'); ?>?type="+type;
        });
    });

    function taken(order_id){
        $.post("<?php echo url('Order/taken'); ?>",{order_id:order_id},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function(){
                    var type=$('.jui_tab_on').find('.types').val();
                    window.location.href="<?php echo url('Order/order_list'); ?>?type="+type;
                },1000)
            }
        },'json')
    }
    function del_order(order_id){
        $.post("<?php echo url('Order/del_order'); ?>",{order_id:order_id},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function(){
                    var type=$('.jui_tab_on').find('.types').val();
                    window.location.href="<?php echo url('Order/order_list'); ?>?type="+type;
                },1000)
            }
        },'json')
    }

</script>
</html>
