<?php /*a:1:{s:82:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/quotations_con.html";i:1586753587;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="/static/index/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="/static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/index/css/css.css">
    <script src="/static/index/js/flexible.js"></script>
    <script src="/static/index/js/jquery-3.3.1.min.js"></script>
    <script src="/static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
    <a class="jui_top_left" href="javascript:history.back(-1)"><img src="/static/index/icons/back_icon.png"></a>
    <div class="jui_top_middle">商品介绍</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
    <!-- 产品图 -->
    <div class="swiper-container hqcon_banner_bar">
        <div class="swiper-wrapper">
            <?php if(is_array($pro_img) || $pro_img instanceof \think\Collection || $pro_img instanceof \think\Paginator): $i = 0; $__LIST__ = $pro_img;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <div class="swiper-slide"><img src="<?php echo htmlentities($v); ?>"></div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <script src="/static/index/js/swiper.min.js"></script>
    <script type="text/javascript">
        var mySwiper = new Swiper('.swiper-container', {
            autoplay: 2500,
            pagination: '.swiper-pagination',
            autoplayDisableOnInteraction: false,
            loop: true,
            paginationType: 'bullets',
            paginationClickable: true,
            observer: true
        })
    </script>
    <!-- 产品图end -->
    <div class="jui_h12"></div>
    <!-- 规格参数 -->
    <div class="jui_bg_fff jui_mar_b12">
        <div class="xqcon_guige_tit"></div>
        <div class="xqcon_guige_bar">
            <div class="xqcon_guige_list">
                <P class="jui_flex_no jui_pad_r20 jui_fc_999">商品名称</P>
                <P class="jui_text_right"><?php echo htmlentities($product['p_title']); ?></P>
            </div>
            <div class="xqcon_guige_list">
                <P class="jui_flex_no jui_pad_r20 jui_fc_999">商品代号</P>
                <P class="jui_text_right"><?php echo htmlentities($product['p_code']); ?></P>
            </div>
            <div class="xqcon_guige_list">
                <P class="jui_flex_no jui_pad_r20 jui_fc_999">厂家</P>
                <P class="jui_text_right"><?php echo htmlentities($product['p_shop_name']); ?></P>
            </div>
            <div class="xqcon_guige_list">
                <P class="jui_flex_no jui_pad_r20 jui_fc_999">当日交易量</P>
                <P class="jui_text_right">0</P>
            </div>
            <div class="xqcon_guige_list">
                <P class="jui_flex_no jui_pad_r20 jui_fc_999">商品描述</P>
                <P class="jui_text_right"><?php echo htmlentities($product['p_desc']); ?></P>
            </div>
        </div>
    </div>
    <!-- 商品详情 -->
    <div class="procon_ztbar jui_bg_fff">
        <div class="jui_public_tit jui_bor_bottom">
            <span class="sy_tit_icon"></span>
            <div class="jui_fs15 jui_fc_000">产品详情</div>
        </div>
        <div class="procon_ztcon">
            <?php echo $product['p_content']; ?>
        </div>
    </div>
    <!-- 商品详情end -->
    <!-- 规格参数end -->
</div>
<!-- 主体end -->
<!-- 固定底部 -->
<div class="jui_footer jui_pad_l16 jui_pad_r16">
    <div class="kqcon_foot_btn jui_bg_zhuse">我要购入</div>
</div>
<!-- 固定底部end -->
</body>
</html>
