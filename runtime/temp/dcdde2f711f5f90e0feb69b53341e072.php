<?php /*a:1:{s:63:"D:\wwwroot\jlh_php_code\application\index\view\center\team.html";i:1602124316;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">我的团队</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
     <!-- 人数 -->
     <div class="jui_bg_fff jui_flex_row_center jui_pad_t12 jui_pad_b12">
          <div class="jui_flex1 jui_flex_col_center jui_bor_right">
                <p><span class="jui_fs15 jui_fc_000 jui_font_weight"><?php echo htmlentities($push_num); ?></span>&nbsp;人</p>
                <p>直推会员</p>
          </div>
          <div class="jui_flex1 jui_flex_col_center">
                <p><span class="jui_fs15 jui_fc_000 jui_font_weight"><?php echo htmlentities($team_num); ?></span>&nbsp;人</p>
                <p>我的团队</p>
          </div>
     </div>
     <!-- 人数 -->
     <div class="jui_bg_fff jui_flex_row_center jui_pad_t12 jui_pad_b12">
        <div class="jui_flex1 jui_flex_col_center jui_bor_right">
              <p><span class="jui_fs15 jui_fc_000 jui_font_weight"><?php echo htmlentities($level_user); ?></span>&nbsp;人</p>
              <p>有效直推</p>
        </div>
        <div class="jui_flex1 jui_flex_col_center">
              <p><span class="jui_fs15 jui_fc_000 jui_font_weight"><?php echo htmlentities($level_team); ?></span>&nbsp;人</p>
              <p>有效团队</p>
        </div>
     </div>
    <!-- 人数end -->    
    <div class="jui_h12"></div>
    <?php if($status == 0): ?>
        <!-- 没有数据 -->
        <div class="jui_none_bar jui_none">
            <img src=" /static/index/icons/none_icon.png">
            <P>暂无数据</P>
        </div>
        <!-- 没有数据end -->
    <?php else: ?>
        <!-- 会员列表 -->
        <div class="jui_bg_fff">
            <?php if(is_array($push_user) || $push_user instanceof \think\Collection || $push_user instanceof \think\Paginator): $i = 0; $__LIST__ = $push_user;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <div class="jui_public_list2">
                    <div class="team_tx jui_bor_rad_50"><img src="<?php echo htmlentities($v['m_avatar']); ?>"></div>
                    <div class="jui_flex1 jui_flex_col">
                        <p><?php echo htmlentities($v['m_name']); ?> -- <?php echo $v['m_level']==0 ? "普通" : ($v['m_level']==1?"有效":($v['m_level']==2?"初级":($v['m_level']==3?"中级":($v['m_level']==4?"高级":($v['m_level']==5?"经理":""))))); ?></p>
                        <p><?php echo htmlentities($v['m_account']); ?></p>
                    </div>
                    <div class="jui_flex_col_center jui_pad_r20">
                        <p class="jui_fs12">余额</p>
                        <p class="jui_fc_000 jui_font_weight">￥<?php echo htmlentities($v['m_balance']); ?></p>
                    </div>
                    <div class="jui_flex_col_center">
                        <p class="jui_fs12">积分</p>
                        <p class="jui_fc_000 jui_font_weight"><?php echo htmlentities($v['m_integral']); ?>积分</p>
                    </div>
                </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <!-- 会员列表end -->
    <?php endif; ?>
</div>
<!-- 主体end -->
</body>
</html>
