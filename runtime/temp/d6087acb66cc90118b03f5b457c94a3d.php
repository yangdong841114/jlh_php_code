<?php /*a:1:{s:77:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/chengjiao.html";i:1592787235;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">成交信息</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
    <!-- 标题 -->
    <ul class="jui_tab_tit order_tit_bar">
        <li class="<?php if($type == 0){ echo 'jui_tab_on'; }?>"><input type="hidden" class="type" value="0"/>全部</li>
        <li class="<?php if($type == 1){ echo 'jui_tab_on'; }?>"><input type="hidden" class="type" value="1"/>买入</li>
        <li class="<?php if($type == 2){ echo 'jui_tab_on'; }?>"><input type="hidden" class="type" value="2"/>卖出</li>
    </ul>
    <!-- 标题end -->
    <div class="jui_h40"></div>
     <!-- 撤单记录 -->
     <div class="jui_flex_col jui_pad_l12 jui_pad_r12 jui_pad_t14">
         <?php if(!empty($list)): foreach($list as $key=>$vo): ?>
                        <div class="buy_list_bar">
                            <input type="hidden" class="d_sn" value="000"/>
                            <div class="jui_public_tit jui_bor_bottom">
                               <p class="jui_font_weight jui_fc_000"><?php echo htmlentities($vo['p_title']); ?></p>
                                <?php if(( $vo['d_type'] == 1) AND ( $vo['d_status'] == 10)): ?>
                                   <div class="jui_tag jui_bg_zhuse jui_mar_l8">买入 待配单</div>
                                <?php elseif(( $vo['d_type'] == 1) AND ( $vo['d_status'] == -1)): ?>
                                   <div class="jui_tag jui_bg_zhuse jui_mar_l8">买入 已撤单</div>
                                <?php elseif(( $vo['d_type'] == 1) AND ( $vo['d_status'] == 2)): ?>
                                   <div class="jui_tag jui_bg_zhuse jui_mar_l8">买入 成功</div>
                                <?php elseif(( $vo['d_type'] == 2) AND ( $vo['d_status'] == -1)): ?>
                                   <div class="jui_tag jui_bg_zhuse jui_mar_l8">卖出 已撤单</div>
                                <?php elseif(( $vo['d_type'] == 2) AND ( $vo['d_status'] == 4)): ?>
                                   <div class="jui_tag jui_bg_zhuse jui_mar_l8">卖出 成功</div>
                                <?php endif; ?>
                            </div>
                            <div class="jui_pad_1216 jui_line_h15 buy_list_con jui_flex">
                               <div class="jui_flex1">
                                   <p class="jui_fc_000 jui_font_weight"><?php echo htmlentities($vo['p_type']); ?></p>
                                   <p>持仓数量：<span class="c_num"><?php echo htmlentities($vo['d_total']); ?></span></p>
                                   <p>买入价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                                   <p>卖出价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                                   <p>成交时间：<?php echo htmlentities($vo['d_finish_time']); ?></p>
                               </div>
                                <?php if(($vo['d_status']==10)): ?>
                               <div class="flex_col">
                                   <div class="buy_list_btn jui_bg_orange che1">撤单</div>
                               </div>
                                <?php endif; ?>
                            </div>
                        </div>
                 <?php endforeach; else: ?>
             <!-- 没有订单 -->
                 <div class="jui_none_bar ">
                     <img src=" /static/index/icons/none_icon.png">
                     <P>暂无数据</P>
                 </div>
             <!-- 没有订单end -->
         <?php endif; ?>
     </div>
     <!-- 撤单记录end -->
     <div class="jui_h12"></div>
</div>
<!-- 主体end -->
</body>
<script src=" /static/index/layer/layer.js"></script>
<script>
	/*可用多个tab*/
    $(document).ready(function(){
        $(".jui_tab_tit li").click(function(){
            var type=$(this).find('.type').val();
            window.location.href="?m=index&c=chengjiao&type="+type;
        });
    });
    $(document).on('click','.che',function(){
        var c_num=$(this).parents('.buy_list_bar').find('.c_num').text();
        $.post("?m=index&c=che",{c_num:c_num},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function () {
                    window.location.reload();
                },1000)
            }
        },'json')
    });
    $(document).on('click','.che1',function(){
        var c_num=$(this).parents('.buy_list_bar').find('.c_num').text();
        var d_sn=$(this).parents('.buy_list_bar').find('.d_sn').val()
        $.post("?m=index&c=che1",{c_num:c_num,d_sn:d_sn},function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function () {
                    window.location.reload();
                },1000)
            }
        },'json')
    })
</script>
</html>
