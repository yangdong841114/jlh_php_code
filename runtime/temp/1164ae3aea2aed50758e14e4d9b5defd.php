<?php /*a:1:{s:79:"/www/wwwroot/www.jiulonghu123.com/application/index/view/login/account_add.html";i:1586753587;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Login/account_list'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">添加账户</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<form id="form1">
<div class="jui_main reg_con">
       <div class="jui_public_list jui_bor_rad_5">
             <img class="login_icon" src=" /static/index/icons/phone_icon.png">
             <input class="jui_flex1 jui_fc_000" type="tel" placeholder="输入交易商账号" id="m_trade_code" name="m_trade_code">
       </div>
       <div class="jui_public_list jui_bor_rad_5">
             <img class="login_icon" src=" /static/index/icons/mm_icon.png">
             <input class="jui_flex1 jui_fc_000" type="password" placeholder="请输入密码" id="m_login_pwd" name="m_login_pwd">
       </div>                    
       <div class="jui_h12"></div>
       <div class="jui_public_btn jui_padnone" id="dosubmit"><input type="button" value="登录"></div>
       <div class="jui_h20"></div>
       <a href="<?php echo url('Login/forget'); ?>" class="jui_block jui_text_center">忘记密码</a>
       <div class="jui_h12"></div>
</div>
</form>
<!-- 主体end -->
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>
    function isPass(num) {
        var reg = /^[\S]{6,12}$/;
        return reg.test(num);
    }
    $('#dosubmit').click(function(){
        var m_login_pwd =  $('#m_login_pwd').val();
        var m_trade_code =  $('#m_trade_code').val();
        if (m_trade_code == '') {
            layer.msg('交易码不能为空');
            return;
        }

        if (m_login_pwd == '') {
            layer.msg('登录密码不能为空');
            return;
        }

        if (!isPass(m_login_pwd)) {
            layer.msg('密码必须6到12位，且不能出现空格');
            return;
        }

        $.post("<?php echo url('Login/account_add'); ?>",$('#form1').serialize(),function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function () {
                    window.location.href="<?php echo url('Login/account_list'); ?>";
                },1000)
            }
        },'json')
    })
</script>
</body>
</html>
