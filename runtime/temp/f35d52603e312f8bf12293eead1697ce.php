<?php /*a:1:{s:71:"D:\wwwroot\jlh_php_code\application\index\view\order\confirm_order.html";i:1603088610;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>确认订单 - <?php echo htmlentities($config['w_name']); ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/diqu.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/key.css"><!-- 安全密码css -->

    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
    <script src=" /static/index/js/picker.min.js"></script>
    <script src=" /static/index/js/city.js"></script>
    <style>
        #dosubmit{
            width: 45%;
            padding: .42666rem;
            float: left;
        }
        #zhihuan{
            width: 45%;
            float: right;
           
        }
        #zhihuan input{
            background: #282828;

        }
    </style>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
    <a class="jui_top_left" href="javascript:history.go(-1)">
        <img src=" /static/index/icons/back_icon.png">
    </a>
    <div class="jui_top_middle">确认订单</div>
</div>
<!-- 头部end -->
<!-- 主体 -->

<form id="form1">
    <div class="jui_main">
        <div class="jui_h12"></div>
        <div class="jui_bg_fff">

            <?php if($status==0): ?>
                <!-- 没有地址 -->
                <a href="<?php echo url('Center/address_list',['type'=>5,'num'=>$num,'gid'=>$gid,'etype'=>$etype]); ?>" class="jui_bg_fff jui_block jui_pad_16">
                    <div class="jui_text_center jui_pad_16 qrdd_addbar">还未添加地址，立即添加</div>
                </a>
                <!-- 没有地址end -->
            <?php else: ?>
                <!-- 收货地址 -->
                <input type="hidden" id="a_id" value="<?php echo htmlentities($address['id']); ?>"/>
                <a href="<?php echo url('Center/address_list',['type'=>5,'num'=>$num,'gid'=>$gid,'etype'=>$etype]); ?>" class="jui_flex_row_center jui_bg_fff jui_pad_16 ">
                    <img class="order_address_icon" src=" /static/index/icons/address_icon.png">
                    <div class="jui_flex1 jui_flex_col jui_pad_l16">
                        <p class="jui_fs15 jui_fc_000 jui_pad_b5"><?php echo htmlentities($address['a_name']); ?><span class="jui_pad_l12"><?php echo htmlentities($address['a_phone']); ?></span></p>
                        <p class="jui_fs13"><?php echo htmlentities($address['a_city']); ?><?php echo htmlentities($address['a_detail']); ?></p>
                    </div>
                    <img class="jui_arrow_rimg jui_mar_l20" src=" /static/index/icons/jt_right.png">
                </a>
                <!-- 收货地址end -->
            <?php endif; ?>

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">产品名称：</p>
                <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($goods['g_title']); ?>" disabled>
            </div>

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">购买数量：</p>
                <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($num); ?>" disabled>
            </div>

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">消耗积分：</p>
                <input class="jui_flex1 jui_fc_000" type="text" value="<?php echo htmlentities($goods['g_credit']*$num); ?>" disabled>
            </div>

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">邮费：</p>
                <input class="jui_flex1 jui_fc_000" type="text" id="postage_money" disabled value="<?php echo htmlentities($postage_money); ?>元">
            </div>

            <!-- <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">姓名：</p>
                <input class="jui_flex1 jui_fc_000" type="text" name="real_name" value="">
            </div>

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">电话：</p>
                <input class="jui_flex1 jui_fc_000" type="text" name="phone" value="">
            </div>

            <div class="jui_public_list jui_flex_justify_between jui_bor_bottom">
                <p class="dlmm_left_text jui_pad_r8">所在地区：</p>
                <div class="address_left jui_flex1" id="sel_city">请选择所在地区</div>
                <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
            </div>
            <input type="hidden" name="a_city" id="a_city"/>

            <div class="jui_public_list" style="height:auto;">
                <p class="dlmm_left_text jui_pad_r8">详细地址：</p>
                <textarea class="jui_flex1" cols="" rows="3" name="a_detail" id="a_detail" placeholder="详细地址" style="padding: 8px 0px;"></textarea>
            </div> -->

            <div class="jui_public_list jui_flex_justify_between jui_bor_bottom" style="height: 80px;">
                <p class="dlmm_left_text jui_pad_r8">购买须知：</p>
                <!-- <div class="jui_flex1" style="color:#f00;">新疆、西藏、内蒙、宁夏、青海地区邮费10元，其他地区邮费7元，请保证账户资金充足！！！</div> -->
                <div class="jui_flex1" style="color:#f00;"><?php echo htmlentities($buy_need_know); ?></div>
            </div>

        </div>

        <div class="jui_public_btn" style="width: 100%;" >
            <input type="button" value="提交" id="J_OpenKeyBoard">
        </div>
    </div>
</form>
<!-- 安全密码 -->
<div class="m-keyboard" id="J_KeyBoard">
    <!-- 自定义内容区域 -->
    <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>
    <!-- 自定义内容区域 -->
</div>
<!-- 安全密码end -->

<!-- 主体end -->
<!-- <script src=" /static/index/js/cityjs.js"></script> -->
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/box_psw.js"></script>
<script>
    //弹框输入安全密码
    !function ($, ydui) {
        var dialog      = ydui.dialog;
        var $keyboard   = $('#J_KeyBoard');
        // 初始化参数
        $keyboard.keyBoard({
            disorder: true,   // 是否打乱数字顺序
            title: '安全键盘' // 显示标题
        });
        // 打开键盘
        $('#J_OpenKeyBoard').on('click', function () {
            $keyboard.keyBoard('open');
        });
        // 六位密码输入完毕后执行
        $keyboard.on('done.ydui.keyboard', function (ret) {
            var pass    =   ret.password;
            $.post("<?php echo url('Center/checking_pay_pwd'); ?>",{pass:pass},function(res){
                layer.msg(res.msg);
                // setup_order();
                if(res.code==1){
                    setup_order();
                }
            },'json');
            // 弹出请求中提示框
            dialog.loading.open('验证支付密码');
            // 模拟AJAX校验密码
            setTimeout(function () {
                // 关闭请求中提示框
                dialog.loading.close();
                // 显示错误信息
            }, 1500);
        });
    }(jQuery, YDUI);

    // 选择地址后回调 计算运费
    $('#a_city').change(function(){
        var postage_money = 7;
        var a_city = $('#a_city').val();
        a_city = a_city.substr(0, 2);
        if(a_city == '新疆' || a_city == '西藏' || a_city == '内蒙' || a_city == '宁夏' || a_city == '青海'){
            postage_money = 10;
        }

        var num = '<?php echo htmlentities($num); ?>';

        var _num = Math.ceil(num/6);

        var _postage_money = _num*postage_money;

        $('#postage_money').val(_postage_money+'元');

    })


    function isPhoneNumber(tel) {
        var reg = /^0?1[3|4|5|6|7|8|9][0-9]\d{8}$/;
        return reg.test(tel);
    }
    
    // 提交
    function setup_order(){
        // var real_name = $('input[name="real_name"]').val();
        // var phone = $('input[name="phone"]').val();
        // var a_city = $('input[name="a_city"]').val();
        // var a_detail = $('textarea[name="a_detail"]').val();
        var a_id = $('#a_id').val();

        // if(real_name == ''){ layer.msg('请填写姓名',{icon:2}); return false; }
        // if(phone == ''){ layer.msg('请填写手机号',{icon:2}); return false; }
        // if(a_city == ''){ layer.msg('请选择地区',{icon:2}); return false; }
        // if(a_detail == ''){ layer.msg('请填写详细地址',{icon:2}); return false; }
        if(a_id == ''){ layer.msg('请选择地址',{icon:2}); return false; }

        // if(!isPhoneNumber(phone)){
        //     layer.msg('手机号格式不正确',{icon:2}); return false;
        // }

        var _param_data = {
            // real_name: real_name,
            // phone: phone,
            // a_city: a_city,
            // a_detail: a_detail,
            a_id: a_id,
        };
        var layIndex = layer.load();
        $.post('<?php echo url("confirm_order", array("num"=>$num, "gid"=>$gid, "etype"=>$etype, )); ?>', _param_data, function(res){
            // console.log(res)
            layer.close(layIndex);
            if(res.code == 1){
                layer.msg(res.msg, {icon:1});
                setTimeout(()=>{
                    window.location.href = '<?php echo url("order/order_list"); ?>';
                }, 1000);
            }else{
                layer.msg(res.msg, {icon:2});
            }
        }, 'json')
    }


</script>
</body>
</html>
