<?php /*a:1:{s:73:"/www/wwwroot/www.jiulonghu123.com/application/index/view/center/info.html";i:1588778167;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Center/center'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">个人资料</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
       <div class="jui_bg_fff">
           <div class="jui_public_list jui_flex_justify_between">
               <p class="dlmm_left_text">交易账号</p>
               <p><?php echo htmlentities($user['m_account']); ?></p>
           </div>
           <a href="<?php echo url('Center/tell_edit'); ?>" class="jui_public_list jui_flex_justify_between">
               <p>绑定手机号</p>
               <div class="jui_flex_row_center">
                   <p class="jui_pad_r8 jui_flex_no"><?php echo htmlentities($user['m_phone']); ?></p>
                   <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
               </div>
           </a>
           <a href="#" class="jui_public_list jui_flex_justify_between">
                   <p>姓名</p>
                   <div class="jui_flex_row_center">
                         <p class="jui_pad_r8 jui_flex_no"><?php echo htmlentities($user['m_name']); ?></p>
                     </div>
           </a>
           <a href="#" class="jui_public_list jui_flex_justify_between">
                   <p>身份证号</p>
                   <div class="jui_flex_row_center">
                         <p class="jui_pad_r8 jui_flex_no"><?php echo htmlentities($user['m_car_id']); ?></p>
                     </div>
           </a>
           <a href="#" class="jui_public_list jui_flex_justify_between">
               <p>所属银行</p>
               <div class="jui_flex_row_center">
                   <p class="jui_pad_r8 jui_flex_no"><?php echo htmlentities($user['m_bank_name']); ?></p>
               </div>
           </a>
           <a href="#" class="jui_public_list jui_flex_justify_between">
               <p>银行卡号</p>
               <div class="jui_flex_row_center">
                   <p class="jui_pad_r8 jui_flex_no"><?php echo htmlentities($user['m_bank_carid']); ?></p>
               </div>
           </a>
           <a href="#" class="jui_public_list jui_flex_justify_between">
               <p>附属账号</p>
               <div class="jui_flex_row_center">
                   <?php if($user['m_virtual_account'] == 0): ?>
                   <p class="jui_pad_r8 jui_flex_no">暂无</p>
                   <?php else: ?>
                   <p class="jui_pad_r8 jui_flex_no copy_text" id="text"><?php echo htmlentities($user['m_virtual_account']); ?></p>
                   <?php endif; ?>
               </div>
           </a>
           <div class="jui_flex_row_center ziliao_sfz_bar jui_bor_bottom">
               <div class="reg_sfz_con jui_flex_col_center">
                   <div class="reg_sfz_img">
                       <img src="<?php echo htmlentities($user['m_car_img'][0]); ?>" style="width: 4rem;">
                   </div>
                   <p class="jui_fs12 jui_pad_t5">身份证正面</p>
               </div>
               <div class="reg_sfz_con jui_flex_col_center">
                   <div class="reg_sfz_img">
                       <img src="<?php echo htmlentities($user['m_car_img'][1]); ?>" style="width: 4rem;">
                   </div>
                   <p class="jui_fs12 jui_pad_t5">身份证反面</p>
               </div>
           </div>
       </div>
</div>
<!-- 主体end -->
</body>
<script src="/static/index/js/clipboard.min.js"></script>
<script>
    $('#text').click(function(){
        var text_1    = document.getElementById('text').innerText;
        var clipboard = new ClipboardJS('.copy_text', {
            text: function() {
                return text_1;
            }
        });
        clipboard.on('success', function(e) {
            alert("复制成功")
        });
        clipboard.on('error', function(e) {
            alert("复制失败")
        });
    });
</script>
</html>
