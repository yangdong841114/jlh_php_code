<?php /*a:1:{s:76:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/recharge.html";i:1592786722;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="javascript:history.back(-1);"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">余额充值</div>
    <a style="color: #fff;" href="<?php echo url('Index/entryRecord'); ?>">入金记录</a>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
     <div class="jui_pad_1216">账户余额：<span class="jui_fs16 jui_font_weight"><?php echo htmlentities($user['m_balance']); ?></span></div>
     <div class="jui_bg_fff">
         <div class="jui_public_list">
              <p class="jui_flex_no jui_pad_r8">充值金额：</p>
              <input class="jui_flex1 jui_fc_000" id="money" type="number" value="" placeholder="请输入充值金额" autofocus>
         </div>
         <div class="jui_public_list">
             <p class="jui_flex_no jui_pad_r8">备&nbsp;&nbsp;&nbsp;注：</p>
             <input class="jui_flex1 jui_fc_000" id="desc" type="text" value="" placeholder="    备注（选填）">
         </div>
     </div>
     <div class="jui_public_tit">入金账户</div>
     <div class="jui_bg_fff" id="cz_style">
          <label class="jui_public_list2">
              &nbsp; &nbsp;<p><?php echo htmlentities($user['m_bank_name']); ?></p>
              &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <p><?php echo htmlentities($user['m_bank_carid']); ?></p>
          </label>

     </div>
     <div class="jui_bg_fff" id="cz_style">
          <label class="jui_public_list2">
              &nbsp; &nbsp;<p>江西省联交运登记结算中心有限公司</p>
              &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                
          </label>

     </div>
     <div class="jui_public_tit"></div>
     <div class="jui_h20"></div>
     <div class="jui_public_btn" id="recharge"><input type="button" value="立即充值"></div>
</div>
<!-- 主体end -->
</body>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/layer/layer.js"></script>
<script>
$('#recharge').click(function(){
    var cz_money = $("#money").val();
    if( cz_money == '' || cz_money<=0){
        layer.msg('请输入充值金额');
        return;
    }
    var cz_desc  = $('#desc').val();
    $.post("<?php echo url('Index/recharge'); ?>",{cz_money:cz_money,cz_desc:cz_desc},function(res){
        layer.msg(res.msg);
        if(res.code==1){
            setTimeout(function(){
                window.location.href="<?php echo url('Index/index'); ?>"
            },1000)
        }
    },'json')
})
</script>
</html>
