<?php /*a:1:{s:62:"D:\wwwroot\jlh_php_code\application\index\view\index\sell.html";i:1603268322;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
    <meta name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/key.css"> <!-- 安全密码css -->
    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
</head>

<body class="jui_bg_grey">
    <!-- 头部 -->
    <div class="jui_top_bar">
        <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
        <div class="jui_top_middle">卖出</div>
    </div>
    <!-- 头部end -->
    <!-- 主体 -->
    <form id="form1">
        <div class="jui_main">
            <div class="jui_flex_row_center jui_bg_fff">
                <div class="jui_flex1 jui_grid_list">
                    <p class="jui_fs16 jui_fc_000 jui_font_weight"><?php echo htmlentities($wallet['m_credit_1']); ?></p>
                    <p>零售产品可售</p>
                </div>
                <div class="jui_flex1 jui_grid_list">
                    <p class="jui_fs16 jui_fc_000 jui_font_weight"><?php echo htmlentities($wallet['m_credit_2']); ?></p>
                    <p>批发产品可售</p>
                </div>
                <div class="jui_flex1 jui_grid_list">
                    <p class="jui_fs16 jui_fc_000 jui_font_weight"><?php echo htmlentities($wallet['m_credit_3']); ?></p>
                    <p>特价产品可售</p>
                </div>
            </div>
            <div class="jui_bg_fff jui_pad_12">
                <!-- 新增下拉框 -->
                <div class="jui_flex_row_center buy_select">
                    <input type="hidden" value="<?php echo htmlentities($pid); ?>" id='hid_pid' />
                    <select class="jui_flex1" id="bill_id">
                        <?php if(is_array($pro_list) || $pro_list instanceof \think\Collection || $pro_list instanceof \think\Paginator): $i = 0; $__LIST__ = $pro_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                        <option name="bill_id" value="<?php echo htmlentities($v['id']); ?>"><?php echo htmlentities($v['p_title']); ?> - <?php echo htmlentities($v['p_code']); ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    <img class="jui_arrow_rimg jui_mar_r12" src="/static/index/icons/jt_right.png">
                </div>
                <!-- 新增下拉框end -->
                <div class="jui_h20"></div>
                <div class="jui_public_btn jui_padnone " id="click_url"><input type="button" value="检索"></div>
                <div class="jui_h12"></div>
            </div>
            <div class="jui_h12"></div>
            <div class="jui_bg_fff jui_pad_12">
                <div class="jui_public_list2 sell_list">
                    <p class="jui_flex_no">选择票类：</p>
                    <select class="jui_flex1 jui_pad_r12 jui_fc_000" dir="rtl" id="p_style" name="p_type">
                        <option value="" disabled selected>请选择票类</option>
                        <option value="1">零售产品</option>
                        <option value="2">批发产品</option>
                        <option value="3">特价产品</option>
                    </select>
                    <img class="jui_arrow_rimg" src=" /static/index/icons/jt_right.png">
                </div>
                <div class="jui_public_list2 sell_list sell_num_bar">
                    <span class="jui_flex_row_center jui_flex_justify_center min_btn">-</span>
                    <input class="jui_flex1 num_input" type="number" min="2" max="10" value="1" placeholder="卖出量"
                        name="sell_num" id="sell_num">
                    <span class="jui_flex_row_center jui_flex_justify_center add_btn">+</span>
                </div>
                <p class="jui_pad_t5">可卖：<span class="jui_fc_orange sell_num" id="can_sell">0</span></p>
                <label class="jui_pad_t8 jui_flex_row_center">
                    <input class="jui_form_check jui_form_check2" type="checkbox" name="ni">
                    <P class="jui_fs12 jui_pad_l5">是否匿名</P>
                </label>
                <div class="jui_h20"></div>
                <div class="jui_public_btn jui_padnone J_OpenKeyBoard "><input class="jui_bg_blue" type="button"
                        value="卖出" id="dosubmit"></div>
                <div class="jui_h12"></div>
            </div>

            <div class="jui_h12"></div>
            <div class="jui_public_tit jui_fc_000 jui_font_weight">挂卖列表</div>

            <?php if(!(empty($selllist) || (($selllist instanceof \think\Collection || $selllist instanceof \think\Paginator ) && $selllist->isEmpty()))): foreach($selllist as $key=>$vo): ?>
            <div class="buy_list_bar">
                <div class="jui_public_tit jui_flex_justify_between jui_bor_bottom">
                    <input name="d_id" type="hidden" id="d_id" value="">
                    <p class="jui_font_weight jui_fc_000"><?php echo htmlentities($vo['p_title']); ?> </p>
                    <!--   <p>2018-12-04 13:44:53</p>-->
                </div>
                <div class="jui_pad_1216 jui_flex jui_flex_justify_between">
                    <div class="jui_line_h15 buy_list_con">
                        <p class="jui_fc_000 jui_font_weight"> <?php echo htmlentities($vo['p_type']); ?> </p>
                        <!--                 <p>市值变化：<span class="jui_fc_zhuse">000&nbsp;&nbsp;000%</span></p>-->
                        <p>卖出数量：<span class="c_num"><?php echo htmlentities($vo['p_num']); ?></span></p>
                        <p>卖出价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                        <p>开始时间：<?php echo htmlentities($vo['p_start_time']); ?></p>
                        <p>结束时间：<?php echo htmlentities($vo['p_end_time']); ?></p>
                    </div>
                    <div class="jui_flex_col">
                        <a href="#" class="buy_list_btn jui_bg_orange cancel_order_btn" data-id="<?php echo htmlentities($vo['id']); ?>">撤单</a>
                        <a href="#" class="buy_list_btn jui_bg_orange exchange_point_btn" data-id="<?php echo htmlentities($vo['id']); ?>">兑换积分</a>
                        <!-- <a href="<?php echo url('Index/index'); ?>" class="buy_list_btn jui_bg_orange no_exchange">撤单</a> -->
                    </div>

                </div>

            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </form>
    <!-- 主体end -->
    <!-- 安全密码 -->
    <div class="m-keyboard" id="J_KeyBoard">
        <!-- 自定义内容区域 -->
        <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>
        <!-- 自定义内容区域 -->
    </div>
    <!-- 安全密码end -->
</body>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/box_psw.js"></script>

<script>
    $('.cancel_order_btn').click(function () {
        var id = $(this).data('id');
        // console.log(id);
        layer.confirm('是否撤出卖单?', { icon: 3, title: '提示' }, function (index) {
            // window.location.href = '<?php echo url("index/cancelorder"); ?>?id=' + a_id


            $.post("<?php echo url('Index/cancelorder'); ?>", { id: id }, function (res) {

                if (res.code == 1) {
                    layer.msg(res.msg, {
                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                    }, function () {
                         location.reload();
                    });
                   
                } else {
    
                    layer.msg(res.msg, {
                        time: 2000 
                    }, function () {
                         location.reload();
                    });
                   
                }

            });


        });
    })
    //兑换积分
    $('.exchange_point_btn').click(function () {
        var id = $(this).data('id');
        // console.log(id);
        layer.confirm('是否确定兑换为积分?', { icon: 3, title: '提示' }, function (index) {
            // window.location.href = '<?php echo url("index/cancelorder"); ?>?id=' + a_id


            $.post("<?php echo url('Index/exchange_point'); ?>", { id: id }, function (res) {

                if (res.code == 1) {
                    layer.msg(res.msg, {
                        time: 2000 
                    }, function () {
                         location.reload();
                    });
                } else {
                    layer.msg(res.msg, {
                        time: 2000 
                    }, function () {
                         location.reload();
                    });
                }

            });


        });
    })
</script>
<script>
    $(function () {
        var hid_pid = $("#hid_pid").val();
        if (hid_pid == 1) {
            $("#bill_id option[value='1']").prop("selected", true);
        } else {
            $("#bill_id option[value='3']").prop("selected", true);
        }
        var sel_check;
        var num;
        var retail_price;
        var who_price;
        $("#p_style").change(function () {
            sel_check = parseInt($("#p_style option:selected").val()); //获取被选中的票种类
            var bill_id = $("#bill_id option:selected").val();
            $.post("<?php echo url('Index/sell_info'); ?>", { selcheck: sel_check, pid: bill_id }, function (res) {
                if (res.code == 1) {
                    var data = res.data;
                    var product = res.product;
                    retail_price = res.retail_price;
                    who_price = res.who_price;
                    $("#can_sell").html(data);
                    // num = parseInt($("#can_sell").text());   //获取被选中的票的种类可卖数量
                    if (sel_check == 1) {
                        // $('.sell_num').text(0);
                        if (num >= retail_price) {
                            $(".num_input").val(retail_price);
                        }
                    }
                    if (sel_check == retail_price) {
                        // $('.sell_num').text(0);
                        if (num >= retail_price) {
                            $(".num_input").val(retail_price);
                        }
                    }
                    if (sel_check == 3) {
                        // $('.sell_num').text(0);
                        if (num >= retail_price) {
                            $(".num_input").val(retail_price);
                        }
                    }
                } else {
                    layer.msg(res.msg);
                }
            });
        });
        $(".add_btn").click(function () {
            var sel_check = parseInt($("#p_style option:selected").val());
            var num_input = $(this).parent('').find('.num_input');
            var now_num = parseInt($(this).parent('').find('.num_input').val());
            if (!now_num) {
                now_num = 0;
            }
            if (!sel_check) {
                box_timer('请先选择票种类型');
                return;
            }
            if (sel_check == 1) {
                if (now_num >= retail_price) {
                    box_timer('零售产品每次最多出售' + retail_price + '张');
                    return;
                }
            }
            if (sel_check == 2) {
                if (now_num >= retail_price) {
                    box_timer('零售产品每次只能出售' + retail_price + '张');
                    return;
                }
            }
            num_input.val(parseInt(now_num + 1));
        });
        $(".min_btn").click(function () {
            var num_input = $(this).parent('').find('.num_input');
            var now_num = parseInt($(this).parent('').find('.num_input').val());
            if (!now_num) {
                now_num = 0;
            }
            if (!sel_check) {
                box_timer('请先选择产品种类型');
                return;
            }
            if (sel_check == 1) {
                if (now_num <= 1) {
                    box_timer('零售产品每次最少出售1张');
                    return;
                }
            }
            if (sel_check == 2) {
                if (now_num <= 1) {
                    box_timer('批发产品每次最少出售1张');
                    return;
                }
            }
            num_input.val(parseInt(now_num - 1));
        });

        function sell_deal() {
            var sel_check = parseInt($("#p_style option:selected").val());
            var buy_num = $('#sell_num').val();
            var max_num = parseInt($("#can_sell").text());
            if (!sel_check) {
                box_timer('请先选择产品种类');
                return;
            }
            if (buy_num == 0) {
                box_timer('请输入出售数量');
                return;
            }
            var msg = '零售产品';
            if (sel_check == 2) {
                msg = '批发产品';
            }
            if (sel_check == 3) {
                msg = '特价产品';
            }
            if (max_num < buy_num) {
                box_timer(msg + '不足' + buy_num + '张');
                return;
            }
            var pid = $("#bill_id option:selected").val();
            var credit_type = $("#p_style option:selected").val();
            var layIndex = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            $.post("<?php echo url('Index/auto_sell'); ?>", { pid: pid, credit_type: credit_type, sell_num: buy_num }, function (res) {
                var msg = res.msg;
                if (res.code == 1) {
                    layer.msg(msg, function () { window.location.href = "<?php echo url('Index/index'); ?>"; });
                } else {
                    layer.msg(msg);
                    return;
                }
            }, 'json')
        }

        //弹框输入安全密码
        !function ($, ydui) {
            var dialog = ydui.dialog;
            var $keyboard = $('#J_KeyBoard');
            // 初始化参数
            $keyboard.keyBoard({
                disorder: true,   // 是否打乱数字顺序
                title: '安全键盘' // 显示标题
            });
            $('.J_OpenKeyBoard').on('click', function () {   // 打开键盘
                $keyboard.keyBoard('open');
            });
            // 六位密码输入完毕后执行
            $keyboard.on('done.ydui.keyboard', function (ret) {
                var pass = ret.password;
                $.post("<?php echo url('Center/checking_pay_pwd'); ?>", { pass: pass }, function (res) {
                    var msg = res.msg;
                    if (res.code == 1) {
                        sell_deal();
                        $keyboard.keyBoard('close');
                    } else {
                        layer.msg(msg);
                    }
                }, 'json');
                // 弹出请求中提示框
                dialog.loading.open('验证支付密码');
                // 模拟AJAX校验密码
                setTimeout(function () {
                    // 关闭请求中提示框
                    dialog.loading.close();
                    // 显示错误信息
                }, 1500);
            });
        }(jQuery, YDUI);

        $('#bill_id').change(function () {
            var bill_id = $("#bill_id option:selected").val();
            window.location.href = "<?php echo url('Index/sell'); ?>" + '?bill_id=' + bill_id;
        });

        $('#click_url').click(function () {

            var bill_id = $("#bill_id option:selected").val();
            window.location.href = "<?php echo url('Index/sell'); ?>" + '?bill_id=' + bill_id;
        });
    })
</script>

</html>