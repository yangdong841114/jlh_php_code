<?php /*a:1:{s:71:"D:\wwwroot\jlh_php_code\application\index\view\center\address_list.html";i:1602825049;}*/ ?>
<!doctype html>

<html>

<head>

    <meta charset="utf-8">

    <title><?php echo htmlentities($config['w_name']); ?></title>

    <meta name="viewport"

          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>

    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">

    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">

    <script src=" /static/index/js/flexible.js"></script>

    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>

    <script src=" /static/index/js/public.js"></script>

    <style>

        .bor{border: 1px solid #e52e2f;}

    </style>

</head>

<body class="jui_bg_grey">

<!-- 头部 -->

<div class="jui_top_bar">

    <?php if($type == 1): ?>

    <a class="jui_top_left" href="<?php echo url('Order/qrdd',['type'=>$type,'num'=>$num,'id'=>$g_id]); ?>">

        <img src=" /static/index/icons/back_icon.png">

    </a>

   <?php elseif($type ==3): ?>

    <a class="jui_top_left" href="<?php echo url('index/chicang'); ?>">

        <img src=" /static/index/icons/back_icon.png">

    </a>
    <?php elseif($type ==4): ?>
    <a class="jui_top_left" href="<?php echo url('index/tihuo',['type'=>$type,'num'=>$num,'id'=>$g_id,'id'=>$deal_id]); ?>">
        <img src=" /static/index/icons/back_icon.png">
    </a>
    <?php elseif($type ==5): ?>
    <a class="jui_top_left" href="<?php echo url('index/tihuo',['type'=>$type,'num'=>$num,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]); ?>">
        <img src=" /static/index/icons/back_icon.png">
    </a>

    <?php else: ?>

    <a class="jui_top_left" href="<?php echo url('Center/center'); ?>"><img src=" /static/index/icons/back_icon.png"></a>

   <?php endif; ?>

    <div class="jui_top_middle">我的地址</div>

</div>

<!-- 头部end -->

<!-- 主体 -->

<div class="jui_main">

    <?php if($status == 0): ?>

    <!-- 没有数据 -->

    <div class="jui_none_bar ">

        <img src=" /static/index/icons/none_icon.png">

        <P>暂无数据</P>

    </div>

    <!-- 没有数据end -->

    <?php else: if(is_array($address) || $address instanceof \think\Collection || $address instanceof \think\Paginator): $i = 0; $__LIST__ = $address;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>

            <div class="address_list jui_flex_row_center jui_flex_justify_between">

                <input type="hidden" id="a_id" value="<?php echo htmlentities($v['id']); ?>"/>

                <div class="jui_pad_r_20 jui_flex1 se_ad">

                    <div class="jui_flex_row_center jui_pad_b5">

                        <?php if($v['a_is_default'] == 1): ?>

                            <div class="jui_tag jui_bg_red jui_mar_r8">默认</div>

                        <?php endif; ?>

                        <p class="jui_fs15 jui_fc_000"><?php echo htmlentities($v['a_name']); ?></p>

                        <p class="jui_fs15 jui_fc_000 jui_pad_l12"><?php echo htmlentities($v['a_phone']); ?></p>

                    </div>

                    <p class="jui_fs12 jui_line_h12"><?php echo htmlentities($v['a_city']); ?><?php echo htmlentities($v['a_detail']); ?></p>

                </div>

                <a href="<?php echo url('Center/address_edit',['a_id'=>$v['id'],'type'=>$type,'num'=>$num,'g_id'=>$g_id,'deal_id'=>$deal_id,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]); ?>">

                    <img class="address_icon" src=" /static/index/icons/edit_icon.png" id="address_edit">

                </a>

            </div>

        <?php endforeach; endif; else: echo "" ;endif; ?>

    <?php endif; ?>



</div>

<!-- 主体end -->

<a id="addBtn" href="<?php echo url('Center/address_add',['type'=>$type,'num'=>$num,'g_id'=>$g_id,'d_id'=>$d_id,'deal_id'=>$deal_id,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]); ?>"

   class="jui_footer jui_bg_zhuse jui_flex_justify_center">

    <img class="address_addicon" src=" /static/index/icons/add_icon.png">

    <p class="jui_fs15 jui_fc_fff jui_pad_l8">添加新地址</p>

</a>

<div id="sureBtn" class="jui_footer jui_bg_zhuse jui_flex_justify_center jui_none">

    <input type="hidden" name="d_id" id="d_id" value="<?php echo htmlentities($d_id); ?>">

    <p class="jui_fs15 jui_fc_fff jui_pad_l8">提交</p>

</div>

<script src=" /static/index/layer/layer.js"></script>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>

<script>

    var a_id;

    var d_id;

    $('.se_ad').click(function () {

        <?php if($type == 1): ?>

            a_id = $(this).parents('.jui_flex_justify_between').find('#a_id').val();

            window.location.href = "<?php echo url('Order/qrdd',['type'=>$type,'num'=>$num,'g_id'=>$g_id]); ?>?a_id="+a_id;

        <?php endif; if($type == 3): ?>

           a_id = $(this).parents('.jui_flex_justify_between').find('#a_id').val();

            d_id = $('#d_id').val();

            $(this).parent('.address_list').addClass('bor').parent('.address_list').siblings('div').removeClass('bor');

            $('#addBtn').addClass('jui_none');

            $('#sureBtn').removeClass('jui_none');



            // window.location.href = "<?php echo url('Order/qrdd',['type'=>$type,'num'=>$num,'g_id'=>$g_id]); ?>?a_id="+a_id;

            <?php endif; if($type == 4): ?>
                a_id = $(this).parents('.jui_flex_justify_between').find('#a_id').val();
                window.location.href = "<?php echo url('index/tihuo',['type'=>$type,'id'=>$deal_id]); ?>?a_id="+a_id;
            <?php endif; if($type == 5): ?>
            a_id = $(this).parents('.jui_flex_justify_between').find('#a_id').val();
            window.location.href = "<?php echo url('order/confirm_order',['type'=>$type,'num'=>$num,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]); ?>?a_id="+a_id;
        <?php endif; ?>

    });



    $('#sureBtn').click(function () {

        $.post("<?php echo url('Index/carry'); ?>",{a_id:a_id,d_id:d_id},function () {



        })

    })

</script>

</body>

</html>

