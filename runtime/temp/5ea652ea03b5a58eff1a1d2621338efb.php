<?php /*a:1:{s:78:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/quotations.html";i:1598383315;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <div class="jui_top_left"></div>
     <div class="jui_top_middle">行情</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main jui_pad_l12 jui_pad_r12 jui_pad_t14">
    <?php if(is_array($product) || $product instanceof \think\Collection || $product instanceof \think\Paginator): $i = 0; $__LIST__ = $product;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
        <div class="jui_flex jyhq_list_bar">
            <a href="<?php echo url('Index/quotations_con',['id'=>$v['id']]); ?>" class="jyhq_list_img"><img src="<?php echo htmlentities($v['p_img']); ?>" style="width: 100%;"></a>
            <div class="jui_flex1">
                <a href="<?php echo url('Index/quotations_con',['id'=>$v['id']]); ?>">
                    <div class="jyhq_list_tit jui_ellipsis_1"><?php echo htmlentities($v['p_title']); ?></div>
                    <p class="jui_fc_999 jui_pad_b5">商品代码：<?php echo htmlentities($v['p_code']); ?></p>
                    <p class="jui_fc_999 jui_pad_b5">上市时间：<?php echo htmlentities($v['p_addtime']); ?></p>
                    <p class="jui_fs16 jui_fc_red jui_font_weight"><span class="jui_fs12">￥</span><?php echo htmlentities($v['p_retail_price']); ?></p>
                </a>
                <div class="jui_flex jui_flex_justify_between">
                    <a class="jyhq_btn" href="<?php echo url('Index/buy'); ?>">我要购入</a>
                    <a class="jyhq_btn jyhq_btn2" href="<?php echo url('Index/sell'); ?>">我要售出</a>
                </div>
            </div>
        </div>
    <?php endforeach; endif; else: echo "" ;endif; ?>
</div>
<!-- 主体end -->
<!-- 固定底部 -->
<div class="jui_footer">
    <a href="<?php echo url('Index/index'); ?>" class="jui_foot_list">
        <b class="foot_index"></b>
        <p>交易</p>
    </a>
    <a href="<?php echo url('Index/quotations'); ?>" class="jui_foot_list jui_hover">
        <b class="foot_hq"></b>
        <p>行情</p>
    </a>
    <?php if($configMsg == '1'): ?>
        <a href="#" class="jui_foot_list no_exchange">
    <?php else: ?>
        <a href="<?php echo url('Order/shop'); ?>" class="jui_foot_list">
    <?php endif; ?>

        <b class="foot_shop"></b>

        <p>置换仓库</p>

    </a>
    <a href="<?php echo url('Center/notice_list'); ?>" class="jui_foot_list">
        <b class="foot_notice"></b>
        <p>公告</p>
    </a>
    <a href="<?php echo url('Center/center'); ?>" class="jui_foot_list">
        <b class="foot_my"></b>
        <p>我的</p>
    </a>
</div>
<!-- 固定底部end -->
</body>
</html>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>
    $('.no_exchange').click(function(){
        layer.msg('非签约时间不可兑换');
    });
</script>
