<?php /*a:1:{s:65:"D:\wwwroot\jlh_php_code\application\index\view\index\chicang.html";i:1603182049;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
    <meta name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/key.css"><!-- 安全密码css -->
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
    <style>
        ul.jui_tab_tit li {
            height: auto;
        }

        .none {
            display: none;
        }
    </style>
</head>

<body class="jui_bg_grey">
    <!-- 头部 -->
    <div class="jui_top_bar">
        <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
        <div class="jui_top_middle">持仓</div>
    </div>
    <!-- 头部end -->
    <!-- 主体 -->
    <div class="jui_main">
        <ul class="jui_flex_row_center jui_bg_fff">
            <li class="jui_flex1 jui_grid_list">
                <p class="jui_fs16 jui_fc_000 jui_font_weight  "><input type="hidden" class="type"
                        value="1" /><?php echo htmlentities($wallet['m_credit_1']); ?></p>
                <p>零售产品持仓</p>
            </li>
            <li class="jui_flex1 jui_grid_list">
                <p class="jui_fs16 jui_fc_000 jui_font_weight  "><input type="hidden" class="type"
                        value="2" /><?php echo htmlentities($wallet['m_credit_2']); ?></p>
                <p>批发产品持仓</p>
            </li>
            <li class="jui_flex1 jui_grid_list">
                <p class="jui_fs16 jui_fc_000 jui_font_weight  "><input type="hidden" class="type"
                        value="3" /><?php echo htmlentities($wallet['m_credit_3']); ?></p>
                <p>特价产品持仓</p>
            </li>
        </ul>
        <div class="jui_bg_fff jui_pad_16">
            <div class="jui_h12"></div>
            <div class="jui_flex_row_center buy_select">
                <input type="hidden" value="<?php echo htmlentities($pid); ?>" id='hid_pid' />
                <select class="jui_flex1" id="bill_id">
                    <?php if(is_array($pro_list) || $pro_list instanceof \think\Collection || $pro_list instanceof \think\Paginator): $i = 0; $__LIST__ = $pro_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option name="bill_id" value="<?php echo htmlentities($v['id']); ?>"><?php echo htmlentities($v['p_title']); ?> - <?php echo htmlentities($v['p_code']); ?></option>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
                <img class="jui_arrow_rimg jui_mar_r12" src="/static/index/icons/jt_right.png">
            </div>
            <div class="jui_h20"></div>
            <div class="jui_public_btn jui_padnone " id="click_url"><input type="button" value="检索"></div>
        </div>
        <!-- 持仓列表 -->
        <div class="jui_public_tit jui_fc_000 jui_font_weight">持仓列表</div>
        <div class="jui_flex_col jui_pad_l12 jui_pad_r12">
            <?php if((!empty($list))): foreach($list as $key=>$vo): if(($vo['p_num'] > 0 && $vo['p_start_time'] != '1900-01-01')): ?>
            <div class="buy_list_bar">
                <div class="jui_public_tit jui_flex_justify_between jui_bor_bottom">
                    <input name="d_id" type="hidden" id="d_id" value="<?php echo htmlentities($vo['id']); ?>">
                    <p class="jui_font_weight jui_fc_000"> <?php echo htmlentities($vo['p_title']); ?></p>
                    <!--   <p>2018-12-04 13:44:53</p>-->
                </div>
                <div class="jui_pad_1216 jui_flex jui_flex_justify_between">
                    <div class="jui_line_h15 buy_list_con">
                        <p class="jui_fc_000 jui_font_weight"><?php echo htmlentities($vo['p_type']); ?></p>
                        <!--                 <p>市值变化：<span class="jui_fc_zhuse">000&nbsp;&nbsp;000%</span></p>-->
                        <p>持仓数量：<span class="c_num"><?php echo htmlentities($vo['p_num']); ?></span></p>
                        <p>买入价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                        <p>开始时间：<?php echo htmlentities($vo['p_start_time']); ?></p>
                        <p>结束时间：<?php echo htmlentities($vo['p_end_time']); ?></p>
                    </div>
                    <?php if(($vo['d_status'] == 2)): ?>
                    <div class="jui_flex_col">
                        <?php if($config['w_exchange'] == '0'): ?>
                        <a href="#" class="buy_list_btn jui_bg_orange no_exchange">提货</a>
                        <?php else: ?>
                        <!-- <a href="<?php echo url('index/tihuo',array('id'=>$vo['id'])); ?>" class="buy_list_btn jui_bg_orange">提货</a> -->
                        <a href="#" class="buy_list_btn jui_bg_orange tihuo_btn" data-id="<?php echo htmlentities($vo['id']); ?>">提货</a>
                        <?php endif; ?>
                        <!-- <div class="buy_list_btn jui_bg_orange ti1">提货</div> -->
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; else: ?>
            <!-- 没有订单 
             <div class="jui_none_bar ">
                 <img src=" /static/index/icons/none_icon.png">
                 <P>暂无数据</P>
             </div>
             没有订单end -->
            <?php endif; if((!empty($list1))): foreach($list1 as $key=>$vo): if(($vo['p_num'] > 0 && $vo['p_start_time'] != '1900-01-01')): ?>
            <div class="buy_list_bar">
                <div class="jui_public_tit jui_flex_justify_between jui_bor_bottom">
                    <input name="d_id" type="hidden" id="d_id" value="<?php echo htmlentities($vo['id']); ?>">
                    <p class="jui_font_weight jui_fc_000"> <?php echo htmlentities($vo['p_title']); ?></p>
                    <!--   <p>2018-12-04 13:44:53</p>-->
                </div>
                <div class="jui_pad_1216 jui_flex jui_flex_justify_between">
                    <div class="jui_line_h15 buy_list_con">
                        <p class="jui_fc_000 jui_font_weight"><?php echo htmlentities($vo['p_type']); ?></p>
                        <!--                 <p>市值变化：<span class="jui_fc_zhuse">000&nbsp;&nbsp;000%</span></p>-->
                        <p>持仓数量：<span class="c_num"><?php echo htmlentities($vo['p_num']); ?></span></p>
                        <p>买入价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                        <p>开始时间：<?php echo htmlentities($vo['p_start_time']); ?></p>
                        <p>结束时间：<?php echo htmlentities($vo['p_end_time']); ?></p>
                    </div>
                    <?php if(($vo['d_status'] == 2)): ?>
                    <div class="jui_flex_col">
                        <?php if($config['w_exchange'] == '0'): ?>
                        <a href="#" class="buy_list_btn jui_bg_orange no_exchange">提货</a>
                        <?php else: ?>
                        <!-- <a href="<?php echo url('index/tihuo',array('id'=>$vo['id'])); ?>" class="buy_list_btn jui_bg_orange">提货</a> -->
                        <a href="#" class="buy_list_btn jui_bg_orange tihuo_btn" data-id="<?php echo htmlentities($vo['id']); ?>">提货</a>
                        <?php endif; ?>
                        <!-- <div class="buy_list_btn jui_bg_orange ti1">提货</div> -->
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; else: ?>
            <!-- 没有订单
             <div class="jui_none_bar ">
                 <img src=" /static/index/icons/none_icon.png">
                 <P>暂无数据</P>
             </div>
             没有订单end -->
            <?php endif; ?>

        </div>
        <!-- 持仓列表end -->
        <div class="jui_h12"></div>
    </div>
    <!-- 主体end -->
    <input type="hidden" id="buy_type" value="1" />
    <!-- 弹出框 -->
    <div class="jui_box_bar jui_none" id="buy_box">
        <div class="jui_box_conbar">
            <div class="jui_box_con jui_bg_ztqian">
                <div class="jui_box_pub_tit jui_font_weight">000</div>
                <div class="jui_box_pub_middle jui_text_left">
                    <p class="jui_fc_000 jui_font_weight">000</p>
                    <p>000</p>
                </div>
                <div class="jui_pad_12 jui_flex_row_center jui_flex_wrap jui_line_h15">
                    <div class="jui_grid_w50 jui_ellipsis_1 jui_fs12">000</div>
                    <div class="jui_grid_w50 jui_fs12">买卖方向：买</div>
                    <div class="jui_grid_w50 jui_flex_row_center">
                        <p class="jui_fs12">成交数量：</p>
                        <p class="jui_fc_zhuse num">000</p>
                    </div>
                    <div class="jui_grid_w50">成交价格：<span class="jui_fc_zhuse">000</span></div>
                </div>
                <div class="jui_h12"></div>
                <div class="jui_box_btnbar jui_flex_row_center jui_flex_justify_center">
                    <div class="jui_box_btn jui_bor_rad_5 jui_fc_zhuse" id="close">拒绝</div>
                    <div class="jui_box_btn jui_bor_rad_5 jui_bg_zhuse jui_bor_none jui_fc_fff" id="submit">接受</div>
                </div>
                <div class="jui_h20"></div>
            </div>
        </div>
    </div>
    <!-- 弹出框end -->

    <!-- 安全密码 -->
    <div class="m-keyboard" id="J_KeyBoard">
        <!-- 自定义内容区域 -->
        <div class="wjmm"><a href="?m=center&c=jymm_edit&type=1" class="jui_fc_green">修改密码</a></div>
        <div class="wjmm"><a href="?m=center&c=jymm_set&type=1" class="jui_fc_green">设置交易密码</a></div>
        <!-- 自定义内容区域 -->
    </div>
    <!-- 安全密码end -->
    <script src=" /static/index/layer/layer.js"></script>
    <script src=" /static/index/js/box_psw.js"></script>
</body>
<script>
    $('.tihuo_btn').click(function () {
        var a_id = $(this).data('id');
        layer.confirm('持仓产品将变成发货产品，是否确认?', { icon: 3, title: '提示' }, function (index) {
            window.location.href = '<?php echo url("index/tihuo"); ?>?id=' + a_id
        });
    })
</script>
<script>
    $('.no_exchange').click(function () {
        layer.msg('暂未开启置换功能');
    });
    $(function () {
        var hid_pid = $("#hid_pid").val();
        if (hid_pid == 1) {
            $("#bill_id option[value='1']").prop("selected", true);
        } else {
            $("#bill_id option[value='3']").prop("selected", true);
        }
        $("#close").on("click", function () {
            $("#buy_box").addClass('jui_none');
        })
        $('.submit').click(function () {
            $('#buy_type').val(1)
            $("#buy_box").removeClass('jui_none');
        })

        /*可用多个tab*/
        $(".jui_tab_tit li").click(function () {
            //$(this).siblings().removeClass("jui_tab_on");
            var type = $(this).find('.type').val();
            window.location.href = "?m=index&c=chicang&type=" + type;
            //$(this).addClass("jui_tab_on");
        });
        $(document).on('click', '.che', function () {
            var c_num = $(this).parents('.buy_list_bar').find('.c_num').text();
            $.post("?m=index&c=che", { c_num: c_num }, function (res) {
                layer.msg(res.msg);
                if (res.code == 1) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000)
                }
            }, 'json')
        })
        $(document).on('click', '.che1', function () {
            var c_num = $(this).parents('.buy_list_bar').find('.c_num').text();
            var d_sn = $(this).parent('.jui_flex_col').find('.d_sn').val();
            $.post("?m=index&c=che1", { c_num: c_num, d_sn: d_sn }, function (res) {
                layer.msg(res.msg);
                if (res.code == 1) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000)
                }
            }, 'json')
        })
        $(document).on('click', '.jiang', function () {
            $('.num').text(parseInt(000));
            $('#buy_type').val(2)
            $("#buy_box").removeClass('jui_none');
        })


    });	
</script>

<script>
    //弹框输入安全密码
    !function ($, ydui) {

        var dialog = ydui.dialog;
        var $keyboard = $('#J_KeyBoard');

        // 初始化参数
        $keyboard.keyBoard({
            disorder: true, // 是否打乱数字顺序
            title: '安全键盘' // 显示标题
        });

        // 打开键盘
        /* $('#J_OpenKeyBoard').on('click', function () {
             $keyboard.keyBoard('open');
         });*/
        $("#submit").on("click", function () {
            $("#buy_box").addClass('jui_none');
            $keyboard.keyBoard('open');
        })

        // 六位密码输入完毕后执行
        $keyboard.on('done.ydui.keyboard', function (ret) {
            // 弹出请求中提示框
            var pass = ret.password;
            var num = $('.num').text();
            var buy_type = $('#buy_type').val();
            $.post("?m=index&c=buy_pi", { pass: pass, num: num, buy_type: buy_type }, function (res) {
                layer.msg(res.msg)
                if (res.code == 1) {
                    $keyboard.keyBoard('close');
                    setTimeout(function () {
                        window.location.reload();
                    }, '1000')
                }
            }, 'json')

            // $keyboard.keyBoard('close');
        });

    }(jQuery, YDUI);

    $('#bill_id').change(function () {
        var bill_id = $("#bill_id option:selected").val();
        window.location.href = "<?php echo url('Index/chicang'); ?>" + '?bill_id=' + bill_id;
    });

    $('#click_url').click(function () {
        var bill_id = $("#bill_id option:selected").val();
        window.location.href = "<?php echo url('Index/chicang'); ?>" + '?bill_id=' + bill_id;
    });
    $(document).on('click', '.ti1', function () {
        var d_id = $('#d_id').val();
        window.location.href = "<?php echo url('center/address_list'); ?>" + '?type=3' + '&d_id=' + d_id;
        $.post("<?php echo url('Index/carry'); ?>", { did: d_id }, function (res) {
            layer.msg(res.msg);
            if (res.code == 1) {
                setTimeout(function () {
                    window.location.reload();
                }, 1000)
            }
        }, 'json')
    })


</script>


</html>