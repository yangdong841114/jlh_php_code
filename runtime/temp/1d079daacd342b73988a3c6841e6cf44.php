<?php /*a:1:{s:67:"D:\wwwroot\jlh_php_code\application\index\view\login\login_set.html";i:1602825046;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href="/static/index/css/style.css">
<link rel="stylesheet" type="text/css" href="/static/index/css/css.css">
<script src="/static/index/js/jquery-3.3.1.min.js"></script>
<script src="/static/index/js/flexible.js"></script>
<script src="/static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Login/account_list'); ?>"><img src="/static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">登录</div>
    <div class="jui_top_right user_del">删除</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<form id="form1">
<div class="jui_main">
     <div class="jui_public_list2 jui_bg_fff">
          <img class="tx_img jui_bor_rad_50" src="<?php echo htmlentities($user['m_avatar']); ?>">
          <div class="jui_flex_col">
                <p class="jui_fs15 jui_fc_000 jui_pad_b5"><?php echo htmlentities($user['m_name']); ?></p>
                <P>交易账号：<?php echo htmlentities($user['m_account']); ?></P>
          </div>
     </div>
     <div class="jui_h12"></div>
       <div class="jui_public_list jui_bor_rad_5 jui_bg_fff">
             <img class="login_icon" src="/static/index/icons/mm_icon.png">
             <input class="jui_flex1 jui_fc_000" type="password" placeholder="请输入密码" id="m_login_pwd" name="m_login_pwd">
       </div>                    
       <div class="jui_h12"></div>
       <input type="hidden" name="m_trade_code" value="<?php echo htmlentities($user['m_account']); ?>"/>
       <div class="jui_public_btn" ><input type="button" value="登录"  id="dosubmit"></div>
       <a href="<?php echo url('Login/forget'); ?>" class="jui_block jui_text_center">忘记密码</a>
       <div class="jui_h12"></div>
</div>
</form>
<!-- 主体end -->
<script src="/static/index/layer/layer.js"></script>
<script src="/static/index/js/jquery-3.3.1.min.js"></script>
<script>
    function isPass(num) {
        var reg = /^[\S]{6,12}$/;
        return reg.test(num);
    }
    $('#dosubmit').click(function(){
        var m_login_pwd =  $('#m_login_pwd').val();
        if (m_login_pwd == '') {
            layer.msg('登录密码不能为空');
            return;
        }
        if (!isPass(m_login_pwd)) {
            layer.msg('密码必须6到12位，且不能出现空格');
            return;
        }
        $.post("<?php echo url('Login/login_set'); ?>",$('#form1').serialize(),function(res){
            layer.msg(res.msg);
            if(res.code==1){
                setTimeout(function () {
                    window.location.href="<?php echo url('Index/index'); ?>";
                },1000)
            }
        },'json')
    });
    $('.user_del').click(function(){
        var id = <?php echo htmlentities($user['id']); ?>;
        $.post("<?php echo url('Login/user_del'); ?>",{id:id},function(res){
            layer.msg(res.msg);
            if(res.code=='1'){
                setTimeout(function(){
                    window.location.href="<?php echo url('Login/account_list'); ?>";
                },1000)
            }
        },'json')
    })


</script>
</body>
</html>
