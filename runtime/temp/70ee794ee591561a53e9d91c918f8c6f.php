<?php /*a:1:{s:74:"/www/wwwroot/www.jiulonghu123.com/application/index/view/index/chedan.html";i:1587222103;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <title><?php echo htmlentities($config['w_name']); ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
<script src=" /static/index/js/flexible.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/public.js"></script>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
     <a class="jui_top_left" href="<?php echo url('Index/index'); ?>"><img src=" /static/index/icons/back_icon.png"></a>
     <div class="jui_top_middle">撤单记录</div>
</div>
<!-- 头部end -->
<!-- 主体 -->
<div class="jui_main">
     <!-- 撤单记录 -->
     <div class="jui_flex_col jui_pad_l12 jui_pad_r12 jui_pad_t14">
         <?php if(!empty($list)): foreach($list as $key=>$vo): ?>
         <div class="buy_list_bar">
             <input name="d_id" type="hidden" id="d_id" value="<?php echo htmlentities($vo['id']); ?>">
             <div class="jui_public_tit jui_bor_bottom">
                 <p class="jui_font_weight jui_fc_000"><?php echo htmlentities($vo['p_title']); ?></p>
                 <?php if(( $vo['d_type'] == 1)): ?>
                 <div class="jui_tag jui_bg_zhuse jui_mar_l8">买入</div>
                 <?php else: ?>
                 <div class="jui_tag jui_bg_blue jui_mar_l8">卖出</div>
                 <?php endif; ?>
             </div>
             <div class="jui_pad_1216 jui_line_h15 buy_list_con">
                 <p class="jui_fc_000 jui_font_weight"><?php echo htmlentities($vo['p_type']); ?></p>
<!--                 <p>市值变化：<span class="jui_fc_zhuse">000&nbsp;&nbsp;000%</span></p>-->
                 <p>数量：<?php echo htmlentities($vo['d_total']); ?></p>
                 <p>买入价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                 <p>卖出价格：¥<?php echo htmlentities($vo['d_price']); ?></p>
                 <p>发起时间：<?php echo htmlentities($vo['d_addtime']); ?></p>
                 <div class="flex_col" id="c_btn">
                     <?php if(( $vo['d_status']==10)): ?>
                     <div class="buy_list_btn jui_bg_orange che1">撤单</div>
                     <?php else: ?>
                     <div class="buy_list_btn jui_bg_orange">已撤单</div>
                     <?php endif; ?>
                 </div>
             </div>
<!--             <div class="jui_public_tit jui_bor_top">撤单原因：自主撤单(系统撤单)</div>-->
         </div>
         <?php endforeach; else: ?>
         <!-- 没有订单 -->
         <div class="jui_none_bar ">
             <img src=" /static/index/icons/none_icon.png">
             <P>暂无数据</P>
         </div>
         <!-- 没有订单end -->
         <?php endif; ?>
     </div>
     <!-- 撤单记录end -->
</div>
<!-- 主体end -->
</body>
<script>
    $(document).on('click','.che1',function(){
        var d_id = $('#d_id').val();
        $.post("<?php echo url('Index/chedan'); ?>",{d_id:d_id},function (res) {
            if(res.code==1){
                layer.msg('撤单成功')
                setTimeout(function () {
                    window.location.reload();
                },1000)
            }else{
                layer.msg('撤单失败')
            }
        })
    })
</script>
</html>
