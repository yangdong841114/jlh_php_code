<?php /*a:1:{s:65:"D:\wwwroot\jlh_php_code\application\index\view\index\duihuan.html";i:1602124316;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>积分兑换</title>
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/diqu.css">
    <link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">
    <script src=" /static/index/js/flexible.js"></script>
    <script src=" /static/index/js/jquery-3.3.1.min.js"></script>
    <script src=" /static/index/js/public.js"></script>
    <script src=" /static/index/js/picker.min.js"></script>
    <script src=" /static/index/js/city.js"></script>
    <style>
        #dosubmit{
            width: 45%;
            padding: .42666rem;
            float: left;
        }
        #zhihuan{
            width: 45%;
            float: right;
           
        }
        #zhihuan input{
            background: #282828;

        }
    </style>
</head>
<body class="jui_bg_grey">
<!-- 头部 -->
<div class="jui_top_bar">
    <a class="jui_top_left" href="javascript:history.go(-1)">
        <img src=" /static/index/icons/back_icon.png">
    </a>
    <div class="jui_top_middle" id="tihuo" style="color:#000;">积分兑换</div>
    <!--<div class="jui_top_middle" id="huzhuan">互转</div>-->
</div>
<!-- 头部end -->
<!-- 主体 -->

<form id="form2">
    <div class="jui_main">
        <div class="jui_h12"></div>
        <div class="jui_bg_fff">

            <div class="jui_public_list">
                <p class="dlmm_left_text jui_pad_r8">兑换数量：</p>
                <input class="jui_flex1 jui_fc_000" type="text" name="account" value="">
            </div>
            <div style="margin: 10px auto; text-align: center; color: #FF0000;">积分兑换仓单每一单扣除186积分，次日购买</div>

        </div>
        <div class="jui_public_btn" id="dohzsubmit" ><input type="button" value="提交"></div>
    </div>
</form>

<!-- 主体end -->
<script src=" /static/index/js/cityjs.js"></script>
<script src=" /static/index/layer/layer.js"></script>
<script src=" /static/index/js/jquery-3.3.1.min.js"></script>
<script>

    // 切换互转和提货
    $('#tihuo').click(function(){
        $('#form1').show();
        $('#form2').hide();
        $('#tihuo').css('color', '#000');
        $('#huzhuan').css('color', '#fff');
    })

    $('#huzhuan').click(function(){
        $('#form2').show();
        $('#form1').hide();
        $('#huzhuan').css('color', '#000');
        $('#tihuo').css('color', '#fff');
    })

    // 点击置换
    $('#zhihuan').click(function(){

        $.get('<?php echo url("index/is_min_exchange_num"); ?>', function(res){

            if(res.code == 200){
                $('#bill_id').attr('disabled', false);
            }else{
                layer.msg(res.msg);
            }
        }, 'json');

        return false;
    })

    function isPhoneNumber(tel) {
        var reg = /^0?1[3|4|5|6|7|8|9][0-9]\d{8}$/;
        return reg.test(tel);
    }
    

    // 互转提交
    $('#dohzsubmit').click(function(){

        var min_num = 1;
        var number = $('input[name="account"]').val();
        
        if(number == ''){
            layer.msg('请填写兑换数量'); return;
        }

        number = parseInt(number);
        if(number < min_num){
            layer.msg('最低兑换数量为'+min_num); return;
        }

        var data = {
            account: number
        };


        layer.confirm('积分兑换仓单每一单扣除186积分，不可撤销，确定要兑换吗？', {icon: 3, title:'提示'}, function(index){

            var layIndex = layer.load();

            $.post('<?php echo url("index/duihuan_turn"); ?>', data, function(res){
                
                layer.close(layIndex);

                if(res.code == 200){
                    layer.msg(res.msg);
                    setTimeout(function(){
                        window.location.reload()
                    }, 800);
                }else{
                    layer.msg(res.msg);
                }
            }, 'json')
        })

    })

</script>
</body>
</html>
