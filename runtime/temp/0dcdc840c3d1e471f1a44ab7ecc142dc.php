<?php /*a:1:{s:63:"D:\wwwroot\jlh_php_code\application\index\view\index\index.html";i:1602825044;}*/ ?>
<!doctype html>

<html>

<head>

<meta charset="utf-8">

<title><?php echo htmlentities($config['w_name']); ?></title>

<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

<meta name="format-detection" content="telephone=no" />

<link rel="stylesheet" type="text/css" href=" /static/index/css/style.css">

<link rel="stylesheet" type="text/css" href=" /static/index/css/css.css">

<link rel="stylesheet" type="text/css" href=" /static/index/css/key.css">        <!-- 安全密码css -->

<script src=" /static/index/js/flexible.js"></script>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>

<script src=" /static/index/js/public.js"></script>

    <style>

        .buy_box_close{  position:relative; bottom:-0.8rem; width:.8rem; height:.8rem; left:50%; margin-left:-.5rem; color:#fff; text-align:center; font-size:.69333rem;}

    </style>

</head>

<body class="jui_bg_grey">

<!-- 主体 -->

<div class="jui_main">

    <!-- 头部 -->

    <div class="sy_topbar jui_bg_ztjb">

         <div class="tx_bar jui_flex_row_center">

              <div class="tx_img jui_bor_rad_50"><img src="<?php echo htmlentities($user['m_avatar']); ?>" alt="头像"></div>

              <div class="jui_flex1">
                  <!--<?php echo $user['house_area']==1 ? '有效区' : '学习区'; ?>-->

                   <p class="jui_fc_fff jui_fs15 jui_pad_b8">交易账号：<?php echo htmlentities($user['m_account']); ?> </p>

                   <div class="sy_level jui_flex_row_center jui_flex_justify_center"><?php echo htmlentities($m_level); ?></div>
                   

              </div>

         </div>

         <div class="jui_h20"></div>

         <div class="jui_flex_row_center">

              <a href="<?php echo url('Index/balanceRecord'); ?>" class="jui_flex1 jui_flex_col_center">

                   <p class="jui_fc_fff7">我的余额</p>

                   <p class="jui_fs15 jui_fc_fff"><?php echo htmlentities($user['m_balance']); ?></p>

              </a>

              <!-- <a href="<?php echo url('Index/integralRecord'); ?>" class="jui_flex1 jui_flex_col_center"> -->
                <?php if($configMsg == '1'): ?>
                <a href="#" class="jui_flex1 jui_flex_col_center no_exchange">
                <?php else: ?>
                <a href="<?php echo url('index/tihuo'); ?>" class="jui_flex1 jui_flex_col_center">
                <?php endif; ?>
                
                   <p class="jui_fc_fff7">提货积分</p>
                   <p class="jui_fs15 jui_fc_fff"><?php echo htmlentities($user['m_integral']); ?></p>
              </a>
             <!-- <a href="<?php echo url('Index/spendRecord'); ?>" class="jui_flex1 jui_flex_col_center"> -->
                <?php if($config['w_exchange'] == '0'): ?>
                <a href="#" class="jui_flex1 jui_flex_col_center no_exchange">
                <?php else: ?>
                <a href="<?php echo url('index/duihuan'); ?>" class="jui_flex1 jui_flex_col_center">
                <?php endif; ?>
                   <p class="jui_fc_fff7">赠送积分</p>
                   <p class="jui_fs15 jui_fc_fff"><?php echo htmlentities($user['m_wot']); ?></p>
              </a>

              <a href="<?php echo url('Index/lockRecord'); ?>" class="jui_flex1 jui_flex_col_center">

                   <p class="jui_fc_fff7">累计积分</p>

                   <p class="jui_fs15 jui_fc_fff"><?php echo htmlentities($user['m_tics_wot']); ?></p>

              </a>

         </div>

    </div>

    <!-- 头部end -->



    <!-- 市值变化 

    <div class="jui_bg_fff jui_pad_12">

         <p class="jui_fc_000 jui_fs15 jui_pad_b12">市值变化</p>

         <div class="jui_flex_row_center jui_flex_justify_between">

              <p class="jui_fc_zhuse jui_font_weight">0.0元</p>

              <P class="jui_fc_zhuse jui_font_weight">0.1%</P>

         </div>

    </div>-->
    
    <div class="jui_bg_fff jui_pad_12">
        <a href="<?php echo url('Index/bouns'); ?>" class="jui_flex1">
         <div class="jui_flex_row_center jui_flex_justify_between">
             
              
              <p class="jui_fc_zhuse jui_font_weight">奖励仓单领取</p>
              
              <p class="jui_fc_zhuse jui_font_weight">团队交易比例<?php echo htmlentities($rate*100); ?>%</p>
         </div>
        </a>
    </div>
 

    

    <!-- 市值变化end -->

    <div class="jui_h12"></div>

    <!-- 资金管理 -->

    <div class="jui_public_tit jui_bg_fff jui_fc_000 jui_font_weight">资金管理</div>

    <div class="jui_bg_fff jui_flex_row_center jui_flex_wrap">

        <a href="<?php echo url('Index/recharge'); ?>" class="jui_grid_w33 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon15.png">

            <p>入金</p>

        </a>

        <a href="<?php echo url('Index/withdrawal'); ?>" class="jui_grid_w33 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon16.png">

            <p>出金</p>

        </a>

        <a href="<?php echo url('Index/balanceRecord'); ?>" class="jui_grid_w33 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon17.png">

            <p>资金明细</p>

        </a>

    </div>

    <!-- 资金管理end -->



    <div class="jui_h12"></div>

    <!-- 实用工具 -->

    <div class="jui_public_tit jui_bg_fff jui_fc_000 jui_font_weight">实用工具</div>

    <div class="jui_bg_fff jui_flex_row_center jui_flex_wrap">

        <a href="<?php echo url('Center/team'); ?>" class="jui_grid_w25 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon08.png">

            <p>市场</p>

        </a>

        <a href="<?php echo url('Order/order_list'); ?>" class="jui_grid_w25 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon02.png">

            <p>订单</p>

        </a>



        <?php if($user['m_real'] == 1 && $user['m_bank_signing'] == 0): ?>

            <a href="<?php echo url('Index/open_account'); ?>" class="jui_grid_w25 jui_grid_list">

                <img class="jui_grid_icon2" src=" /static/index/icons/my_icon04.png">

                <p>开户</p>

            </a>

        <?php elseif($user['m_real'] == 1 && ($user['m_bank_signing'] == 2 || $user['m_bank_signing'] == 3)): ?>

            <a href="<?php echo url('Center/set_bank'); ?>" class="jui_grid_w25 jui_grid_list">

                <img class="jui_grid_icon2" src=" /static/index/icons/my_icon04.png">

                <p>签约</p>

            </a>

        <?php elseif($user['m_bank_signing'] == 1): ?>

            <a href="<?php echo url('Center/set_bank'); ?>" class="jui_grid_w25 jui_grid_list">

                <img class="jui_grid_icon2" src=" /static/index/icons/my_icon04.png">

                <p>已签约</p>

            </a>

        <?php else: ?>

            <a href="#" id="is_open_account" class="jui_grid_w25 jui_grid_list">

                <img class="jui_grid_icon2" src=" /static/index/icons/my_icon04.png">

                <p>开户</p>

            </a>

        <?php endif; ?>

        <a href="<?php echo url('Center/share'); ?>" class="jui_grid_w25 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon03.png">

            <p>二维码</p>

        </a>
        <!-- 废弃 -->
        <!-- <a href="<?php echo url('Center/express'); ?>" class="jui_grid_w25 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/express.png">

            <p>快递信息</p>

        </a> -->
    </div>

    <!-- 实用工具end -->



    <!-- 交易操作 -->

    <div class="jui_h12"></div>

    <div class="jui_public_tit jui_bg_fff jui_fc_000 jui_font_weight">交易操作</div>

    <div class="jui_bg_fff jui_flex_row_center jui_flex_wrap">

        <a href="<?php echo url('Index/buy'); ?>" class="jui_grid_w20 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon13.png">

            <p>买入</p>

        </a>

        <a href="<?php echo url('Index/sell'); ?>" class="jui_grid_w20 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon14.png">

            <p>卖出</p>

        </a>

        <a href="<?php echo url('Index/chicang'); ?>" class="jui_grid_w20 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon19.png">

            <p>持仓</p>

        </a>

        <a href="<?php echo url('Index/chedan'); ?>" class="jui_grid_w20 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon18.png">

            <p>撤单</p>

        </a>

        <a href="<?php echo url('Index/chengjiao'); ?>" class="jui_grid_w20 jui_grid_list">

            <img class="jui_grid_icon2" src=" /static/index/icons/my_icon11.png">

            <p>成交信息</p>

        </a>

    </div>

    <div class="jui_h12"></div>

    <!-- 交易操作end -->



</div>

<!-- 主体end -->

<!-- 固定底部 -->

<div class="jui_footer">

    <a href="<?php echo url('Index/index'); ?>" class="jui_foot_list jui_hover">

        <b class="foot_index"></b>

        <p>交易</p>

    </a>

    <a href="<?php echo url('Index/quotations'); ?>" class="jui_foot_list">

        <b class="foot_hq"></b>

        <p>行情</p>

    </a>
    
    <?php if($configMsg == '1'): ?>
        <a href="#" class="jui_foot_list no_exchange">
    <?php else: ?>
        <a href="<?php echo url('Order/shop'); ?>" class="jui_foot_list">
    <?php endif; ?>

        <b class="foot_shop"></b>

        <p>置换仓库</p>

    </a>

    <a href="<?php echo url('Center/notice_list'); ?>" class="jui_foot_list">

        <b class="foot_notice"></b>

        <p>咨询</p>

    </a>

    <a href="<?php echo url('Center/center'); ?>" class="jui_foot_list">

        <b class="foot_my"></b>

        <p>我的</p>

    </a>

</div>

<!-- 固定底部end -->

<?php if($grant!=0): ?>

    <!-- 弹出 -->

    <div class="jui_box_bar buy_box_bar" id="divtejiapiao">

        <div class="jui_box_conbar">

            <div class="jui_box_con">

                <div class="jui_box_pub_tit jui_font_weight">特价票信息</div>

                <div class="jui_box_pub_middle">

                    <div class="">您有新的特价票，请尽快领取</div>

                    <div class="jui_public_btn buy_box_btn J_OpenKeyBoard" id="btngoumai"><input type="button" value="确认"></div>

                </div>

            </div>

            <img class="buy_box_close" src="/static/index/icons/close2.png">

        </div>

    </div>

    <!-- 弹出end -->

<?php endif; ?>

<!-- 安全密码 -->

<div class="m-keyboard" id="J_KeyBoard">

    <!-- 自定义内容区域 -->

    <div class="wjmm"><a href="<?php echo url('Center/jymm_edit',['type'=>1]); ?>" class="jui_fc_green">修改密码</a></div>

    <!-- 自定义内容区域 -->

</div>

<!-- 安全密码end -->

</body>

<script src=" /static/index/js/jquery-3.3.1.min.js"></script>

<script src=" /static/index/js/box_psw.js"></script>

<script src=" /static/index/layer/layer.js"></script>

<script>

    $('.no_exchange').click(function(){
        layer.msg('非签约时间不可兑换');
    });

    $('#is_open_account').click(function () {

        layer.msg('实名审核中,请耐心等待');

    });

    $('#is_bank_account').click(function () {

        layer.msg('请先前往添加银行卡信息');

        setTimeout(function () {

            window.location.href="<?php echo url('Center/set_bank'); ?>"

        },1000)

    });

    $(".buy_box_close").click(function(){

        $(".buy_box_bar").addClass("jui_none")

    });

    $("#btngoumai").click(function(){
        window.location.href="<?php echo url('index/bouns'); ?>";
    });

</script>

</html>

