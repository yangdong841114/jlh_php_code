<?php
return [
    // 产品信息
    'product_name'      => '江西九龙',
    'product_version'   => '2.43',
    'build_version'     => '#',
    'product_website'   => '#',
    'product_update'    => '#',
    'develop_team'      => 'PHP',
    // 公司信息
    'company_name'    => '',
    'company_website' => '#',
];