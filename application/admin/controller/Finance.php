<?php

namespace app\admin\controller;

use app\common\builder\ZBuilder;

use think\Db;



class Finance extends Admin

{



    //财务管理

    public function finance_list(){

        return ZBuilder::make('table')

            ->fetch(); // 渲染模板

    }



    //充值记录

    public function user_logs(){

        do_alogs('查看用户充值记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('l_time desc');

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $data_list  = Db::name('Logs a')->join('w_user b','b.id=a.l_uid')->where($map)->field('a.*,b.m_nickname')->order($order)->limit(20)->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('logs')

            ->setSearch(['b.id' => 'ID', 'uid' => '用户ID','m_nickname'=>'用户昵称'])                        // 设置搜索参数

            ->addTimeFilter('l_time','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户昵称', 'text'],

                ['l_num', '金额', 'text'],

                ['l_type', '充值类型', 'status','',[1=>'余额',2=>'积分',3=>'消费积分',4=>'累计积分']],

                ['l_info', '备注', 'text'],

                ['l_time','操作时间','datetime'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('user_logs_export', ['uniqkey' => $uniqkey])])

            ->hideCheckbox()

            ->addOrder('id,l_time')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function login_log(){

        do_alogs('查看用户登陆记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('login_time desc');

        $data_list  = Db::name('Login_log a')->join('w_user b','b.id=a.uid')->where($map)->field('a.*,b.m_nickname')->order($order)->limit(20)->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('login_log')

            ->setSearch(['b.id' => 'ID', 'uid' => '用户ID','m_nickname'=>'用户昵称'])                        // 设置搜索参数

            ->addTimeFilter('login_time','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户昵称', 'text'],

                ['login_ip', '登陆IP', 'text'],

                ['login_city', '登陆城市', 'text'],

                ['login_type', '登陆方式', 'status','',[1=>'前台登陆',2=>'后台登陆',3=>'其他方式登陆']],

                ['login_time','登录时间','datetime'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addOrder('id,login_time')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function flog(){

        do_alogs('查看文件记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('f_addtime desc');

        $data_list  = Db::name('flogs')

            ->where($map)

            ->order($order)

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        $btn_access = [

            'title' => '已读',

            'icon'  => 'fa fa-fw fa-key',

            'class' => 'btn btn-xs btn-default ajax-get confirm',

            'href'  => url('read', ['id' => '__id__']),

            'data-title' => '确认已读？'

        ];

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('flogs')

            ->setSearch(['id' => 'ID'])                        // 设置搜索参数

            ->addTimeFilter('f_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['f_title','标题', 'text'],

                ['f_type', '文件类型', 'text'],

                ['admin_id', '报送管理员', 'text'],

                ['f_path', '文件路径', 'text'],

                ['f_status', '是否成功', 'status','',[0=>'未成功',1=>'成功']],

                ['f_read', '是否已读', 'switch','',[0=>'未读',1=>'已读']],

                ['f_identity', '发送方', 'status','',[1=>'我方发送',2=>'我方接受']],

                ['f_addtime', '记录时间', 'datetime'],

                ['f_date', '报送日期', 'text'],

                ['f_desc', '备注', 'text'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addRightButton('custom', $btn_access)

            ->addOrder('id,f_addtime')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }





    public function ilog()

    {

        do_alogs('查看资料修改记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('i_addtime desc');

        $data_list  = Db::name('ilogs')

            ->alias('i')

            ->join('user u','i.uid=u.id','LEFT')

            ->join('bank_database b','i.i_old_open_bank_no=b.bank_open_code','LEFT')

            ->join('bank_database b2','i.i_new_open_bank_no=b2.bank_open_code','LEFT')

            ->where($map)

            ->order($order)

            ->field('i.*,u.m_nickname,u.m_phone,b.bank_name,b2.bank_name as new_name')

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('ilogs')

            ->setSearch(['i.id' => 'ID','m_nickname'=>'用户名','i_code'=>'请求编号'])                        // 设置搜索参数

            ->addTimeFilter('i_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户名', 'text'],

                ['m_phone', '用户手机号', 'text'],

                ['bank_name', '旧开户行', 'text'],

                ['i_old_bank_carid', '旧卡号', 'text'],

                ['new_name', '新开户行', 'text'],

                ['i_new_bank_carid', '新卡号', 'text'],

                ['i_status', '状态', 'status','',[0=>'未成功',1=>'成功']],

                ['i_addtime', '添加时间', 'datetime'],

                ['last_time', '更新时间', 'datetime'],

                ['i_code', '请求编号', 'text'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addOrder('id,i_addtime')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }





    public function ulogs()

    {

        do_alogs('查看积分释放记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('u_addtime desc');

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $data_list  = Db::name('ulogs')

            ->alias('l')

            ->join('user u','l.uid=u.id','LEFT')

            ->where($map)

            ->order($order)

            ->field('l.*,u.m_nickname,u.m_phone')

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('ulogs')

            ->setSearch(['l.id' => 'ID','m_nickname'=>'用户名','m_phone'=>'用户手机号'])                        // 设置搜索参数

            ->addTimeFilter('u_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户名', 'text'],

                ['m_phone', '用户手机号', 'text'],

                ['u_date', '释放日期', 'text'],

                ['u_push_num', '直推人数', 'text'],

                ['u_team_num', '团队人数', 'text'],

                ['u_jt_num', '静态释放数量', 'text'],

                ['u_dt_num', '动态释放数量', 'text'],

                ['u_total', '释放总数', 'text'],

                ['u_addtime', '释放时间', 'datetime'],

                ['u_status', '状态', 'status','',[0=>'未完成',1=>'已完成']],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('ulogs_export', ['uniqkey' => $uniqkey])])

            ->hideCheckbox()

            ->addOrder('id,u_addtime')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }





    public function wlogs()

    {

        do_alogs('查看钱包操作记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('w_addtime desc');

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $data_list  = Db::name('wlogs')

            ->alias('w')

            ->join('user u','w.uid=u.id','LEFT')

            ->join('product p','w.pid=p.id','LEFT')

            ->where($map)

            ->order($order)

            ->field('w.*,u.m_nickname,u.m_phone,p.p_title')

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('wlogs')

            ->setSearch(['w.id' => 'ID','u.m_nickname'=>'用户名','u.m_phone'=>'用户手机号','p.p_title'=>'产品名称'])                        // 设置搜索参数

            ->addTimeFilter('w_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户名', 'text'],

                ['m_phone', '用户手机号', 'text'],

                ['p_title', '产品名称', 'text'],

                ['w_type', '操作方式', 'status','',[1=>'买',2=>'卖']],

                ['w_num', '操作数量', 'text'],

                ['w_desc', '操作说明', 'text'],

                ['w_credit_type', '种类', 'status','', [0=>'默认',1=>'零售',2=>'批发',3=>'奖励']],

                ['w_addtime', '操作时间', 'datetime'],

            ])

            ->setExtraCss($css)

            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('wlogs_export', ['uniqkey' => $uniqkey])])

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addOrder('id,w_addtime')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }







    public function zlog()

    {

        do_alogs('查看入金记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('z_addtime desc');

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $data_list  = Db::name('zlog')

            ->alias('z')

            ->join('user u','z.uid=u.id','LEFT')

            ->where($map)

            ->where(array('z_type'=>1))

            ->order($order)

            ->field('z.*,u.m_nickname,u.m_phone')

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('wlogs')

            ->setSearch(['z.id' => 'ID','z_account' => '账号','m_nickname'=>'用户名','m_phone'=>'用户手机号'])                        // 设置搜索参数

            ->addTimeFilter('z_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户名', 'text'],

                ['m_phone', '用户手机号', 'text'],

                ['z_account', '账号', 'text'],

                ['z_type', '操作方式', 'status','',[1=>'入金',2=>'出金']],

                ['z_price', '金额', 'text'],

                ['z_fee', '手续费比例', 'text'],

                ['z_fee_price', '手续费额度', 'text'],

                ['z_bank_id', '出入金银行卡', 'text'],

                ['z_num', '交易流水号', 'text'],

                ['z_desc', '出入金描述', 'text'],

                ['z_status', '状态', 'status','',[0=>'发起',1=>'成功',2=>'异常']],

                ['z_addtime', '操作时间', 'datetime'],

                ['last_time', '更新时间', 'datetime'],

                ['z_overtime', '完成时间', 'datetime'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addTopButton('export', ['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('zlogs_export', ['uniqkey' => $uniqkey])])

            ->addOrder('id,z_addtime,z_price')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function zlog2()

    {

        do_alogs('查看入金记录');

        $map        = $this->getMap();

        $order      = $this->getOrder('z_addtime desc');

        $data_list  = Db::name('zlog')

            ->alias('z')

            ->join('user u','z.uid=u.id','LEFT')

            ->where($map)

            ->where(array('z_type'=>2))

            ->order($order)

            ->field('z.*,u.m_nickname,u.m_phone')

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

        </style>";

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('wlogs')

            ->setSearch(['z.id' => 'ID','z_account' => '账号','m_nickname'=>'用户名','m_phone'=>'用户手机号'])                        // 设置搜索参数

            ->addTimeFilter('z_addtime','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['m_nickname','用户名', 'text'],

                ['m_phone', '用户手机号', 'text'],

                ['z_account', '账号', 'text'],

                ['z_type', '操作方式', 'status','',[1=>'入金',2=>'出金']],

                ['z_price', '金额', 'text'],

                ['z_fee', '手续费比例', 'text'],

                ['z_fee_price', '手续费额度', 'text'],

                ['z_bank_id', '出入金银行卡', 'text'],

                ['z_num', '交易流水号', 'text'],

                ['z_desc', '出入金描述', 'text'],

                ['z_status', '状态', 'status','',[0=>'发起',1=>'成功',2=>'异常']],

                ['z_addtime', '操作时间', 'datetime'],

                ['last_time', '更新时间', 'datetime'],

                ['z_overtime', '完成时间', 'datetime'],

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->hideCheckbox()

            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('zlog2_export', ['uniqkey' => $uniqkey])])

            ->addOrder('id,z_addtime,z_price')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    //分公司提现

    public function withdrawal()

    {

        do_alogs('分公司提现审核');

        $map        = $this->getMap();

        $order      = $this->getOrder('create_time desc');

        $data_list  = Db::name('gold')

            ->where($map)

            ->order($order)

            ->limit(20)

            ->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        $yes_access = [

            'title' => '通过',

            'icon'  => 'fa fa-fw fa-check',

            'class' => 'btn btn-xs btn-default ajax-get confirm',

            'href'  => url('pass', ['id' => '__id__']),

            'data-title' => '确认通过？'

        ];

        $no_access = [

            'title' => '拒绝',

            'icon'  => 'fa fa-fw fa-remove',

            'class' => 'btn btn-xs btn-default ajax-get confirm',

            'href'  => url('noPass', ['id' => '__id__']),

            'data-title' => '确认拒绝？'

        ];



        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('gold')

            ->setSearch(['id' => 'ID','nickname'=>'用户名','phone'=>'用户手机号'])                        // 设置搜索参数

            ->addTimeFilter('create_time','','开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['nickname','用户名', 'text'],

                ['phone', '用户手机号', 'text'],

                ['money', '金额', 'text'],

                ['status', '状态', 'status','',[0=>'待审核',1=>'成功',2=>'失败']],

                ['create_time', '申请时间', 'datetime'],

                ['update_time', '审核时间', 'datetime'],

                ['right_button', '操作', 'btn']

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addRightButton('custom', $yes_access)

            ->addRightButton('custom', $no_access)

            ->hideCheckbox()

            ->addOrder('id,create_time,update_time')                                                               //添加排序

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function pass()

    {

        $id = $this->request->param('id');

        $info = Db::name('gold')->where('id',$id)->find();

        if($info['status']!=0){

            $this->error('已经审核过了');

        }

        $res = Db::name('gold')->where('id',$id)->update(['update_time'=>time(),'status'=>1]);

        if($res){

            $this->success('操作成功');

        }

        $this->error('操作失败');

    }



    public function noPass()

    {

        $id = $this->request->param('id');

        $info = Db::name('gold')->where('id',$id)->find();

        if($info['status']!=0){

            $this->error('已经审核过了');

        }

        $res = Db::name('gold')->where('id',$id)->update(['update_time'=>time(),'status'=>2]);

        $res_balance_sell = do_logs($info['uid'],1,'m_balance',$info['money'],'提现拒绝,余额返还');

        if($res && $res_balance_sell){

            $this->success('操作成功');

        }

        $this->error('操作失败');

    }


        // 用户充值记录导出
    public function user_logs_export()
    {
        $begintime = microtime(true);
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');
                    $sql = 'SELECT a.`id`, b.`m_nickname`, a.`l_num`, CASE a.`l_type` WHEN 1 THEN "余额" WHEN 2 THEN "积分" WHEN 3 THEN "消费积分" WHEN 4 THEN "累计积分" ELSE "其它" END AS `l_type`, a.`l_info`, FROM_UNIXTIME(a.`l_time`) AS `l_date` FROM w_logs a INNER JOIN `w_user` b ON b.`id` = a.`l_uid`';
                    if(!empty($qrycache['where']) && is_array($qrycache['where']))
                    {
                        $where = $qrycache['where'];
                        $wheres = [];
                        foreach($where as $condition)
                        {
                            if(is_array($condition) && count($condition) === 3)
                            {
                                switch($condition[1])
                                {
                                    case 'between time':
                                        $wheres[] = 'a.`' . $condition[0] . '` > UNIX_TIMESTAMP("' . $condition[2][0] . '") AND a.`' . $condition[0] . '` < UNIX_TIMESTAMP("' . $condition[2][1] . '")';
                                        break;
                                    case 'like':
                                        $fields = is_int(strpos($condition[0], '|')) ? explode('|', $condition[0]) : [$condition[0]];
                                        $likewhere = [];
                                        foreach($fields as $field)
                                        {
                                            if($field === 'm_nickname') $field = 'b.`m_nickname`';
                                            else $field = 'a.`' . $field . '`';
                                            $likewhere[] = $field . ' LIKE "' . $condition[2] . '"';
                                        }
                                        $wheres[] = count($likewhere) > 1 ? '(' . implode(' OR ', $likewhere) . ')' : $likewhere[0];
                                        break;
                                }
                            }
                        }
                        $sql .= ' WHERE ' . implode(' AND ', $wheres);
                    }

                    $sql .= empty($qrycache['order']) ? ' ORDER BY ' . str_replace(['l_time', 'asc', 'desc'], ['a.`l_time`', 'ASC', 'DESC'], $qrycache['order']) : ' ORDER BY a.`l_time` DESC';
                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $totalrows = $res -> num_rows;
                        if($totalrows > 0)
                        {
                            defined('NOW') || define('NOW', time());
                            if($totalrows > 1e5)
                            {
                                $res -> free();
                                $totalpg = ceil($totalrows / 1e5);
                                $zipfile = new \ZipArchive;
                                $_filename = 'user_logs_' . date('YmdHis', NOW);
                                if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
                                {
                                    ini_set('memory_limit', '512M');
                                    ini_set('max_execution_time', 0);
                                    for($pg = 0; $pg < $totalpg;)
                                    {
                                        $res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
                                        if($res && $res -> num_rows > 0)
                                        {
                                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                                            $res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $fp = fopen($filename, 'w');
                                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                                            fputcsv($fp, ['ID', '昵称', '金额', '类型', '备注', '操作时间']);
                                            array_map(function($item) use ($fp){
                                                fputcsv($fp, $item);
                                            }, $result);
                                            unset($result);
                                            fclose($fp);
                                            $zipfile -> addFile($filename);
                                        }
                                    }
                                    $mysql -> close();
                                    $zipfile -> close();
                                    $filename = $_filename . '.zip';
                                    // ob_end_clean();
                                    // ob_start();
                                    header('Content-Type: application/zip');
                                    header('Content-Disposition: filename=' . $filename);
                                    $fsize = filesize($filename);
                                    header('Content-Length: ' . $fsize);
                                    exit(file_get_contents($filename));
                                    $fp = fopen($filename, 'r');
                                    $outp = fopen('php://output', 'w');
                                    do
                                    {
                                        $_len = $fsize > 2e3 ? 2e3 : $fsize;
                                        fwrite($outp, fread($fp, $_len), $_len);
                                        ob_flush();
                                        flush();
                                        $_len -= 2e3;
                                    }
                                    while($fsize > 2e3);
                                    fclose($fp);
                                    // unlink($filename);
                                    fclose($outp);
                                    ob_end_clean();
                                    exit;
                                }
                            }
                            else
                            {
                                $result = $res -> fetch_all(MYSQLI_ASSOC);
                                $res -> free();
                                $mysql -> close();
                                // 导出csv文件
                                $filename = 'user_logs_' . date('YmdHis');
                                return export_csv($filename . '.csv', ['ID', '昵称', '金额', '类型'], $result);
                            }
                        }
                    }
                    $mysql -> close();
                }
            }
        }
    }

    // 积分释放记录导出
    public function ulogs_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');
                    $sql = 'SELECT l.`id`, u.`m_nickname`, CONCAT("'."\t".'", u.`m_phone`) AS `m_phone`, l.`u_date`, l.`u_push_num`, l.`u_team_num`, l.`u_jt_num`, l.`u_dt_num`, l.`u_total`, FROM_UNIXTIME(l.`u_addtime`) AS `u_adddate`, IF(l.`u_status` = 1, "已完成", "未完成") AS `u_status` FROM `w_ulogs` l LEFT JOIN `w_user` u ON u.`id` = l.`uid`';
                    if(!empty($qrycache['where']) && is_array($qrycache['where']))
                    {
                        $where = $qrycache['where'];
                        $wheres = [];
                        foreach($where as $condition)
                        {
                            if(is_array($condition) && count($condition) === 3)
                            {
                                switch($condition[1])
                                {
                                    case 'between time':
                                        $wheres[] = 'l.`' . $condition[0] . '` > UNIX_TIMESTAMP("' . $condition[2][0] . '") AND l.`' . $condition[0] . '` < UNIX_TIMESTAMP("' . $condition[2][1] . '")';
                                        break;
                                    case 'like':
                                        $fields = is_int(strpos($condition[0], '|')) ? explode('|', $condition[0]) : [$condition[0]];
                                        $likewhere = [];
                                        foreach($fields as $field)
                                        {
                                            // if($field === 'm_nickname') $field = 'u.`m_nickname`';
                                            if($field === 'l.id') $field = 'l.`id`';
                                            else $field = 'u.`' . $field . '`';
                                            $likewhere[] = $field . ' LIKE "' . $condition[2] . '"';
                                        }
                                        $wheres[] = count($likewhere) > 1 ? '(' . implode(' OR ', $likewhere) . ')' : $likewhere[0];
                                        break;
                                }
                            }
                        }
                        $sql .= ' WHERE ' . implode(' AND ', $wheres);
                    }

                    $sql .= empty($qrycache['order']) ? ' ORDER BY ' . str_replace(['u_addtime', 'asc', 'desc'], ['l.`u_addtime`', 'ASC', 'DESC'], $qrycache['order']) : ' ORDER BY l.`u_addtime` DESC';

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        if($res -> num_rows > 0)
                        {
                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                            $res -> free();

                            ob_end_clean();
                            ob_start();
                            header('Content-Type: text/csv');
                            header('Content-Disposition: filename=积分释放记录_' . date('YmdHis') . '.csv');
                            $fp = fopen('php://output', 'w');
                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                            fputcsv($fp, ['ID', '用户名', '用户手机号', '释放日期', '直推人数', '团队人数', '静态释放数量', '动态释放数量', '释放总数', '释放时间']);
                            $size = 0;
                            array_map(function($item) use ($fp, $size){
                                fputcsv($fp, $item);
                                if(ftell($fp) - $size > 1e3){
                                    $size += 1e3;
                                    ob_flush();
                                    flush();
                                }
                            }, $result);
                            unset($result);
                            ob_flush();
                            flush();
                            fclose($fp);
                        }
                    }
                    $mysql -> close();
                }
            }
        }
    }

    // 钱包操作记录导出
    public function wlogs_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');

                    $sql = 'SELECT w.`id`, u.`m_nickname`, CONCAT("'."\t".'", u.`m_phone`) AS `m_phone`, p.`p_title`, IF(w.`w_type` = 1, "买", "卖") AS `wtype`, w.`w_num`, w.`w_desc`, CASE w.`w_credit_type` WHEN 0 THEN "默认" WHEN 1 THEN "零售" WHEN 2 THEN "批发" WHEN 3 THEN "奖励" ELSE "其它" END AS `w_credit_type`, FROM_UNIXTIME(w.`w_addtime`) AS `w_addtime` FROM `w_wlogs` w LEFT JOIN `w_user` u ON w.`uid` = u.`id` LEFT JOIN `w_product` p ON w.`pid` = p.`id`';
                    if(!empty($qrycache['where']) && is_array($qrycache['where']))
                    {
                        $where = $qrycache['where'];
                        $wheres = [];
                        foreach($where as $condition)
                        {
                            if(is_array($condition) && count($condition) === 3)
                            {
                                switch($condition[1])
                                {
                                    case 'between time':
                                        $wheres[] = 'w.`' . $condition[0] . '` > UNIX_TIMESTAMP("' . $condition[2][0] . '") AND w.`' . $condition[0] . '` < UNIX_TIMESTAMP("' . $condition[2][1] . '")';
                                        break;
                                    case 'like':
                                        $fields = is_int(strpos($condition[0], '|')) ? explode('|', $condition[0]) : [$condition[0]];
                                        $likewhere = [];
                                        foreach($fields as $field)
                                        {
                                            if(is_int(strpos($field, '.')))
                                            {
                                                $_arr = explode('.', $field);
                                                $field = $_arr[0] . '.`' . $_arr[1] . '`';
                                            }

                                            $likewhere[] = $field . ' LIKE "' . $condition[2] . '"';
                                        }
                                        $wheres[] = count($likewhere) > 1 ? '(' . implode(' OR ', $likewhere) . ')' : $likewhere[0];
                                        break;
                                }
                            }
                        }
                        $sql .= ' WHERE ' . implode(' AND ', $wheres);
                    }

                    $sql .= empty($qrycache['order']) ? ' ORDER BY ' . str_replace(['w_addtime', 'asc', 'desc'], ['w.`w_addtime`', 'ASC', 'DESC'], $qrycache['order']) : ' ORDER BY w.`w_addtime` DESC';

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        if($res -> num_rows > 0)
                        {
                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                            $res -> free();

                            ob_end_clean();
                            ob_start();
                            header('Content-Type: text/csv');
                            header('Content-Disposition: filename=钱包操作记录_' . date('YmdHis') . '.csv');
                            $fp = fopen('php://output', 'w');
                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                            fputcsv($fp, ['ID', '用户名', '用户手机号', '产品名称', '操作方式', '操作数量', '操作说明', '种类', '操作时间']);
                            $size = 0;
                            array_map(function($item) use ($fp, $size){
                                fputcsv($fp, $item);
                                if(ftell($fp) - $size > 1e3){
                                    $size += 1e3;
                                    ob_flush();
                                    flush();
                                }
                            }, $result);
                            unset($result);
                            ob_flush();
                            flush();
                            fclose($fp);
                        }
                    }
                    $mysql -> close();
                }
            }
        }
    }

    // 入金记录导出
    public function zlogs_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $data_list  = Db::name('zlog') -> alias('z')
                        -> join('user u','z.uid=u.id','LEFT')
                        // -> field('z.*, u.m_nickname, u.m_phone')
                        -> field([
                            'z.id',
                            'u.m_nickname',
                            DB::raw('CONCAT("' . "\t" . '", u.`m_phone`) AS `m_phone`'),
                            // 'z.z_account',
                            DB::raw('CONCAT("' . "\t" .'", z.`z_account`) AS `z_account`, IF(z.`z_type` = 2, "出金", "入金") AS `z_type`'),
                            'z.z_price',
                            'z.z_fee',
                            'z.z_fee_price',
                            'z.z_num',
                            DB::raw('CONCAT("' . "\t" .'", z.`z_bank_id`) AS `z_bank_id`, CONCAT("' . "\t" .'", z.`z_num`) AS `z_num`, IF(z.`z_desc` IS NULL, "", z.`z_desc`) AS `z_desc`, IF(z.`z_status` = 1, "成功", IF(z.`z_status` = 0, "发起", "异常")) AS `z_status`, FROM_UNIXTIME(z.`z_addtime`) AS `z_adddate`, FROM_UNIXTIME(z.`last_time`) AS `last_date`, FROM_UNIXTIME(z.`z_overtime`) AS `z_overdate`'),
                        ])
                        -> where(array('z_type' => 1));
                    if(isset($qrycache['where']) && !empty($qrycache['where'])) $data_list -> where($qrycache['where']);
                    if(isset($qrycache['order']) && !empty($qrycache['order'])) $data_list -> order($qrycache['order']);

                    $result = $data_list -> select();
                    // var_dump($data_list);

                    ob_end_clean();
                    ob_start();
                    header('Content-Type: text/csv');
                    header('Content-Disposition: filename=入金记录_' . date('YmdHis') . '.csv');
                    $fp = fopen('php://output', 'w');
                    fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                    fputcsv($fp, ['ID', '用户名', '用户手机号', '账号', '操作方式', '金额', '手续费比例', '手续费额度', '出入金银行卡', '交易流水号', '出入金描述', '状态', '操作时间', '更新时间', '完成时间']);
                    $size = 0;
                    array_map(function($item) use ($fp, $size){
                        fputcsv($fp, $item);
                        if(ftell($fp) - $size > 1e3){
                            $size += 1e3;
                            ob_flush();
                            flush();
                        }
                    }, $result);
                    unset($result);
                    ob_flush();
                    flush();
                    fclose($fp);
                }
            }
        }
    }


    // 出金列表导出
    public function zlog2_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);
                    
                    $data_list  = Db::name('zlog')->alias('z')->join('user u','z.uid=u.id','LEFT')->where($qrycache['where'])->where(array('z_type'=>2))->order($qrycache['order'])->field('z.*,u.m_nickname,u.m_phone')->select();

                    $titlArr = [
                        'ID', '用户名', '用户手机号', '账号', '操作方式', '金额', '手续费比例', '手续费额度', '出入金银行卡', '交易流水号', '出入金描述', '状态', '操作时间', '更新时间', '完成时间'
                    ];
                    
                    $dataArr = [];

                    $status_arr = [0=>'发起',1=>'成功',2=>'异常'];

                    foreach ($data_list as $k => $v){

                        $status_txt = !empty($status_arr[$v['z_status']])?$status_arr[$v['z_status']]:'异常';

                        $dataArr[] = [
                            'ID' => $v['id'],
                            '用户名' => $v['m_nickname'],
                            '用户手机号' => filter_value($v['m_phone']),
                            '账号' => filter_value($v['z_account']),
                            '操作方式' => ($v['z_type']==1)?'入金':'出金',
                            '金额' => $v['z_price'],
                            '手续费比例' => $v['z_fee'],
                            '手续费额度' => $v['z_fee_price'],
                            '出入金银行卡' => filter_value($v['z_bank_id']),
                            '交易流水号' => filter_value($v['z_num']),
                            '出入金描述' => $v['z_desc'],
                            '状态' => $status_txt,
                            '操作时间' => date('Y-m-d H:i:s', $v['z_addtime']),
                            '更新时间' => date('Y-m-d H:i:s', $v['last_time']),
                            '完成时间' => date('Y-m-d H:i:s', $v['z_overtime']),
                        ];

                    }


                    $filename = '出金记录-' . date('YmdHis');
                    return export_csv($filename . '.csv', $titlArr, $dataArr);

                }
            }
        }
    }



}