<?php
namespace app\admin\controller;

use think\Db;
use app\common\builder\ZBuilder;
use app\admin\model\Financialvoucher as FVModel;

class Financialvoucher extends Admin
{

    // 凭证列表
    public function index()
    {

        $map = $this->getMap();

        $order = $this->getOrder('id desc');

        $data_list = FVModel::where($map)->where('isdel=0')->order($order)->limit(20)->paginate();

        $voucher_class_list = [1 => '出金凭证', 2 => '入金凭证', 3 => '批发票凭证', 4 => '奖励票凭证', 5 => '零售票持仓凭证', 6 => '批发票持仓凭证', 7 => '奖励票持仓凭证', 8 => '提货积分凭证', 9 => '空单凭证', 10 => '不扣手续费买单', 11 => '不扣手续费卖单', 12 => '扣手续费买单', 13 => '扣手续费卖单',14=>'可兑换凭证',15=>'已兑换凭证'];

        $status_text = ['0' => '未审核', '1' => '审核通过', '2' => '审核未通过'];

        $btn_auditing  = [
            'title' => '审核',
            'icon'  => 'fa fa-fw fa-resistance',
            'href'  => url('status_auditing', ['id' => '__id__'])
        ];

        $admin_ids = Db::name('admin_user')->field('id, nickname')->select();

        $admin_users = [];

        foreach($admin_ids as $k => $v){
            $admin_users[$v['id']] = $v['nickname'];
        }

        foreach($data_list as &$vv){
            $vv['admin_nick_name'] = Db::name('admin_user')->where('id', $vv['add_admin_id'])->value('nickname');
        }

        return ZBuilder::make('table')
            ->setTableName('financial_voucher')
            ->addTopSelect('voucher_class', '凭证类型', $voucher_class_list)
            ->addTopSelect('status', '审核状态', $status_text)
            ->addTopSelect('add_admin_id', '操作员', $admin_users)
            ->setSearch(['voucher_code' => '凭证编号', 'user_account' => '会员账号'])
            ->addTimeFilter('add_time', '', '开始时间,结束时间')
            ->addColumns([
                // ['id', 'ID'],
                ['voucher_code', '凭证编号', 'text'],
                ['voucher_class', '凭证类型', 'status', '', $voucher_class_list],
                ['user_account', '入账账号', 'text'],
                ['from_user_account', '交易账号', 'text'],
                ['number', '金额数量', 'text'],
                ['remarks', '备注', 'text'],
                ['admin_nick_name', '操作员', 'text'],
                ['add_time', '发起时间', 'datetime'],
                ['status', '审核状态', 'status', '', $status_text],
                ['right_button', '操作']
            ])
            ->hideCheckbox()
            ->setColumnWidth(['remarks'=>200])
            ->addRightButton('auditing', $btn_auditing, ['area' => ['350px', '450px']])
            ->replaceRightButton(['status'=>['<>',0]], '', 'auditing')
            ->setRowList($data_list)
            ->fetch();
    }


    // 上传凭证
    public function add()
    {

        $product_list = Db::name('product')->field('id, p_title, p_code')->select();

        $admin_id   = session('user_auth')['uid'];

        if($this->request->post()){

            $param = $this->request->post();

            if(empty($param['voucher_class'])) $this->error('请选择凭证分类');
            $voucher_class_list = [1,2,3,4,5,6,7,8];
            if(!in_array($param['voucher_class'], $voucher_class_list)){
                $this->error('凭证分类不正确');
            }
            if(empty($param['voucher_code'])) $this->error('请重新选择凭证分类');

            // 判断提交的凭证分类编号是否存在
            $voucher_code = Db::name('financial_voucher')->where('voucher_code', $param['voucher_code'])->find();
            if($voucher_code){
                $this->error('凭证编号已存在，请刷新后重试');
            }
            if(empty($param['product_id'])) $this->error('请选择分类');
            if(empty($param['number'])) $this->error('请填写数量');
            if(empty($param['user_account'])) $this->error('请填写会员账号');
            if($param['voucher_class'] == 3){
                if($param['number']%2){
                    $this->error('数量必须是2的倍数');
                }
            }
            if($param['voucher_class'] == 8){
                if($param['number']%186){
                    $this->error('数量必须是186的倍数');
                }
            }
            $user_info = Db::name('user')->where('m_account', $param['user_account'])->find();
            if(!$user_info){
                $this->error('填写的会员账号不存在');
            }

            $wallet_where = [
                'uid' => $user_info['id'],
                'pid' => $param['product_id']
            ];

            $user_where = [
                'id' => $user_info['id']
            ];

            if($param['voucher_class'] == 5 || $param['voucher_class'] == 6 || $param['voucher_class'] == 7){
                $wallet_info = Db::name('wallet')->where($wallet_where)->find();
                if(!$wallet_info){
                    $this->error('该用户不存在钱包信息');
                }
            }

            $param['remarks'] = htmlspecialchars_decode($param['remarks']); // 备注
            $param['user_id'] = $user_info['id']; // 添加的会员账号的会员id
            $param['add_time'] = time(); // 添加时间
            $param['status'] = 0; // 状态 0未审核 1审核通过 2审核不通过
            $param['add_admin_id'] = $admin_id; // 当前用户id

            $res = Db::name('financial_voucher')->insert($param);

            if($res){
                $this->success('操作成功');
            }else{
                $this->error('操作失败');
            }
        }

        $this->assign('product_list', $product_list);


        return $this->fetch();
    }

    // 获取凭证分类编号
    public function get_voucher_code()
    {
        if($this->request->post()){

            $param = $this->request->post();

            if(empty($param['voucher_class'])){
                $this->error('请先选择凭证分类');
            }

            $voucher_class_list = [1,2,3,4,5,6,7,8,9,10,11,12,13];

            if(!in_array($param['voucher_class'], $voucher_class_list)){
                $this->error('凭证分类不正确');
            }

            $voucher_class = create_voucher_code($param['voucher_class']);

            $this->success($voucher_class);
                
        }
    }

    // 失去焦点根据会员账号获取用户信息
    public function get_user_info()
    {
        if($this->request->post()){

            $param = $this->request->post();

            if(empty($param['user_account'])){
                $this->error('请填写会员账号');
            }

            $user_info = Db::name('user')->where('m_account', $param['user_account'])->field('id, m_account, m_nickname, m_name')->find();

            if($user_info){
                $this->success('存在会员', '', $user_info);
            }else{
                $this->error('不存在该会员账号用户');
            }
        }

    }

    // 审核状态
    public function status_auditing()
    {

        $param = $this->request->param();

        $admin_id = session('user_auth')['uid'];

        $data = Db::name('financial_voucher')->where('id', $param['id'])->find(); // 当前的凭证信息

        if($this->request->post()){

            $re_param = $this->request->post();

            if($data['status'] != 0){
                $this->error('已经审核，不能重复审核');
            }

            $time = time();

            $up_data = [
                'status' => $re_param['status'], // 修改状态
                'edit_time' => $time, // 最后修改时间
                'auditing_admin_id' => $admin_id // 当前的用户id
            ];

            // 如果是审核通过
            if($re_param['status'] == 1){
                $user_id = $data['user_id']; // 用户id

                $user_info = Db::name('user')->where('id', $user_id)->find(); // 用户信息
                if(!$user_info){
                    $this->error('用户不存在');
                }

                $user_where = [
                    'id' => $data['user_id']
                ];
    
                $number = $data['number']; // 数量

                $product_id = $data['product_id']; // 商品id

                $wallet_where = [
                    'uid' => $user_id,
                    'pid' => $product_id,
                ];
                // 操作会员的余额信息
                switch ($data['voucher_class']) {
                    case '1':
                        // 出金凭证
                        Db::name('user')->where($user_where)->setDec('m_balance', $number);
                        $l_info = '系统操作凭证减少金额';
                        $l_type = 1;
                        $number = -$number;
                        break;

                    case '2':
                        // 入金凭证
                        Db::name('user')->where($user_where)->setInc('m_balance', $number);
                        $l_info = '系统操作凭证增加金额';
                        $l_type = 1;
                        break;

                    case '3':
                        // 批发票凭证
                        Db::name('wallet')->where($wallet_where)->setInc('m_pifa', $number);
                        $l_info = '系统操作凭证增加批发金额';
                        $l_type = 1;
                        break;

                    case '4':
                        // 奖励票凭证
                        Db::name('wallet')->where($wallet_where)->setInc('m_jiangli', $number);
                        $l_info = '系统操作凭证增加奖励金额';
                        $l_type = 1;
                        break;

                    case '5':
                        // 零售票持仓凭证
                        Db::name('wallet')->where($wallet_where)->setInc('m_credit_1', $number);
                        $w_desc = '系统操作凭证增加零售余额';
                        $w_credit_type = 1;
                        break; 

                    case '6':
                        // 批发票持仓凭证
                        Db::name('wallet')->where($wallet_where)->setInc('m_credit_2', $number);
                        $w_desc = '系统操作凭证增加批发余额';
                        $w_credit_type = 2;
                        break;

                    case '7':
                        // 奖励票持仓凭证
                        Db::name('wallet')->where($wallet_where)->setInc('m_credit_3', $number);
                        $w_desc = '系统操作凭证增加奖励余额';
                        $w_credit_type = 3;
                        break;

                    case '8':
                        // 提货积分凭证
                        Db::name('user')->where($user_where)->setInc('m_integral', $number);
                        $l_type = 2; //2积分
                        $l_info = '系统操作凭证增加积分';
                        break;

                    case '9':
                        // 审核空单
                        $deal_data = [
                            'd_num' =>  $number,
                            'd_sell_num' => $data['deal_num'],
                            'is_Fee' => 2,
                            'd_date' => date('Y-m-d')
                        ];
                        Db::name('deal')->where('id', $data['deal_id'])->update($deal_data);
                        $l_type = 0;
                        $l_info = '审核操作空单凭证';
                        break;
                    
                    case '10':
                        // 不扣手续费买单
                        $_fee = 0;
                        if($data['deal_type'] == 1){
                            $_f_price = 186;
                        }elseif($data['deal_type'] == 2){
                            $_f_price = 36;
                        }elseif($data['deal_type'] == 3){
                            $_f_price = 36;
                        }

                        $amounts = ($number*$_f_price) + $_fee; // 扣除的用户余额
                        if($user_info['m_balance'] < $amounts){
                            $this->error('用户余额不足');
                            return;
                        }

                        $deal_data = [
                            'uid' => $user_id,
                            'pid' => $data['product_id'],
                            'sid' => $data['product_id'],
                            'd_code' => randomkeys(8),
                            'd_credit_1' => ($data['deal_type'] == 1)?$number:0,
                            'd_credit_2' => ($data['deal_type'] == 2)?$number:0,
                            'd_credit_3' => ($data['deal_type'] == 3)?$number:0,
                            'd_total' => $number,
                            'd_num' => $number,
                            'd_type' => 1,
                            'd_fee' => $_fee,
                            'd_addtime' => $time,
                            'd_price' => $_f_price,
                            'd_date' => date('Y-m-d', $time),
                            'd_status' => 10,
                            'is_Fee' => 2
                        ];
                        $insert_deal_id = Db::name('deal')->insertGetId($deal_data);
                        
                        $fine_data = [
                            'bid' => $user_id,
                            'did' => $insert_deal_id,
                            'pid' => $data['product_id'],
                            'sid' => 0,
                            'f_type' => 0,
                            'f_status' => 7,
                            'f_price' => $_f_price,
                            'f_profit_start_time' => 0,
                            'f_profit_end_time' => 0,
                            'f_addtime' => $time,
                            'f_date' => date('Y-m-d', $time),
                            'is_Fee' => 2
                        ];
                        // 插入子表
                        for ($i = 1; $i <= $number; $i++) {
                            $fine_data['f_code'] = randomkeys(8);
                            $insert_fine = Db::name('fine')->insert($fine_data);
                        }

                        
                        Db::name('user')->where('id', $user_id)->setDec('m_balance', $amounts);
                        $log_data = [
                            'l_uid' => $user_id,
                            'l_type' => 1,
                            'l_num' => -$amounts,
                            'l_info' => '系统操作，不扣手续费买单',
                            'l_time' => $time,
                            'admin_id' => $admin_id
                        ];
                        Db::name('logs')->insert($log_data);

                        break;

                    case '11':
                        // 不扣手续费卖单
                        $_fee = 0;

                        if($data['deal_type'] == 1){
                            $_f_price = 186;
                        }elseif($data['deal_type'] == 2){
                            $_f_price = 36;
                        }elseif($data['deal_type'] == 3){
                            $_f_price = 36;
                        }

                        $deal_data = [
                            'uid' => $user_id,
                            'pid' => $product_id,
                            'sid' => $product_id,
                            'd_code' => randomkeys(8),
                            'd_type' => 2,
                            'd_total' => $number,
                            'd_num' => $number,
                            'd_sell_num' => 0,
                            'd_addtime' => $time,
                            'd_status' => 1,
                            'last_time' => $time,
                            'd_grant' => -1,
                            'd_date' => date('Y-m-d', $time),
                            'is_Fee' => 2,
                            'd_sfee' => $_fee
                        ];
                    
                        $deal_id = Db::name('deal')->insertGetId($deal_data);
                        
                        $amounts = ($number*$_f_price)-$_fee;
                        Db::name('user')->where('id', $user_id)->setInc('m_balance', $amounts);
                        if($data['deal_type'] == 2){
                            Db::name('user')->where('id', $user_id)->setInc('m_integral', ($number*93));
                            Db::name('user')->where('id', $user_id)->setInc('m_balance', ($number*93));
                            $integral_log_data = [
                                'l_uid' => $user_id,
                                'l_type' => 2,
                                'l_num' => ($number*93),
                                'l_info' => '系统操作，不扣手续费卖单',
                                'l_time' => $time,
                                'admin_id' => $admin_id
                            ];
                            Db::name('logs')->insert($integral_log_data);
                            $balance_log_data = [
                                'l_uid' => $user_id,
                                'l_type' => 1,
                                'l_num' => ($number*93),
                                'l_info' => '系统操作，不扣手续费卖单',
                                'l_time' => $time,
                                'admin_id' => $admin_id
                            ];
                            Db::name('logs')->insert($balance_log_data);
                        }

                        $log_data = [
                            'l_uid' => $user_id,
                            'l_type' => 1,
                            'l_num' => $amounts,
                            'l_info' => '系统操作，不扣手续费卖单',
                            'l_time' => $time,
                            'admin_id' => $admin_id
                        ];
                        Db::name('logs')->insert($log_data);

                        break;

                    case '12':
                        // 扣手续费买单
                        $_fee = $number*0.55;

                        $amounts = ($number*$_f_price) + $_fee;

                        if($data['deal_type'] == 1){
                            $_f_price = 186;
                        }elseif($data['deal_type'] == 2){
                            $_f_price = 36;
                        }elseif($data['deal_type'] == 3){
                            $_f_price = 36;
                        }

                        if($user_info['m_balance'] < $amounts){
                            $this->error('用户余额不足');
                            return;
                        }

                        $deal_data = [
                            'uid' => $user_id,
                            'pid' => $data['product_id'],
                            'sid' => $data['product_id'],
                            'd_code' => randomkeys(8),
                            'd_credit_1' => ($data['deal_type'] == 1)?$number:0,
                            'd_credit_2' => ($data['deal_type'] == 2)?$number:0,
                            'd_credit_3' => ($data['deal_type'] == 3)?$number:0,
                            'd_total' => $number,
                            'd_num' => $number,
                            'd_type' => 1,
                            'd_fee' => $_fee,
                            'd_addtime' => $time,
                            'd_price' => '',
                            'd_date' => date('Y-m-d', $time),
                            'd_status' => 10,
                            'is_Fee' => 1
                        ];
                        $insert_deal_id = Db::name('deal')->insertGetId($deal_data);
                        
                        $fine_data = [
                            'bid' => $user_id,
                            'did' => $insert_deal_id,
                            'pid' => $data['product_id'],
                            'sid' => 0,
                            'f_type' => 0,
                            'f_status' => 7,
                            'f_price' => $_f_price,
                            'f_profit_start_time' => 0,
                            'f_profit_end_time' => 0,
                            'f_addtime' => $time,
                            'f_date' => date('Y-m-d', $time),
                            'is_Fee' => 1
                        ];
                        // 插入子表
                        for ($i = 1; $i <= $number; $i++) {
                            $fine_data['f_code'] = randomkeys(8);
                            $insert_fine = Db::name('fine')->insert($fine_data);
                        }

                        Db::name('user')->where('id', $user_id)->setDec('m_balance', $amounts);

                        $log_data = [
                            'l_uid' => $user_id,
                            'l_type' => 1,
                            'l_num' => -$amounts,
                            'l_info' => '系统操作，扣手续费买单',
                            'l_time' => $time,
                            'admin_id' => $admin_id
                        ];
                        Db::name('logs')->insert($log_data);

                        break;

                    case '13':
                        // 扣手续费卖单
                        $_fee = $number*0.55;

                        if($data['deal_type'] == 1){
                            $_f_price = 186;
                        }elseif($data['deal_type'] == 2){
                            $_f_price = 36;
                        }elseif($data['deal_type'] == 3){
                            $_f_price = 36;
                        }

                        $deal_data = [
                            'uid' => $user_id,
                            'pid' => $product_id,
                            'sid' => $product_id,
                            'd_code' => randomkeys(8),
                            'd_type' => 2,
                            'd_total' => $number,
                            'd_num' => $number,
                            'd_sell_num' => 0,
                            'd_addtime' => $time,
                            'd_status' => 1,
                            'last_time' => $time,
                            'd_grant' => -1,
                            'd_date' => date('Y-m-d', $time),
                            'is_Fee' => 1,
                            'd_sfee' => $_fee
                        ];
                    
                        $deal_id = Db::name('deal')->insertGetId($deal_data);
                        
                        $amounts = ($number*$_f_price)-$_fee;
                        Db::name('user')->where('id', $user_id)->setInc('m_balance', $amounts);
                        if($data['deal_type'] == 2){
                            Db::name('user')->where('id', $user_id)->setInc('m_integral', ($number*93));
                            Db::name('user')->where('id', $user_id)->setInc('m_balance', ($number*93));
                            $integral_log_data = [
                                'l_uid' => $user_id,
                                'l_type' => 2,
                                'l_num' => ($number*93),
                                'l_info' => '系统操作，扣手续费卖单',
                                'l_time' => $time,
                                'admin_id' => $admin_id
                            ];
                            Db::name('logs')->insert($integral_log_data);
                            $balance_log_data = [
                                'l_uid' => $user_id,
                                'l_type' => 1,
                                'l_num' => ($number*93),
                                'l_info' => '系统操作，扣手续费卖单',
                                'l_time' => $time,
                                'admin_id' => $admin_id
                            ];
                            Db::name('logs')->insert($balance_log_data);
                        }
                        $log_data = [
                            'l_uid' => $user_id,
                            'l_type' => 1,
                            'l_num' => $amounts,
                            'l_info' => '系统操作，扣手续费卖单',
                            'l_time' => $time,
                            'admin_id' => $admin_id
                        ];
                        Db::name('logs')->insert($log_data);

                        break;

                    default:
                        // $this->error('凭证分类不正确');
                        break;
                }

                // wallet 的日志
                if(in_array($data['voucher_class'], [5, 6, 7])){
                    $log_data = [
                        'uid' => $user_id,
                        'pid' => $data['product_id'],
                        'w_num' => $number,
                        'w_desc' => $w_desc,
                        'w_type' => 1, // 1买 2卖
                        'w_credit_type' => $w_credit_type, // 1零售 2批发 3奖励
                        'w_addtime' => $time,
                    ];
                    Db::name('wlogs')->insert($log_data);
                }elseif(in_array($data['voucher_class'], [1, 2, 3, 4, 8])){

                    $log_data = [
                        'l_uid' => $user_id,
                        'l_type' => $l_type,
                        'l_num' => $number,
                        'l_info' => $l_info,
                        'l_time' => $time,
                        'admin_id' => $admin_id
                    ];
                    Db::name('logs')->insert($log_data);
                }elseif(in_array($data['voucher_class'], [10, 11, 12, 13])){
                    // 不扣手续费买单 不扣手续费卖单 扣手续费买单 扣手续费卖单

                }else{
                    // 空单审核操作日志

                }
            }

            $result = Db::name('financial_voucher')->where('id', $data['id'])->update($up_data);

            if($result){
                $this->success('操作成功', url('index'), '_parent_reload');
            }else{
                $this->error('操作失败');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('审核')
            ->addFormItems([
                ['hidden', 'id'],
                ['radio', 'status', '状态', '', ['2' => '不通过', '1' => '审核通过']]
            ])
            ->setFormData($data)
            ->fetch();
    }

    // 空单凭证
    public function empty_financial()
    {

        $admin_id = session('user_auth')['uid'];

        if($this->request->post()){

            $param = $this->request->post();

            if(empty($param['voucher_code'])) $this->error('请先获取凭证编号');
            if(empty($param['deal_id'])) $this->error('请先填写单号');
            if(empty($param['user_id'])) $this->error('请先填写单号');
            if(!isset($param['total_num'])) $this->error('请先填写单号');
            if(!isset($param['deal_num'])) $this->error('请先填写单号');
            if(empty($param['deal_type'])) $this->error('请先填写单号');
            // if(empty($param['voucher_class'])) $this->error('请先填写单号');

            if($param['total_num'] == $param['deal_num']){
                $this->error('挂单数和成交数相同,不能提交');
            }
            // if(!in_array($param['voucher_class'], [5, 6, 7])){
            //     $this->error('提交失败，请刷新后重试');
            // }

            $new_data = [
                'voucher_class' => 9,
                'voucher_code' => $param['voucher_code'],
                'user_account' => Db::name('user')->where('id', $param['user_id'])->value('m_account'),
                'user_id' => $param['user_id'],
                'number' => $param['total_num']-$param['deal_num'],
                'deal_id' => $param['deal_id'],
                'deal_num' => $param['deal_num'],
                'remarks' => htmlspecialchars_decode($param['remarks']),
                'add_time' => time(),
                'add_admin_id' => $admin_id,
                'status' => 0,
            ];

            $result = Db::name('financial_voucher')->insert($new_data);
            if($result){
                $this->success('操作成功');
            }else{
                $this->error('操作失败');
            }

        }

        $voucher_code = create_voucher_code(9);


        $this->assign('voucher_code', $voucher_code);

        return $this->fetch();
    }

    // 获取单号详情
    public function get_deal_info()
    {
        if($this->request->post()){

            $param = $this->request->post();

            if(empty($param['deal_id'])){
                $this->error('请先填写单号');
            }
            $id = $param['deal_id'];
            $deal_info = Db::name('deal')->where('id', $id)->find();

            if(empty($deal_info)){
                $this->error('无所填单号信息');
            }
            
            if($deal_info['d_type'] == 1){
                // 买单
                $count = Db::name('fine')->where('did', $deal_info['id'])->count();
            }elseif($deal_info['d_type'] == 2){
                // 卖单
                $count = Db::name('fine')->where('sdid', $deal_info['id'])->count();
            }else{
                $this->error('单号类型错误');
            }

            if($deal_info['d_credit_1'] > 0){
                $deal_type = '零售票';
                $voucher_class = 5;
            }elseif($deal_info['d_credit_2'] > 0){
                $deal_type = '批发票';
                $voucher_class = 6;
            }elseif($deal_info['d_credit_3'] > 0){
                $deal_type = '特价票';
                $voucher_class = 7;
            }else{
                $this->error('单号类型错误');
            }

            $data = [
                'user_id' => $deal_info['uid'],
                'deal_type' => $deal_type,
                'voucher_class' => $voucher_class,
                'total_num' => $deal_info['d_total'],
                'deal_num' => $count
            ];

            $this->success('获取成功', '', $data);
        }
    }

    // 订单凭证
    public function order_financial()
    {

        $product_list = Db::name('product')->field('id, p_title, p_code')->select();

        $admin_id   = session('user_auth')['uid'];

        if($this->request->post()){

            $param = $this->request->post();

            $_voucher_class = [10, 11, 12, 13];

            if(empty($param['voucher_class'])) $this->error('请先选择凭证分类');
            if(!in_array($param['voucher_class'], $_voucher_class)){
                $this->error('凭证分类不正确');
            }

            if(empty($param['voucher_code'])) $this->error('请先选择凭证分类');
            if(empty($param['user_account'])) $this->error('请填写会员账号');
            if(empty($param['user_id'])) $this->error('请填写会员账号');
            if(empty($param['number'])) $this->error('请填写数量');
            if(empty($param['deal_type'])) $this->error('请选择交易类型');
            if(empty($param['product_id'])) $this->error('请选择产品');

            $_preg = '/^(?:[1-9]\d*(?:\.\d+)?|0\.\d*[1-9]\d*)$/';
            if(!preg_match($_preg, $param['number'])){
                $this->error('数量格式不正确');
            }

            $param['remarks'] = htmlspecialchars_decode($param['remarks']);
            $param['add_time'] = time();
            $param['add_admin_id'] = $admin_id;
            $param['status'] = 0;

            $result = Db::name('financial_voucher')->insert($param);
            if($result){
                $this->success('操作成功');
            }else{
                $this->error('操作失败');
            }
        }

        $this->assign('product_list', $product_list);

        return $this->fetch();
    }




}