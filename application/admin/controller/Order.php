<?php



namespace app\admin\controller;



use app\admin\model\Deal;
use app\admin\model\DealHistory;
use app\admin\model\Fine;

use app\common\builder\ZBuilder;

use think\Db;



class Order extends Admin

{

    //商品订单

    public function order_list()

    {

        do_alogs('查看订单列表');

        $map = $this->getMap();

        $order = $this->getOrder('o_addtime desc');

        $express = Db::name('Express')->select();

        $arr = array();

        foreach ($express as $k => $v) {

            $arr[$v['id']] = $v['name'];

        }

        $time = time();
        if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 所有商品
        $_goods_list = Db::name('goods')->where('g_del', 0)->select();

        $goods_list = [];
        foreach($_goods_list as $k => $v){
            $goods_list[$v['id']] = $v['g_title'];
        }


        $data_list = Db::name('Order a')

            ->join('w_user b', 'b.id=a.uid')

            ->join('w_goods c', 'c.id=a.gid')

            ->join('w_address d', 'd.id=a.aid')

            ->where($map)

            ->where('a.o_type','<>',7)

            ->field(

            'a.*,b.m_nickname,b.m_phone,b.m_account,c.g_type,c.g_title,c.g_pic,d.a_name,d.a_phone,d.a_city,d.a_detail')

            ->order($order)

            ->limit(20)

            ->paginate();

        $css = "<style>

            tr,th,td{text-align: center;}

		</style>";

        $status = ['待付款', '待发货', '待收货', '已完成'];

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('order')

            ->setSearch(['a.id' => 'ID', 'a.o_code' => '订单编号', 'c.g_title' => '商品名称','b.m_account' => '交易账号', 'b.m_nickname' => '用户昵称']) // 设置搜索参数

            ->addTimeFilter('o_addtime', '', '开始时间,结束时间') // 添加时间段筛选

            ->addTopSelect('o_status', '', $status)
            ->addTopSelect('gid', '', $goods_list)

            ->addColumns([                                                                // 批量添加数据列

                ['id', 'ID'],

                ['o_code', '订单编号', 'text'],

                ['m_nickname', '昵称', 'text'],

                ['m_phone', '手机号', 'text'],

                ['m_account', '交易账号', 'text'],

                ['o_status', '订单状态', 'status', '', $status],

                ['g_title', '商品名称', 'text'],

                ['o_price', '单价', 'text'],

                ['o_credit', '积分单价', 'text'],

                ['o_buy_num', '订单数量', 'text'],

                ['o_credit_1', '订单总价', 'text'],

                ['o_credit_2', '积分总价', 'text'],

                ['g_pic', '商品图片', 'img_urls'],

                ['g_type', '商品类型', 'status', '', ['1' => '购物专区', '2' => '积分专区']],

                ['a_name', '收货人', 'text'],

                ['a_phone', '收货电话', 'text'],

                ['a_city', '收货城市', 'text'],

                ['a_detail', '收货城市', 'text'],

                ['o_info', '订单备注', 'text'],

                ['o_addtime', '创建时间', 'datetime'],

                ['o_pay_time', '付款时间', 'datetime'],

                ['o_send_time', '发货时间', 'datetime'],

                ['o_take_time', '收货时间', 'datetime'],

                ['right_button', '操作', 'btn']

            ])

            ->setColumnWidth(['last_time' => 150, 'id' => 60])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addOrder('id,last_time,o_addtime')                                                               //添加排序

            ->addTopButton('delete', ['href' => url('order_del')])

            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('order_list_export', ['uniqkey' => $uniqkey])])

            ->addRightButton('edit', ['title' => '发货', 'icon' => 'fa fa-fw fa-truck', 'href' => url('order_set', ['id' => '__id__'])], ['area' => ['500px', '500px']])
            ->addTopButton('delete', ['title'=>'待付款', 'href'=>url('send_out_good_status', ['status'=>0]), 'class'=>'btn btn-default ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待发货', 'href'=>url('send_out_good_status', ['status'=>1]), 'class'=>'btn btn-primary ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待收货', 'href'=>url('send_out_good_status', ['status'=>2]), 'class'=>'btn btn-info ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'已完成', 'href'=>url('send_out_good_status', ['status'=>3]), 'class'=>'btn btn-success ajax-post', 'icon'=>''])

            ->addRightButtons(['delete' => ['title' => '删除', 'href' => url('order_del', ['id' => '__id__'])]])

            ->replaceRightButton(['o_status' => ['<>', 1]], '', 'edit')

            ->replaceRightButton(['o_status' => ['<>', 3]], '', 'delete')

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }





    //提货订单

    public function carry_list()

    {

        do_alogs('查看提货订单列表');

        $map = $this->getMap();

        $order = $this->getOrder('o_addtime desc');

        $express = Db::name('Express')->select();

        $arr = array();

        foreach ($express as $k => $v) {

            $arr[$v['id']] = $v['name'];

        }

        $data_list = Db::name('Order a')

            ->join('w_product p','p.id=a.gid','left')

            ->join('w_user b', 'b.id=a.uid')

            ->join('w_address d', 'd.id=a.aid')

            ->where($map)

            ->where('a.o_type','=',7)

            ->field(

                'a.*,b.m_nickname,b.m_phone,b.m_account,d.a_name,d.a_phone,d.a_city,d.a_detail,p.p_title,p.p_image')

            ->order($order)

            ->limit(20)

            ->paginate();

        $css = "<style>

            tr,th,td{text-align: center;}

		</style>";

        $status = ['待付款', '待发货', '待收货', '已完成'];

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('order')

            ->setSearch(['a.id' => 'ID', 'a.o_code' => '订单编号', 'c.g_title' => '商品名称', 'b.m_nickname' => '用户昵称']) // 设置搜索参数

            ->addTimeFilter('o_addtime', '', '开始时间,结束时间') // 添加时间段筛选

            ->addTopSelect('o_status', '', $status)

            ->addColumns([                                                                // 批量添加数据列

                ['id', 'ID'],

                ['o_code', '订单编号', 'text'],

                ['m_nickname', '昵称', 'text'],

                ['m_phone', '手机号', 'text'],

                ['m_account', '交易账号', 'text'],

                ['o_status', '订单状态', 'status', '', $status],

                ['p_title', '商品名称', 'text'],

                ['o_price', '单价', 'text'],

                ['o_credit', '积分单价', 'text'],

                ['o_buy_num', '订单数量', 'text'],

                ['o_credit_1', '订单总价', 'text'],

                ['o_credit_2', '积分总价', 'text'],

                ['p_image', '商品图片', 'img_urls'],

                ['a_name', '收货人', 'text'],

                ['a_phone', '收货电话', 'text'],

                ['a_city', '收货城市', 'text'],

                ['a_detail', '收货城市', 'text'],

                ['o_info', '订单备注', 'text'],

                ['o_addtime', '创建时间', 'datetime'],

                ['o_pay_time', '付款时间', 'datetime'],

                ['o_send_time', '发货时间', 'datetime'],

                ['o_take_time', '收货时间', 'datetime'],

                ['right_button', '操作', 'btn']

            ])

            ->setColumnWidth(['last_time' => 150, 'id' => 60])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addOrder('id,last_time,o_addtime')                                                               //添加排序

            ->addTopButton('delete', ['href' => url('order_del')])

            ->addRightButton('edit', ['title' => '发货', 'icon' => 'fa fa-fw fa-truck', 'href' => url('carry_set', ['id' => '__id__'])], ['area' => ['500px', '500px']])

            ->addRightButtons(['delete' => ['title' => '删除', 'href' => url('order_del', ['id' => '__id__'])]])

            ->replaceRightButton(['o_status' => ['<>', 1]], '', 'edit')

            ->replaceRightButton(['o_status' => ['<>', 3]], '', 'delete')

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function carry_set($id = '')

    {

        $express = Db::name('Express')->select();

        $order = Db::name('Order')->where(array('id' => $id))->find();

        if (!empty($order) and $order['o_status'] != 1) {

            $this->error('订单状态有误');

        }

        $arr = array();

        foreach ($express as $k => $v) {

            $arr[$v['code']] = $v['name'];

        }

        if ($this->request->post()) {

            do_alogs('设置产品发货');

            $fields = input('post.');

            if (!$fields['o_company_code']) {

                $this->error('快递公司 不能为空');

            }

            if (!$fields['o_company_num']) {

                $this->error('快递编号 不能为空');

            }

            $fields['o_company'] = $arr[$fields['o_company_code']];

            $fields['o_status'] = 2;

            $fields['o_send_time'] = time();

            $fields['last_time'] = time();

            $res = Db::name('Order')->where(array('id' => $id))->update($fields);

            if ($res) {

                $this->success('设置成功', url('Order/carry_list'), '_parent_reload');

            } else {

                $this->error('设置失败 稍后重试');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('填写物流信息')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['select', 'o_company_code', '快递公司', '(请选择快递公司)', $arr],

                ['text', 'o_company_num', '快递编号', '(请输入快递编号)'],

                ['number', 'o_company_price', '运费', '(请输入运费)'],

            ])

            ->fetch();

    }







    //

    public function order_set($id = '')

    {

        $express = Db::name('Express')->select();

        $order = Db::name('Order')->where(array('id' => $id))->find();

        if (!empty($order) and $order['o_status'] != 1) {

            $this->error('订单状态有误');

        }

        $arr = array();

        foreach ($express as $k => $v) {

            $arr[$v['code']] = $v['name'];

        }

        if ($this->request->post()) {

            do_alogs('设置产品发货');

            $fields = input('post.');

            if (!$fields['o_company_code']) {

                $this->error('快递公司 不能为空');

            }

            if (!$fields['o_company_num']) {

                $this->error('快递编号 不能为空');

            }

            $fields['o_company'] = $arr[$fields['o_company_code']];

            $fields['o_status'] = 2;

            $fields['o_send_time'] = time();

            $fields['last_time'] = time();

            $res = Db::name('Order')->where(array('id' => $id))->update($fields);

            if ($res) {

                $this->success('设置成功', url('Order/order_list'), '_parent_reload');

            } else {

                $this->error('设置失败 稍后重试');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('填写物流信息')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['select', 'o_company_code', '快递公司', '(请选择快递公司)', $arr],

                ['text', 'o_company_num', '快递编号', '(请输入快递编号)'],

                ['number', 'o_company_price', '运费', '(请输入运费)'],

            ])

            ->fetch();

    }



    public function order_del()

    {

        do_alogs('删除商城订单信息');

        if ($this->request->isPost()) {

            $data = input('post.');

            $ids = implode($data['ids'], ',');

            $is_del = Db::name('Order')->where(array('id' => array('in', $ids), 'o_status' => 3))->delete();



            if ($is_del) {

                $this->success('删除成功', url('Order/Order_list'));

            } else {

                $this->error('删除失败');

            }

        }

        $order_id = input('id');

        $is_del = Db::name('Order')->where(array('id' => $order_id, 'o_status' => 3))->delete();

        if ($is_del) {

            $this->success('删除成功', url('Order/Order_list'));

        } else {

            $this->error('删除失败');

        }

    }



    public function deal_list()

    {

            do_alogs('查看产品订单列表');

            $map = $this->getMap();

            $order = $this->getOrder('d_addtime desc');

            $deal_list = Deal::

            alias('d')

                ->where($map)

                ->where(array('d_type'=>1))

                ->join('user u', 'd.uid=u.id', 'LEFT')

                ->field('d.id as d_id,d_credit_1,d_credit_2,d_credit_3,d.uid,pid,d_code,d_type,d_total,d_num,d_sell_num,d_addtime,d_price,

                d_status,d_finish_time,d.last_time,d_back_time,u.m_nickname,u.m_phone,u.m_account,d.areaId')

                ->order($order)

                ->limit(20)

                ->paginate();

            foreach ($deal_list as &$row) {

                if ($row['uid'] == 0 || empty($row['uid'])) {

                    continue;

                }

                $row['d_user']   = $this->getUser($row['uid']);

                $row['d_credit'] = 1;

                if($row['d_credit_2']){

                    $row['d_credit'] = 2;

                }

                if($row['d_credit_3']){

                    $row['d_credit'] = 3;

                }

            }

            defined('NOW') || define('NOW', time());
            if(isset($_SESSION['tblqry']))
            {
                $qrycache = $_SESSION['tblqry'];
                foreach($qrycache as $_k => $_v)
                {
                    if($_v['expire'] < NOW) unset($qrycache[$_k]);
                }
            }
            else $qrycache = [];

            $uniqkey = uniqid();
            $map[] = ['d_type', '=', 1];
            $qrycache[$uniqkey] = ['where' => $map, 'order' => $order,'expire' => NOW + 600];
            $_SESSION['tblqry'] = $qrycache;

            $fine_list = [

                'title' => '子订单列表',

                'icon'  => 'fa fa-fw fa-key',

                'href'  => url('fine_list', ['id' => '__id__'])

            ];

		//            halt($deal_list);

            $css = "<style>

            tr,th,td{text-align: center;}

		</style>";

            $status = [0 => '配售', 1 => '交易中', -1 => '已撤单', 2 => '收益中',3 =>'收益完成', 4 => '已出售', 5 => '提货', 6 => '订单完成', 10 => '匹配中'];
            
            $orderType = [1 => '宁红一号', 3 => '宁红二号'];

            $credit = [1 => '零售产品', 2 => '批发产品', 3 => '奖励产品'];
            
            $areaType = [1 => '一区' , 2 => '二区'];

            // 使用ZBuilder快速创建数据表格

            return ZBuilder::make('table')

                ->setTableName('deal')

                ->setSearch(['d.id' => 'ID','d_code'=>'订单号','u.m_nickname' => '昵称','u.m_phone' => '手机号','u.m_account' => '交易号']) // 设置搜索参数

                ->addTimeFilter('d_addtime', '', '开始时间,结束时间') // 添加时间段筛选

                ->addTopSelect('d_status', '', $status)
                
                ->addTopSelect('pid', '', $orderType)
                
                ->addTopSelect('areaId', '', $areaType)

                //->addTopSelect('d_credit', '', $credit)

                ->addColumns([                                                                // 批量添加数据列

                    ['d_id', 'ID'],
                    
                    ['pid', '产品名称', 'status', '', [1 => '宁红一号', 3 => '宁红二号']],
                    
                    ['areaId', '区域', 'status', '', [1 => '一区', 2 => '二区']],

                    ['d_code', '订单编号', 'text'],

                    ['d_type', '交易类型', 'status', '', [1 => '购买', 2 => '卖出']],

                    ['d_user', '用户信息', 'text'],

                    ['d_status', '订单状态', 'status', '', $status],

                    ['d_credit', '产品类型', 'status', '', $credit],

                    ['d_price', '单价', 'text'],

                    ['d_total', '匹配总数', 'text'],

                    ['d_num', '剩余数量', 'text'],

                    ['d_sell_num', '已交易数量', 'text'],

                    ['d_addtime', '创建时间', 'datetime'],

                    ['last_time', '最后交易时间', 'datetime','无'],

                    ['d_finish_time', '结束时间', 'datetime','无'],

                    ['d_back_time', '撤单时间', 'datetime','无'],

                    ['right_button', '操作', 'btn']

                ])

                ->setColumnWidth(['last_time' => 150, 'id' => 60])

                ->setExtraCss($css)

                ->setPrimaryKey('d_id')

                ->addOrder('d_id,last_time')                                                               //添加排序

                ->setRowList($deal_list) // 设置表格数据

                ->addRightButton('custom', $fine_list,['area' => ['1200px', '950px']]) // 添加授权按钮

                ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('order_export', ['uniqkey' => $uniqkey])])

                ->fetch(); // 渲染模板



    }



    public function sell_list()

    {

        do_alogs('查看产品订单列表');

        $map = $this->getMap();

        $order = $this->getOrder('d_addtime desc');

        $deal_list = Deal::

        alias('d')

            ->where($map)

            ->where(array('d_type'=>2,'isShow'=>0))

            ->join('user u', 'd.uid=u.id', 'LEFT')

            ->field('d.id as d_id,d_credit_1,d_credit_2,d_credit_3,d.uid,pid,d_code,d_type,d_total,d_num,d_sell_num,d_addtime,d_price,

                d_status,d_finish_time,d.last_time,d_back_time,u.m_nickname,u.m_phone,u.m_account,d.areaId')

            ->order($order)

            ->limit(20)

            ->paginate();

        foreach ($deal_list as &$row) {

            if ($row['uid'] == 0 || empty($row['uid'])) {

                continue;

            }

            $row['d_user'] = $this->getUser($row['uid']);

            $row['d_credit'] = 1;

            if($row['d_credit_2']){

                $row['d_credit'] = 2;

            }

            if($row['d_credit_3']){

                $row['d_credit'] = 3;

            }

        }

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        // $map['d_type'] = 2;
        $map[] = ['d_type', '=', 2];
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $fine_list = [

            'title' => '子订单列表',

            'icon'  => 'fa fa-fw fa-key',

            'href'  => url('fine_list', ['id' => '__id__'])

        ];

//            halt($deal_list);

        $css = "<style>

            tr,th,td{text-align: center;}

</style>";

        $credit = [1 => '零售产品', 2 => '批发产品', 3 => '奖励产品'];
        
        $orderType = [1 => '宁红一号', 3 => '宁红二号'];

        $status = [0 => '配售', 1 => '交易中', -1 => '已撤单', 2 => '收益中',3 =>'收益完成', 4 => '已出售', 5 => '提货', 6 => '订单完成', 10 => '匹配中'];
        
        $areaType = [1 => '一区' , 2 => '二区'];

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('deal')

            ->setSearch(['d.id' => 'ID','d_code'=>'订单号','u.m_nickname' => '昵称','u.m_phone' => '手机号','u.m_account' => '交易号']) // 设置搜索参数

            ->addTimeFilter('d_addtime', '', '开始时间,结束时间') // 添加时间段筛选

            ->addTopSelect('d_status', '', $status)
            
            ->addTopSelect('pid', '', $orderType)
            
            ->addTopSelect('areaId', '', $areaType)

            ->addColumns([                                                                // 批量添加数据列

                ['d_id', 'ID'],
                
                ['pid', '产品名称', 'status', '', [1 => '宁红一号', 3 => '宁红二号']],
                
                ['areaId', '区域', 'status', '', [1 => '一区', 2 => '二区']],

                ['d_code', '订单编号', 'text'],

                ['d_type', '交易类型', 'status', '', [1 => '购买', 2 => '卖出']],

                ['d_user', '用户信息', 'text'],

                ['d_status', '订单状态', 'status', '', $status],

                ['d_credit', '产品类型', 'status', '', $credit],

                ['d_price', '单价', 'text'],

                ['d_total', '匹配总数', 'text'],

                ['d_num', '剩余数量', 'text'],

                ['d_sell_num', '已交易数量', 'text'],

                ['d_addtime', '创建时间', 'datetime'],

                ['last_time', '最后交易时间', 'datetime','无'],

                ['d_finish_time', '结束时间', 'datetime','无'],

                ['d_back_time', '撤单时间', 'datetime','无'],

                ['right_button', '操作', 'btn']

            ])

            ->setColumnWidth(['last_time' => 150, 'id' => 60])

            ->setExtraCss($css)

            ->setPrimaryKey('d_id')

            ->addOrder('d_id,last_time')                                                               //添加排序

            ->setRowList($deal_list) // 设置表格数据

            ->addRightButton('custom', $fine_list,['area' => ['1200px', '950px']]) // 添加授权按钮
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('order_export', ['uniqkey' => $uniqkey])])

            ->fetch(); // 渲染模板



    }

    public function fine_list()

    {

        do_alogs('查看产品订单列表');

        $map              = $this->getMap();

        $order            = $this->getOrder('f.id asc');

        $id               = $this->request->param('id');

        $deal             = Db::name('Deal')->where(array('id'=>$id))->find();

        if(empty($deal) || $deal['d_type'] == 1){

            $condition['did']   =$id;

        }else{

            $condition['sdid']  =$id;

        }

        $fine_list = Fine::

        alias('f')

            ->where($map)

            ->where($condition)

            ->join('user u', 'f.bid=u.id', 'LEFT')

            ->join('user su', 'f.sid=su.id', 'LEFT')

            ->field('f.id,f.bid,f.sid,f.sdid,f_code,f_status,f_back_time,f_type,

            f_price,f_addtime,f_profit_start_time,f_profit_end_time,f_set_strike,f_end,f_finish_time,u.m_nickname,su.m_nickname')

            ->order($order)

            ->limit(20)

            ->paginate();

        foreach ($fine_list as &$row) {

            $row['f_user'] = $this->getUser($row['bid']);

            $row['f_suser'] = $this->getUser($row['sid']);

        }

//        halt($fine_list);

        $css = "<style>

            tr,th,td{text-align: center;}

</style>";

        $status = [0 => '售卖中', 1 => '持有中', -1 => '已撤单', 2 => '代转让',3=>'已完成', 4 => '已提货',7 => '匹配中'];

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('fine')

            ->setSearch(['f.id' => 'ID', 'f.f_code' => '订单号','u.m_nickname'=>'买家昵称','su.m_nickname'=>'卖家昵称']) // 设置搜索参数

            ->addTimeFilter('f_addtime', '', '开始时间,结束时间') // 添加时间段筛选

            ->addTopSelect('f_status', '', $status)

            ->addColumns([                                                                // 批量添加数据列

                ['id', 'ID'],

                ['f_code', '订单编号', 'text'],

                ['f_user', '买家信息', 'text'],

                ['f_suser', '卖家信息', 'text'],

                ['f_status', '订单状态', 'status', '', $status],

                ['f_price', '产品价格', 'text'],

                ['f_addtime', '创建时间', 'datetime'],

                ['f_profit_start_time', '收益开始时间', 'datetime','无'],

                ['f_profit_end_time', '收益结束时间', 'datetime','无'],

                ['f_finish_time', '结束时间', 'datetime','无'],

                ['f_back_time', '撤单时间', 'datetime','无'],

            ])

            ->setColumnWidth(['last_time' => 120, 'id' => 60])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addOrder('d_id,last_time')                                                               //添加排序

            ->setRowList($fine_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }

    // 订单列表导出
    public function order_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {

                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);

                    $sql = Deal::alias('d')
                        ->where($qrycache['where'])
                        ->join('user u', 'd.uid=u.id', 'LEFT')
                        ->field([
                            'd.id as d_id',
                            'd.uid',
                            'd_code',
                            'd_total',
                            'd_num',
                            'd_sell_num',
                            'd_price',
                            Db::raw('CASE d_status WHEN 0 THEN "配售" WHEN 1 THEN "交易中" WHEN -1 THEN "已撤单" WHEN 2 THEN "受益中" WHEN 3 THEN "收益完成" WHEN 4 THEN "已出售" WHEN 5 THEN "提货" WHEN 6 THEN "订单完成" WHEN 10 THEN "匹配中" END AS d_status'),
                            Db::raw('IF (d_type=1, "购买", "卖出") AS d_type'),
                            Db::raw('IF(d_addtime>0, FROM_UNIXTIME(d_addtime), "无")  AS d_addtime'),
                            Db::raw('IF(d.d_finish_time>0, FROM_UNIXTIME(d.d_finish_time), "无")  AS d_finish_time'),
                            Db::raw('IF(d.last_time>0, FROM_UNIXTIME(d.last_time), "无")  AS last_time'),
                            Db::raw('IF(d.d_back_time>0, FROM_UNIXTIME(d.d_back_time), "无")  AS d_back_time'),
                            Db::raw('IF (d_credit_3, "奖励产品", IF (d_credit_2, "批发产品", "零售产品") ) AS d_credit'),
                            'u.m_nickname',
                            'u.m_phone',
                            Db::raw('CONCAT("' . "\t" . '", u.m_phone ) AS m_phone'),
                            Db::raw('CONCAT("' . "\t" . '", u.m_account ) AS m_account'),
                        ])
                        ->order($qrycache['order'])
                        ->buildSql();

                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }

                    $mysql -> query('SET NAMES UTF8');

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $result = $res -> fetch_all(MYSQLI_ASSOC);

                        $res -> free();
                        // 导出csv文件
                        $fileName = 'deal_list-' . date('YmdHis').'.csv';

                        $arrTitle = [ 'ID', '用户ID', '订单编号', '匹配总数', '剩余数量', '已交易数量', '单价', '订单状态', '交易类型', '创建时间', '结束时间', '最后交易时间', '撤单时间', '产品类型', '用户昵称', '用户手机号', '用户账号'
                        ];

                        ob_end_clean();
                        ob_start();
                        header("Content-Type: text/csv");
                        header("Content-Disposition:filename=" . $fileName);
                        $fp = fopen('php://output', 'w');
                        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));// 转码 防止乱码(比如微信昵称)

                        fputcsv($fp, $arrTitle);
                        array_map(function($item) use ($fp){
                            fputcsv($fp, $item);
                        }, $result);

                        fclose($fp);
                        ob_flush();
                        flush();
                        ob_end_clean();
                    }

                    $mysql -> close();

                }
            }
        }
    }


        // 商品订单导出
    public function order_list_export()
    {

        $uniqkey = input('uniqkey');
    	if(!empty($uniqkey) && isset($_SESSION['tblqry'])){

            $tblqry = $_SESSION['tblqry'];
            
			if(isset($tblqry[$uniqkey])){

                $qrycache = $tblqry[$uniqkey];
                $map = $qrycache['where'];
                $order = $qrycache['order'];

                $data_list = Db::name('Order a')
                ->join('w_user b', 'b.id=a.uid')
                ->join('w_goods c', 'c.id=a.gid')
                ->join('w_address d', 'd.id=a.aid')
                ->where($map)
                ->where('a.o_type','<>',7)
                ->field(
                'a.*,b.m_nickname,b.m_phone,b.m_account,c.g_type,c.g_title,c.g_pic,d.a_name,d.a_phone,d.a_city,d.a_detail')
                ->order($order)
                ->select();

                $titlArr = [
                    'ID', '订单编号', '昵称', '手机号', '交易账号', '订单状态', '商品名称', '单价', '积分单价', '订单数量', '订单总价', '积分总价', '商品类型', '收货人', '收货电话', '收货地址', '订单备注', '创建时间', '付款时间', '发货时间', '收货时间'
                ];
                
                $dataArr = [];

                foreach ($data_list as $k => $v){

                    $status = ['待付款','待发货','待收货','已完成'];

                    $status_txt = $status[$v['o_status']];

                    $dataArr[] = [
                        'ID' => $v['id'],
                        '订单编号' => $v['o_code'],
                        '昵称' => $v['m_nickname'],
                        '手机号' => filter_value($v['m_phone']),
                        '交易账号' => filter_value($v['m_account']),
                        '订单状态' => $status_txt,
                        '商品名称' => $v['g_title'],
                        '单价' => $v['o_price'],
                        '积分单价' => $v['o_credit'],
                        '订单数量' => $v['o_buy_num'],
                        '订单总价' => $v['o_credit_1'],
                        '积分总价' => $v['o_credit_2'],
                        '商品类型' => ($v['g_type']==1)?'购物专区':'积分专区',
                        '收货人' => $v['a_name'],
                        '收货电话' => filter_value($v['a_phone']),
                        '收货地址' => $v['a_city'] . ' ' . $v['a_detail'],
                        '订单备注' => $v['o_info'],
                        '创建时间' => date('Y-m-d H:i:s', $v['o_addtime']),
                        '付款时间' => ($v['o_pay_time']==0)?'未付款':date('Y-m-d H:i:s', $v['o_pay_time']),
                        '发货时间' => ($v['o_send_time']==0)?'未发货':date('Y-m-d H:i:s', $v['o_send_time']),
                        '收货时间' => ($v['o_take_time']==0)?'未收货':date('Y-m-d H:i:s', $v['o_take_time'])
                    ];

                }

                $filename = '商品订单-' . date('YmdHis');
                return export_csv($filename . '.csv', $titlArr, $dataArr);




            }
			
        }

    }


    // 修改订单状态
    public function send_out_good_status()
    {
        $param = $this->request->param();

        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];

        if(!isset($param['status'])) return $this->error('缺少参数');
        if(empty($param['ids'])) return $this->error('请选择需要操作的订单');

        $result = Db::name('order')->where('id', 'in', $param['ids'])->update(['o_status' => $param['status']]);
        if($result){
            // 添加日志
            // var_export($param['ids'], true)
            $_deac = '修改订单，ids为：'.implode(',', $param['ids']).'，状态修改为'.$param['status'];
            $log_info = [
                'admin_id' => $admin_id,
                'role_id' => 0,
                'last_time' => time(),
                'a_desc' => $_deac,
            ];
            Db::name('Alogs')->insert($log_info);
            return $this->success('修改失败', url('order_list'));
        }else{
            return $this->error('操作失败');
        }
    }

    // 历史买单列表
    public function history_deal_list()
    {
        do_alogs('查看产品订单历史列表');
		$map = $this->getMap();
		$order = $this->getOrder('d_addtime desc');
		$deal_list = DealHistory::alias('d')
			->where($map)
			->where(array('d_type'=>1))
			->join('user u', 'd.uid=u.id', 'LEFT')
			->field('d.id as d_id,d_credit_1,d_credit_2,d_credit_3,d.uid,pid,d_code,d_type,d_total,d_num,d_sell_num,d_addtime,d_price,
			d_status,d_finish_time,d.last_time,d_back_time,u.m_nickname,u.m_phone,u.m_account')
			->order($order)
			->limit(20)
            ->paginate();
            
		foreach ($deal_list as &$row) {
			if ($row['uid'] == 0 || empty($row['uid'])) {
				continue;
			}
			$row['d_user']   = $this->getUser($row['uid']);
			$row['d_credit'] = 1;
			if($row['d_credit_2']){
				$row['d_credit'] = 2;
			}
			if($row['d_credit_3']){
				$row['d_credit'] = 3;
			}
        }
		$fine_list = [
			'title' => '子订单列表',
			'icon'  => 'fa fa-fw fa-key',
			'href'  => url('fine_list', ['id' => '__id__'])
		];
		// halt($deal_list);
		$css = "<style>\n\t\ttr,th,td{text-align: center;}\t</style>";
		$status = [0 => '配售', 1 => '交易中', -1 => '已撤单', 2 => '收益中',3 =>'收益完成', 4 => '已出售', 5 => '提货', 6 => '订单完成', 10 => '匹配中'];
		$credit = [1 => '零售产品', 2 => '批发产品', 3 => '奖励产品'];

		defined('NOW') || define('NOW', time());
		if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
		else $qrycache = [];

		$uniqkey = uniqid();
		$map[] = ['d_type', '=', 1];
		$qrycache[$uniqkey] = ['where' => $map, 'order' => $order,'expire' => NOW + 600];
		$_SESSION['tblqry'] = $qrycache;


		// 使用ZBuilder快速创建数据表格
		return ZBuilder::make('table')
			->setTableName('deal_history')
			->setSearch(['d.id' => 'ID','d_code'=>'订单号','u.m_nickname' => '昵称','u.m_phone' => '手机号','u.m_account' => '交易号']) // 设置搜索参数
			->addTimeFilter('d_addtime', '', '开始时间,结束时间') // 添加时间段筛选
			->addTopSelect('d_status', '', $status)
			//->addTopSelect('d_credit', '', $credit)
			->addColumns([                                                                // 批量添加数据列
				['d_id', 'ID'],
				['d_code', '订单编号', 'text'],
				['d_type', '交易类型', 'status', '', [1 => '购买', 2 => '卖出']],
				['d_user', '用户信息', 'text'],
				['d_status', '订单状态', 'status', '', $status],
				// ['d_credit', '产品类型', 'status', '', $credit],
				['d_price', '单价', 'text'],
				['d_total', '匹配总数', 'text'],
				['d_num', '剩余数量', 'text'],
				['d_sell_num', '已交易数量', 'text'],
				['d_addtime', '创建时间', 'datetime'],
				['last_time', '最后交易时间', 'datetime','无'],
				['d_finish_time', '结束时间', 'datetime','无'],
				['d_back_time', '撤单时间', 'datetime','无'],
				// ['right_button', '操作', 'btn']
			])
			->setColumnWidth(['last_time' => 150, 'id' => 60])
			->setExtraCss($css)
			->setPrimaryKey('d_id')
			->addOrder('d_id,last_time')                                                               //添加排序
			->setRowList($deal_list) // 设置表格数据
			// ->addRightButton('custom', $fine_list,['area' => ['1200px', '950px']]) // 添加授权按钮
			->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('history_order_export', ['uniqkey' => $uniqkey])])
			->fetch(); // 渲染模板


    }


    // 历史卖单列表
    public function history_sell_list()
    {
        do_alogs('查看产品订单历史列表');
        $map = $this->getMap();
        $order = $this->getOrder('d_addtime desc');
        if(!empty($map))
		{

		}
        $deal_list = DealHistory::alias('d')
            ->where($map)
            ->where(array('d_type' => 2))
            ->join('user u', 'd.uid=u.id', 'LEFT')
            ->field('d.id as d_id,d_credit_1,d_credit_2,d_credit_3,d.uid,pid,d_code,d_type,d_total,d_num,d_sell_num,d_addtime,d_price,
                d_status,d_finish_time,d.last_time,d_back_time,u.m_nickname,u.m_phone,u.m_account')
            ->order($order)
            ->limit(20)
            ->paginate();
        foreach ($deal_list as &$row){
            if ($row['uid'] == 0 || empty($row['uid'])) {
                continue;
            }
            $row['d_user'] = $this->getUser($row['uid']);
            $row['d_credit'] = 1;
            if($row['d_credit_2']){
                $row['d_credit'] = 2;
            }
            if($row['d_credit_3']){
                $row['d_credit'] = 3;
            }
        }

        defined('NOW') || define('NOW', time());
		if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
		else $qrycache = [];

		$uniqkey = uniqid();
		$map[] = ['d_type', '=', 2];
		$qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
		$_SESSION['tblqry'] = $qrycache;

        $fine_list = [
            'title' => '子订单列表',
            'icon'  => 'fa fa-fw fa-key',
            'href'  => url('fine_list', ['id' => '__id__'])
        ];
        // halt($deal_list);
        $css = "<style>\n\t\ttr,th,td{text-align: center;}\t</style>";
        $credit = [1 => '零售产品', 2 => '批发产品', 3 => '奖励产品'];
        $status = [0 => '配售', 1 => '交易中', -1 => '已撤单', 2 => '收益中',3 =>'收益完成', 4 => '已出售', 5 => '提货', 6 => '订单完成', 10 => '匹配中'];
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('deal_history')
            ->setSearch(['d.id' => 'ID','d_code'=>'订单号','u.m_nickname' => '昵称','u.m_phone' => '手机号','u.m_account' => '交易号']) // 设置搜索参数
            ->addTimeFilter('d_addtime', '', '开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('d_status', '', $status)
            ->addColumns([                                                                // 批量添加数据列
                ['d_id', 'ID'],
                ['d_code', '订单编号', 'text'],
                ['d_type', '交易类型', 'status', '', [1 => '购买', 2 => '卖出']],
                ['d_user', '用户信息', 'text'],
                ['d_status', '订单状态', 'status', '', $status],
                // ['d_credit', '产品类型', 'status', '', $credit],
                ['d_price', '单价', 'text'],
                ['d_total', '匹配总数', 'text'],
                ['d_num', '剩余数量', 'text'],
                ['d_sell_num', '已交易数量', 'text'],
                ['d_addtime', '创建时间', 'datetime'],
                ['last_time', '最后交易时间', 'datetime','无'],
                ['d_finish_time', '结束时间', 'datetime','无'],
                ['d_back_time', '撤单时间', 'datetime','无'],
                // ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time' => 150, 'id' => 60])
            ->setExtraCss($css)
            ->setPrimaryKey('d_id')
            ->addOrder('d_id,last_time')                                                               //添加排序
            ->setRowList($deal_list) // 设置表格数据
            // ->addRightButton('custom', $fine_list,['area' => ['1200px', '950px']]) // 添加授权按钮
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('history_order_export', ['uniqkey' => $uniqkey])])
            ->fetch(); // 渲染模板

    }

    // 历史订单列表导出
	public function history_order_export()
	{
		$uniqkey = input('uniqkey');
		if(!empty($uniqkey) && isset($_SESSION['tblqry']))
		{
			$tblqry = $_SESSION['tblqry'];
			if(isset($tblqry[$uniqkey]))
			{

				$qrycache = $tblqry[$uniqkey];
				if(isset($qrycache['expire']) && $qrycache['expire'] > time())
				{
                    ini_set('memory_limit', '1024M');
                    ini_set('max_execution_time', 0);

                    $sql = DealHistory::alias('d')
                        ->join('user u', 'd.uid=u.id', 'LEFT')
                        ->where($qrycache['where'])
                        ->field([
                            'd.id as d_id',
                            'd.uid',
                            'd_code',
                            'd_total',
                            'd_num',
                            'd_sell_num',
                            'd_price',
                            Db::raw('CASE d_status WHEN 0 THEN "配售" WHEN 1 THEN "交易中" WHEN -1 THEN "已撤单" WHEN 2 THEN "受益中" WHEN 3 THEN "收益完成" WHEN 4 THEN "已出售" WHEN 5 THEN "提货" WHEN 6 THEN "订单完成" WHEN 10 THEN "匹配中" END AS d_status'),
                            Db::raw('IF (d_type=1, "购买", "卖出") AS d_type'),
                            Db::raw('IF(d_addtime>0, FROM_UNIXTIME(d_addtime), "无")  AS d_addtime'),
                            Db::raw('IF(d.d_finish_time>0, FROM_UNIXTIME(d.d_finish_time), "无")  AS d_finish_time'),
                            Db::raw('IF(d.last_time>0, FROM_UNIXTIME(d.last_time), "无")  AS last_time'),
                            Db::raw('IF(d.d_back_time>0, FROM_UNIXTIME(d.d_back_time), "无")  AS d_back_time'),
                            Db::raw('IF (d_credit_3, "奖励产品", IF (d_credit_2, "批发产品", "零售产品") ) AS d_credit'),
                            'u.m_nickname',
                            'u.m_phone',
                            Db::raw('CONCAT("' . "\t" . '", u.m_phone ) AS m_phone'),
                            Db::raw('CONCAT("' . "\t" . '", u.m_account ) AS m_account'),
                        ])
                        ->order($qrycache['order'])
                        ->buildSql();

                        $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
					if($mysql -> connect_error)
					{
						header('Content-Type: application/json; charset=utf-8');
						exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }

                    $mysql -> query('SET NAMES UTF8');

					$res = $mysql -> query($sql);
					if($res)
					{
						$arrTitle = [ 'ID', '用户ID', '订单编号', '匹配总数', '剩余数量', '已交易数量', '单价', '订单状态', '交易类型', '创建时间', '结束时间', '最后交易时间', '撤单时间', '产品类型', '用户昵称', '用户手机号', '用户账号'
                        ];

						$totalrows = $res -> num_rows;
						if($totalrows > 0)
						{
							defined('NOW') || define('NOW', time());
							if($totalrows > 1e5)
							{
								$res -> free();
								$totalpg = ceil($totalrows / 1e5);
								$zipfile = new \ZipArchive;
								$_filename = 'history_deal_list_' . date('YmdHis', NOW);
								if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
								{
									ini_set('memory_limit', '512M');
									ini_set('max_execution_time', 0);
									for($pg = 0; $pg < $totalpg;)
									{
										$res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
										if($res && $res -> num_rows > 0)
										{
											$result = $res -> fetch_all(MYSQLI_ASSOC);
											$res -> free();
											$filename = $_filename . '_pg' . $pg . '.csv';
											$fp = fopen($filename, 'w');
											fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
											fputcsv($fp, $arrTitle);
											array_map(function($item) use ($fp){
												fputcsv($fp, $item);
											}, $result);
											unset($result);
											fclose($fp);
											$zipfile -> addFile($filename);
										}
									}
									$mysql -> close();
									$zipfile -> close();
									$filename = $_filename . '.zip';
									// ob_end_clean();
									// ob_start();
									header('Content-Type: application/zip');
									header('Content-Disposition: filename=' . $filename);
									$fsize = filesize($filename);
									header('Content-Length: ' . $fsize);
									exit(file_get_contents($filename));
									$fp = fopen($filename, 'r');
									$outp = fopen('php://output', 'w');
									do
									{
										$_len = $fsize > 2e3 ? 2e3 : $fsize;
										fwrite($outp, fread($fp, $_len), $_len);
										ob_flush();
										flush();
										$_len -= 2e3;
									}
									while($fsize > 2e3);
									fclose($fp);
									// unlink($filename);
									fclose($outp);
									ob_end_clean();
									exit;
								}
							}
							else
							{
								$result = $res -> fetch_all(MYSQLI_ASSOC);
								$res -> free();
								$mysql -> close();
								// 导出csv文件
								$filename = 'history_deal_list_' . date('YmdHis');
								return export_csv($filename . '.csv', $arrTitle, $result);
							}
						}
                        // $result = $res -> fetch_all(MYSQLI_ASSOC);

                        // $res -> free();
                        // // 导出csv文件
                        // $fileName = 'history_deal_list-' . date('YmdHis').'.csv';

                        // $arrTitle = [ 'ID', '用户ID', '订单编号', '匹配总数', '剩余数量', '已交易数量', '单价', '订单状态', '交易类型', '创建时间', '结束时间', '最后交易时间', '撤单时间', '产品类型', '用户昵称', '用户手机号', '用户账号'
                        // ];

                        // ob_end_clean();
                        // ob_start();
                        // header("Content-Type: text/csv");
                        // header("Content-Disposition:filename=" . $fileName);
                        // $fp = fopen('php://output', 'w');
                        // fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));// 转码 防止乱码(比如微信昵称)

                        // fputcsv($fp, $arrTitle);
                        // array_map(function($item) use ($fp){
                        //     fputcsv($fp, $item);
                        // }, $result);

                        fclose($fp);
                        ob_flush();
                        flush();
                        ob_end_clean();
                    }

                    $mysql -> close();

				}
			}
		}
    }



        // 每日配票明细
    public function day_join_list()
    {

        $map = $this->getMap();
        $order = $this->getOrder('user_amounts desc');
        $time       = time();
        $date       = date('Y-m-d',$time);
        if (!isset($map['0'])) {

            $map['0'] = ['addDate','between time', [''.$date.' 00:00:00', ''.$date.' 23:59:59']];

        }

        $field = [
            'a.*', 'b.p_title', Db::raw('SUM(a.amounts) AS user_amounts')
        ];
        $list = Db::name('wallet_bouns')
                    ->alias('a')
                    ->join('product b', 'a.pid = b.id', 'left')
                    ->where($map)
                    ->order($order)
                    ->group('a.uid,a.pid,a.addDate,a.bounstype')
                    ->field($field)
                    // ->buildSql();
                    ->paginate(20);
		// dump($list);die;

        defined('NOW') || define('NOW', time());
		if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
		else $qrycache = [];

		$uniqkey = uniqid();
		$qrycache[$uniqkey] = ['where' => $map, 'order' => $order,'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        $_product_list = Db::name('product')->where('p_del', 0)->select();
        $product_list = [];

        foreach($_product_list as $k => $v){
            $product_list[$v['id']] = $v['p_title'];
        }

        // 使用ZBuilder快速创建数据表格
		return ZBuilder::make('table')
        ->setTableName('wallet_bouns')
        ->setSearch(['a.maccount' => '用户账号']) // 设置搜索参数
        ->addTopSelect('a.bounstype', '配票类型', [1 => '批发票', 2 => '奖励票'])
        ->addTopSelect('a.pid', '产品', $product_list)
        ->addTimeFilter('a.addDate','','开始时间,结束时间') // 添加时间段筛选
        ->addColumns([
            ['id', 'ID'],
            ['maccount', '用户账号', 'text'],
            ['p_title', '产品名称', 'text'],
            ['bounstype', '配票类型', 'status', '', [1 => '批发票', 2 => '奖励票']],
            ['user_amounts', '配票数量', 'text'],
            ['addDate', '时间', 'text'],
        ])
        ->addOrder('id,user_amounts,addDate')
        ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('export_day_join_list', ['uniqkey' => $uniqkey])])
        ->setRowList($list) // 设置表格数据
        ->hideCheckbox()
        ->fetch(); // 渲染模板

    }



    // 每日配票导出
    public function export_day_join_list()
    {
    	    $uniqkey = input('uniqkey');
		if(!empty($uniqkey) && isset($_SESSION['tblqry']))
		{
			$tblqry = $_SESSION['tblqry'];
			if(isset($tblqry[$uniqkey]))
			{

				$qrycache = $tblqry[$uniqkey];
				if(isset($qrycache['expire']) && $qrycache['expire'] > time())
				{
                    ini_set('memory_limit', '1024M');
                    ini_set('max_execution_time', 0);
                    $field = [
			            'a.*', 'b.p_title', Db::raw('SUM(a.amounts) AS user_amounts')
			        ];
			        $list = Db::name('wallet_bouns')
			                    ->alias('a')
			                    ->join('product b', 'a.pid = b.id', 'left')
			                    ->where($qrycache['where'])
			                    ->order($qrycache['order'])
			                    ->group('a.uid,a.pid,a.addDate,a.bounstype')
			                    ->field($field)
			                    // ->buildSql();
			                    ->select();

                    // 表格标题
                    $tileArray = [                          
                        'ID',
                        '用户账号',
                        '产品名称',
                        '配票类型',
                        '配票数量',
                        '时间', 
                    ];
                    // 表格内容
                    $dataArray = [];
                    foreach($list as $k => $v){

                        $dataArray[] = [
                            'ID' => $v['id'],
                            '用户账号' => filter_value($v['maccount']),
                            '产品名称' => $v['p_title'],
                            '配票类型' => ($v['bounstype'] == 1) ? '批发票' : '奖励票',
                            '配票数量' => $v['user_amounts'],
                            '时间' => $v['addDate'],
                        ];
                    }
                    // 导出csv文件
                    $filename = 'wallet_bouns-' . date('YmdHis');
                    return export_csv($filename . '.csv', $tileArray, $dataArray);

                }
            }
        }

    }





}