<?php
// 凭证管理
namespace app\admin\controller;

use think\Db;
use app\common\builder\ZBuilder;
use think\Validate;

class Voucher extends Admin
{

    // 录入凭证
    public function voucher_list()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        $field = 'a.*, b.abstract_num, b.abstract_name, c.nickname as add_admin, d.nickname as toexam_admin';

        $join = [
            ['sw_financial_abstract b', 'a.abstract_id = b.id', 'left'],
            ['admin_user c', 'a.add_uid = c.id', 'left'],
            ['admin_user d', 'a.toexam_uid = d.id', 'left'],
        ];

        $data_list = Db::name('sw_voucher')->alias('a')->join($join)->field($field)->where($map)->order($order)->limit(20)->paginate();

        return ZBuilder::make('table')
            ->setTableName('sw_voucher')
            ->setSearch(['a.id' => '凭证号', 'b.abstract_num' => '凭证摘要号', 'b.abstract_name' => '凭证摘要'])
            ->addColumns([
                ['id', '凭证号', 'text'],
                ['abstract_num', '凭证摘要号', 'text'],
                ['abstract_name', '凭证摘要', 'text'],
                ['status', '凭证状态', 'status', '', [0 => '待确认', 1 => '待审核', 2 => '已结算', 3 => '未结算']],
                ['add_admin', '录入员', 'text'],
                ['create_time', '录入时间', 'date'],
                ['toexam_admin', '审核员', 'text'],
                ['toexam_time', '审核时间', 'date'],
            ])
            ->addTopButton('add', ['href' => url('add_voucher')])
            ->addTopButton('delete', ['href' => url('del_voucher')]) // 删除
            // ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }

    // 添加凭证
    public function add_voucher()
    {

        $admin_id   = session('user_auth')['uid'];

        if($this->request->post()){
            $param = $this->request->post();

            $rule = [
                'abstract_id' => 'require',
                // 'abstract_name' => 'require',
                'contract_code' => 'max:25',
                'remarks' => 'max:200',
                'from_data' => 'require'
            ];
            $msg = [
                'abstract_id.require' => '请填写凭证摘要号',
                // 'abstract_name.require' => '请填写凭证摘要',
                'contract_code.max' => '合同号不能超过25位字符',
                'remarks.max' => '附加说明不能超过200字符',
                'from_data.require' => '请填写摘要'
            ];

            $validate = new Validate($rule, $msg);
            if(!$validate->check($param)){
                $this->error($validate->getError());
            }
            $abstract_info = Db::name('sw_financial_abstract')->where('id', $param['abstract_id'])->find();
            if(!$abstract_info){
                $this->error('摘要不存在');
            }

            $submit_status = false;
   
            Db::startTrans();
            try{
                // 添加 凭证 和 凭证详情
                $time = time();
                $voucher = [
                    'abstract_id' => $param['abstract_id'],
                    'contract_code' => $param['contract_code'],
                    'remarks' => $param['remarks'],
                    'status' => 0,
                    'create_time' => $time,
                    'add_uid' => $admin_id,
                ];
                $voucher_id = Db::name('sw_voucher')->insertGetId($voucher);

                foreach($param['from_data'] as $k => $v){
                    $subject_info = Db::name('sw_financial_subject')->where('id', $v['subject_id'])->find();
                    if(!$subject_info){
                        Db::rollback();
                        return $this->error('科目代码不存在');
                    }

                    $voucher_info = [
                        'voucher_id' => $voucher_id,
                        'entry_abstract' => $v['entry_abstract'],
                        'subject_id' => $v['subject_id'],
                        'borrow_money' => $v['borrow_money'],
                        'loan_money' => $v['loan_money'],
                        'status' => 1,
                        'create_time' => time()
                    ];
                    Db::name('sw_voucher_list')->insert($voucher_info);
                }
             
                // 提交事务
                Db::commit(); 
                $submit_status = true;
                
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $submit_status = fasle;
                
            }
            if($submit_status){
                return $this->success('添加成功');
            }else{
                return $this->error('添加失败');
            }
        }

        return $this->fetch();
    }

    // 删除凭证
    public function del_voucher()
    {

        if($this->request->post()){
            $param = $this->request->post();

            if(empty($param['ids'])) $this->error('请选择要操作的数据');
            $status = false;
            Db::startTrans();
            try{
                Db::name('sw_voucher')->delete($param['ids']);
                foreach($param['ids'] as $v){
                    Db::name('sw_voucher_list')->delete($v);
                }
                // 提交事务
                Db::commit();
                $status = true;
                // $this->success('删除成功');
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $status = false;
                // $this->error('删除失败');
            }
            if($status === true){
                return $this->success('删除成功');
            }else{
                return $this->error('删除失败');
            }
        }

    }


    // 凭证确认
    public function voucher_confirm()
    {

        $param = $this->request->param();
        if(!empty($param['o_id'])){
            // 凭证确认操作
            $result = Db::name('sw_voucher')->where('id', $param['o_id'])->update(['status'=>1]);
            if($result){
                $this->success('审核成功');
            }else{
                $this->error('操作失败');
            }
        }

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        $field = 'a.*, b.abstract_num, b.abstract_name, c.nickname as add_admin, d.nickname as toexam_admin';

        $join = [
            ['sw_financial_abstract b', 'a.abstract_id = b.id', 'left'],
            ['admin_user c', 'a.add_uid = c.id', 'left'],
            ['admin_user d', 'a.toexam_uid = d.id', 'left'],
        ];
        $map[] = ['a.status', '=', 0];

        $data_list = Db::name('sw_voucher')->alias('a')->join($join)->field($field)->where($map)->order($order)->limit(20)->paginate();

        $btn_submit = [
            'title' => '提交审核',
            'class' => 'btn btn-xs btn-default ajax-get',
            'icon'  => 'fa fa-fw fa-check-circle-o',
            'href'  => url('voucher_confirm', ['o_id' => '__id__'])
        ];

        return ZBuilder::make('table')
            ->setTableName('sw_voucher')
            ->setSearch(['a.id' => '凭证号', 'b.abstract_num' => '凭证摘要号', 'b.abstract_name' => '凭证摘要'])
            ->addColumns([
                ['id', '凭证号', 'text'],
                ['abstract_num', '凭证摘要号', 'text'],
                ['abstract_name', '凭证摘要', 'text'],
                ['status', '凭证状态', 'status', '', [0 => '待确认', 1 => '待审核', 2 => '已结算', 3 => '未结算']],
                ['add_admin', '录入员', 'text'],
                ['create_time', '录入时间', 'date'],
                ['toexam_admin', '审核员', 'text'],
                ['toexam_time', '审核时间', 'date'],
                ['right_button', '操作', 'btn']
            ])
            ->addRightButton('btn_submit', $btn_submit)
            // ->addTopButton('add', ['href' => url('add_voucher')])
            // ->addTopButton('delete', ['href' => url('del_voucher')]) // 删除
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }

    // 财务审核
    public function finance_toexam()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        $field = 'a.*, b.abstract_num, b.abstract_name, c.nickname as add_admin, d.nickname as toexam_admin';

        $join = [
            ['sw_financial_abstract b', 'a.abstract_id = b.id', 'left'],
            ['admin_user c', 'a.add_uid = c.id', 'left'],
            ['admin_user d', 'a.toexam_uid = d.id', 'left'],
        ];

        $map[] = ['a.status', '=', 1];

        $data_list = Db::name('sw_voucher')->alias('a')->join($join)->field($field)->where($map)->order($order)->limit(20)->paginate();

        $btn_to_examine = [
            'title' => '审核',
            'icon'  => 'fa fa-fw fa-check-circle-o',
            'href'  => url('toexam', ['o_id' => '__id__'])
        ];

        return ZBuilder::make('table')
            ->setTableName('sw_voucher')
            ->setSearch(['a.id' => '凭证号', 'b.abstract_num' => '凭证摘要号', 'b.abstract_name' => '凭证摘要'])
            ->addColumns([
                ['id', '凭证号', 'text'],
                ['abstract_num', '凭证摘要号', 'text'],
                ['abstract_name', '凭证摘要', 'text'],
                ['status', '凭证状态', 'status', '', [0 => '待确认', 1 => '待审核', 2 => '已结算', 3 => '未结算']],
                ['add_admin', '录入员', 'text'],
                ['create_time', '录入时间', 'date'],
                ['toexam_admin', '审核员', 'text'],
                ['toexam_time', '审核时间', 'date'],
                ['right_button', '操作', 'btn']
            ])
            ->addRightButton('btn_to_examine', $btn_to_examine, ['area' => ['400px', '330px']])
            // ->addTopButton('add', ['href' => url('add_voucher')])
            // ->addTopButton('delete', ['href' => url('del_voucher')]) // 删除
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
    }

    // 审核操作
    public function toexam()
    {

        $admin_id   = session('user_auth')['uid'];

        if($this->request->post()){
            $param = $this->request->param();
            if(!isset($param['status'])) $this->error('请选择审核状态');

            $new_data = [
                'toexam_uid' => $admin_id,
                'toexam_time' => time(),
            ];
            // 通过
            if($param['status'] == 1){
                $new_data['status'] = 2;
                $new_data['settlement_time'] = time();
                $result = Db::name('sw_voucher')->where('id', $param['o_id'])->update($new_data);
            }else{
                // 不通过
                $new_data['status'] = 3;
                $result = Db::name('sw_voucher')->where('id', $param['o_id'])->update($new_data);
            }

            if($result){
                $this->success('审核成功', url('finance_toexam'), '_parent_reload');
            }else{
                $this->fail('操作失败');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('审核凭证')
            ->addFormItems([
                // ['hidden', 'id'],
                ['radio', 'status', '审核凭证', '', [1 => '通过', 0 => '不通过']],
            ])
            // ->setFormData($info)
            ->fetch();

    }

    // 未结算凭证查询
    public function unsettled_voucher_list()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        $field = 'a.*, b.abstract_num, b.abstract_name, c.nickname as add_admin, d.nickname as toexam_admin';

        $join = [
            ['sw_financial_abstract b', 'a.abstract_id = b.id', 'left'],
            ['admin_user c', 'a.add_uid = c.id', 'left'],
            ['admin_user d', 'a.toexam_uid = d.id', 'left'],
        ];

        $map[] = ['a.status', '=', 3];

        $data_list = Db::name('sw_voucher')->alias('a')->join($join)->field($field)->where($map)->order($order)->limit(20)->paginate();

        $_user_list = Db::name('admin_user')->field('id, nickname')->select();
        $user_list = [];
        foreach($_user_list as $k => $v){
            $user_list[$v['id']] = $v['nickname'];
        }

        return ZBuilder::make('table')
            ->setTableName('sw_voucher')
            ->setSearch(['a.id' => '凭证号', 'b.abstract_num' => '凭证摘要号', 'b.abstract_name' => '凭证摘要'])
            ->addTopSelect('a.add_uid', '录入员', $user_list)
            ->addTopSelect('a.toexam_uid', '审核员', $user_list)
            ->addColumns([
                ['id', '凭证号', 'text'],
                ['abstract_num', '凭证摘要号', 'text'],
                ['abstract_name', '凭证摘要', 'text'],
                ['status', '凭证状态', 'status', '', [0 => '待确认', 1 => '待审核', 2 => '已结算', 3 => '未结算']],
                ['add_admin', '录入员', 'text'],
                ['create_time', '录入时间', 'date'],
                ['toexam_admin', '审核员', 'text'],
                ['toexam_time', '审核时间', 'date'],
            ])
            // ->addTopButton('add', ['href' => url('add_voucher')])
            // ->addTopButton('delete', ['href' => url('del_voucher')]) // 删除
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
    }

    // 已结算凭证查询
    public function settled_voucher_list()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        $field = 'a.*, b.abstract_num, b.abstract_name, c.nickname as add_admin, d.nickname as toexam_admin';

        $join = [
            ['sw_financial_abstract b', 'a.abstract_id = b.id', 'left'],
            ['admin_user c', 'a.add_uid = c.id', 'left'],
            ['admin_user d', 'a.toexam_uid = d.id', 'left'],
        ];

        $map[] = ['a.status', '=', 2];

        $data_list = Db::name('sw_voucher')->alias('a')->join($join)->field($field)->where($map)->order($order)->limit(20)->paginate();

        $_user_list = Db::name('admin_user')->field('id, nickname')->select();
        $user_list = [];
        foreach($_user_list as $k => $v){
            $user_list[$v['id']] = $v['nickname'];
        }

        return ZBuilder::make('table')
            ->setTableName('sw_voucher')
            ->setSearch(['a.id' => '凭证号', 'b.abstract_num' => '凭证摘要号', 'b.abstract_name' => '凭证摘要'])
            ->addTopSelect('a.add_uid', '录入员', $user_list)
            ->addTopSelect('a.toexam_uid', '审核员', $user_list)
            ->addTimeFilter('settlement_time','','开始时间,结束时间')
            ->addColumns([
                ['id', '凭证号', 'text'],
                ['settlement_time', '结算日期', 'date'],
                ['abstract_num', '凭证摘要号', 'text'],
                ['abstract_name', '凭证摘要', 'text'],
                ['status', '凭证状态', 'status', '', [0 => '待确认', 1 => '待审核', 2 => '已结算', 3 => '未结算']],
                ['add_admin', '录入员', 'text'],
                ['create_time', '录入时间', 'date'],
                ['toexam_admin', '审核员', 'text'],
                ['toexam_time', '审核时间', 'date'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
    }

    // 快速录入
    public function fast_voucher()
    {

        $financial_mould_list = Db::name('sw_financial_mould')->select();
    
        if($this->request->post()){

        }

        $this->assign('financial_mould_list', $financial_mould_list);

        return $this->fetch();
    }


}