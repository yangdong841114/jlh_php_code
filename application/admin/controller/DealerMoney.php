<?php
// 交易商资金
namespace app\admin\controller;

use think\Db;
use app\common\builder\ZBuilder;

class DealerMoney extends Admin
{

    // 交易商当前资金
    public function dealer_current_money()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['', '交易商代码', 'text'],
                ['', '交易商名称', 'text'],
                ['', '交易系统当前余额', 'text'],
                ['', '财务结算余额', 'text'],
                ['', '财务未结算余额', 'text'],
                ['', '差额', 'text'],
                ['', '可用资金', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }


    // 交易商资金流水
    public function deale_capital_flow()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '流水编号', 'text'],
                ['', '交易商代码', 'text'],
                ['', '业务名称', 'text'],
                ['', '合同号', 'text'],
                ['', '商品代码', 'text'],
                ['', '发生额(元)', 'text'],
                ['', '资金余额(元)', 'text'],
                ['', '附加帐金额(元)', 'text'],
                ['', '凭证号', 'text'],
                ['', '发生日期', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }


    // 交易商总账单
    public function deale_total_bill()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['', '结算日期', 'text'],
                ['', '交易商代码', 'text'],
                ['', '银行代码', 'text'],
                ['', '期初余额', 'text'],
                ['', '入金', 'text'],
                ['', '出金', 'text'],
                ['', '银转手续费', 'text'],
                ['', '当日其他项', 'text'],
                ['aaaa', '订单订金变动', 'text'],
                ['aaaa', '订单货值变化变动', 'text'],
                ['aaaa', '订单交收订金变动', 'text'],
                ['', '订单利润', 'text'],
                ['aaaa', '订单交收货值变化', 'text'],
                ['aaaa', '订单销售收入', 'text'],
                ['aaaa', '订单购货支出', 'text'],
                ['aaaa', '订单交易手续费', 'text'],
                ['aaaa', '订单交收手续费', 'text'],
                ['aaaa', '订单交割补偿费', 'text'],
                ['aaaa', '订单当日其他项', 'text'],
                ['aaaa', '挂牌转让市值变动', 'text'],
                ['aaaa', '挂牌销售收入', 'text'],
                ['aaaa', '挂牌购货支出', 'text'],
                ['aaaa', '挂牌交易手续费', 'text'],
                ['aaaa', '挂牌当日其他项', 'text'],
                ['aaaa', '挂牌卖出杂费', 'text'],
                ['aaaa', '挂牌交货费用', 'text'],
                ['', '商城提现', 'text'],
                ['', '商城充值', 'text'],
                ['', '期末余额', 'text'],
                ['', '期初担保金', 'text'],
                ['', '期末担保金', 'text'],
            ])
            ->hideCheckbox()
            ->setColumnWidth('aaaa', 200)
            ->setRowList($data_list)
            ->fetch();

    }


    // 交易商总账单合计
    public function deale_total_bill_total()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['', '交易商代码', 'text'],
                ['', '期初余额', 'text'],
                ['', '入金', 'text'],
                ['', '出金', 'text'],
                ['', '银转手续费', 'text'],
                ['', '当日其他项', 'text'],
                ['aaaa', '订单订金变动', 'text'],
                ['aaaa', '订单货值变化变动', 'text'],
                ['aaaa', '订单交收订金变动', 'text'],
                ['', '订单利润', 'text'],
                ['aaaa', '订单交收货值变化', 'text'],
                ['aaaa', '订单销售收入', 'text'],
                ['aaaa', '订单购货支出', 'text'],
                ['aaaa', '订单交易手续费', 'text'],
                ['aaaa', '订单交割补偿费', 'text'],
                ['aaaa', '订单当日其他项', 'text'],
                ['aaaa', '挂牌转让市值变动', 'text'],
                ['aaaa', '挂牌销售收入', 'text'],
                ['aaaa', '挂牌购货支出', 'text'],
                ['aaaa', '挂牌交易手续费', 'text'],
                ['aaaa', '挂牌当日其他项', 'text'],
                ['aaaa', '挂牌卖出杂费', 'text'],
                ['aaaa', '挂牌交货费用', 'text'],
                ['', '商城提现', 'text'],
                ['', '商城充值', 'text'],
                ['', '期末余额', 'text'],
                ['', '期初担保金', 'text'],
                ['', '期末担保金', 'text'],
            ])
            ->hideCheckbox()
            ->setColumnWidth('aaaa', 200)
            ->setRowList($data_list)
            ->fetch();

    }
    




}