<?php



namespace app\admin\controller;



use app\common\builder\ZBuilder;

use app\index\controller\Api;

use think\Db;



class Notice extends Admin

{



    public function get_config()

    {



        $data = Db::name('Config')->where(array('id' => 1))->find();

        if ($data['w_deal_date'] === '1') {

            $data['w_deal_date_type'] = 1;

            $data['w_deal_date']      = 0;

        } else {

            $data['w_deal_date_type'] = 0;

        }

        $w_deal_time1                   = explode('-', $data['w_deal_time1']);

        $data['w_deal_time1_start']     = $w_deal_time1[0];

        $data['w_deal_time1_end']       = $w_deal_time1[1];

        $w_deal_time2                   = explode('-', $data['w_deal_time2']);

        $data['w_deal_time2_start']     = $w_deal_time2[0];

        $data['w_deal_time2_end']       = $w_deal_time2[1];

        $w_sign_time                    = explode('-', $data['w_sign_time']);

        $data['w_sign_time_start']      = $w_sign_time[0];

        $data['w_sign_time_end']        = $w_sign_time[1];

        $week = array(

            '1' => '周一',

            '2' => '周二',

            '3' => '周三',

            '4' => '周四',

            '5' => '周五',

            '6' => '周六',

            '7' => '周日',

        );

        if ($this->request->post()) {

            do_alogs('操作基础设置');

            $Api = new Api();

            $fields         =   input('post.');

            if ($fields['w_deal_date_type'] == 0) {

                if (empty($fields['w_deal_date'])) {

                    $this->error('交易日期不能为空');

                }

                $fields['w_deal_date']  = implode(',', $fields['w_deal_date']);

            } else {

                $fields['w_deal_date']  = $fields['w_deal_date_type'];

            }

            if (!$fields['w_account_start']) {

                $this->error('交易商账号前缀不能为空');

            }

            if (!$fields['w_deal_time1_start']) {

                $this->error('开始交易时间段1不能为空');

            }

            if (!$fields['w_deal_time1_end']) {

                $this->error('结束交易时间段1不能为空');

            }



            if (!$fields['w_deal_time2_start']) {

                $this->error('开始交易时间段2不能为空');

            }

            if (!$fields['w_deal_time2_end']) {

                $this->error('结束交易时间段2不能为空');

            }

            if ($fields['w_deal_time1_start'] >= $fields['w_deal_time1_end'] || $fields['w_deal_time1_start'] >= $fields['w_deal_time2_start'] || $fields['w_deal_time1_start'] >= $fields['w_deal_time2_end']) {

                $this->error('交易时间请按规则填写');

            }

            if ($fields['w_deal_time1_end'] >= $fields['w_deal_time2_start'] || $fields['w_deal_time1_end'] >= $fields['w_deal_time2_end']) {

                $this->error('交易时间请按规则填写');

            }

            if ($fields['w_deal_time2_start'] >= $fields['w_deal_time2_end']) {

                $this->error('交易时间请按规则填写');

            }

            $fields['w_deal_time1'] = $fields['w_deal_time1_start'] . '-' . $fields['w_deal_time1_end'];

            $fields['w_deal_time2'] = $fields['w_deal_time2_start'] . '-' . $fields['w_deal_time2_end'];

            if (!$fields['w_deal_fee']) {

                $fields['w_deal_fee'] = 0;

            }

            if (!$fields['w_basic_rate']) {

                $fields['w_basic_rate'] = 0;

            }

            if (!$fields['w_cash_fee']) {

                $fields['w_cash_fee'] = 0;

            }

            if ($fields['w_max_num'] <= 0) {

                $this->error('用户最大限购数量不能小于0');

            }

            if ($fields['w_max_num'] <= 0) {

                $this->error('用户最大限购数量不能小于1');

            }

            if ($fields['w_max_sell_num'] <= 0) {

                $this->error('用户最大限售数量不能小于0');

            }

            if ($fields['w_max_sell_num'] <= 0) {

                $this->error('用户最大限售数量不能小于0');

            }

            if ($fields['w_max_sell_num'] <= 0) {

                $this->error('用户最大限售数量不能小于1');

            }

            if ($fields['w_max_sell_num_day'] <= 1) {

                $this->error('每日最大限售数量不能小于1');

            }

            if ($fields['w_recovery_day'] <= 0) {

                $this->error('批发产品回收天数不能小于1');

            }

            if ($fields['w_recovery_day'] <= 0) {

                $this->error('批发产品回收天数不能小于1');

            }

            if (empty($fields['w_sign_date'])) {

                $this->error('签约日期不能为空');

            }

            $fields['w_sign_date'] = implode(',', $fields['w_sign_date']);

            if (!$fields['w_sign_time_start']) {

                $this->error('签约开始时间不能为空');

            }

            if (!$fields['w_sign_time_end']) {

                $this->error('签约结束时间不能为空');

            }

            if ($fields['w_sms'] == 1) {

                if (!$fields['w_sms_account']) {

                    $this->error('短信账号不能为空');

                }

                if (!$fields['w_sms_pwd']) {

                    $this->error('短信密码不能为空');

                }

                if (!$fields['w_sms_token']) {

                    $this->error('短信密钥不能为空');

                }

            }

            if ($fields['w_sign_time_start'] >= $fields['w_sign_time_end']) {

                $this->error('签约时间请按规则填写');

            }

            if(!isset($fields['w_exchange'])){
                $this->error('请选择开启置换功能');
            }
      
            if(empty($fields['w_min_exchange_num'])){
                $this->error('请正确填写最低持有可置换数量');
            }

            $fields['w_min_exchange_num'] = intval($fields['w_min_exchange_num']);

            if(!is_int($fields['w_min_exchange_num'])){
                $this->error('请正确填写最低持有可置换数量');
            }

            $fields['w_sign_time'] = $fields['w_sign_time_start'] . '-' . $fields['w_sign_time_end'];

            $res = Db::name('Config')->where(array('id' => 1))->update($fields);

            if ($res) {

                $this->success('设置成功');

            } else {

                $this->error('设置失败');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('基础设置')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['hidden', 'id'],

                ['text', 'w_account_start', '交易商账号前缀'],

                ['radio', 'w_deal_date_type', '交易日期设置', '(交易日期设置)', ['自主设置', '法定节假日']],

                ['radio', 'w_auto_deal', '自动回收','(是否由系统账号自动回收)',['否','是']],

                ['checkbox', 'w_deal_date', '交易日期', '(请选择交易日期)', $week],

                ['time', 'w_deal_time1_start', '开始交易时间段1', '(请选择交易时间)', '', 'HH:mm'],

                ['time', 'w_deal_time1_end', '结束交易时间段1', '(请选择交易时间)', '', 'HH:mm'],

                ['time', 'w_deal_time2_start', '开始交易时间段2', '(请选择交易时间)', '', 'HH:mm'],

                ['time', 'w_deal_time2_end', '结束交易时间段2', '(请选择交易时间)', '', 'HH:mm'],

                ['number', 'w_deal_fee', '手续费设置 单位：%', '(请填写基础手续费)'],

                ['number', 'w_basic_rate', '基础增加率 单位：%', '(请填写基础增加率)'],

                ['number', 'w_cash_fee', '提现手续费 单位：%', '(请填写提现手续费)'],

                ['number', 'w_max_num', '最大限购数量 单位：数量', '(用户交易最大限购数量设置 周)'],

                ['number', 'w_max_sell_num', '最大限售数量 单位：数量', '(用户交易最大限售数量设置 周)'],

                ['number', 'w_max_sell_num_day', '最大限售数量 单位：数量', '(用户交易最大限售数量设置 日)'],

                ['number', 'w_recovery_day', '批发产品回收天数 单位：天'],

                ['checkbox', 'w_sign_date', '签约日期设置', '', $week],

                ['time', 'w_sign_time_start', '签约开始时间', '', '', 'HH:mm'],

                ['time', 'w_sign_time_end', '签约结束时间', '', '', 'HH:mm'],

                ['radio', 'w_deal_type', '交易设置', '', ['开启', '关闭']],

                ['radio', 'w_wot_type', '消费积分', '', ['开启', '关闭']],

                ['number', 'w_wot_jt_scale', '静态释放比例 单位：%', '(请输入静态释放比例)'],

                ['number', 'w_wot_dt__scale', '动态释放比例 单位：%', '(请输入动态释放比例)'],

                ['number', 'w_wot_push_ask', '直推递增率要求 单位：%', '(请输入直推递增率要求)'],

                ['number', 'w_wot_team_ask', '团队递增率要求 单位：%', '(请输入团队递增率要求)'],

                ['radio', 'w_sms', '短信开关', '', ['关闭', '开启']],

                ['text', 'w_sms_account', '短信账号'],

                ['password', 'w_sms_pwd', '短信密码'],

                ['text', 'w_sms_token', '短信密钥'],

                ['ckeditor', 'w_notice_desc', '首页公告', '(请输入公告内容)'],
                ['radio', 'w_exchange', '开启置换功能', '', ['关闭', '开启']],
                ['number', 'w_min_exchange_num', '最低持有可置换数量 单位：盒', '提宁红一号xx盒之后才可以置换 单位：盒'],

            ])

            ->layout([

                'w_account_start' => 2, 'w_deal_time1_start' => 3, 'w_deal_time1_end' => 3, 'w_deal_time2_start' => 3, 'w_deal_time2_end' => 3,

                'w_deal_fee' => 2, 'w_basic_rate' => 2, 'w_cash_fee' => 2, 'w_max_num' => 2, 'w_max_sell_num' => 2, 'w_max_sell_num_day' => 2, 'w_recovery_day' => 3,

                'w_sign_time_start' => 2, 'w_sign_time_end' => 2,  'w_sms_token' => 4, 'w_sms_account' => 2,

                'w_wot_jt_scale' => 2, 'w_wot_dt__scale' => 2,  'w_wot_push_ask' => 2, 'w_wot_team_ask' => 2,

                'w_wot_type' => 2, 'w_sms_pwd' => 2, 'w_notice_desc' => 7, 'w_deal_next_date' => 2, 'w_exchange' => 4, 'w_min_exchange_num' => 4

            ])

            ->setFormData($data)

            ->fetch();

    }





    //交易所打卡

    public function set_sign()

    {

        $data = Db::name('Config')->where(array('id' => 1))->find();

        $operating_status = array(0 => '正常', 1 => '日终', 2 => '暂停');

        if ($this->request->post()) {

            do_alogs('交易所签到设置');

            $Api = new Api();

            $fields         =   input('post.');

            $res = $Api->open_or_close_exchange($fields['w_sign']);

            $res    = json_decode($res, true);

            if ($res['error_no'] != 0) {

                $this->error($res['error_info']);

            }

            Db::name('Config')->where(array('id' => 1))->update($fields);

            $res = 1;

            if ($res) {

                $this->success('设置成功');

            } else {

                $this->error('设置失败');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('基础设置')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['hidden', 'id'],

                ['radio', 'w_sign', '银行签到签退', '', $operating_status],

            ])

            ->setFormData($data)

            ->fetch();

    }

    //交易所开市

    public function open_market()

    {

        $data = Db::name('Config')->where(array('id' => 1))->find();

        $open_market     = array(0 => '闭市', 1 => '开市');

        if ($this->request->post()) {

            do_alogs('交易所开闭市设置');

            $Api        = new Api();

            $fields     =   input('post.');

            if ($fields['w_deal_next_date'] != '') {

                $res    = $Api->update_third_party_calendar_info($fields['w_deal_next_date'], $fields['w_open_market']);

                $res    = json_decode($res, true);

                if ($res['error_no'] != 0) {

                    $this->error($res['error_info']);

                }

            }

            Db::name('Config')->where(array('id' => 1))->update($fields);

            if ($res) {

                $this->success($res['error_info']);

            } else {

                $this->error($res['error_info']);

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('基础设置')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['hidden', 'id'],

                ['radio', 'w_open_market', '开市', '', $open_market],

                ['date', 'w_deal_next_date', '下个交易日', '上个设置日期' . $data['w_deal_next_date'], '', 'yyyymmdd'],

            ])

            ->layout(['w_deal_next_date' => 2])

            ->setFormData($data)

            ->fetch();

    }


    // 清算 插入到数据库 set_file 生成文件
    public function set_file()
    {

        $table_sql = 'select * from w_user_report_qs where m_balance <> qsye or qsye < 0;';

        // $begintime = microtime(true);
        $fee_ratio              = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        $time                   = time();
        $date                   = date('Y-m-d', $time);
        //$date                   = '2020-05-11';
        $init_date              = date('Ymd');              //业务时间
        //$init_date              = '20200511';               //业务时间
        $exchange_code          = '02000016';               //交易所代码
        $biz_type               = 1;                        //业务类型
        $exchange_market_type   = 1;
        $money_type = array(
            ['type_name'  => '人民币', 'type' => 0],
        );
        // $Api            = new Api();
        $admin_id       = session('user_auth')['uid'];
        $file_type      = array('I2' => '成交数据文件', 'I3' => '资金余额文件', 'I6' => '持仓明细文件', 'I9' => '外部费用明细文件', 'IB' => '结算价文件');
        //必须文件      客户信息变更文件1    成交数据文件2  资金余额文件3 成交清算文件4 持仓明细文件5 结算价文件6 交割单文件7  外部费用明细文件8 _fees
        if ($this->request->post()) {
            do_alogs('交易所文件清算设置');
            $fields         =   input('post.');
            // if ($fields['f_type'] == '') {
            //     $this->error('请选择清算类型');
            // }
            if (!$fields['f_start_time'] || !$fields['f_end_time']) {
                $fields['f_start_time']         =   $date;
                $fields['f_end_time']           =   date("Y-m-d", strtotime("+1 day"));
            } else {
                $init_date                      =   date('Ymd', strtotime($fields['f_start_time']));
            }
            //          if(!empty($has_flogs)){
            //                $this->error('当前类型今日已经报送过了,请勿重复报送');
            //          }
            $start_time = strtotime($fields['f_start_time']);
            $end_time   = strtotime($fields['f_end_time']);
            $from_data  = array();
            

            ini_set('memory_limit', '1024M');
            ini_set('max_execution_time', 0);
            // if($fields['file_type'] == '5'){
            //     return ['code' => 200, 'msg' => '全部清算成功'];
            // }else{
            //     $this->success('已清算');
            // }

            if ($fields['file_type'] == '0') {
                //1.成交数据文件
                $fields_type = 'I2';

                $f_title = $init_date . '_' . $exchange_code . '_deal.txt';
                $sql = 'INSERT INTO `w_file_transaction_data` (`init_date`, `exchange_code`, `exchange_market_type`, `biz_type`, `deal_id`, `open_mem_code`, `open_fund_account`, `open_trade_account`, `opp_mem_code`, `opp_fund_account`, `opp_trade_account`, `product_category_id`, `product_code`, `entrust_bs`, `deal_type`, `opp_deal_type`, `hpsp_trade_type`, `deal_quantity`, `deal_price`, `hold_price`, `deal_total_price`, `deposit_rate`, `deposit_ratio_type`, `deposit_type`, `deposit_balance`, `receipt_quantity`, `open_poundage`, `opp_poundage`, `deal_time`, `depot_order_no`, `opp_depot_order_no`, `order_id`, `opp_order_id`, `settlement_date`, `init_time`) VALUES ';
                $sql_none = true;
                /*deal_id	            成交编号		                        Y
                    open_mem_code	    会员编码(发起方)		                Y
                    open_fund_account	资金账号（发起方）	                    Y
                    open_trade_account	交易账号(发起方)		                Y
                    opp_mem_code	    会员编码(对手方)		                Y
                    opp_fund_account	资金账号(对手方)		                Y
                    opp_trade_account	交易账号(对手方)		                Y
                    product_code	    产品代码		                        Y
                    entrust_bs	        买卖方向(1买2卖)		                Y
                    deal_type	        成交类型(1-开仓(建仓) 2-平仓)		    Y
                    opp_deal_type	    对手方成交类型(1-开仓(建仓) 2-平仓)	    Y
                    hpsp_trade_type	    交易类型(0-默认(做市商) 1-协议开仓 2-协议平仓 3-协议换手 4-强平 5-现金交割)		Y
                    deal_price	        成交单价		                        Y
                    hold_price	        持仓价格		                        Y
                    deal_quantity	    成交数量		                        Y
                    deal_total_price	成交总价		                        Y
                    deal_time	        成交时间(HHmmss)		                Y
                    depot_order_no	    建仓单号		                        Y
                    opp_depot_order_no	对手方建仓单号		                Y
                    settlement_date	    结算日期(yyyyMMdd)		            Y*/
                //$buy_data   = array();
                //$sell_data  = array();
                // set_deal();                                                 //检查订单进行退回
                $fine_data  = Db::name('Fine')->where("f_addtime>=$start_time and f_addtime<$end_time and f_status in (1,2,4,7) and bid<>0 and sid<>0 and did<>0 and sdid<>0")->group('did,sdid')->order('f_addtime desc')->select();
                if (!empty($fine_data)) {
                    foreach ($fine_data as $k => $v) {
                        // $bid_info = Db::name('User')->where('id', $v['bid'])->find();
                        // $sid_info = Db::name('User')->where('id', $v['sid'])->find();
                        $row['init_date']               = $init_date;
                        $row['exchange_code']           = $exchange_code;
                        $row['exchange_market_type']    = $exchange_market_type;
                        $row['biz_type']                = $biz_type;
                        $row['deal_id']                 = '911' . $v['id'];
                        $row['open_mem_code']           = getMember($v['bid'], 'm_account');
                        $row['open_fund_account']       = getMember($v['bid'], 'm_account');
                        $row['open_trade_account']      = getMember($v['bid'], 'm_account');
                        $row['opp_mem_code']            = getMember($v['sid'], 'm_account');
                        $row['opp_fund_account']        = getMember($v['sid'], 'm_account');
                        $row['opp_trade_account']       = getMember($v['sid'], 'm_account');
                        // $row['open_mem_code']           = $bid_info['m_account'];
                        // $row['open_fund_account']       = $bid_info['m_account'];
                        // $row['open_trade_account']      = $bid_info['m_account'];
                        // $row['opp_mem_code']            = $sid_info['m_account'];
                        // $row['opp_fund_account']        = $sid_info['m_account'];
                        // $row['opp_trade_account']       = $sid_info['m_account'];
                        $product                        = getProduct($v['pid']);
                        $row['product_category_id']     = $product['p_cate'];
                        $row['product_code']            = $product['p_code'];
                        $row['entrust_bs']              = 1;
                        $row['deal_type']               = 1;
                        $row['opp_deal_type']           = 1;
                        $row['hpsp_trade_type']         = 0;
                        // $deal_num                       = Db::name('Fine')->where(array('did' => $v['did'], 'sdid' => $v['sdid']))->order('f_addtime desc')->count();
                        $deal_num                       = Db::name('Fine')->where(array('did' => $v['did'], 'sdid' => $v['sdid']))->count();
                        $row['deal_quantity']           = $deal_num;
                        $row['deal_price']              = $v['f_price'] * 100;                    //成交单价
                        $row['hold_price']              = $v['f_price'] * 100;                    //持仓价格
                        $deal_price                     = $v['f_price'] * $deal_num;
                        $row['deal_total_price']        = $deal_price * 100;          //订单总价
                        $row['deposit_rate']            = '';
                        $row['deposit_ratio_type']      = '';
                        $row['deposit_type']            = '';
                        $row['deposit_balance']         = '';
                        $row['receipt_quantity']        = '';
                        $row['open_poundage']           = bcmul($deal_price, $fee_ratio, 2) * 100;
                        $row['opp_poundage']            = 0;
                        $row['deal_time']               = date('His', $v['f_addtime']);
                        $row['depot_order_no']          = $v['f_code'];
                        $row['opp_depot_order_no']      = $v['f_code'] . '001';
                        $row['order_id']                = '';
                        $row['opp_order_id']            = '';
                        $row['settlement_date']         = $init_date;
                        $row['init_time']           = strtotime($init_date);
                        $from_data[]                    = $row;
                        $open_poundage           = bcmul($deal_price, $fee_ratio, 2);

                        $sql .= '("'.$init_date.'", "'.$exchange_code.'", "'.$exchange_market_type.'", "'.$biz_type.'", "'.$row['deal_id'].'", "'.$row['open_mem_code'].'", "'.$row['open_fund_account'].'", "'.$row['open_trade_account'].'", "'.$row['opp_mem_code'].'", "'.$row['opp_fund_account'].'", "'.$row['opp_trade_account'].'", "'.$row['product_category_id'].'", "'.$row['product_code'].'", "'.$row['entrust_bs'].'", "'.$row['deal_type'].'", "'.$row['opp_deal_type'].'", "'.$row['hpsp_trade_type'].'", "'.$row['deal_quantity'].'", "'.$v['f_price'].'", "'.$v['f_price'].'", "'.$deal_price.'", "'.$row['deposit_rate'].'", "'.$row['deposit_ratio_type'].'", "'.$row['deposit_type'].'", "'.$row['deposit_balance'].'", "'.$row['receipt_quantity'].'", "'.$open_poundage.'", "'.$row['opp_poundage'].'", "'.$row['deal_time'].'", "'.$row['depot_order_no'].'", "'.$row['opp_depot_order_no'].'", "'.$row['order_id'].'", "'.$row['opp_order_id'].'", "'.$row['settlement_date'].'", "'.strtotime($row['init_date']).'") , ';
                    }
                    // dump(microtime(true)-$begintime);
                    $sql = rtrim($sql, ' , ') . ' ;';
                    $sql_none = false;
                    // dump($sql);die;

                }
                
            } elseif ($fields['file_type'] == '1') {
                //2.资金余额文件
                $fields_type = 'I3';
                $f_title = $init_date . '_' . $exchange_code . '_memberFund.txt';
                $data    = Db::name('User')->where(array('m_bank_signing' => 1, 'm_del' => 0))->select();
                $sql = 'INSERT INTO `w_file_fund_balance` (`init_date`, `exchange_code`, `mem_code`, `fund_account`, `money_type`, `occur_balance`, `current_balance`, `init_time` ) VALUES ';
                $sql_none = true;
                if (!empty($data)) {
                    foreach ($data as $k => $v) {
                        $z_price                  = Db::name('Zlog')->where(array('uid' => $v['id']))->where("z_addtime>=$start_time and z_addtime<$end_time and z_status=1")->sum('z_price');
                        if (!$z_price) {
                            $z_price              = 0;
                        }
                        //$z_price                    = Db::name('Zlog')->where(array('uid'=>$v['id']))->where("z_addtime>=1588176000 and z_addtime<=1589212800 and z_status=1")->sum('z_price');
                        $row['init_date']           = $init_date;
                        $row['exchange_code']       = $exchange_code;
                        $row['mem_code']            = $v['m_account'];
                        $row['fund_account']        = $v['m_account'];
                        $row['money_type']          = 'CNY';
                        $row['occur_balance']       = $z_price * 100;
                        $row['current_balance']     = $v['m_balance'] * 100;
                        $row['init_time']           = strtotime($init_date);
                        $from_data[]                = $row;
                        // $row['occur_balance']       = $z_price;
                        // $row['current_balance']     = $v['m_balance'];
                        // $from_data_a[]                = $row;
                        $sql .= '("'.$init_date.'", "'.$exchange_code.'", "'.$v['m_account'].'", "'.$v['m_account'].'", "'.$row['money_type'].'", "'.$z_price.'", "'.$v['m_balance'].'", "'.$row['init_time'].'" ) , ';
                    }
                    // dump(microtime(true)-$begintime);
                    $sql = rtrim($sql, ' , ') . ' ;';
                    $sql_none = false;
                    // dump($sql); die;
                }
            } elseif ($fields['file_type'] == '2') {
                //3.持仓明细文件
                $fields_type = 'I6';
                $f_title = $init_date . '_' . $exchange_code . '_memberPositionDetail.txt';
                $sql = 'INSERT INTO `w_file_position_details` (`init_date`, `exchange_code`, `hold_id`, `mem_code`, `trade_account`, `product_category_id`, `product_code`, `entrust_bs`, `deposit_way`, `open_price`, `hold_price`, `deal_quantity`, `left_quantity`, `present_unit`, `trade_poundage`, `delay_fees`, `perform_balance`, `deposit_rate`, `square_profit_loss`, `settle_profit_loss`, `settle_price`, `deposit_ratio_type`, `deposit_type`, `today_hold_flag`, `deal_time`, `init_time` ) VALUES ';
                $sql_none = true;
                /*    hold_id	            持仓单号		                        Y
                        mem_code	        会员编码		                        Y
                        trade_account	    交易账号		                        Y
                        product_code	    产品代码		                        Y
                        entrust_bs	        是否买方向(0-卖 1-买)		            Y
                        deposit_way	        是否定金方式(0-仓单 1-定金)	        Y
                        open_price	        开仓价格		                        Y
                        hold_price	        持仓价格		                        Y
                        deal_quantity	    成交数量		                        Y
                        left_quantity	    剩余数量		                        Y
                        present_unit	    数量单位		                        Y
                        trade_poundage	    手续费		                        Y
                        delay_fees	        滞纳金		                        Y
                        perform_balance	    履约准备金		                    Y
                        deposit_rate	    定金率		                        Y
                        square_profit_loss	平仓盈亏		                        Y
                        settle_profit_loss	结算盈亏		                        Y
                        settle_price	    结算价		                        Y
                        deposit_ratio_type	定金率是否比率(0-固定 1-比率)		    Y
                        deposit_type	    定金收取方式(0-开仓价 1-持仓价) 	    Y
                        today_hold_flag	    是否今仓(0-否 1-是)		            Y */
                $data   = Db::name('Deal')->where("(d_status=3 or d_status=2)")->order('d_addtime desc')->select();
                //$data   = Db::name('Deal')->where("(d_addtime>=1588176000 and d_addtime<=1589212800) and (d_status=3 or d_status=2)")->order('d_addtime desc')->select();
                if (!empty($data)) {
                    foreach ($data as $k => $v) {
                        // $uid_info = Db::name('User')->where('id', $v['uid'])->find();
                        $product                    = getProduct($v['pid']);
                        $row['init_date']           = $init_date;
                        $row['exchange_code']       = $exchange_code;
                        $row['hold_id']             = '912' . $v['id'];
                        $row['mem_code']            = getMember($v['uid'], 'm_account');
                        $row['trade_account']       = getMember($v['uid'], 'm_account');
                        // $row['mem_code']            = $uid_info['m_account'];
                        // $row['trade_account']       = $uid_info['m_account'];
                        $row['product_category_id'] = $product['p_cate'];
                        $row['product_code']        = $product['p_code'];
                        $row['entrust_bs']          = 1;
                        $row['deposit_way']         = 0;
                        $row['open_price']          = $v['d_price'] * 100;
                        $row['hold_price']          = $v['d_price'] * 100;
                        $row['deal_quantity']       = $v['d_sell_num'];
                        $row['left_quantity']       = $v['d_num'];
                        $row['present_unit']        = 0;
                        $row['trade_poundage']      = $v['d_fee'] * 100;
                        $row['delay_fees']          = 0;
                        $row['perform_balance']     = 0;
                        $row['deposit_rate']        = 0;
                        $row['square_profit_loss']  = 0;
                        $row['settle_profit_loss']  = 0;
                        $row['settle_price']        = 0;
                        $row['deposit_ratio_type']  = 1;
                        $row['deposit_type']        = 0;
                        $row['today_hold_flag']     = 0;
                        $row['deal_time']           = date('YmdHis', $v['d_addtime']);
                        $row['init_time']           = strtotime($init_date);
                        $from_data[]                = $row;
                        // $row['open_price']          = $v['d_price'];
                        // $row['hold_price']          = $v['d_price'];
                        // $row['trade_poundage']      = $v['d_fee'];
                        // $from_data_a[]                = $row;
                        $sql .= '("'.$init_date.'", "'.$exchange_code.'", "'.$row['hold_id'].'", "'.$row['mem_code'].'", "'.$row['trade_account'].'", "'.$row['product_category_id'].'", "'.$row['product_code'].'", "'.$row['entrust_bs'].'", "'.$row['deposit_way'].'", "'.$v['d_price'].'", "'.$v['d_price'].'", "'.$row['deal_quantity'].'", "'.$row['left_quantity'].'", "'.$row['present_unit'].'", "'.$v['d_fee'].'", "'.$row['delay_fees'].'", "'.$row['perform_balance'].'", "'.$row['deposit_rate'].'", "'.$row['square_profit_loss'].'", "'.$row['settle_profit_loss'].'", "'.$row['settle_price'].'", "'.$row['deposit_ratio_type'].'", "'.$row['deposit_type'].'", "'.$row['today_hold_flag'].'", "'.$row['deal_time'].'", "'.$row['init_time'].'" ) , ';
                    }
                    // dump(microtime(true)-$begintime);
                    $sql = rtrim($sql, ' , ') . ' ;';
                    $sql_none = false;
                    // dump($sql); die;
                }
            } elseif ($fields['file_type'] == '3') {
                //4.外部费用明细文件
                $fields_type = 'I9';
                $f_title = $init_date . '_' . $exchange_code . '_fees.txt';
                $sql = 'INSERT INTO `w_file_external_expenses_details` (`init_date`, `serial_no`, `exchange_code`, `exchange_market_type`, `biz_type`, `exchange_fees_type`, `fees_balance`, `payer_mem_code`, `payer_fund_account`, `payee_mem_code`, `payee_fund_account`, `deal_id`, `remark`, `busi_datetime`, `init_time` ) VALUES  @';
                $sql_none = true;
                /*   serial_no	                费用流水号		                            Y
                        exchange_fees_type	        费用类型，1佣金，2-其他费用，3-交易类费用		    Y
                        fees_balance	            费用金额		                                Y
                        payer_mem_code	            付费会员编码		                            Y
                        payer_fund_account	        付款资金账号		                            Y
                        payee_mem_code	            收款会员编码		                            Y
                        payee_fund_account	        收款资金账号		                            Y
                        remark	                    备注		                                    Y
                        busi_datetime	            操作时间(yyyyMMddHHmmss)		                Y*/
                $from_data1  = array();
                $from_data2  = array();
                $from_data3  = array();
                $from_data4  = array();
                // $from_data1_a  = array();
                // $from_data2_a  = array();
                // $from_data3_a  = array();
                $data1       = Db::name('Deal')->where("d_status in (1,2,3,4,5,6) and d_fee<>0 and d_type=1")->select();
                //$data1     = Db::name('Deal')->where("d_status in (1,2,3,4,6) and d_addtime>=1588176000 and d_addtime<=1589212800 and d_fee<>0 and d_type=1")->select();
                if (!empty($data1)) {
                    $sql = rtrim($sql, '@');
                    // dump($sql);
                    foreach ($data1 as $k1 => $v1) {
                        $user   = getUserInfo($v1['uid']);
                        if (!empty($user)) {
                            // $uid_info = Db::name('user')->where('id', $v1['uid'])->find();
                            $row['init_date']                   = $init_date;
                            $row['serial_no']                   = '913' . $v1['id'];
                            $row['exchange_code']               = $exchange_code;
                            $row['exchange_market_type']        = 1;
                            $row['biz_type']                    = $biz_type;
                            $row['exchange_fees_type']          = 2;                                  //必填
                            $row['fees_balance']                = $v1['d_fee'] * 100;                   //必填
                            $row['payer_mem_code']              = getMember($v1['uid'], 'm_account');
                            $row['payer_fund_account']          = getMember($v1['uid'], 'm_account');
                            // $row['payer_mem_code']              = $uid_info['m_account'];
                            // $row['payer_fund_account']          = $uid_info['m_account'];
                            $row['payee_mem_code']              = $exchange_code;
                            $row['payee_fund_account']          = $exchange_code;
                            $row['deal_id']                     = '';
                            $row['remark']                      = '';
                            $row['busi_datetime']               = date('YmdHis', $v1['d_addtime']);
                            $row['init_time']           = strtotime($init_date);
                            $from_data1[]                       = $row;
                            // $row['fees_balance']                = $v1['d_fee'];
                            // $from_data1_a[]                       = $row;
                            $sql .= '("'.$init_date.'", "'.$row['serial_no'].'", "'.$exchange_code.'", "'.$row['exchange_market_type'].'", "'.$biz_type.'", "'.$row['exchange_fees_type'].'", "'.$v1['d_fee'].'", "'.$row['payer_mem_code'].'", "'.$row['payer_fund_account'].'", "'.$exchange_code.'", "'.$exchange_code.'", "'.$row['deal_id'].'", "'.$row['remark'].'", "'.$row['busi_datetime'].'", "'.$row['init_time'].'" ),';
                        }
                    }
                    $sql = rtrim($sql, ',') . ';';
                    $sql_none = false;
                }
                $data2      = Db::name('Deal')->where("d_status in (1,2,3,4,5,6) and d_sfee<>0 and d_type=2 and d_sell_num <> 0")->select();
                //$data2    = Db::name('Deal')->where("d_status in (1,2,3,4,6) and d_addtime>=1588176000 and d_addtime<=1589212800 and d_sfee<>0 and d_type=2")->select();
                if (!empty($data2)) {
                    if(substr($sql, -1) == '@'){
                        $sql = rtrim($sql, '@');
                    }elseif(substr($sql, -1) == ';'){
                        $sql = rtrim($sql, ';') . ',';
                    }
                    // dump($sql);
                    foreach ($data2 as $k2 => $v2) {
                        $user   = getUserInfo($v2['uid']);
                        if (!empty($user)) {
                            // $uid_info = Db::name('user')->where('id', $v2['uid'])->find();
                            $row['init_date']                   = $init_date;
                            $row['serial_no']                   = '914' . $v2['id'];
                            $row['exchange_code']               = $exchange_code;
                            $row['exchange_market_type']        = 1;
                            $row['biz_type']                    = $biz_type;
                            $row['exchange_fees_type']          = 2;                                  //必填
                            $row['fees_balance']                = $v2['d_sfee'] * 100;                   //必填
                            $row['payer_mem_code']              = getMember($v2['uid'], 'm_account');
                            $row['payer_fund_account']          = getMember($v2['uid'], 'm_account');
                            // $row['payer_mem_code']              = $uid_info['m_account'];
                            // $row['payer_fund_account']          = $uid_info['m_account'];
                            $row['payee_mem_code']              = $exchange_code;
                            $row['payee_fund_account']          = $exchange_code;
                            $row['deal_id']                     = '';
                            $row['remark']                      = '';
                            $row['busi_datetime']               = date('YmdHis', $v2['d_addtime']);
                            $row['init_time']           = strtotime($init_date);
                            $from_data2[]                       = $row;
                            // $row['fees_balance']                = $v2['d_sfee'];
                            // $from_data2_a[]                       = $row;
                            $sql .= '("'.$init_date.'", "'.$row['serial_no'].'", "'.$exchange_code.'", "'.$row['exchange_market_type'].'", "'.$biz_type.'", "'.$row['exchange_fees_type'].'", "'.$v2['d_sfee'].'", "'.$row['payer_mem_code'].'", "'.$row['payer_fund_account'].'", "'.$exchange_code.'", "'.$exchange_code.'", "'.$row['deal_id'].'", "'.$row['remark'].'", "'.$row['busi_datetime'].'", "'.$row['init_time'].'" ),';
                        }
                    }
                    $sql = rtrim($sql, ',') . ';';
                    $sql_none = false;
                    // dump($sql);
                    
                }
                $data3       =   Db::name('Deal')->where("d_type=2 and d_zljl > 0 and areaId = 1 and d_token = 0")->select();
                if (!empty($data3)) {
                    if(substr($sql, -1) == '@'){
                        $sql = rtrim($sql, '@');
                    }elseif(substr($sql, -1) == ';'){
                        $sql = rtrim($sql, ';') . ',';
                    }
                    // dump($sql);
                    foreach ($data3 as $k3 => $v3) {
                        // $uid_info = Db::name('user')->where('id', $v3['uid'])->find();
                        $row['init_date']                   = $init_date;
                        $row['serial_no']                   = '915' . $v3['id'];
                        $row['exchange_code']               = $exchange_code;
                        $row['exchange_market_type']        = 1;
                        $row['biz_type']                    = $biz_type;
                        $row['exchange_fees_type']          = 3;                                                //必填
                        $product                            = getProduct($v3['pid']);
                        $row['fees_balance']                = $v3['d_zljl'] * 100;//($product['p_retail_price'] * $v3['d_sell_num'] * 100) / 2;                   //必填
                        $row['payer_mem_code']              = getMember($v3['uid'], 'm_account');
                        $row['payer_fund_account']          = getMember($v3['uid'], 'm_account');
                        // $row['payer_mem_code']              = $uid_info['m_account'];
                        // $row['payer_fund_account']          = $uid_info['m_account'];
                        $row['payee_mem_code']              = '1818691191807';
                        $row['payee_fund_account']          = '1818691191807';
                        $row['deal_id']                     = '';
                        $row['remark']                      = '';
                        $row['busi_datetime']               = date('YmdHis', $v3['d_addtime']);
                        $row['init_time']           = strtotime($init_date);
                        $from_data3[]                       = $row;
                        // $from_data3_a[]                       = $row;
                        $sql .= '("'.$init_date.'", "'.$row['serial_no'].'", "'.$exchange_code.'", "'.$row['exchange_market_type'].'", "'.$biz_type.'", "'.$row['exchange_fees_type'].'", "'.($v3['d_zljl']).'", "'.$row['payer_mem_code'].'", "'.$row['payer_fund_account'].'", "'.$row['payee_mem_code'].'", "'.$row['payee_fund_account'].'", "'.$row['deal_id'].'", "'.$row['remark'].'", "'.$row['busi_datetime'].'", "'.$row['init_time'].'" ),';
                    }
                    // dump(microtime(true)-$begintime);
                    $sql = rtrim($sql, ',') . ';';
                    $sql_none = false;
                    // dump($sql); die;
                }

                // 当天的邮费数据
                $data4 = Db::name('postage_log')->where("add_time>=$start_time and add_time<$end_time")->select();
                if(!empty($data4)){
                	if(substr($sql, -1) == '@'){
                        $sql = rtrim($sql, '@');
                    }elseif(substr($sql, -1) == ';'){
                        $sql = rtrim($sql, ';') . ',';
                    }
                    foreach($data4 as $k => $v){
                        $row['init_date'] = $init_date;
                        $row['serial_no'] = '916' . $v['id'];
                        $row['exchange_code'] = $exchange_code;
                        $row['exchange_market_type'] = 1;
                        $row['biz_type'] = 1;
                        $row['exchange_fees_type'] = 3;  
                        $row['fees_balance'] = $v['postage_money']*100;
                        $row['payer_mem_code'] = $v['from_user'];
                        $row['payer_fund_account'] = $v['from_user'];
                        $row['payee_mem_code'] = $v['to_user'];
                        $row['payee_fund_account'] = $v['to_user'];
                        $row['deal_id'] = '';
                        $row['remark'] = '';
                        $row['busi_datetime'] = $v['l_time'];
                        $row['init_time'] = time();
                        $from_data4[] = $row;
                        $sql .= '("'.$init_date.'", "'.$row['serial_no'].'", "'.$exchange_code.'", "'.$row['exchange_market_type'].'", "'.$biz_type.'", "'.$row['exchange_fees_type'].'", "'.intval($v['postage_money']).'", "'.$row['payer_mem_code'].'", "'.$row['payer_fund_account'].'", "'.$row['payee_mem_code'].'", "'.$row['payee_fund_account'].'", "'.$row['deal_id'].'", "'.$row['remark'].'", "'.$row['busi_datetime'].'", "'.strtotime($init_date).'" ),';
                    }
                    $sql = rtrim($sql, ',') . ';';
                    $sql_none = false;
                }

                // dump($sql);
                // die;
                $from_data = array_merge($from_data1, $from_data2, $from_data3, $from_data4);

                // dump($sql);
                // die;
            
                // $from_data_a = array_merge($from_data1_a, $from_data2_a, $from_data3_a);
            } elseif ($fields['file_type'] == '4') {
                //5.结算价文件
                $fields_type = 'IB';
                $data    = Db::name('Product')->where(array('p_setup_status' => 1))->select();
                $sql = 'INSERT INTO `w_file_settlement_price` (`init_date`, `exchange_code`, `exchange_market_type`, `product_category_id`, `product_code`, `money_type`, `settle_price`, `init_time`) VALUES ';
                $sql_none = true;
                if (!empty($data)) {
                    foreach ($data as $k => $v) {
                        $row['init_date']           = $init_date;
                        $row['exchange_code']       = $exchange_code;
                        $row['exchange_market_type'] = 1;                   //必填
                        $row['product_category_id'] = 0;                   //非必填
                        $row['product_code']        = $v['p_code'];
                        $row['money_type']          = $money_type[0]['type'];
                        $row['settle_price']        = $v['p_retail_price'] * 100;
                        $row['init_time']           = strtotime($init_date);
                        $from_data[]                = $row;
                        // $row['settle_price']        = $v['p_retail_price'];
                        // $from_data_a[]                = $row;
                        $sql .= '("'.$init_date.'", "'.$exchange_code.'", "'.$row['exchange_market_type'].'", "'.$row['product_category_id'].'", "'.$row['product_code'].'", "'.$row['money_type'].'", "'.$v['p_retail_price'].'", "'.$row['init_time'].'" ) , ';
                    }
                    // dump(microtime(true)-$begintime);
                    $sql = rtrim($sql, ' , ') . ' ;';
                    $sql_none = false;
                    // dump($sql);die;
                }
                $f_title = $init_date . '_' . $exchange_code . '_closeprice.txt';
            } elseif ($fields['file_type'] == '5') {
                // 执行存储过程

                $_startTime = $start_time;
                // $_endTime = $end_time-1;
                $_date = $init_date;
                // 返回的数据
                $result = ['code' => 200, 'msg' => '全部清算成功', 'count' => 0];

                // 修改配置表is_qs清算字段
                Db::name('config')->where('id', 1)->update(['is_qs'=>1]);

                if(empty($fields['set_file'])){

                    Db::query(' call qingsuan(:startTime,:date)', [
                        'startTime' => $_startTime,
                        'date' => $_date,
                    ]);

                    $count = Db::execute($table_sql);
                    $result['count'] = $count;

                    // if($resultSet){
                    //     $result['count'] = count(!empty($resultSet[0])?$resultSet[0]:0);
                    // }else{
                    //     $result['count'] = 0;
                    // }
                }else{
                    
                    $count = Db::execute($table_sql);

                    $result['code'] = 201;
                    $result['count'] = $count;
                }

                return $result;
            }
           
            $fields['f_title']                  =   $f_title;
            $fields['f_path']                   =   $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/static/open_bank/';
            $path                               =   './static/open_bank/' . $fields['f_title'];
            //            if(empty($from_data)){
            //                $this->error('您选择报送的数据为空');
            //            }
            ########循环写入文件#########
            //            if(file_exists($path)){
            //                $copy_data          = array();
            //                $set_data           = array();
            //                $file_data          = fopen($path, "r");
            //                $i                  = 0;
            //                while(!feof($file_data)) {
            //                    $copy_data[$i]   = fgets($file_data);//fgets()函数从文件指针中读取一行
            //                    $i++;
            //                }
            //                fclose($file_data);
            //                $copy_data   = array_filter($copy_data);
            //                for($i=0;$i<count($copy_data);$i++){
            //                    $un_data       = explode('|',$copy_data[$i]);
            //                    $set_data[]    = $un_data;
            //                }
            //                $from_data = array_merge($from_data,$set_data);
            //            }
            
            // 生成报送文件
            if(!empty($fields['set_file']) && $fields['set_file'] == 1){

                $file                        = fopen($path, 'w');

                foreach ($from_data as $k => $v) {
                    $sta_data = implode('|', $v);
                    if (file_exists($path)) {
                        file_put_contents($path, $sta_data . PHP_EOL, FILE_APPEND);
                    } else {
                        if ($file) {
                            fwrite($file, $sta_data);
                        }
                    }
                }
            }

            // fclose($file);
            //$this->success('报送成功');
            ########循环写入文件#########
            $fields['f_path']                   = $fields['f_path'] . $fields['f_title'];
            $fields['f_start_time']             = $start_time;
            $fields['f_end_time']               = $end_time;
            $fields['f_date']                   = $init_date;
            $fields['admin_id']                 = $admin_id;
            $fields['f_addtime']                = $time;
            $fields['f_status']                 = 0;
            $fields['f_identity']               = 1;
            $fields['f_read']                   = 1;
            $fields['f_type']                   = $fields_type;
            $has_flogs  = Db::name('Flogs')->where(array('f_type' => $fields_type, 'f_date' => $date, 'f_status' => 0))->find();
            if (!empty($has_flogs)) {
                $res_id = Db::name('Flogs')->where(array('id' => $has_flogs['id']))->update($fields);
            } else {
                $res_id = Db::name('Flogs')->insertGetId($fields);
            }
            if ($res_id) {

                // 判断类型 并插入数据库
                if ($fields['file_type'] == '0') {
                    if(!$sql_none){
                        // Db::name('file_transaction_data')->where('init_time', strtotime($init_date))->delete();
                        $del_sql = 'DELETE FROM w_file_transaction_data WHERE `init_time` = ' . strtotime($init_date);
                        Db::execute($del_sql);
                        // Db::name('file_transaction_data')->insertAll($from_data_a);
                        Db::execute($sql);
                    }
                } elseif ($fields['file_type'] == '1') {
                    if(!$sql_none){
                        // Db::name('file_fund_balance')->where('init_time', strtotime($init_date))->delete();
                        $del_sql = 'DELETE FROM w_file_fund_balance WHERE `init_time` = ' . strtotime($init_date);
                        Db::execute($del_sql);
                        // Db::name('file_fund_balance')->insertAll($from_data_a);
                        Db::execute($sql);
                    }
                } elseif ($fields['file_type'] == '2') {
                    if(!$sql_none){
                        // Db::name('file_position_details')->where('init_time', strtotime($init_date))->delete();
                        $del_sql = 'DELETE FROM w_file_position_details WHERE `init_time` = ' . strtotime($init_date);
                        Db::execute($del_sql);
                        // Db::name('file_position_details')->insertAll($from_data_a);
                        Db::execute($sql);
                    }
                } elseif ($fields['file_type'] == '3') {
                    if(!$sql_none){
                        // Db::name('file_external_expenses_details')->where('init_time', strtotime($init_date))->delete();
                        $del_sql = 'DELETE FROM w_file_external_expenses_details WHERE `init_time` = ' . strtotime($init_date);
                        Db::execute($del_sql);
                        // Db::name('file_external_expenses_details')->insertAll($from_data_a);
                        Db::execute($sql);
                    }
                } elseif ($fields['file_type'] == '4') {
                    if(!$sql_none){
                        // Db::name('file_settlement_price')->where('init_time', strtotime($init_date))->delete();
                        $del_sql = 'DELETE FROM w_file_settlement_price WHERE `init_time` = ' . strtotime($init_date);
                        Db::execute($del_sql);
                        // Db::name('file_settlement_price')->insertAll($from_data_a);
                        Db::execute($sql);
                    }
                }
                
                // 第一次导入银行的两个文件
                if($fields['file_type'] == '0'){
                    import_bank_check(0, $init_date, $exchange_code);
                    import_bank_check(1, $init_date, $exchange_code);
                }

                $this->success('已清算');
               
            } else {
                $this->error('清算失败');
            }

            
        }


        // 清算完成按钮是否显示
        $config = Db::name('config')->where('id', 1)->find();
        $this->assign('config', $config);

        // 下边的表格是否显示
        $table_num = Db::execute($table_sql);
        $this->assign('table_num', $table_num);

        // 判断文件是否存在并且判断文件状态等
        $file_status = un_file_is_qs($init_date, $exchange_code);
        $this->assign('file_status', $file_status);

        return $this->fetch();

        // return ZBuilder::make('form')
        //     ->setPageTitle('交易所文件报送')          // 设置页面标题
        //     ->addFormItems([                         // 批量添加表单项
        //         ['radio', 'f_type', '报送文件类型', '', $file_type, 'I2'],
        //         ['radio', 'f_resend', '重要标识', '', array('否', '是'), 0],
        //         ['datetime', 'f_start_time', '开始时间'],
        //         ['datetime', 'f_end_time', '结束时间'],
        //     ])
        //     ->layout(['f_title' => 4, 'f_start_time' => 3, 'f_end_time' => 3])
        //     ->fetch();
    }


    // 提交联交运
    public function submitFile()
    {
        $time                   = time();
        $date                   = date('Y-m-d', $time);
        $init_date              = date('Ymd');              //业务时间
        $exchange_code          = '02000016';               //交易所代码


        
        do_alogs('交易所文件报送设置');
        $fields         =   input('post.');

        if (!$fields['f_start_time'] || !$fields['f_end_time']) {
            $fields['f_start_time']         =   $date;
            $fields['f_end_time']           =   date("Y-m-d", strtotime("+1 day"));
        } else {
            $init_date                      =   date('Ymd', strtotime($fields['f_start_time']));
        }
        
        $start_time = strtotime($fields['f_start_time']);
        $end_time   = strtotime($fields['f_end_time']);
        $from_data  = array();

        // 判断是否清算
        if($this->getConfig('is_qs') !== 2){
            $this->error('未完成清算，请到清算中心进行清算');
        }

        $date = date('Y-m-d', !empty($fields['f_start_time'])?strtotime($fields['f_start_time']):$time);
        //$date = '2020-09-03';
        //$init_date = '20200903';
        if ($fields['file_type'] == '0') {
            // 1.成交数据文件
            $fields_type = 'I2';
            $f_title = $init_date . '_' . $exchange_code . '_deal.txt';
        } elseif ($fields['file_type'] == '1') {
            //2.资金余额文件
            $fields_type = 'I3';
            $f_title = $init_date . '_' . $exchange_code . '_memberFund.txt';
        } elseif ($fields['file_type'] == '2') {
            //3.持仓明细文件
            $fields_type = 'I6';
            $f_title = $init_date . '_' . $exchange_code . '_memberPositionDetail.txt';
        } elseif ($fields['file_type'] == '3') {
            //4.外部费用明细文件
            $fields_type = 'I9';
            $f_title = $init_date . '_' . $exchange_code . '_fees.txt';
        } elseif ($fields['file_type'] == '4') {
            //5.结算价文件
            $fields_type = 'IB';
            $f_title = $init_date . '_' . $exchange_code . '_closeprice.txt';
        } elseif ($fields['file_type'] == '5') {
            return ['code' => 200, 'msg' => '全部报送成功'];
        }

        $f_path = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/static/open_bank/';

        $file_path = $f_path . $f_title;

        $has_flogs  = Db::name('Flogs')->where(array('f_type' => $fields_type, 'f_date' => $date, 'f_status' => 0))->find();

        if(empty($has_flogs)){
            $has_flogs = Db::name('Flogs')->where(array('f_type' => $fields_type, 'f_date' => $date, 'f_status' => 1))->find();
            if($has_flogs){
                $this->success('已报送');
            }
            $has_flogs  = Db::name('Flogs')->where(array('f_type' => $fields_type, 'f_date' => $date, 'f_status' => 2))->find();
            if(empty($has_flogs)){
                $this->error('请先清算后再提交');
            }
        }

        $Api = new Api();
        $api_data                           = array(
            'file_path'                     => $file_path,
            'init_date'                     => $init_date,
            'busi_datetime'                 => $init_date . date('His'),
            'file_name'                     => $f_title,
            'resend'                        => $fields['f_resend'],
            'file_md5'                      => md5_file($file_path),
        );
        // $this->success('已报送');
        $res        = $Api->file_upload_notify($api_data, $fields_type);
        $msg_arr    = json_decode($res, true);

        // $msg_arr['error_no']=0 表示提交联交运成功
        if (isset($msg_arr['error_no']) && $msg_arr['error_no'] == 0) {
            // 发送成功
            Db::name('Flogs')->where(array('id' => $has_flogs['id']))->update(array('f_status' => 1));
            $this->success('已报送');
        } else {
            Db::name('Flogs')->where(array('id' => $has_flogs['id']))->update(array('f_status' => 2));
            $this->error(isset($msg_arr['error_info'])?$msg_arr['error_info']:'报送失败');
        }

    }

    // 点击清算完成按钮
    public function set_is_qs()
    {
        $res = Db::name('config')->where('id', 1)->update(['is_qs' => 2]);
        return ['code' => 200, 'msg' => '操作成功'];
    }

    // 获取清算后的数据
    public function getQingsaunData()
    {

        // $table_sql = 'SELECT * FROM w_user_report_qs WHERE m_balance <> qsye';

        $page = 1;

        $param = $this->request->param();

        if(!empty($param['page'])){
            $page = $param['page'];
        }

        $where = 'm_balance <> qsye or qsye < 0';

        $count = Db::name('user_report_qs')->where($where)->count();

        $list = Db::name('user_report_qs')->where($where)->limit(20)->page($page)->select();

        return [
            'code' => 0,
            'msg' => '',
            'count' => $count,
            'data' => $list,
        ];
    }





    public function notice_list()

    {

        do_alogs('查看公告列表');

        $map        = $this->getMap();

        $order      = $this->getOrder('n_time desc');

        $data_list  = Db::name('Notice')->where($map)->order($order)->limit(20)->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('notice')

            ->setSearch(['id' => 'ID', 'n_title' => '标题'])                        // 设置搜索参数

            ->addTimeFilter('n_time', '', '开始时间,结束时间')        // 添加时间段筛选

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['n_title', '标题', 'text'],

                ['n_img', '图片', 'img_url'],

                ['n_auth', '浏览人数', 'text'],

                ['n_index', '排序', 'text'],

                ['n_time', '创建时间', 'datetime'],

                ['last_time', '上次更新时间', 'datetime'],

                ['right_button', '操作', 'btn']

            ])

            ->setExtraCss($css)

            ->setPrimaryKey('id')

            ->addOrder('id,n_index,n_auth')                                                               //添加排序

            ->addTopButton('add', ['href' => url('notice_add')])

            ->addTopButton('delete', ['href' => url('notice_del')])                                             // 添加顶部按钮

            ->addRightButtons([

                'edit'  => ['title' => '编辑', 'href' => url('notice_edit', ['id' => '__id__'])],

                'delete' => ['title' => '删除', 'href' => url('notice_del', ['id' => '__id__'])]

            ])

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }



    public function notice_add()

    {

        $admin_id       = session('user_auth')['uid'];

        if ($this->request->post()) {

            do_alogs('添加公告信息');

            $fields         =   input('post.');

            if (!$fields['n_title']) {

                $this->error('公告标题不能为空');

            }

            if (!$fields['n_content']) {

                $this->error('公告内容不能为空');

            }

            if (!$fields['n_img']) {

                $this->error('公告封面不能为空');

            }

            $fields['n_img']    = get_file_path($fields['n_img']);

            $fields['admin_id'] = $admin_id;

            $fields['n_time']   = time();

            $notice_id       = Db::name('Notice')->insertGetId($fields);

            if ($notice_id) {

                $this->success('添加成功', url('notice/notice_list'));

            } else {

                $this->error('发生未知错误，请稍后重试');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('新增')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['text', 'n_title', '标题', '(请输入公告标题)'],

                ['image', 'n_img', '公告封面', '(请输入公告标题)'],

                ['number', 'n_index', '公告排序', '(请输入公告排序)'],

                ['ckeditor', 'n_content', '公告内容', '(请输入公告内容)'],

            ])

            ->fetch();

    }



    public function notice_edit($id = '')

    {

        $data   =    Db::name('Notice')->where(array('id' => $id))->find();

        if ($this->request->post()) {

            do_alogs('编辑公告信息');

            $fields         =   input('post.');

            if (!$fields['n_title']) {

                $this->error('公告标题不能为空');

            }

            if (!$fields['n_content']) {

                $this->error('公告内容不能为空');

            }

            if (!$fields['n_img']) {

                $this->error('公告封面不能为空');

            }

            $fields['n_img']        = setImagePath($fields['n_img']);

            $fields['last_time']    = time();

            $notice_id              = Db::name('Notice')->where(array('id' => $id))->update($fields);

            if ($notice_id) {

                $this->success('编辑成功', url('notice/notice_list'));

            } else {

                $this->error('发生未知错误，请稍后重试');

            }

        }

        return ZBuilder::make('form')

            ->setPageTitle('编辑')          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['hidden', 'id'],

                ['text', 'n_title', '标题', '(请输入公告标题)'],

                ['image', 'n_img', '公告封面', '(请输入公告标题)'],

                ['number', 'n_index', '公告排序', '(请输入公告排序)'],

                ['ckeditor', 'n_content', '公告内容', '(请输入公告内容)'],

            ])

            ->setFormData($data)

            ->fetch();

    }



    public function notice_del()

    {

        do_alogs('删除公告信息');

        if ($this->request->isPost()) {

            $data   = input('post.');

            $ids    = implode($data['ids'], ',');

            $is_del = Db::name('Notice')->where(array('id' => array('in', $ids)))->delete();



            if ($is_del) {

                $this->success('删除成功', url('Notice/notice_list'));

            } else {

                $this->error('删除失败');

            }

        }

        $notice_id = input('id');

        $is_del = Db::name('Notice')->where(array('id' => $notice_id))->delete();

        if ($is_del) {

            $this->success('删除成功', url('Notice/notice_list'));

        } else {

            $this->error('删除失败');

        }

    }



    public function about_list()

    {

        do_alogs('查看文本信息');

        $type   =   array(1 => '关于我们', 2 => '用户协议', 3 => '弹窗公告', 4 => '代理说明');

        $map        = $this->getMap();

        $data_list  = Db::name('About')->where($map)->limit(20)->paginate();

        $css        = "<style>

            tr,th,td{text-align: center;}

</style>";

        // 使用ZBuilder快速创建数据表格

        return ZBuilder::make('table')

            ->setTableName('about')

            ->setSearch(['id' => 'ID', 'n_title' => '标题'])                        // 设置搜索参数

            ->addColumns([ // 批量添加数据列

                ['id', 'ID'],

                ['n_title', '标题', 'text'],

                ['n_desc', '描述', 'text'],

                ['id', '分类', 'status', '', $type],

                ['right_button', '操作', 'btn']

            ])

            ->setExtraCss($css)

            ->hideCheckbox()

            ->setPrimaryKey('id')

            ->addOrder('id')                                                               //添加排序

            ->addRightButtons(['edit'  => ['title' => '编辑', 'href' => url('about_edit', ['id' => '__id__'])]])

            ->setRowList($data_list) // 设置表格数据

            ->fetch(); // 渲染模板

    }

    public function about_edit($id = '')

    {

        $data   =   Db::name('About')->where(array('id' => $id))->find();

        $type   =   array(1 => '关于我们', 2 => '用户协议', 3 => '弹窗公告', 4 => '代理说明', 5 => '风险提示');

        if ($this->request->post()) {

            do_alogs('修改文本信息');

            $fields         =   input('post.');

            if (!$fields['n_desc']) {

                $this->error('描述不能为空');

            }

            if (!$fields['n_content']) {

                $this->error('内容不能为空');

            }

            $res = Db::name('About')->where(array('id' => $id))->update($fields);

            if ($res) {

                $this->success('编辑成功', url('Notice/about_list'));

            } else {

                $this->error('编辑失败');

            }

        }

        $title  = $type[$id];

        return ZBuilder::make('form')

            ->setPageTitle($title)          // 设置页面标题

            ->addFormItems([                // 批量添加表单项

                ['hidden', 'id'],

                ['textarea:6', 'n_desc', '描述', '(请输入描述)'],

                ['ckeditor', 'n_content', '内容', '(请输入内容)'],

            ])

            ->setFormData($data)

            ->fetch();

    }


    // time_type=1 当天的 不等于1 前一天的
    public function getWhereTime($dbname = '', $time_type = 1)
    {

        $map = [];
        $day_time = 86400;

        $filter_time = 'init_time';
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');

        if($filter_time_from != '' && $filter_time_to != ''){

            $start_time = strtotime($filter_time_from);
            $end_time =strtotime($filter_time_to)+$day_time;

            $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);

        }else{
            
            if($time_type === 1){

                // 查询当天的数据
                $start_time = strtotime(date('Y-m-d'));
                $end_time = $start_time + $day_time+$day_time;

                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);

            }else{
                
                // 查询上一个有数据的那天的
                $start_time = Db::name($dbname)->max($filter_time);
                $end_time = $start_time + $day_time;

                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
            
        }

        return $map;

    }

// 成交数据列表
    public function file_transaction_data()
    {
        $dbname = 'file_transaction_data';
        $map = [];
        $day_time = 86400;
        $order      = $this->getOrder('id desc');
        $search_field     = input('param.search_field/s', '', 'trim');
        $keyword          = input('param.keyword/s', '', 'trim');
        $filter_time = 'init_time';
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');

        // 搜索框搜索
        if ($search_field != '' && $keyword != '') {

            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            if(substr_count($search_field, 'open_mem_code|opp_mem_code')){
                $search_field_s = 'open_mem_code|open_fund_account|open_trade_account|opp_mem_code|opp_fund_account|opp_trade_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'open_mem_code')){
                $search_field_s = 'open_mem_code|open_fund_account|open_trade_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'opp_mem_code')){
                $search_field_s = 'opp_mem_code|opp_fund_account|opp_trade_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }
        }else{
            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time = strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }else{
                // 查询当天的数据
                $start_time = strtotime(date('Y-m-d'));
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
        }

        $data_list_count = Db::name($dbname)->where($map)->count();

        if($data_list_count > 0){
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }else{
            if ($search_field == '' && $keyword == '' && $filter_time_from == '' && $filter_time_to == '') {
                unset($map[0]);
                // 查询上一个有数据的那天的
                $start_time = Db::name($dbname)->max($filter_time);
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }

        $deal_price_count = Db::name($dbname)->where($map)->sum('deal_price');
        $deal_total_price_count = Db::name($dbname)->where($map)->sum('deal_total_price');

        $time = time();
        if(isset($_SESSION['tblqry'])){
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}else{ $qrycache = []; } 
        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('file_transaction_data')
            ->hideCheckbox()
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('file_transaction_data_export', ['uniqkey' => $uniqkey])])
            ->addTimeFilter('init_time','','开始时间,结束时间')
            ->setSearch(['open_mem_code' => '买入方', 'opp_mem_code' => '卖出方']) // 设置搜索参数
            ->addColumns([
                ['id', 'ID'],
                ['init_date', '业务时间', 'text'],
                ['exchange_code', '交易所代码', 'text'],
                // ['exchange_market_type', '市场兑换类型', 'text'],
                // ['biz_type', 'biz类型'],
                ['deal_id', '成交编号'],
                ['open_mem_code', '会员编码(发起方)'],
                ['open_fund_account', '资金账号（发起方）'],
                ['open_trade_account', '交易账号(发起方)'],
                ['opp_mem_code', '会员编码(对手方)'],
                ['opp_fund_account', '资金账号(对手方)'],
                ['opp_trade_account', '交易账号(对手方)'],
                ['product_category_id', '产品分类ID'],
                ['product_code', '产品代码'],
                ['entrust_bs', '买卖方向', 'status', '', [1 => '买', 2 => '卖']],
                ['deal_type', '成交类型', 'status', '', [1 => '开仓(建仓)', 2 => '平仓']],
                ['opp_deal_type', '对手方成交类型', 'status', '', [1 => '开仓(建仓)', 2 => '平仓']],
                ['hpsp_trade_type', '交易类型', 'status', '', [0 => '默认(市商)', 1 => '协议开仓', 2 => '协议平仓', 3 => '协议换手', 4 => '强平', 5 => '现金交割']],
                ['deal_quantity', '成交数量('.$deal_price_count.')'],
                ['deal_price', '成交单价'],
                ['deal_total_price', '成交总价('.$deal_total_price_count.')'],
                // ['a_deal_price', '成交单价'], // /100显示
                // ['a_deal_total_price', '成交总价'], // /100显示
                // ['deposit_rate', 'deposit_rate'],
                // ['deposit_ratio_type', 'deposit_ratio_type'],
                // ['deposit_type', 'deposit_type'],
                // ['deposit_balance', 'deposit_balance'],
                // ['receipt_quantity', 'receipt_quantity'],
                // ['open_poundage', 'open_poundage'],
                // ['opp_poundage', 'opp_poundage'],
                ['deal_time', '成交时间'],
                ['depot_order_no', '建仓单号'],
                ['opp_depot_order_no', '对手方建仓单号'],
                // ['order_id', 'order_id'],
                // ['opp_order_id', 'opp_order_id'],
                ['settlement_date', '结算日期'],
            ])
            ->setColumnWidth('open_mem_code,open_fund_account,open_trade_account,opp_mem_code,opp_fund_account,opp_trade_account,opp_deal_type,opp_depot_order_no', 150)
            ->setColumnWidth('deal_quantity,deal_total_price', 200)
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板

    }

    // 成交数据列表导出
    public function file_transaction_data_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {

            ini_set('memory_limit', '512M');
            ini_set('max_execution_time', 0);
            
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $titleArr = ['ID', '业务时间', '交易所代码', '成交编号', '会员编码(发起方)', '资金账号(发起方)', '交易账号(发起方)', '会员编码(对手方)', '资金账号(对手方)', '交易账号(对手方)', '产品分类ID', '产品代码', '买卖方向', '成交类型', '对手方成交类型', '交易类型', '成交数量', '成交单价', '成交总价', '成交时间', '建仓单号', '对手方建仓单号', '结算日期'];
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');

                    $sql = Db::name('file_transaction_data')->where($qrycache['where'])->field([
                        'id', 'init_date', 'exchange_code',
                        Db::raw('CONCAT("' . "\t" . '", deal_id) AS deal_id'),
                        Db::raw('CONCAT("' . "\t" . '", open_mem_code) AS open_mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", open_fund_account) AS open_fund_account'),
                        Db::raw('CONCAT("' . "\t" . '", open_trade_account) AS open_trade_account'),
                        Db::raw('CONCAT("' . "\t" . '", opp_mem_code) AS opp_mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", opp_fund_account) AS opp_fund_account'),
                        Db::raw('CONCAT("' . "\t" . '", opp_trade_account) AS opp_trade_account'),
                        Db::raw('CONCAT("' . "\t" . '", product_category_id) AS product_category_id'),
                        'product_code', 
                        Db::raw('IF(`entrust_bs`=1, "买", "卖") AS entrust_bs'),
                        Db::raw('IF(`deal_type`=1, "开仓(建仓)", "平仓") AS deal_type'),
                        Db::raw('IF(`opp_deal_type`=1, "开仓(建仓)", "平仓") AS opp_deal_type'),
                        Db::raw('CASE `hpsp_trade_type` WHEN 0 THEN "默认(市商)" WHEN 1 THEN "协议开仓" WHEN 2 THEN "协议平仓" WHEN 3 THEN "协议换手" WHEN 4 THEN "强平" WHEN 5 THEN "现金交割" END AS hpsp_trade_type'),
                        'deal_quantity', 'deal_price', 'deal_total_price', 'deal_time', 'depot_order_no', 'opp_depot_order_no', 'settlement_date'
                    ])->order($qrycache['order'])->buildSql();

                    // Db::raw('IF(`exchange_market_type`=1, "佣金", IF(`exchange_market_type`=2,"其他费用","交易类费用")) AS exchange_market_type'),
                    // Db::raw('CONCAT("' . "\t" . '", biz_type) AS biz_type'),

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $totalrows = $res -> num_rows;
                        if($totalrows > 0)
                        {
        
                            $time = time();
                            $max_num = 50000;
                            if($totalrows > $max_num)
                            {
                                $res -> free();
                                $totalpg = ceil($totalrows / $max_num);
                                $zipfile = new \ZipArchive;
                                $_filename = 'file_transaction_data_' . date('YmdHis', $time);
                                if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
                                {
                                    $filenames = [];
                                    
                                    for($pg = 0; $pg < $totalpg;)
                                    {
                                        $res = $mysql -> query($sql . ' LIMIT ' . $pg++ * $max_num . ', '.$max_num);
                                        if($res && $res -> num_rows > 0)
                                        {
                                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                                            $res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $filenames[] = $filename;
                                            $fp = fopen($filename, 'w');
                                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                                            fputcsv($fp, $titleArr);
                                            array_map(function($item) use ($fp){
                                                fputcsv($fp, $item);
                                            }, $result);
                                            unset($result);
                                            fclose($fp);
                                            $zipfile -> addFile($filename);
                                        }
                                    }
                                    $mysql -> close();
                                    $zipfile -> close();
                                    $filename = $_filename . '.zip';
                                    // ob_end_clean();
                                    // ob_start();
                                    header('Content-Type: application/zip');
                                    header('Content-Disposition: filename=' . $filename);
                                    $fsize = filesize($filename);
                                    header('Content-Length: ' . $fsize);
                                    echo(file_get_contents($filename));
                                    foreach($filenames as $_filename) unlink($_filename);
                                    // sleep(3);
                                    unlink($filename);
                                    exit();
                                }
                            }
                            else
                            {
                                $result = $res -> fetch_all(MYSQLI_ASSOC);
                                $res -> free();
                                $mysql -> close();
                                // 导出csv文件
                                $filename = 'file_transaction_data_' . date('YmdHis');
                                return export_csv($filename . '.csv', $titleArr, $result);
                            }
                        }
                    }
                    $mysql -> close();
                }
            }
        }
    }


    // 资金金额列表
    public function file_fund_balance()
    {

        // $map        = $this->getMap();
        // $order      = $this->getOrder('id desc');
        // $data_list = Db::name('file_fund_balance')->where($map)->limit(20)->order($order)->paginate();

        $dbname = 'file_fund_balance';
        $map = [];
        $day_time = 86400;
        $order      = $this->getOrder('id desc');
        $search_field     = input('param.search_field/s', '', 'trim');
        $keyword          = input('param.keyword/s', '', 'trim');
        $filter_time = 'init_time';
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');

        // 搜索框搜索
        if ($search_field != '' && $keyword != '') {

            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time = strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            if(substr_count($search_field, 'mem_code')){
                $search_field_s = 'mem_code';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }
            
        }else{
            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }else{
                // 查询当天的数据
                $start_time = strtotime(date('Y-m-d'));
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
        }

        $data_list_count = Db::name($dbname)->where($map)->count();

        if($data_list_count > 0){
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }else{
            if ($search_field == '' && $keyword == '' && $filter_time_from == '' && $filter_time_to == '') {
                unset($map[0]);
                // 查询上一个有数据的那天的
                $start_time = Db::name($dbname)->max($filter_time);
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }

        $occur_balance_sum = Db::name($dbname)->where($map)->sum('occur_balance');
        $current_balance_sum = Db::name($dbname)->where($map)->sum('current_balance');

        $time = time();
        if(isset($_SESSION['tblqry'])){
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}else{ $qrycache = []; } 
        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->hideCheckbox()
            ->setTableName('file_fund_balance')
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('file_fund_balance_export', ['uniqkey' => $uniqkey])])
            ->setSearch(['mem_code' => '会员编号']) // 设置搜索参数
            ->addTimeFilter('init_time','','开始时间,结束时间')
            ->addColumns([
                ['id', 'ID'],
                ['init_date', '业务时间'],
                ['exchange_code', '交易所代码'],
                ['mem_code', '会员编码'],
                ['fund_account', '资金账号'],
                ['money_type', '金额类型'],
                ['occur_balance', '累计余额('.$occur_balance_sum.')'],
                ['current_balance', '剩余余额('.$current_balance_sum.')'],
            ])
            ->setColumnWidth('occur_balance,current_balance', 150)
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板

    }

    // 资金金额列表导出
    public function file_fund_balance_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $titleArr = ['ID', '业务时间', '交易所代码', '会员编码', '资金账号', '金额类型', '累计余额', '剩余余额'];
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');

                    $sql = Db::name('file_fund_balance')->where($qrycache['where'])->field([
                        'id', 'init_date', 'exchange_code', 
                        Db::raw('CONCAT("' . "\t" . '", mem_code) AS mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", fund_account) AS fund_account'),
                        'money_type', 
                        Db::raw('CONCAT("' . "\t" . '", occur_balance) AS occur_balance'),
                        Db::raw('CONCAT("' . "\t" . '", current_balance) AS current_balance'),
                    ])->order($qrycache['order'])->buildSql();

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $totalrows = $res -> num_rows;
                        if($totalrows > 0)
                        {

                            $time = time();
                            if($totalrows > 1e5)
                            {
                                $res -> free();
                                $totalpg = ceil($totalrows / 1e5);
                                $zipfile = new \ZipArchive;
                                $_filename = 'file_fund_balance_' . date('YmdHis', $time);
                                if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
                                {
                                    ini_set('memory_limit', '512M');
                                    ini_set('max_execution_time', 0);
                                    $filenames = [];
                                    for($pg = 0; $pg < $totalpg;)
                                    {
                                        $res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
                                        if($res && $res -> num_rows > 0)
                                        {
                                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                                            $res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $filenames[] = $filename;
                                            $fp = fopen($filename, 'w');
                                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                                            fputcsv($fp, $titleArr);
                                            array_map(function($item) use ($fp){
                                                fputcsv($fp, $item);
                                            }, $result);
                                            unset($result);
                                            fclose($fp);
                                            $zipfile -> addFile($filename);
                                            // unlink($filename);

                                        }
                                    }
                                    $mysql -> close();
                                    $zipfile -> close();
                                    $filename = $_filename . '.zip';
                                    // ob_end_clean();
                                    // ob_start();
                                    header('Content-Type: application/zip');
                                    header('Content-Disposition: filename=' . $filename);
                                    $fsize = filesize($filename);
                                    header('Content-Length: ' . $fsize);
                                    echo(file_get_contents($filename));
                                    foreach($filenames as $_filename) unlink($_filename);
                                    // sleep(3);
                                    unlink($filename);
                                    exit;
                                }
                            }
                            else
                            {
                                $result = $res -> fetch_all(MYSQLI_ASSOC);
                                $res -> free();
                                $mysql -> close();
                                // 导出csv文件
                                $filename = 'file_fund_balance_' . date('YmdHis');
                
                                return export_csv($filename . '.csv', $titleArr, $result);
                            }
                        }
                    }
                    $mysql -> close();
                }
            }
        }

    }


    // 持仓明细列表
    public function file_position_details()
    {

        // $map        = $this->getMap();
        // $order      = $this->getOrder('id desc');
        // $data_list = Db::name('file_position_details')->where($map)->limit(20)->order($order)->paginate();

        $dbname = 'file_position_details';
        $map = [];
        $day_time = 86400;
        $order      = $this->getOrder('id desc');
        $search_field     = input('param.search_field/s', '', 'trim');
        $keyword          = input('param.keyword/s', '', 'trim');
        $filter_time = 'init_time';
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');

        // 搜索框搜索
        if ($search_field != '' && $keyword != '') {

            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            if(substr_count($search_field, 'mem_code|trade_account')){
                $search_field_s = 'mem_code|trade_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'mem_code')){
                $search_field_s = 'mem_code';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'trade_account')){
                $search_field_s = 'trade_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }
            
        }else{
            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }else{
                // 查询当天的数据
                $start_time = strtotime(date('Y-m-d'));
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
        }

        $data_list_count = Db::name($dbname)->where($map)->count();
        

        if($data_list_count > 0){
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }else{
            if ($search_field == '' && $keyword == '' && $filter_time_from == '' && $filter_time_to == '') {
                unset($map[0]);
                // 查询上一个有数据的那天的
                $start_time = Db::name($dbname)->max($filter_time);
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }

        $deal_quantity_sum = Db::name($dbname)->where($map)->sum('deal_quantity');
        $left_quantity_sum = Db::name($dbname)->where($map)->sum('left_quantity');
        $present_unit_sum = Db::name($dbname)->where($map)->sum('present_unit');
        $trade_poundage_sum = Db::name($dbname)->where($map)->sum('trade_poundage');

        $time = time();
        if(isset($_SESSION['tblqry'])){
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}else{ $qrycache = []; } 
        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('file_position_details')
            ->hideCheckbox()
            ->setSearch(['mem_code' => '会员编号', 'trade_account' => '交易账户']) // 设置搜索参数
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('file_position_details_export', ['uniqkey' => $uniqkey])])
            ->addTimeFilter('init_time','','开始时间,结束时间')
            ->addColumns([
                ['id', 'ID'],
                ['init_date', '业务时间'],
                ['exchange_code', '交易所代码'],
                ['hold_id', '持仓单号'],
                ['mem_code', '会员编码'],
                ['trade_account', '交易账号'],
                ['product_category_id', '产品分类ID'],
                ['product_code', '产品代码'],
                ['entrust_bs', '是否买方向', 'status', '', [0 => '卖', 1 => '买']],
                ['deposit_way', '是否定金方式', 'status', '', [0 => '仓单', 1 => '定金']],
                ['open_price', '开仓价格'],
                ['hold_price', '持仓价格'],
                ['deal_quantity', '成交数量('.$deal_quantity_sum.')'],
                ['left_quantity', '剩余数量('.$left_quantity_sum.')'],
                ['present_unit', '数量单位('.$present_unit_sum.')'],
                ['trade_poundage', '手续费('.$trade_poundage_sum.')'],
                // ['delay_fees', '滞纳金'],
                // ['perform_balance', '履约准备金'],
                // ['deposit_rate', '定金率'],
                // ['square_profit_loss', '平仓盈亏'],
                // ['settle_profit_loss', '结算盈亏'],
                // ['settle_price', '结算价'],
                // ['deposit_ratio_type', '定金率是否比率', 'status', '', [0 => '固定', 1 => '比率']],
                ['deposit_type', '定金收取方式', 'status', '', [0 => '开仓价', 1 => '持仓价']],
                ['today_hold_flag', '是否今仓', 'status', '', [0 => '否', 1 => '是']],
                ['deal_time', '操作时间'],
            ])
            ->setColumnWidth('deal_quantity,left_quantity,present_unit,trade_poundage', 200)
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
        
    }

    // 持仓明细列表导出
    public function file_position_details_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {

            $titleArr = ['ID', '业务时间', '交易所代码', '持仓单号', '会员编码', '交易账号', '产品分类ID', '产品代码', '是否买方向', '是否定金方式', '开仓价格', '持仓价格', '成交数量', '剩余数量', '数量单位', '手续费', '定金收取方式', '是否今仓', '操作时间'];
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');

                    $sql = Db::name('file_position_details')->where($qrycache['where'])->field([
                        'id', 'init_date', 'exchange_code', 
                        Db::raw('CONCAT("' . "\t" . '", hold_id) AS hold_id'),
                        Db::raw('CONCAT("' . "\t" . '", mem_code) AS mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", trade_account) AS trade_account'),
                        Db::raw('CONCAT("' . "\t" . '", product_category_id) AS product_category_id'),
                        Db::raw('CONCAT("' . "\t" . '", product_code) AS product_code'),
                        Db::raw('IF(`entrust_bs`=0, "卖", "买") AS entrust_bs'),
                        Db::raw('IF(`deposit_way`=0, "卖", "买") AS deposit_way'),
                        'open_price', 'hold_price', 'deal_quantity', 'left_quantity', 'present_unit', 'trade_poundage',
                        Db::raw('IF(`deposit_type`=0, "开仓价", "持仓价") AS deposit_type'),
                        Db::raw('IF(`today_hold_flag`=0, "否", "是") AS today_hold_flag'),
                        Db::raw('CONCAT("' . "\t" . '", deal_time) AS deal_time'),
                    ])->order($qrycache['order'])->buildSql();

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $totalrows = $res -> num_rows;
                        if($totalrows > 0)
                        {

                            $time = time();
                            if($totalrows > 1e5)
                            {
                                $res -> free();
                                $totalpg = ceil($totalrows / 1e5);
                                $zipfile = new \ZipArchive;
                                $_filename = 'file_position_details_' . date('YmdHis', $time);
                                if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
                                {
                                    ini_set('memory_limit', '512M');
                                    ini_set('max_execution_time', 0);
                                    $filenames = [];
                                    for($pg = 0; $pg < $totalpg;)
                                    {
                                        $res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
                                        if($res && $res -> num_rows > 0)
                                        {
                                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                                            $res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $filenames[] = $filename;
                                            $fp = fopen($filename, 'w');
                                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                                            fputcsv($fp, $titleArr);
                                            array_map(function($item) use ($fp){
                                                fputcsv($fp, $item);
                                            }, $result);
                                            unset($result);
                                            fclose($fp);
                                            $zipfile -> addFile($filename);
                                            // unlink($filename);

                                        }
                                    }
                                    $mysql -> close();
                                    $zipfile -> close();
                                    $filename = $_filename . '.zip';
                                    // ob_end_clean();
                                    // ob_start();
                                    header('Content-Type: application/zip');
                                    header('Content-Disposition: filename=' . $filename);
                                    $fsize = filesize($filename);
                                    header('Content-Length: ' . $fsize);
                                    echo(file_get_contents($filename));
                                    foreach($filenames as $_filename) unlink($_filename);
                                    // sleep(3);
                                    unlink($filename);
                                    exit;
                                }
                            }
                            else
                            {
                                $result = $res -> fetch_all(MYSQLI_ASSOC);
                                $res -> free();
                                $mysql -> close();
                                // 导出csv文件
                                $filename = 'file_position_details_' . date('YmdHis');
                
                                return export_csv($filename . '.csv', $titleArr, $result);
                            }
                        }
                    }
                    $mysql -> close();
                }
            }
        }

    }


    // 外部费用明细列表
    public function file_external_expenses_details()
    {
        
        // $map        = $this->getMap();
        // $order      = $this->getOrder('id desc');
        // $data_list = Db::name('file_external_expenses_details')->where($map)->limit(20)->order($order)->paginate();

        $dbname = 'file_external_expenses_details';
        $map = [];
        $day_time = 86400;
        $order      = $this->getOrder('id desc');
        $search_field     = input('param.search_field/s', '', 'trim');
        $keyword          = input('param.keyword/s', '', 'trim');
        $filter_time = 'init_time';
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');
        $select_field     = input('param._select_field/s', '', 'trim');
        $select_value     = input('param._select_value/s', '', 'trim');

        // 搜索框搜索
        if ($search_field != '' && $keyword != '') {

            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }

            if(substr_count($search_field, 'payer_mem_code|payee_mem_code')){
                $search_field_s = 'payer_mem_code|payer_fund_account|payee_mem_code|payee_fund_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'payer_mem_code')){
                $search_field_s = 'payer_mem_code|payer_fund_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }elseif(substr_count($search_field, 'payee_mem_code')){
                $search_field_s = 'payee_mem_code|payee_fund_account';
                $map[] = [$search_field_s, 'like', "%$keyword%"];
            }
            
        }else{
            if($filter_time_from != '' && $filter_time_to != ''){
                $start_time = strtotime($filter_time_from);
                $end_time =strtotime($filter_time_to) + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }else{
                // 查询当天的数据
                $start_time = strtotime(date('Y-m-d'));
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
        }

        // 下拉筛选
        if ($select_field != '') {
            $select_field = array_filter(explode('|', $select_field), 'strlen');
            $select_value = array_filter(explode('|', $select_value), 'strlen');
            foreach ($select_field as $key => $item) {
                if ($select_value[$key] != '_all') {
                    // $map[] = [$item, '=', $select_value[$key]];
                    $map[] = Db::raw($item . ' = ' . $select_value[$key]);
                }
            }
        }


        $data_list_count = Db::name($dbname)->where($map)->count();

        if($data_list_count > 0){
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }else{
            if ($search_field == '' && $keyword == '' && $filter_time_from == '' && $filter_time_to == '') {
                unset($map[0]);
                // 查询上一个有数据的那天的
                $start_time = Db::name($dbname)->max($filter_time);
                $end_time = $start_time + $day_time;
                $map[] = Db::raw($filter_time . ' >= ' . $start_time . ' AND ' . $filter_time .' < ' . $end_time);
            }
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }

        $fees_balance_sum = Db::name($dbname)->where($map)->sum('fees_balance');

        $time = time();
        if(isset($_SESSION['tblqry'])){
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}else{ $qrycache = []; } 
        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->hideCheckbox()
            ->setTableName('file_external_expenses_details')
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('file_external_expenses_details_export', ['uniqkey' => $uniqkey])])
            ->addTopSelect('exchange_fees_type', '兑换费类型',[ 2 => '手续费', 3 => '截流款'])
            ->addTimeFilter('init_time','','开始时间,结束时间')
            ->setSearch(['payer_mem_code' => '付费方', 'payee_mem_code' => '收款方']) // 设置搜索参数
            ->addColumns([
                ['id', 'ID'],
                ['init_date', '业务时间'],
                ['serial_no', '费用流水号'],
                ['exchange_code', '交易所代码'],
                ['exchange_market_type', '费用类型', 'status', '', [1 => '佣金', 2 => '其他费用', 3 => '交易类费用']],
                ['biz_type', '业务类型'],
                ['exchange_fees_type', '兑换费类型', 'status', '', [2 => '手续费', 3 => '截流款']],
                ['fees_balance', '费用金额('.$fees_balance_sum.')'],
                ['payer_mem_code', '付费会员编码'],
                ['payer_fund_account', '付款资金账号'],
                ['payee_mem_code', '收款会员编码'],
                ['payee_fund_account', '收款资金账号'],
                // ['deal_id', 'deal_id'],
                // ['remark', 'remark'],
                ['busi_datetime', '操作日期'],
            ])
            ->setColumnWidth('fees_balance', 200)
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
        
    }

    // 外部费用明细导出
    public function file_external_expenses_details_export()
    {

        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $titleArr = [ 'ID', '业务时间', '费用流水号', '交易所代码', '费用类型', '业务类型', '兑换费类型', '费用金额', '付费会员编码', '付款资金账号', '收款会员编码', '收款资金账号', '操作日期' ];

            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    $mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
                    if($mysql -> connect_error)
                    {
                        header('Content-Type: application/json; charset=utf-8');
                        exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
                    }
                    $mysql -> query('SET NAMES UTF8');

                    $sql = Db::name('file_external_expenses_details')->where($qrycache['where'])->field([
                        'id', 'init_date', 
                        Db::raw('CONCAT("' . "\t" . '", serial_no) AS serial_no'),
                        'exchange_code',
                        Db::raw('IF(`exchange_market_type`=1, "佣金", IF(`exchange_market_type`=2,"其他费用","交易类费用")) AS exchange_market_type'),
                        Db::raw('CONCAT("' . "\t" . '", biz_type) AS biz_type'),
                        Db::raw('CONCAT("' . "\t" . '", exchange_fees_type) AS exchange_fees_type'),
                        Db::raw('CONCAT("' . "\t" . '", fees_balance) AS fees_balance'),
                        Db::raw('CONCAT("' . "\t" . '", payer_mem_code) AS payer_mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", payer_fund_account) AS payer_fund_account'),
                        Db::raw('CONCAT("' . "\t" . '", payee_mem_code) AS payee_mem_code'),
                        Db::raw('CONCAT("' . "\t" . '", payee_fund_account) AS payee_fund_account'),
                        Db::raw('CONCAT("' . "\t" . '", busi_datetime) AS busi_datetime'),
                    ])->order($qrycache['order'])->buildSql();

                    $res = $mysql -> query($sql);
                    if($res)
                    {
                        $totalrows = $res -> num_rows;
                        if($totalrows > 0)
                        {

                            $time = time();
                            if($totalrows > 1e5)
                            {
                                $res -> free();
                                $totalpg = ceil($totalrows / 1e5);
                                $zipfile = new \ZipArchive;
                                $_filename = 'file_external_expenses_details_' . date('YmdHis', $time);
                                if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
                                {
                                    ini_set('memory_limit', '512M');
                                    ini_set('max_execution_time', 0);
                                    $filenames = [];
                                    for($pg = 0; $pg < $totalpg;)
                                    {
                                        $res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
                                        if($res && $res -> num_rows > 0)
                                        {
                                            $result = $res -> fetch_all(MYSQLI_ASSOC);
                                            $res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $filenames[] = $filename;
                                            $fp = fopen($filename, 'w');
                                            fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
                                            fputcsv($fp, $titleArr);
                                            array_map(function($item) use ($fp){
                                                fputcsv($fp, $item);
                                            }, $result);
                                            unset($result);
                                            fclose($fp);
                                            $zipfile -> addFile($filename);
                                            // unlink($filename);

                                        }
                                    }
                                    $mysql -> close();
                                    $zipfile -> close();
                                    $filename = $_filename . '.zip';
                                    // ob_end_clean();
                                    // ob_start();
                                    header('Content-Type: application/zip');
                                    header('Content-Disposition: filename=' . $filename);
                                    $fsize = filesize($filename);
                                    header('Content-Length: ' . $fsize);
                                    echo(file_get_contents($filename));
                                    foreach($filenames as $_filename) unlink($_filename);
                                    // sleep(3);
                                    unlink($filename);
                                    exit;
                                }
                            }
                            else
                            {
                                $result = $res -> fetch_all(MYSQLI_ASSOC);
                                $res -> free();
                                $mysql -> close();
                                // 导出csv文件
                                $filename = 'file_external_expenses_details_' . date('YmdHis');
                
                                return export_csv($filename . '.csv', $titleArr, $result);
                            }
                        }
                    }
                    $mysql -> close();
                }
            }
        }

    }


    // 结算价列表
    public function file_settlement_price()
    {
        
        // $map        = $this->getMap();
        // $order      = $this->getOrder('id desc');

        // $data_list = Db::name('file_settlement_price')->where($map)->limit(20)->order($order)->paginate();

        $dbname = 'file_settlement_price';
        $map        = $this->getWhereTime($dbname);
        $order      = $this->getOrder('id desc');

        $data_list_count = Db::name($dbname)->where($map)->count();

        if($data_list_count > 0){

            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
            
        }else{
            $map        = $this->getWhereTime($dbname, 2);
            $data_list = Db::name($dbname)->where($map)->limit(20)->order($order)->paginate();
        }

        $time = time();
        if(isset($_SESSION['tblqry'])){
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < $time) unset($qrycache[$_k]);
            }
        }else{ $qrycache = []; } 
        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('file_settlement_price')
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('file_settlement_price_export', ['uniqkey' => $uniqkey])])
            ->addTimeFilter('init_time','','开始时间,结束时间')
            ->hideCheckbox()
            ->addColumns([
                ['id', 'ID'],
                ['init_date', '业务时间'],
                ['exchange_code', '交易所代码'],
                ['exchange_market_type', '市场兑换类型'],
                ['product_category_id', '产品分类ID'],
                ['product_code', '产品代码'],
                ['money_type', '金额类型'],
                ['settle_price', '结算价'],
            ])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
        
    }

    // 结算价列表导出
    public function file_settlement_price_export()
    {
        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);

                    $map = $qrycache['where'];
                    $order = $qrycache['order'];

                    $data_list = Db::name('file_settlement_price')->where($map)->order($order)->select();

                    $titleArr = ['ID', '业务时间', '交易所代码', '市场兑换类型', '产品分类ID', '产品代码', '金额类型', '结算价'];

                    $dataArr = [];
                            
                    foreach ($data_list as $row) {

                        $dataArr[] = [
                            'ID' => $row['id'],
                            '业务时间' => $row['init_date'],
                            '交易所代码' => $row['exchange_code'],
                            '市场兑换类型' => $row['exchange_market_type'],
                            '产品分类ID' => $row['product_category_id'],
                            '产品代码' => $row['product_code'],
                            '金额类型' => $row['money_type'],
                            '结算价' => $row['settle_price'],
                        ];
                    }

                    $filename = '结算价报表-' . date('YmdHis');
                    return export_csv($filename . '.csv', $titleArr, $dataArr);

                }
            }
        }
    }

}

