<?php

namespace app\index\controller;

use think\Db;

class UserLevel extends Home
{
    //用户升级
    public function userLevel($user_id)
    {
        //获取该用户所有上级
        $upUser = Db::name('User')
            ->where('id', $user_id)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->value('m_line');
        if (empty($upUser)) {
            return ['code' => 0, 'msg' => '无须升级'];
        }
        $upUser = str_replace('0,', '', $upUser);
//      $upUser = str_replace(",$user_id",'',$upUser);
        $array  = explode(',', $upUser);         //转成数组
        $array  = array_reverse($array);                  //反转数组 从低级用户向上级用户升级
        foreach ($array as $value) {
            $userInfo = getUserInfo($value);
            if ($userInfo == 0 || $userInfo['m_lock'] == 1) { //删除或锁定跳过
                continue;
            }
            if (!$value) {
                continue;
            }
            //todo::开始升级 升降级实时 从低等级向高等级一级一级判断
            $total = $this->getLow($value);            //获取直推以及团队人数
            $this->up1($value);               //有效
            $this->up2($value, $total);       //初级
            $this->up3($value, $total);       //中级
            $this->up4($value, $total);       //高级
            $this->up5($value, $total);       //区域
        }
        return 1;
    }

    /**
     * 升有效
     * @param $user_id
     * @param $total
     */
    public function up1($user_id)
    {
        $user    = getUserInfo($user_id);
        if($user['m_level'] == 0){
            $is_have = Db::name('Deal')->where('uid', $user_id)->where('d_status', 2)->find();
            if (!empty($is_have) || $user['m_balance']>0) {
                return Db::name('User')->where('id', $user_id)->update(['m_level' => 1]);
            }
        }
    }

    //初级
    public function up2($user_id, $total)
    {
        $user    = getUserInfo($user_id);
        if($user['m_level']<=1){
            if ($total['low'] >= 5 && $total['total'] >= 30) {//直推和团队满足
                return Db::name('user')->where('id', $user_id)->update(['m_level' => 2]);
            }
        }
    }

    //中级
    public function up3($user_id, $total)
    {
        $user    = getUserInfo($user_id);
        if($user['m_level']<=2){
            $lowCount = Db::name('User') //初级下级
            ->where('m_tid', $user_id)
                ->where('m_level', '>', 1)
                ->where('m_del', 0)
                ->where('m_lock', 0)
                ->count();
            if ($total['low'] >= 5 && $lowCount >= 2) {
                return Db::name('User')->where('id', $user_id)->update(['m_level' => 3]);
            }
        }
    }

    //高级
    public function up4($user_id, $total)
    {
        $user    = getUserInfo($user_id);
        if($user['m_level']<=3) {
            $lowCount = Db::name('User')//初级下级
            ->where('m_tid', $user_id)
                ->where('m_level', '>', 1)
                ->where('m_del', 0)
                ->where('m_lock', 0)
                ->count();
            $inCount = Db::name('User')//中级下级
            ->where('m_tid', $user_id)
                ->where('m_level', '>', 2)
                ->where('m_del', 0)
                ->where('m_lock', 0)
                ->count();
            if ($total['low'] >= 5 && $inCount >= 2 && $lowCount >= 3) {
                return Db::name('user')->where('id', $user_id)->update(['m_level' => 4]);
            }
        }
    }


    //区域
    public function up5($user_id, $total)
    {
        $user    = getUserInfo($user_id);
        if($user['m_level']<=4) {
            $inCount = Db::name('User')//中级下级
            ->where('m_tid', $user_id)
                ->where('m_level', '>', 2)
                ->where('m_del', 0)
                ->where('m_lock', 0)
                ->count();
            $upCount = Db::name('User')//高级下级
            ->where('m_tid', $user_id)
                ->where('m_level', '>', 3)
                ->where('m_del', 0)
                ->where('m_lock', 0)
                ->count();
            if ($total['low'] >= 5 && $inCount >= 3 && $upCount >= 2) {
                return Db::name('user')->where('id', $user_id)->update(['m_level' => 5]);
            }
        }
    }

    /**
     * 获取直推人数与团队人数
     * @param $user_id
     * @return mixed
     */
    public function getLow($user_id)
    {
        $low                    = Db::name('User')->where('id', $user_id)->field('m_line,m_level,m_push_num')->find();
        $total['low']           = Db::name('User')->where('m_tid', $user_id)->where('m_level', '>=', 1)->where('m_del', 0)->where('m_lock', 0)->count();;                                                                                                                                     //直推人数
        $total['total']         = Db::name('User')->whereLike('m_line', '%,' . $user_id . ',%')->where('m_level >= 1 and m_del=0 and m_lock=0')->count();     //团队人数
        $total['user_level']    = $low['m_level'];
        return $total;
    }




}