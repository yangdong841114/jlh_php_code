<?php

namespace app\index\controller;

use app\common\controller\Common;

use think\Db;

use think\facade\Env;

use think\Session;



class Api extends Common

{

    protected $AppKey           = '327c0976-9845-4d05-811e-c2afe6f3fcea';

    protected $AppSecret        = 'a248853c-39dc-4024-9356-9a60f03a6acc';

    protected $Basic            = 'Basic';

    protected $api_url          = 'http://api.lianjiaoyun.cn/hpsp/v1/';

    protected $targetcomp_id    = '15001';    //目标机构编码

    protected $sendercomp_id    = '80109';    //发送机构编码

    protected $exchange_code    = '02000016'; //交易所编号

    //获取token

    public function setToken(){

        $bytesString        = ($this->AppKey.':'.$this->AppSecret);

        $url                = 'http://api.lianjiaoyun.cn/oauth2/oauth2/token';

        $header             = array(

            'Content-Type:application/x-www-form-urlencoded',

            'Authorization:Basic '.base64_encode($bytesString),

        );

        $field              = array('grant_type'=>'client_credentials');

        $res                = sendPost($url,$header,$field);

        $data               = json_decode($res,true);

        if(!empty($data)){

            return $data['access_token'];

        }else{

            return 0;

        }

    }



    public function getToken($type=''){

        $time               = time();

        $date               = date('Y-m-d',$time);

        $token              = Db::name('Token')->where(array('t_status'=>1))->find();       // 检查数据库中的token是否过期 如果过期了则重新获取并更新数据库

        if(!empty($token)){

            if($token['t_endtime']<$time || strtotime($token['t_date'])!=strtotime($date)){                                                     // 检查是否过期

                $new_token = array(

                    'token'             =>  $this->setToken(),

                    't_addtime'         =>  $time,

                    't_endtime'         =>  $time+64800,

                    't_date'            =>  $date,

                    't_status'          =>  1,

                );

                Db::name('Token')->where(array('id'=>$token['id']))->update(array('t_status'=>2));

                $res_id = Db::name('Token')->insertGetId($new_token);

                if($res_id){

                    return $new_token['token'];

                }

            }else{

                if($type){

                    $new_token = array(

                        'token'             =>  $this->setToken(),

                        't_addtime'         =>  $time,

                        't_endtime'         =>  $time+64800,

                        't_date'            =>  $date,

                        't_status'          =>  1,

                    );

                    Db::name('Token')->where(array('id'=>$token['id']))->update(array('token'=>$new_token['token']));

                    return $new_token['token'];

                }

            }

            return $token['token'];

        }else{

            $new_token = array(

                'token'             =>  $this->setToken(),

                't_addtime'         =>  $time,

                't_endtime'         =>  $time+64800,

                't_date'            =>  $date,

                't_status'          =>  1,

            );

            $res_id = Db::name('Token')->insertGetId($new_token);

            if($res_id){

                return $new_token['token'];

            }

        }

    }



    //开市/闭市 999000

    public function update_third_party_calendar_info($next_workday='',$type){

        if($next_workday == ''){

            $next_workday = date('Ymd',strtotime("+1 day"));

        }

        $url                    =  $this->api_url."update_third_party_calendar_info";

        $exchange_market_type   = array(

            ['type_name'  => '不区分',  'type'=>1],

            ['type_name'  => '做市交易','type'=>2],

            ['type_name'  => '挂牌交易','type'=>3],

            ['type_name'  => '电子商城','type'=>4],

        );

        $biz_type               = array(

            ['type_name'  => '不区分'  ,'type'=>1],

            ['type_name'  => '大宗商品','type'=>2],

            ['type_name'  => '金融资产','type'=>3],

            ['type_name'  => '文化产权','type'=>4],

            ['type_name'  => '邮币卡'  ,'type'=>5],

        );

        $data['init_date']              = date('Ymd');

        $data['exchange_market_type']   = $exchange_market_type[0]['type'];

        $data['biz_type']               = $biz_type[0]['type'];

        $data['close_flag']             = $type;

        $data['next_workday']           = $next_workday;

        $data['busi_datetime']          = date('YmdHis');                //业务时间

        $data['exchange_code']          = $this->exchange_code;                 //交易所编号

        $data['targetcomp_id']          = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']          = $this->sendercomp_id;                 //发送机构编码



        $access_token                   = $this->getToken();

        $header                         = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data    =            json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['data'])){

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }else{

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }

        return json_encode($arr);

    }



    //银行签到/签退 111129

    public function open_or_close_exchange($type=0){

        $url            =  $this->api_url."open_or_close_exchange";

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $operating_status= array(

            ['type_name'  => '正常','type'=>0 ],

            ['type_name'  => '日终','type'=>1 ],

            ['type_name'  => '暂停','type'=>2 ],

        );

        $data['init_date']          = date('Ymd');                  //交易时间

        $data['operating_status']   = $operating_status[$type]['type'];     //交易时间

        $data['bank_pro_code']      = $bank_pro_code[0]['type'];            //交易所编号

        $data['exchange_code']      = $this->exchange_code;                 //交易所编号

        $data['targetcomp_id']      = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']      = $this->sendercomp_id;                 //发送机构编码

        $access_token               = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data    =            json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['data'])){

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }else{

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }

        return json_encode($arr);

    }







    //开户 109100

    public function mem_user_register($data = array(),$uid){

        $url            =  $this->api_url."mem_user_register";

        $user           = Db::name('User')->where(array('id'=>$uid))->find();

        if(empty($user)){

            return json_encode(array('error_info'=>'交易商信息不存在','error_no'=>'404'));

        }

        if($user['m_real'] == 0){

            return json_encode(array('error_info'=>'未通过系统实名认证，请耐心等待','error_no'=>'404'));

        }

        if($user['m_bank_signing'] != 0){

            return json_encode(array('error_info'=>'您已完成开户,无需重复进行','error_no'=>'404'));

        }

        $member_type    = array(

            ['type_name'  => '综合类','type'=>2],

            ['type_name'  => '结算类','type'=>3],

            ['type_name'  => '经纪类','type'=>4],

            ['type_name'  => '交易类','type'=>5],

        );

        $cust_type      = array(

            ['type_name'  => '个人' ,'type'=>0],

            ['type_name'  => '机构' ,'type'=>1],

        );

        $exchange_member_status = array(

            ['type_name'  => '已签约','type'=>1 ],

            ['type_name'  => '未签约','type'=>2 ],

            ['type_name'  => '销户'  ,'type'=>3 ],

        );

        $id_kind                = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],

        );

        // 判断是机构还是个人
        if($user['is_mechanism'] == 1){
            $_cust_type = $cust_type[1]['type'];
            $_id_kind = $id_kind[2]['type']; 
            $data['legal_repr'] = $user['legal_person_name']; // 法人姓名
            $data['organ_code'] = $user['m_car_id']; // 组织机构代码
        }else{
            $_cust_type = $cust_type[0]['type'];
            $_id_kind = $id_kind[0]['type']; 
        }

        $data['member_type']            = $member_type[2]['type'];              //会员类型

        $data['cust_type']              = $_cust_type;                //个人/机构

        $data['exchange_member_status'] = $exchange_member_status[0]['type'];   //签约状态

        $data['id_kind']                = $_id_kind;                  //证件类型

        $data['up_mem_code']            = $this->exchange_code;                 //经济类填写交易所会员编号,交易类必须挂在经纪商下边

        $data['exchange_code']          = $this->exchange_code;                 //交易所编号

        $data['targetcomp_id']          = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']          = $this->sendercomp_id;                 //发送机构编码

        $access_token                   = $this->getToken();                    //获取公钥文件

        $header                         = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res            = ihttp_request($url,$data,$header);

        $return_data    = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }





    //修改开户信息资料

    public function sync_mem_user_modify($uid){

        $url                            =  $this->api_url."sync_mem_user_modify";

        $user                           =  Db::name('User')->where(array('id'=>$uid))->find();

        $id_kind                = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],

        );

        $exchange_member_status = array(

            ['type_name'  => '已签约','type'=>1 ],

            ['type_name'  => '未签约','type'=>2 ],

            ['type_name'  => '销户'  ,'type'=>3 ],

        );

        $data['full_name']              = $user['m_name'];                    //经济类填写交易所会员编号,交易类必须挂在经纪商下边

        $data['trade_account']          = $user['m_account'];                  //

        $data['id_no']                  = $user['m_car_id'];                   //

        $data['gender']                 = $user['m_sex'];                      //

        $data['id_kind']                = $id_kind[0]['type'];                  //证件类型

        $data['exchange_member_status'] = $exchange_member_status[0]['type'];   //签约状态

        $data['mem_code']               = $user['m_account'];                          //
        $data['up_mem_code']            = $this->exchange_code;                 //经济类填写交易所会员编号,交易类必须挂在经纪商下边

        $data['tel']                    = $user['m_phone'];                     //

        $data['exchange_code']          = $this->exchange_code;                 //交易所编号

        $data['targetcomp_id']          = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']          = $this->sendercomp_id;                 //发送机构编码

        $access_token                   = $this->getToken();                    //获取公钥文件

        $header                         = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );
		// file_put_contents('sync_mem_user_modify.txt', var_export($data, true).PHP_EOL, FILE_APPEND);
		// file_put_contents('sync_mem_user_modify.txt', var_export($header, true).PHP_EOL, FILE_APPEND);


        $res            = ihttp_request($url,$data,$header);
		// file_put_contents('res.txt', var_export($res, true).PHP_EOL, FILE_APPEND);
        $return_data    = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }







    //签约/解约  112122

    public function client_sign_bind_account($type=0,$uid=0){

        $url            =  $this->api_url."client_sign_bind_account";

        if($uid){

            $user           =  Db::name('User')->where(array('id'=>$uid))->find();

        }else{

            $user           =  $this->check_user();

        }

        if(empty($user)){

            return json_encode(array('error_info'=>'交易商信息不存在','error_no'=>'404'));

        }

        if($user['m_real'] == 0){

            return json_encode(array('error_info'=>'未通过系统实名认证，请耐心等待','error_no'=>'404'));

        }

        if($user['m_bank_signing'] == 0){

            return json_encode(array('error_info'=>'还未完成开户,请先完成开户','error_no'=>'404'));

        }

        $cust_type      = array(

            ['type_name'  => '个人' ,'type'=>0],

            ['type_name'  => '机构' ,'type'=>1],

        );

        $id_kind                = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],

        );

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $sign_type      = array(

            ['type_name'  => '签约','type'=>1],

            ['type_name'  => '解约','type'=>2],

            ['type_name'  => '强签','type'=>3],

            ['type_name'  => '强解','type'=>4],

            ['type_name'  => '预签约','type'=>5],

            ['type_name'  => '创建资金账户','type'=>6],

            ['type_name'  => '关闭资金账户','type'=>7],

        );

        $money_type = array(

            ['type_name'  => '人民币','type'=>0 ],

        );

        // 判断是个人还是机构 传值不同
        // 1机构
        if($user['is_mechanism'] == 1){
            $_cust_type = $cust_type[1]['type'];
            $_id_kind = $id_kind[2]['type']; 
            // $data['legal_repr'] = $user['legal_person_name']; // 法人姓名
            // $data['organ_code'] = $user['m_car_id'];
        }else{
            $_cust_type = $cust_type[0]['type'];
            $_id_kind = $id_kind[0]['type']; 
        }
        // $_cust_type = $cust_type[0]['type'];
        // $_id_kind = $id_kind[0]['type']; 

        $data['init_date']              = date('Ymd');                  //交易时间

        $data['request_id']             = randomkeys(16);               //会员类型

        $data['exchange_code']          = $this->exchange_code;                 //交易所编号

        $data['exchange_fund_account']  = $user['m_account'];                   //交易账号

        $data['bank_pro_code']          = $bank_pro_code[0]['type'];            //银行类型

        $data['bank_account']           = $user['m_bank_carid'];                //银行卡号

        $data['money_type']             = $money_type[0]['type'];               //交易币种

        $data['bank_account_name']      = $user['m_name'];                      //银行类型

        $data['busi_datetime']          = date('YmdHis');               //业务时间

        $data['cust_type']              = $_cust_type;                //个人/机构

        $data['full_name']              = $user['m_name'];                      //银行业务类型

        $data['id_kind']                = $_id_kind;                  //证件类型

        $data['id_no']                  = $user['m_car_id'];                    //证件号码

        $data['sign_type']              = $sign_type[$type]['type'];                //签约类型

        $data['open_bank_no']           = $user['m_bank_open_code'];             //银行开户行编号

        $data['targetcomp_id']          = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']          = $this->sendercomp_id;                 //发送机构编码

        $access_token                   = $this->getToken();                    //获取公钥文件

        $header                         = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res            = ihttp_request($url,$data,$header);

        $return_data    = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }





    //本行客户入金 115101

    public function ext_in_golden($data = array()){

        $url            =  $this->api_url."ext_in_golden";

        $user           = $this->check_user();

        if(empty($user)){

            return json_encode(array('error_info'=>'交易商信息不存在','error_no'=>'404'));

        }

        if($user['m_real'] == 0){

            return json_encode(array('error_info'=>'未通过系统实名认证，请耐心等待','error_no'=>'404'));

        }

        if($user['m_bank_signing'] != 1){

            return json_encode(array('error_info'=>'您还未完成签约,请先进行签约','error_no'=>'404'));

        }

        $money_type = array(

            ['type_name'  => '人民币','type'=>0 ],

        );

        $bisin_type = array(

            ['type_name'  => '普通','type'=>1 ],

            ['type_name'  => '冲正','type'=>2 ],

            ['type_name'  => '重发','type'=>3 ],

            ['type_name'  => '调账','type'=>4 ],

        );

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $cust_type      = array(

            ['type_name'  => '个人' ,'type'=>0],

            ['type_name'  => '机构' ,'type'=>1],

        );

        $id_kind        = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],



        );

        $data['occur_balance'] 	            = $data['occur_balance']*100;

        $data['request_id']                 = randomkeys(16);               //交易所流水号

        $data['exchange_fund_account']      = $user['m_account'];                  //交易账号

        $data['bank_account_name']          = $user['m_name'];                     //姓名

        $data['full_name']                  = $user['m_name'];                     //姓名

        $data['bank_account']               = $user['m_bank_carid'];               //卡号

        $data['init_date']                  = date('Ymd');                  //交易时间

        $data['cust_type']		            = $cust_type[0]['type'];                //客户类别

        $data['busi_datetime']              = date('YmdHis');                //业务时间

        $data['money_type']                 = $money_type[0]['type'];               //交易币种

        $data['bisin_type']                 = $bisin_type[0]['type'];               //银行业务类型

        $data['bank_pro_code']              = $bank_pro_code[0]['type'];            //银行产品代码

        $data['id_kind']                    = $id_kind[0]['type'];                  //证件类型

        $data['id_no']                      = $user['m_car_id'];                    //证件号码

        $data['exchange_code']              = $this->exchange_code;                 //交易所编号

        $data['targetcomp_id']              = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']              = $this->sendercomp_id;                 //发送机构编码

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $deal_data                  = array(

                'z_account'     => $data['exchange_fund_account'],

                'uid'           => $user['id'],

                'z_type'        => 1,

                'z_status'      => 0,

                'z_price'       => $data['occur_balance']/100,

                'z_num'         => $data['request_id'],

                'z_addtime'     => time(),

                'z_bank_id'     => $data['bank_account'],

                'z_fee'         => 0,

                'z_fee_price'   => 0

            );

            if(isset($data['remark']) && $data['remark']){

                $deal_data['z_desc'] = $data['remark'];

            }

            Db::name('Zlog')->insertGetId($deal_data);

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }



    //客户出金 115102

    public function ext_out_golden($data = array()){

        $url            =  $this->api_url."ext_out_golden";

        $user           =  $this->check_user();

        if(empty($user)){

            return json_encode(array('error_info'=>'交易商信息不存在','error_no'=>'404'));

        }

        if($user['m_real'] == 0){

            return json_encode(array('error_info'=>'未通过系统实名认证，请耐心等待','error_no'=>'404'));

        }

        if($user['m_bank_signing'] != 1){

            return json_encode(array('error_info'=>'您还未完成签约,请先进行签约','error_no'=>'404'));

        }





        $money_type = array(

            ['type_name'  => '人民币','type'=>0 ],

        );

        $bisin_type = array(

            ['type_name'  => '普通','type'=>1 ],

            ['type_name'  => '冲正','type'=>2 ],

            ['type_name'  => '重发','type'=>3 ],

            ['type_name'  => '调账','type'=>4 ],

        );

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $cust_type      = array(

            ['type_name'  => '个人' ,'type'=>0],

            ['type_name'  => '机构' ,'type'=>1],

        );

        $id_kind        = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],

        );

        $Api                        = new Api();

        $balance                    = $Api->cash_fee_trial(2,$data['occur_balance']);

        $balance                    = json_decode($balance,true);

        if(isset($balance['error']) && $balance['error'] == 'invalid_token'){

            $this->getToken(1);

            $arr = array(

                'error_info' => '获取令牌中,稍后重试',

                'error_no'   => "404",

            );

            return json_encode($arr);

        }

        $occur_balance              = $balance['occur_balance'];        //出入金金额

        $withdraw_balance           = $balance['withdraw_balance'];     //实际到账金额

        $out_poundage               = $balance['out_poundage'];         //手续费

        $fee_poundage               = $balance['fee_poundage'];         //手续费比率

        if($occur_balance > $user['m_balance']){ // && $user['id'] != 1753

            return json_encode(array('error_info'=>'您的出金余额不足','error_no'=>'404'));

        }

        $data['occur_balance']              = $occur_balance*100;

        $data['request_id']                 = randomkeys(16);                //交易所流水号

        $data['init_date']                  = date('Ymd');                   //交易时间

        $data['exchange_code']              = $this->exchange_code;                 //交易所编号

        $data['exchange_fund_account']      = $user['m_account'];                   //交易账号

        $data['money_type']                 = $money_type[0]['type'];               //交易币种

        $data['bisin_type']                 = $bisin_type[0]['type'];               //银行业务类型

        $data['bank_pro_code']              = $bank_pro_code[0]['type'];            //银行产品代码

        $data['bank_account_name']          = $user['m_name'];                      //姓名

        $data['bank_account']               = $user['m_bank_carid'];                //卡号

        $data['busi_datetime']              = date('YmdHis');               //业务时间

        $data['cust_type']		            = $cust_type[0]['type'];                //客户类别

        $data['full_name']                  = $user['m_name'];                      //姓名

        $data['id_kind']                    = $id_kind[0]['type'];                  //证件类型

        $data['id_no']                      = $user['m_car_id'];                    //证件号码

        $data['targetcomp_id']              = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']              = $this->sendercomp_id;                 //发送机构编码

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        // dump($data);
        // file_put_contents('aaa.txt', var_export($header, true).PHP_EOL, FILE_APPEND);
        // file_put_contents('aaa.txt', var_export($data, true).PHP_EOL, FILE_APPEND);

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        // dump($return_data);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $deal_data                  = array(

                'z_account'     => $data['exchange_fund_account'],

                'uid'           => $user['id'],

                'z_type'        => 2,

                'z_status'      => 0,

                'z_price'       => $occur_balance,

                'z_num'         => $data['request_id'],

                'z_addtime'     => time(),

                'z_bank_id'     => $data['bank_account'],

                'z_fee'         => $fee_poundage,

                'z_fee_price'   => $out_poundage,

            );



            setup_fee($user['id'],round($out_poundage,4),$fee_poundage,'出金手续费',2);

            if(isset($data['remark']) && $data['remark']){

                $deal_data['z_desc'] = $data['remark'];

            }
       
            Db::name('Zlog')->insertGetId($deal_data);
            $arr = array(

                'error_info'        => $return_data['data'][0]['error_info'],

                'error_no'          => $return_data['data'][0]['error_no'],

                'occur_balance'     => $occur_balance,

            );

        }

        // die;

        return json_encode($arr);

    }



    //客户银行信息变更 119105

    public function update_main_bank_info($data=array(), $uid = 0){

        $url            =  $this->api_url."update_main_bank_info";

        if($uid){
            $user           =  Db::name('User')->where(array('id'=>$uid))->find();
        }else{
            $user           =  $this->check_user();
        }

        if(empty($user)){

            return json_encode(array('error_info'=>'交易商信息不存在','error_no'=>'404'));

        }

        if($user['m_real'] == 0){

            return json_encode(array('error_info'=>'未通过系统实名认证，请耐心等待','error_no'=>'404'));

        }

        if($user['m_bank_signing'] != 1){

            return json_encode(array('error_info'=>'您还未完成签约,请先进行签约','error_no'=>'404'));

        }

        $id_kind        = array(

            ['type_name'  => '身份证','type'=>0],

            ['type_name'  => '营业执照','type'=>2],

            ['type_name'  => '统一社会信用代码','type'=>'z'],

            ['type_name'  => '全国组织机构代码','type'=>'P'],

        );

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $data['mobile_tel']                 = $user['m_phone'];

        $data['id_no']                      = $user['m_car_id'];                    //证件号码

        $data['id_kind']                    = $id_kind[0]['type'];                  //证件类型

        $data['request_id']                 = randomkeys(16);                //交易所流水号

        $data['init_date']                  = date('Ymd');                   //交易时间

        $data['exchange_code']              = $this->exchange_code;                 //交易所编号

        $data['exchange_fund_account']      = $user['m_account'];                   //交易账号

        $data['bank_pro_code']              = $bank_pro_code[0]['type'];            //银行产品代码

        $data['bank_account_name']          = $user['m_name'];                      //银行户主名称

        $data['full_name']                  = $user['m_name'];                      //银行户主名称

        $data['targetcomp_id']              = $this->targetcomp_id;                 //目标机构编码

        $data['sendercomp_id']              = $this->sendercomp_id;                 //发送机构编码

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $i_info = array(

                'uid'                   => $user['id'],

                'i_type'                => 1,

                'i_old_open_bank_no'    => $user['m_bank_open_code'],

                'i_old_bank_carid'      => $user['m_bank_carid'],

                'i_new_open_bank_no'    => $data['open_bank_no'],

                'i_new_bank_carid'      => $data['bank_account'],

                'i_code'                => $data['request_id'],

                'i_status'              => 0,

                'i_addtime'             => time(),

            );

            Db::name('Ilogs')->insertGetId($i_info);

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }



    //交易所报送文件通知 169000

    public function file_upload_notify($data=array(),$type='I1'){

        $url            =  $this->api_url."file_upload_notify";

        /*

        客户信息变更文件

        成交数据文件

        资金余额文件

        成交清算文件

        持仓汇总文件

        持仓明细文件

        出入金流水文件*/

        $data['exchange_code']              = $this->exchange_code;                //交易所编号

        $data['file_type']                  = $type;

        $data['targetcomp_id']              = $this->targetcomp_id;                //目标机构编码

        $data['sendercomp_id']              = $this->sendercomp_id;                //发送机构编码

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $arr = array(

                'error_info' => $return_data['data'][0]['error_info'],

                'error_no'   => $return_data['data'][0]['error_no'],

            );

        }

        return json_encode($arr);

    }



    //报送产品信息

    public function insert_category($pid=0,$cate_name,$cate_code){

        $url                    =  $this->api_url."insert_category";

        $product                =  getProduct($pid);

        if(empty($product)){

            return json_encode( array(

                'error_info' => '报送商品不存在',

                'error_no'   => '302',

            ));

        }

        $exchange_market_type   = array(

            ['type_name'  => '不区分',  'type'=>1],

            ['type_name'  => '做市交易','type'=>2],

            ['type_name'  => '挂牌交易','type'=>3],

            ['type_name'  => '电子商城','type'=>4],

        );

        $biz_type               = array(

            ['type_name'  => '不区分'  ,'type'=>1],

            ['type_name'  => '大宗商品','type'=>2],

            ['type_name'  => '金融资产','type'=>3],

            ['type_name'  => '文化产权','type'=>4],

            ['type_name'  => '邮币卡'  ,'type'=>5],

        );

        $data['exchange_code']              = $this->exchange_code;                 //交易所编号

        $data['exchange_market_type']       = $exchange_market_type[0]['type'];     //交易所市场编码

        $data['biz_type']                   = $biz_type[0]['type'];                 //交易市场业务类型

        $data['product_category_max_code']  = $cate_name;                            //产品类别代码

        $data['product_category_max_name']  = $cate_code;                               //产品类别名称

        $data['sort_order']                 = $product['id'];                       //排序

        $data['enable_status']              = 'I1';                                 //启用状态

        $data['product_attr_json']          = '';                                   //产品属性json信息

        $data['busi_datetime']              = date('YmdHis');

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

            return json_encode($arr);

        }else{

            $arr = array(

                'error_info'            => $return_data['data'][0]['error_info'],

                'error_no'              => $return_data['data'][0]['error_no'],

                'product_category_id'   => $return_data['data'][0]['product_category_id'],

            );

            Db::name('Product')->where(array('id'=>$pid))->update(array('p_cate'=>$arr['product_category_id']));

            $this->add_product_info($pid);

        }

    }



    //报送商品新增

    public function add_product_info($pid=1){

        $url                    =  $this->api_url."add_product_info";

        $product                =  getProduct($pid);

        if(empty($product)){

            return json_encode( array(

                'error_info' => '报送商品不存在',

                'error_no'   => '302',

            ));

        }

        $present_unit   = array(

            ['type_name'  => '克'  ,'type'=>0],

            ['type_name'  => '千克','type'=>1],

            ['type_name'  => '吨'  ,'type'=>2],

            ['type_name'  => '张'  ,'type'=>3],

            ['type_name'  => '副'  ,'type'=>4],

            ['type_name'  => '个'  ,'type'=>5],

            ['type_name'  => '枚'  ,'type'=>6],

            ['type_name'  => '俩'  ,'type'=>7],

            ['type_name'  => '只'  ,'type'=>8],

            ['type_name'  => '件'  ,'type'=>9],

        );

        $product_attr = array(

            'productCategoryInld'       => $product['p_cate'],

            'productCategoryMaxName'    => $product['p_cate_name'],

            'presentUnit'               => '0',

            'productCategoryMaxCode'    => $product['p_cate_code'],

            'exchangeCode'              => $this->exchange_code,

            'productCode'               => $product['p_code'],

            'productName'               => $product['p_title'],

        );

        $data['exchange_code']              =  $this->exchange_code;                  //交易所编号

        $data['product_category_id']        =  $product['p_cate'];                    //产品类别ID

        $data['product_category_max_code']  =  $product['p_cate_code'];               //产品类别代码

        $data['product_code']               =  $product['p_code'];                    //产品代码

        $data['product_name']               =  $product['p_title'];                   //产品代码

        $data['present_unit']               =  $present_unit[0]['type'];              //单位

        $data['busi_datetime']              =  date('YmdHis');                 //业务时间

        $data['product_attr_json']          =  json_encode($product_attr);                                    //产品属性json信息

        $access_token                       =  $this->getToken();

        $header                             =  array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $arr = array(

                'error_info'        => $return_data['data'][0]['error_info'],

                'error_no'          => $return_data['data'][0]['error_no'],

                'product_in_id'     => $return_data['data'][0]['product_in_id'],

            );

            Db::name('Product')->where(array('id'=>$pid))->update(array('p_in_id'=>$arr['product_in_id'],'p_status'=>1));

        }

        return json_encode($arr);

    }



    //手续费计算

    public function cash_fee_trial($type='',$num=''){

        if($type==''){

            $type = 2;

        }

        if($num==''){

            $num = 100000;

        }else{

            $num = $num*100;

        }

        $url            =  $this->api_url."cash_fee_trial";

        $bank_pro_code  = array(

            ['type_name'  => '上海银行','type'=>'boshzjjg'],

        );

        $data['bank_pro_code']              = $bank_pro_code[0]['type'];            //银行产品代码

        $data['occur_balance']              = $num;

        $data['vast_flag']                  = $type;

        $data['targetcomp_id']              = $this->targetcomp_id;                //目标机构编码

        $data['sendercomp_id']              = $this->sendercomp_id;                //发送机构编码

        $data['exchange_code']              = $this->exchange_code;                //交易所编号

        $access_token                       = $this->getToken();

        $header                     = array('Content-Type'=>'application/x-www-form-urlencoded',

            'Authorization'=>'Bearer '.$access_token

        );

        $res                        = ihttp_request($url,$data,$header);

        $return_data                = json_decode($res['content'],true);

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error']) && $return_data['error'] == 'invalid_token'){

            $this->getToken(1);

        }

        if(isset($return_data['error_no']) && $return_data['error_no'] != 0){

            if(isset($return_data['error_description'])){

                $arr = array(

                    'error_info' => $return_data['error_description'],

                    'error_no'   => $return_data['error_no'],

                );

            }else{

                $arr = array(

                    'error_info' => $return_data['error_info'],

                    'error_no'   => $return_data['error_no'],

                );

            }

        }else{

            $occur_balance          =  $return_data['data'][0]['occur_balance']/100;

            $withdraw_balance       =  $return_data['data'][0]['withdraw_balance']/100;

            $out_poundage           =  $occur_balance-$withdraw_balance;                                    //手续费

            $fee_poundage           =  round($out_poundage/$occur_balance*100,2);            //手续费比例

            $arr = array(

                'error_info'        => $return_data['data'][0]['error_info'],

                'error_no'          => $return_data['data'][0]['error_no'],

                'occur_balance'     => $occur_balance,                              //出入金金额

                'withdraw_balance'  => $withdraw_balance,                           //实际到账金额

                'out_poundage'      => $out_poundage,                               //手续费

                'fee_poundage'      => $fee_poundage,                               //手续费比率

            );

        }

        return json_encode($arr);

    }







    ########异步通知#########

    //客户与银行签约解约通知 (异步)

    public function contract_notice(){

        if($this->request->post()){

            $fields                     =   input('post.');

            $json                       =   json_encode($fields);

            file_put_contents('./1.txt',$json . PHP_EOL, FILE_APPEND);

            $return_info                =   $fields['return_info'];

            $exchange_fund_account      =   $fields['exchange_fund_account'];

            $return_code                =   $fields['return_code'];

            $sign_type                  =   $fields['sign_type'];

            $user                       =   Db::name('User')->where(array('m_account'=>$exchange_fund_account,'m_bank_carid'=>$fields['bind_account_no']))->find();

            file_put_contents('./8.txt',json_encode($user) . PHP_EOL, FILE_APPEND);

            if(empty($user)){

                $data_msg  = array('data'=>array(array(

                    'error_info'            => '操作成功',

                    'error_no'              => "0",

                    'serial_no'             => " ",

                    'trade_serial_no'       => " ",

                )));

                $json_data               =   json_encode($data_msg);

                return $json_data;

            }

            $uid                         =   $user['id'];

            file_put_contents('./8.txt','111|'.$uid.'|'.$return_code . PHP_EOL, FILE_APPEND);

            if($sign_type == 1){

                $virtual_account         =   $fields['virtual_account'];

                if($return_code==1 && $virtual_account){

                    file_put_contents('./8.txt','222|'.$virtual_account.'|'.$return_code . PHP_EOL, FILE_APPEND);

                    $code            = "0";

                    $info            = '操作成功';

                    Db::name('User')->where(array('m_account'=>$exchange_fund_account))->update(array('m_bank_signing'=>1,'m_bank_settime'=>time(),'m_virtual_account'=>$virtual_account));

                    get_advise($uid,1,'签约通知',$return_info,1);

                }else{

                    file_put_contents('./8.txt','333|'.$virtual_account . PHP_EOL, FILE_APPEND);

                    $code            = "0";

                    $info            = '操作成功';

                    Db::name('User')->where(array('m_account'=>$exchange_fund_account))->update(array('m_bank_signing'=>2,'last_time'=>time()));

                    get_advise($uid,1,'签约通知',$return_info,0);

                }

                $data_msg  = array('data'=>array(array(

                    'error_info'            => $info,

                    'error_no'              => $code,

                    'serial_no'             => " ",

                    'trade_serial_no'       => " ",

                )));

                $json_data                  =   json_encode($data_msg);

                return $json_data;

            }elseif($sign_type == 2){

                file_put_contents('./9.txt','333' . PHP_EOL, FILE_APPEND);

                if($return_code == 1){

                    file_put_contents('./9.txt','222' . PHP_EOL, FILE_APPEND);

                    $code            = "0";

                    $info            = '操作成功';

                    Db::name('User')->where(array('m_account'=>$exchange_fund_account))->update(array('m_bank_signing'=>2,'m_bank_settime'=>time(),'m_virtual_account'=>0));

                    get_advise($uid,2,'解约通知',$return_info,2);

                }else{

                    file_put_contents('./9.txt','111' . PHP_EOL, FILE_APPEND);

                    $code            = "0";

                    $info            = '操作成功';

                    Db::name('User')->where(array('m_account'=>$exchange_fund_account))->update(array('m_bank_signing'=>1,'last_time'=>time()));

                    get_advise($uid,2,'解约通知',$return_info,0);

                }

                $data_msg  = array('data'=>array(array(

                    'error_info'            => $info,

                    'error_no'              => $code,

                    'serial_no'             => " ",

                    'trade_serial_no'       => " ",

                )));

                $json_data                  =   json_encode($data_msg);

                return $json_data;

            }

        }

    }





    //客户入金 280003

    public function bank_fund_in(){

        $time                           = time();

        if($this->request->post()){

            $fields                     =   input('post.');

            $json                       =   json_encode($fields);

            file_put_contents('./2.txt',$json . PHP_EOL, FILE_APPEND);

            $exchange_fund_account      =   $fields['exchange_fund_account'];

            $user                       =   Db::name('User')->where(array('m_account'=>$exchange_fund_account))->find();

            if(empty($user)){

                $data_msg  = array('data'=>array(array(

                    'error_info'            => '操作成功',

                    'error_no'              => "0",

                    'serial_no'             => " ",

                    'trade_serial_no'       => " ",

                )));

                $json_data               =   json_encode($data_msg);

                return $json_data;

            }

            $uid                        = $user['id'];

            $occur_balance              = $fields['occur_balance']/100;

            $deal_data                  = array(

                'z_account' => $exchange_fund_account,

                'uid'       => $user['id'],

                'z_type'    => 1,

                'z_price'   => $occur_balance,

                'z_num'     => $fields['request_id'],

                'z_addtime' => time(),

                'z_bank_id' => $fields['bank_account'],

                'last_time' => $time,

                'z_overtime'=> $time,

                'z_status'  => 1

            );

            if(isset($fields['remark']) && $fields['remark']){

                $deal_data['z_desc'] = $fields['remark'];

            }

            $res_id = Db::name('Zlog')->insertGetId($deal_data);

            if($res_id){

                do_logs($uid,1,'m_balance',$occur_balance,'充值入金到帐');

            }

            $data_msg  = array('data'=>array(array(

                'error_info'            => "操作成功",

                'error_no'              => "0",

                'serial_no'             => " ",

                'trade_serial_no'       => $fields['request_id'],

            )));

            $json_data                  =  json_encode($data_msg);

            return $json_data;

        }

    }









    //银行信息变更结果通知

    public function bank_info_change(){

        $time                           = time();

        if($this->request->post()){

            $fields                     =   input('post.');

            $return_info                =   $fields['return_info'];

            $json                       =   json_encode($fields);

            file_put_contents('./3.txt',$json . PHP_EOL, FILE_APPEND);

            $exchange_fund_account      =   $fields['exchange_fund_account'];

            $return_code                =   $fields['return_code'];

            $bind_account_no            =   $fields['bind_account_no'];

            $user                       =   Db::name('User')->where(array('m_account'=>$exchange_fund_account))->find();

            $i_logs                     =   Db::name('Ilogs')->where(array('uid'=>$user['id'],'i_status'=>0))->order('id desc')->find();

            if(empty($user) || empty($i_logs)){

                $data_msg  = array('data'=>array(array(

                    'error_info'         => '操作成功',

                    'error_no'           => "0",

                    'serial_no'          => " ",

                    'trade_serial_no'    => " ",

                )));

                $json_data               =   json_encode($data_msg);

                return $json_data;

            }

            $uid                         =   $user['id'];

            if($return_code == 1){

                $code            = "0";

                $info            = '操作成功';

                Db::name('User')->where(array('id'=>$uid))->update(array('m_bank_signing'=>1,'m_bank_settime'=>time(),'m_bank_carid'=>$i_logs['i_new_bank_carid'],'m_bank_open_code'=>$i_logs['i_new_open_bank_no']));

                Db::name('Ilogs')->where(array('id'=>$i_logs['id']))->update(array('last_time'=>$time,'i_status'=>1));

                Db::name('Ilogs')->where(array('i_status'=>0))->update(array('last_time'=>$time,'i_status'=>2));

                get_advise($uid,1,'修改银行资料通知','恭喜您已修改完成，快去进行交易吧',1);

            }else{

                $code            = "0";

                $info            = '操作成功';

                Db::name('User')->where(array('id'=>$uid))->update(array('m_bank_signing'=>1));

                Db::name('Ilogs')->where(array('id'=>$i_logs['id']))->update(array('last_time'=>$time,'i_status'=>2));

                get_advise($uid,1,'修改银行资料通知',$return_info,0);

            }

            $data_msg  = array('data'=>array(array(

                'error_info'            => $info,

                'error_no'              => $code,

                'serial_no'             => " ",

                'trade_serial_no'       => " ",

            )));

            $json_data                  =   json_encode($data_msg);

            return $json_data;

        }

    }







    //推送文件通知

    public function report_mem_user_register(){

        if($this->request->post()){

            $bank_file_type = array(

                0       =>'请求',

                1       =>'应答',

                2       =>'开户',

                3       =>'销户',

                4       =>'对明细账',

                5       =>'对总账',

                'I1'    =>'出入金',

                'I2'    =>'开销户',

                'I3'    =>'出入金手续费',

            );

            $time              = time();

            $date              = date('Y-m-d',$time);

            $fields                                    = input('post.');

            $json                                      = json_encode($fields);

            file_put_contents('./4.txt',$json . PHP_EOL, FILE_APPEND);

            $set_file_data['f_path']                   = $fields['file_path'];

            $set_file_data['f_title']                  = $fields['file_name'];

            $set_file_data['f_start_time']             = 0;

            $set_file_data['f_type']                   = $fields['bank_file_type'];

            $set_file_data['f_end_time']               = 0;

            $set_file_data['f_date']                   = $date;

            $set_file_data['admin_id']                 = 0;

            $set_file_data['f_addtime']                = $time;

            $set_file_data['f_status']                 = 0;

            $set_file_data['f_identity']               = 2;

            $set_file_data['f_desc']                   = $bank_file_type[$fields['bank_file_type']];

            Db::name('Flogs')->insertGetId($set_file_data);

            $data_msg  = array('data'=>array(array(

                'error_info'            => '操作成功',

                'error_no'              => "0",

            )));

            $json_data      =   json_encode($data_msg);

            //$this->check_account();         //内部信息对账

            return $json_data;

        }

    }



    //核对账户信息

    public function check_account(){

        $time               = time();

        $date               = date('Y-m-d',$time);

        //$date               = '2020-05-08';

        $start_time         = strtotime($date);

        $end_time           = $start_time+8640000;

        $exchange_code      = $this->exchange_code;

        $init_date          = date('Ymd',$time);

        //$init_date          = '20200508';

        #### 银行出入金对账文件 ####

        $f_name_1           = $init_date.'_'.$exchange_code.'_bankCheck.txt';

        $flog_data1         = Db::name('Flogs')->where(array('f_date'=>$date,'f_identity'=>2,'f_title'=>$f_name_1,'f_read'=>0))->find();

        if(empty($flog_data1)){

            return 0;

        }

        $f_path_1           = $flog_data1['f_path'].'/'.$f_name_1;

        $file_one           = fopen($f_path_1, "r");

        $data_one           = array();

        $set_data_one       = array();

        $i                  = 0;

        while(!feof($file_one)) {

            $data_one[$i]   = fgets($file_one);//fgets()函数从文件指针中读取一行

            $i++;

        }

        fclose($file_one);

        unset($data_one[0]);

        /*

            [0]     => 清算中心流水号

            [1]     => 交易所流水号

            [2]     => 业务发生日期

            [3]     => 交易所代码

            [4]     => 会员编码

            [5]     => 资金账号

            [6]     => 出入金方式

            [7]     => 出入金类型

            [8]     => 出入金发起方

            [9]     => 发生金额

            [10]    => 币种

            [11]    => 银行代码

            [12]    => 银行产品代码

            [13]    => 银行帐号

            [14]    => 状态

            [15]    => 备注

        */

        $data_one   = array_filter($data_one);

        for($i=1;$i<=count($data_one);$i++){

            $un_data_one        = explode('&',$data_one[$i]);

            $set_data_one[]     = $un_data_one;

        }

        ######清算中心文件多报送的处理######

        $set_request_data       = array();

        $set_clear_data         = array();

        if(!empty($set_data_one)){

            foreach ($set_data_one as $k1=>$v1) {

                $inout_type     = 1;

                $occur_balance  = $v1[9]/100;

                if($v1[7] == 0){

                    $inout_type             = 2;

                    $occur_balance          = -$occur_balance;

                }

                $set_request_data[]      = $v1[1];

                $set_clear_data[]        = $v1[0];

                $zlog_data               = Db::name('Zlog')->where("(z_num= '$v1[1]' OR z_clear_num='$v1[0]')")->where("z_addtime>$start_time and z_addtime<$end_time")->find();

                if(empty($zlog_data)){

                    if($v1[1] == 'null'){

                        $v1[1] = randomkeys(16);

                    }

                    $user       = Db::name('User')->where(array('m_account'=>$v1[5]))->find();

                    $m_account  = $user['m_account'];

                    $uid        = $user['id'];

                    $deal_data  = array(

                        'z_account'     => $m_account,

                        'uid'           => $uid,

                        'z_type'        => $inout_type,

                        'z_price'       => $v1[9]/100,

                        'z_num'         => $v1[1],

                        'z_addtime'     => $time,

                        'z_bank_id'     => $v1[13],

                        'last_time'     => $time,

                        'z_overtime'    => $time,

                        'z_status'      => 1,

                        'z_clear_num'   => $v1[0],

                    );

                    if(isset($v1[15]) && $v1[15]){

                        $deal_data['z_desc'] = $v1[15];

                    }

                    $res_id = Db::name('Zlog')->insertGetId($deal_data);

                    if($res_id){

                        do_logs($uid,1,'m_balance',$occur_balance,'日终对账,根据清算中心文件操作余额1');

                    }

                }else{

                    $uid        = $zlog_data['uid'];

                    Db::name('Zlog')->where(array('id'=>$zlog_data['id']))->update(array('z_clear_num'=>$v1[0],'last_time'=>$time));

                    if($zlog_data['z_status']!=1){

                        $res_id = Db::name('Zlog')->where(array('id'=>$zlog_data['id']))->update(array('z_status'=>1,'z_overtime'=>$time));

                        if($res_id){

                            do_logs($uid,1,'m_balance',$occur_balance,'日终对账,根据清算中心文件操作余额2');

                        }

                    }

                }

            }

            ######清算中心文件多报送的处理######



            ######我方文件多报送的处理######

            $zlog_data          = Db::name('Zlog')->where("z_addtime>$start_time and z_addtime<$end_time and z_status<>1 and z_overtime=0")->select();

            if(!empty($zlog_data)){

                foreach ($zlog_data as $k3=>$v3){

                    if(!in_array($v3['z_clear_num'],$set_clear_data) && !in_array($v3['z_num'],$set_clear_data)){

                        $uid            = $v3['uid'];

                        $occur_balance  = -$v3['z_price'];

                        if($v3['z_type'] == 2){

                            $occur_balance          = $v3['z_price'];

                        }

                        $res_id = Db::name('Zlog')->where(array('id'=>$v3['id']))->update(array('z_status'=>2,'last_time'=>$time));

                        if($res_id && $v3['z_status']==1){

                            do_logs($uid,1,'m_balance',$occur_balance,'日终对账,根据清算中心文件操作余额3');

                        }

                    }

                }

            }

            ######我方文件多报送的处理######

        }

        Db::name('Flogs')->where(array('id'=>$flog_data1['id'],'f_read'=>0))->update(array('f_read'=>1,'f_status'=>1));

        #### 银行出入金对账文件 ####

        #### 查找费率手续费文件 ####

        $f_name_2           = $init_date.'_'.$exchange_code.'_bankCheckFee.txt';

        $flog_data2         = Db::name('Flogs')->where(array('f_date'=>$date,'f_identity'=>2,'f_title'=>$f_name_2))->find();

        if(empty($flog_data2)){

            return 0;

        }

        $f_path_2           = $flog_data2['f_path'].'/'.$f_name_2;

        $file_two           = fopen($f_path_2, "r");

        $data_two           = array();

        $set_data_two       = array();

        $t      = 0;

        while(!feof($file_two)) {

            $data_two[$t]   = fgets($file_two);//函数从文件指针中读取一行

            $t++;

        }

        fclose($file_two);

        unset($data_two[0]);

        /*  [0]     => 清算中心流水号

            [1]     => 交易所流水号

            [2]     => 业务发生日期

            [3]     => 交易所代码

            [4]     => 会员编码

            [5]     => 资金账号

            [6]     => 出入金类型

            [7]     => 发生金额

            [8]     => 手续费金额

            [9]     => 银行产品代码

            [10]    => 备注

        */

        $data_two   = array_filter($data_two);

        for($t=1;$t<=count($data_two);$t++){

            $un_data_two        = explode('&',$data_two[$t]);

            $set_data_two[]     = $un_data_two;

        }

        if(!empty($set_data_two)){

            foreach ($set_data_two as $k2=>$v2) {

                $zlog_data          = Db::name('Zlog')->where(array('z_status'=>1,'z_num'=>$v2[1]))->where("z_addtime>$start_time and z_addtime<$end_time")->find();

                if(!empty($zlog_data) and $zlog_data['z_fee_price']==0){

                    $deal_data      = array(

                        'z_fee_price'   => round($v2['8']/100,4),

                        'z_fee'         => round($v2['8']/($v2['7']-$v2['8'])*100,2),

                    );

                    Db::name('Zlog')->where(array('z_num'=>$v2[1]))->update($deal_data);

                }

            }

        }

        Db::name('Flogs')->where(array('id'=>$flog_data2['id'],'f_read'=>0))->update(array('f_read'=>1,'f_status'=>1));

        #### 查找费率手续费文件 ####

        return 1;

    }





    //出入金结果通知

    public function golden_in_notice(){

        if($this->request->post()){

            $fields                     =   input('post.');

            $time                       =   time();

            $json                       =   json_encode($fields);

            file_put_contents('./5.txt',$json . PHP_EOL, FILE_APPEND);

            $request_id                 =   $fields['request_id'];

            $zlog                       =   Db::name('Zlog')->where(array('z_num'=>$request_id))->find();

            $cash_type                  =   $fields['cash_type'];            //1-出金、2-入金

            $status                     =   $fields['status'];               //0-成功，1-失败

            if(!empty($zlog)){

                $uid                    =   $zlog['uid'];

                $price                  =   $zlog['z_price'];

                if($cash_type == 2){                     //2-入金

                    if($status == 0){

                        $res            = Db::name('Zlog')->where(array('id'=>$zlog['id']))->update(array('last_time'=>$time,'z_overtime'=>$time,'z_status'=>1));   //更新Zlog表

                        if($res){

                            do_logs($uid,1,'m_balance',$price,'充值入金到帐');

                        }

                    }else{

                        $error_info                 =   $fields['error_info'];

                        Db::name('Zlog')->where(array('id'=>$zlog['id']))->update(array('last_time'=>$time,'z_status'=>2,'z_desc'=>$error_info));

                    }

                    $data_msg  = array('data'=>array(array(

                        'error_info'            => '操作成功',

                        'error_no'              => "0",

                    )));

                    $json_data               =   json_encode($data_msg);

                    return $json_data;

                }

                if($cash_type == 1){                     //1-出金

                    if($status == 0){

                        Db::name('Zlog')->where(array('id'=>$zlog['id']))->update(array('last_time'=>$time,'z_overtime'=>$time,'z_status'=>1));   //更新Zlog表

                    }else{

                        $error_info                 =   $fields['error_info'];

                        $res = Db::name('Zlog')->where(array('id'=>$zlog['id']))->update(array('last_time'=>$time,'z_status'=>2,'z_desc'=>$error_info));                //更新Zlog表

                        if($res){

                            do_logs($uid,1,'m_balance',$price,'出金失败,资金退回余额');

                        }

                    }

                    $data_msg  = array('data'=>array(array(

                        'error_info'            => '操作成功',

                        'error_no'              => "0",

                    )));

                    $json_data               =   json_encode($data_msg);

                    return $json_data;

                }

            }

            $data_msg  = array('data'=>array(array(

                'error_info'            => '操作成功',

                'error_no'              => "0",

            )));

            $json_data               =   json_encode($data_msg);

            return $json_data;

        }

    }



    //收益结转审核通知

    public function income_extraction_result(){

        if($this->request->post()){

            $fields         =   input('post.');

            return json($fields);

        }

    }



}