<?php

namespace app\index\controller;

use think\Db;

use think\facade\Env;

use think\Session;

/**

 * 前台首页控制器

 * @package app\index\controller

 */

class Center extends Home

{

    public function center(){

        $config     = $this->set_config();

        $user       = $this->check_user();
        
        $uid  = $user["id"];
        
        $configMsg = 0;
        
        $time = time();
        
        $s_time = mktime(9, 0 , 0,date("m"),date("d"),date("Y"));
        
        $e_time = mktime(16, 0 , 0,date("m"),date("d"),date("Y"));
        

        if (($time < $s_time || $time > $e_time) && ($uid != 1309 && $uid != 2261 && $uid != 904 && $uid != 1129)) {

            $configMsg = 1;

        }
        
        $this->assign(array('config'=>$config,'user'=>$user,'configMsg'=>$configMsg));
        
        return $this->fetch();

    }
    public function bounsdetails(){
        $user       = $this->check_user();
        $uid        = $user['id'];
        //select count(fromType) , uid , addDate , fromType from w_user_bouns where fromType <> 0 group by uid , addDate , fromType
        $field = [
            'uid', 'addDate', 'fromType' , 'fromDesc', Db::raw('round(sum(bouns),2) AS fromTypeCounts')
        ];
        $list = Db::name('UserBouns')->where("fromType <> 0 and uid = $uid")->group('uid , addDate , fromType , fromDesc')->field($field)->order('addDate desc')->select();
        $this->assign(array('user'=>$user ,'list' => $list));
        return $this->fetch();
    }
    public function suboundetails($fromType = '' , $addDate = '' , $accounts = ''){
        $user       = $this->check_user();
        $uid        = $user['id'];
        $field = [
            'a.*', 'b.m_account', 'b.m_nickname'
        ];
        $sqlWhere = '';
        if($accounts != ''){
            $sqlWhere = "b.m_account like '%".$accounts."%'";
        }
        $list = Db::name('UserBouns')
            ->alias('a')
            ->join('user b', 'a.fromUser = b.id')
            ->field($field)
            ->where("fromType = $fromType and uid = $uid and a.addDate = '$addDate'")
            ->where($sqlWhere)
            ->select();
        foreach ($list as &$row) {
            $row['bouns'] = round($row['bouns'] , 3);
        }
        $this->assign(array('user'=>$user,'fromType'=>$fromType,'addDate'=>$addDate ,'accounts' => $accounts,'list' => $list));
        return $this->fetch();
    }
    public function fengongsi(){
        $user       = $this->check_user();
        $uid        = $user['id'];
        $firstday   = date('Y-m-16', strtotime('-1 month'));
        $seconday   = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
        //$amounts    = Db::name('userFh')->where("uid = $uid and addDate >= '$firstday' and addDate <= '$seconday'")->sum('fh');
        //$list       = Db::name('userFh')->where("uid = $uid and addDate >= '$firstday' and addDate <= '$seconday'")->order('addDate desc')->select();
        $amounts    = Db::name('userFh')->where("addDate > '2020-09-15'")->where("uid = $uid")->sum('fh');
        $list       = Db::name('userFh')->where("addDate > '2020-09-15'")->where("uid = $uid")->order('addDate desc')->select();
        $this->assign(array('user'=>$user ,'amounts' => $amounts , 'list' => $list));
        return $this->fetch();
    }

    public function integral(){
        $user       = $this->check_user();
        $uid        = $user['id'];
        $deal = Db::name('Deal')->where(array('uid'=>$user['id'] , 'd_type'=>2 , 'd_token' => 0, 'areaId' => 1))->where('d_credit_2 > 0')->order('id desc')->select();
        $dealHistory = Db::name('DealHistory')->where(array('uid' => $user['id'], 'd_type' => 2, 'd_token' => 0, 'areaId' => 1))->where('d_credit_2 > 0 or d_zljl > 0')->order('id desc')->select();
        $order = Db::name('order')->where("uid = $uid")->select();
        $buyAmounts = 0;
        $payAmounts = 0;
        foreach ($order as &$row) {
            $row['o_addtime'] = date('Y-m-d H:i', $row['o_addtime']);
            $payAmounts += ($row['o_buy_num'] * 186);
        }
        foreach ($dealHistory as &$row) {
            if($row['d_zljl'] == 0){
                $buyAmounts += $row['d_total'] * 93;
            }else{
                $buyAmounts += $row['d_zljl'];
            }
        }
        foreach ($deal as &$row) {
            if($row['d_zljl'] == 0){
                $buyAmounts += $row['d_total'] * 93;
            }else{
                $buyAmounts += $row['d_zljl'];
            }
        }
        $this->assign(array('user'=>$user ,'order' => $order, 'deal' => $deal ,'dealHistory' => $dealHistory , 'buyAmounts' => $buyAmounts , 'payAmounts' => $payAmounts));
        return $this->fetch();
    }
    
    public function balance(){
        $user       = $this->check_user();
        $account        = $user['m_account'];
        $startTime = strtotime('-7 days');
        $endTime = strtotime('now');
        $startDate = date('Y-m-d', $startTime);
        $endDate = date('Y-m-d', $endTime);
        $list = Db::name('FileFundBalance')->where("mem_code = $account")->order('id desc')->select();
        foreach ($list as &$row) {
            $row['init_time'] = date('Y-m-d H:i', $row['init_time']);
        }
        $this->assign(array('user' => $user , 'list' => $list));
        return $this->fetch();
    }

    public function login_out(){

        $user               = $this->check_user();

        session('uid',null);

        $z_uid              = cookie('z_uid');

        $url                = url('Login/login');

        if($z_uid>0){

            $url                = url('Login/account_list');

        }

        return json(array('code' => 1,'z_uid'=>$z_uid,'url'=>$url,'msg' => '退出成功'));

    }



    public function info(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        $this->assign(array('config'=>$config,'user'=>$user,'uid'=>$uid));

        return $this->fetch();

    }



    public function set_bank(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $bank       = $this->bankCode();

        $open_code  = $this->bankOpenCode();

        $area_info  = Db::name('Area')->where(array('pid'=>0))->order('sid asc')->select();

        $bank_info  = Db::name('Bank')->where(array('is_del'=>0))->order('sid asc')->select();

        $uid        = $user['id'];

        if($this->request->post()){

            $fields     =   input('post.');

            if($fields['bank_type'] == 2){
                // 开户审核 防止直接访问页面
                // if($user['m_is_pass'] !== 1){
                //     return json(array('code'=>0,'msg'=>'请先等待开户审核完成'));
                // }

                if(!isset($fields['m_bank_code']) || $fields['m_bank_code'] == 0){

                    return json(array('code'=>0,'msg'=>'银行名称不能为空'));

                }

                $fields['m_bank_name']  = $bank[$fields['m_bank_code']];

                if(!isset($fields['m_bank_carid']) || $fields['m_bank_carid'] == ''){

                    return json(array('code'=>0,'msg'=>'银行卡号不能为空'));

                }

                $has_bank   = Db::name('User')->where("id<>$uid")->where(array('m_bank_carid'=>$fields['m_bank_carid']))->find();

                if(!empty($has_bank)){

                    return json(array('code'=>0,'msg'=>'您的银行卡信息已被添加'));

                }

                if(!isset($fields['m_bank_open_code']) || $fields['m_bank_open_code'] == ''){

                    return json(array('code'=>0,'msg'=>'开户行所在城市不能为空'));

                }

                if(!isset($fields['m_bank_open']) || $fields['m_bank_open'] == ''){

                    return json(array('code'=>0,'msg'=>'开户行信息不能为空'));

                }

                $bank_open  =  Db::name('Bank_database')->where(array('bank_open_code'=>$fields['m_bank_open_code']))->find();

                $fields['m_bank_open'] = $bank_open['bank_name'];

                $fields['last_time']    = time();

                $res        = Db::name('User')->where(array('id'=>$uid))->update($fields);

            }else{

                $res        = 1;

            }

            if($res){

                $Api        = new Api();

                $msg        = $Api->client_sign_bind_account(4);

                $msg_arr    = json_decode($msg,true);

                if($msg_arr['error_no'] == 0){

                    Db::name('User')->where(array('id'=>$uid))->update(array('m_bank_signing'=>3,'last_time'=>time()));

                    return json(array('code'=>1,'msg'=>'提交成功,请耐心等待'));

                }else{

                    return json(array('code'=>0,'msg'=>$msg_arr['error_info']));

                }



            }else{

                return json(array('code'=>0,'msg'=>'设置失败'));

            }



        }

        $this->assign(array('config'=>$config,'user'=>$user,'bank'=>$bank,'open_code'=>$open_code,'area_info'=>$area_info,'bank_info'=>$bank_info));

        return $this->fetch();

    }



    public function save_bank(){

        $config             =  $this->set_config();

        $user               =  $this->check_user();

        $bank               =  $this->bankCode();

        $open_code          =  $this->bankOpenCode();

        $area_info          =  Db::name('Area')->where(array('pid'=>0))->order('sid asc')->select();

        $bank_info          =  Db::name('Bank')->where(array('is_del'=>0))->order('sid asc')->select();

        $uid                =  $user['id'];

        if($this->request->post()){

            $fields     =   input('post.');

            if(!isset($fields['m_bank_code']) || $fields['m_bank_code'] == 0){

                return json(array('code'=>0,'msg'=>'银行名称不能为空'));

            }

            $fields['m_bank_name']  = $bank[$fields['m_bank_code']];

            if(!isset($fields['m_bank_carid']) || $fields['m_bank_carid'] == ''){

                return json(array('code'=>0,'msg'=>'银行卡号不能为空'));

            }

            $has_bank   = Db::name('User')->where("id<>$uid")->where(array('m_bank_carid'=>$fields['m_bank_carid']))->find();

            if(!empty($has_bank)){

                return json(array('code'=>0,'msg'=>'您的银行卡信息已被添加'));

            }

            if(!isset($fields['m_bank_open_code']) || $fields['m_bank_open_code'] == ''){

                return json(array('code'=>0,'msg'=>'开户行所在城市不能为空'));

            }

            if(!isset($fields['m_bank_open']) || $fields['m_bank_open'] == ''){

                return json(array('code'=>0,'msg'=>'开户行信息不能为空'));

            }

            $data['open_bank_no']   = $fields['m_bank_open_code'];

            $data['bank_account']   = $fields['m_bank_carid'];

            $Api                    = new Api();

            $msg                    = $Api->update_main_bank_info($data);

            $msg_arr                = json_decode($msg,true);

            if($msg_arr['error_no'] == 0){

                Db::name('User')->where(array('id'=>$uid))->update(array('m_bank_signing'=>3,'m_bank_open_code'=>$fields['m_bank_open_code'],'last_time'=>time()));

                return json(array('code'=>1,'msg'=>'修改成功,请耐心等待'));

            }else{

                return json(array('code'=>0,'msg'=>$msg_arr['error_info']));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user,'bank'=>$bank,'open_code'=>$open_code,'area_info'=>$area_info,'bank_info'=>$bank_info));

        return $this->fetch();

    }





    public function get_bank_info(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        if($this->request->post()){

            $fields     =   input('post.');

            $pid        =   $fields['pid'];

            $city_info  =   Db::name('Area')->where(array('pid'=>$pid))->order('sid asc')->select();

            return json_encode($city_info);

        }

    }



    public function getOpenBankCode(){

        $config         = $this->set_config();

        $user           = $this->check_user();

        if($this->request->post()){

            $bank_open_data = array();

            $fields     =   input('post.');

            if($fields['bank_open_code'] && $fields['area_code'] && $fields['city_code']){

                $sql = '';

                if($fields['key_world']){

                    $key_world  = $fields['key_world'];

                    $sql        .= "bank_name like '%$key_world%' or address like '%$key_world%'";

                }

                $bank_open_data  =  Db::name('Bank_database')->where(array('bank_id'=>$fields['bank_open_code'],'area_id'=>$fields['city_code'],'area_pid'=>$fields['area_code']))->where($sql)->order('id asc')->select();

            }

            return json_encode($bank_open_data);

        }

    }





    //修改头像

    public function avatar(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        if($this->request->file()){

            $avatar     = upload('headimg');

            if($avatar){

                $res =Db::name('User')->where('id',$uid)->update(array('m_avatar'=>$avatar));

                if($res){

                    return json(array('code'=>1,'msg'=>'修改成功'));

                }else{

                    return json(array('code'=>0,'msg'=>'修改失败'));

                }

            }else{

                return json(array('code'=>0,'msg'=>'图片格式有误'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user));

        return $this->fetch();

    }



    public function tell_edit(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        if($this->request->post()){

            $fields     =   input('post.');

            $has_user   =   Db::name('User')->where(array('m_phone'=>$fields['m_phone'],'id'=>$uid,'m_del'=>0))->find();

            if(!empty($has_user)){

                return json(array('code' => 0, 'msg' => '不能和原手机号一致'));

            }

            $has_phone  =   Db::name('User')->where(array('m_phone'=>$fields['m_phone'],'m_del'=>0))->where("id<>$uid")->find();

            if(!empty($has_phone)){

                return json(array('code' => 0, 'msg' => '您输入的手机号已被注册'));

            }

            $phone      = session('phone');

            $verycode   = session('verycode');

            if(!$phone || !$verycode){

                return json(array('code' => 0, 'msg' => '请获取短信验证码'));

            }

            if($phone!=trim($fields['m_phone'])){

                return json(array('code' => 0, 'msg' => '请输入验证的手机号'));

            }

            if($verycode!=trim($fields['m_code'])){

                return json(array('code' => 0, 'msg' => '短信验证码错误'));

            }

            $res = Db::name('User')->where(array('id'=>$uid))->update(array('m_phone'=>$fields['m_phone']));

            if($res){

                return json(array('code' => 1, 'msg' => '修改成功'));

            }else{

                return json(array('code' => 0, 'msg' => '修改失败'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user));

        return $this->fetch();

    }



    public function dlmm_edit(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        if($this->request->post()){

            $fields         =   input('post.');

            $old_pwd        =   md5(trim($fields['old_pwd']));

            if($old_pwd != $user['m_login_pwd']){

                return json(array('code' => 0, 'msg' => '原密码输入错误'));

            }

            if(trim($fields['new_pwd']) != trim($fields['que_pwd'])){

                return json(array('code' => 0, 'msg' => '两次密码不一致'));

            }

            if(md5(trim($fields['new_pwd']))==$user['m_login_pwd']){

                return json(array('code' => 0, 'msg' => '新密码不能与原密码一致'));

            }

            $res = Db::name('User')->where(array('id'=>$uid))->update(array('m_login_pwd'=>md5(trim($fields['new_pwd']))));

            if($res){

                return json(array('code' => 1, 'msg' => '修改登陆成功'));

            }else{

                return json(array('code' => 0, 'msg' => '修改登陆失败'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user));

        return $this->fetch();

    }





    public function jymm_edit(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        if($this->request->post()){

            $fields         =   input('post.');

            $old_pwd        =   md5(trim($fields['old_pwd']));

            if($old_pwd != $user['m_pay_pwd']){

                return json(array('code' => 0, 'msg' => '原密码输入错误'));

            }

            if(trim($fields['new_pwd']) != trim($fields['que_pwd'])){

                return json(array('code' => 0, 'msg' => '两次密码不一致'));

            }

            if(md5(trim($fields['new_pwd']))==$user['m_pay_pwd']){

                return json(array('code' => 0, 'msg' => '新密码不能与原密码一致'));

            }

            $res = Db::name('User')->where(array('id'=>$uid))->update(array('m_pay_pwd'=>md5(trim($fields['new_pwd']))));

            if($res){

                return json(array('code' => 1, 'msg' => '修改交易密码成功'));

            }else{

                return json(array('code' => 0, 'msg' => '修改交易密码失败'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user));

        return $this->fetch();

    }



    public function jymm_set(){

        $config     = $this->set_config();

        $user       = $this->check_user();

        $uid        = $user['id'];

        if($this->request->post()){

            $fields         =   input('post.');

            if(trim($fields['new_pwd']) != trim($fields['que_pwd'])){

                return json(array('code' => 0, 'msg' => '两次密码不一致'));

            }

            $res = Db::name('User')->where(array('id'=>$uid))->update(array('m_pay_pwd'=>md5(trim($fields['new_pwd']))));

            if($res){

                return json(array('code' => 1, 'msg' => '设置交易密码成功'));

            }else{

                return json(array('code' => 0, 'msg' => '设置交易密码失败'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user));

        return $this->fetch();

    }



    public function address_list(){

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $type       =   intval(input('type'));

        if($type==1){

            $num    =   intval(input('num'));

            $g_id   =   intval(input('g_id'));;

        }else{

            $num    =   0;

            $g_id   =   0;

        }

        if($type==3){

            $d_id = input('d_id');

        }else{

            $d_id = 0;

        }
        if($type == 4){
            $deal_id = input('deal_id');
        }else{
            $deal_id = 0;
        }
        if($type == 5){
            $num = input('num');
            $exchange_id = input('exchange_id');
            $gid = input('gid');
            $etype = input('etype');
        }else{
            $exchange_id = 0;
            $gid = 0;
            $etype = 0;
        }

        $uid        = $user['id'];

        $address    = Db::name('Address')->where(array('uid'=>$uid))->order('a_is_default desc')->select();

        $status     = 0;

        if(!empty($address)){

            $status     = 1;

        }



        $this->assign(array('config'=>$config,'user'=>$user,'address'=>$address,'status'=>$status,'num'=>$num,'g_id'=>$g_id,'type'=>$type,'d_id'=>$d_id));

        $this->assign('deal_id', $deal_id);
        $this->assign(['exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]);
        return $this->fetch();

    }





    public function address_edit(){

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $uid        =   $user['id'];

        $type       =   intval(input('type'));

        $a_id       =   intval(input('a_id'));

        if($type==1){

            $num    =   intval(input('num'));

            $g_id   =   intval(input('g_id'));;

        }else{

            $num    =   0;

            $g_id   =   0;

        }
        if($type == 4){
            $deal_id = input('deal_id');
        }else{
            $deal_id = 0;
        }
        if($type == 5){
            $num = input('num');
            $exchange_id = input('exchange_id');
            $gid = input('gid');
            $etype = input('etype');
        }else{
            $exchange_id = 0;
            $gid = 0;
            $etype = 0;
        }

        if($this->request->post()){

            $fields =   input('post.');

            if(isset($fields['a_is_default'])&&$fields['a_is_default']=='on'){

                Db::name('Address')->where(array('uid'=>$uid))->update(array('a_is_default'=>0));

                $fields['a_is_default']  =   1;

            }else{

                $fields['a_is_default']  =   0;

            }

            $data       =   array(

                'uid'           =>  $uid,

                'a_name'        =>  $fields['a_name'],

                'a_phone'       =>  $fields['a_phone'],

                'a_city'        =>  $fields['a_city'],

                'a_detail'      =>  $fields['a_detail'],

                'a_is_default'  =>  $fields['a_is_default'],

                'a_time'        =>  time(),

            );

            $address_id         = Db::name('Address')->where(array('id'=>$a_id))->update($data);

            if($address_id){

                return json(array('code' => 1, 'msg' => '地址修改成功','type'=>$fields['type'],'num'=>$fields['num'],'g_id'=>$fields['g_id'],'deal_id'=>$deal_id,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype));

            }else{

                return json(array('code' => 0, 'msg' => '地址修改失败'));

            }

        }

        $address    =  Db::name('Address')->where(array('id'=>$a_id,'uid'=>$uid))->find();

        $this->assign(array('config'=>$config,'user'=>$user,'num'=>$num,'g_id'=>$g_id,'type'=>$type,'address'=>$address));
        $this->assign('deal_id', $deal_id);
        $this->assign(['exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]);
        return $this->fetch();

    }



    public function address_add(){

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $uid        =   $user['id'];

        $type       =   intval(input('type'));

        if($type==1){

            $num    =   intval(input('num'));

            $g_id   =   intval(input('g_id'));;

        }else{

            $num    =   0;

            $g_id   =   0;

        }

        if($type==3){

            $d_id = input('d_id');

        }else{

            $d_id = 0;

        }
        if($type == 4){
            $deal_id = input('deal_id');
        }else{
            $deal_id = 0;
        }
        if($type == 5){
            $num = input('num');
            $exchange_id = input('exchange_id');
            $gid = input('gid');
            $etype = input('etype');
        }else{
            $exchange_id = 0;
            $gid = 0;
            $etype = 0;
        }

        if($this->request->post()){

            $fields =   input('post.');

            if(isset($fields['a_is_default'])&&$fields['a_is_default']=='on'){

                Db::name('Address')->where(array('uid'=>$uid))->update(array('a_is_default'=>0));

                $fields['a_is_default']  =   1;

            }else{

                $fields['a_is_default']  =   0;

            }

            $data       =   array(

                'uid'           =>  $uid,

                'a_name'        =>  $fields['a_name'],

                'a_phone'       =>  $fields['a_phone'],

                'a_city'        =>  $fields['a_city'],

                'a_detail'      =>  $fields['a_detail'],

                'a_is_default'  =>  $fields['a_is_default'],

                'a_time'        =>  time(),

            );

            $address_id         = Db::name('Address')->insertGetId($data);

            if($address_id){

                return json(array('code' => 1, 'msg' => '地址添加成功','type'=>$fields['type'],'num'=>$fields['num'],'g_id'=>$fields['g_id'],'deal_id'=>$deal_id,'exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype));

            }else{

                return json(array('code' => 0, 'msg' => '地址添加失败'));

            }

        }

        $this->assign(array('config'=>$config,'user'=>$user,'num'=>$num,'g_id'=>$g_id,'type'=>$type,'d_id'=>$d_id));
		$this->assign('deal_id', $deal_id);
		$this->assign(['exchange_id'=>$exchange_id,'gid'=>$gid,'etype'=>$etype]);
        return $this->fetch();

    }







    public function address_del(){

        $user               =   $this->check_user();

        $uid                =   $user['id'];

        if($this->request->post()){

            $fields         =   input('post.');

            $id             =   $fields['a_id'];

            $address_del    =   Db::name('Address')->where(array('uid'=>$uid))->order('last_time desc,a_time desc')->find();

            if(!empty($address_del)){

                $res        =   Db::name('Address')->where(array('id'=>$id))->delete();

                if($res){

                    $address    =  Db::name('Address')->where(array('uid'=>$uid))->order('last_time desc,a_time desc')->find();

                    if(!empty($address) and $address_del['a_is_default']==1){

                        Db::name('Address')->where(array('uid'=>$uid,'id'=>$address['id']))->update(array('a_is_default'=>1));

                    }

                    return json(array('code' => 1, 'msg' => '地址删除成功','type'=>$fields['type'],'num'=>$fields['num'],'g_id'=>$fields['g_id']));

                }else{

                    return json(array('code' => 0, 'msg' => '地址删除失败','type'=>$fields['type'],'num'=>$fields['num'],'g_id'=>$fields['g_id']));

                }

            }else{

                return json(array('code' => 0, 'msg' => '地址删除失败','type'=>$fields['type'],'num'=>$fields['num'],'g_id'=>$fields['g_id']));

            }

        }

    }





    public function about(){

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $info       =   Db::name('About')->where(array('id'=>1))->find();;

        $this->assign(array('config'=>$config,'user'=>$user,'info'=>$info));

        return $this->fetch();

    }



    public function clear_cache(){

        delFileByDir('../runtime');

        echo json_encode(array('code' => 1, 'msg' => '清除成功'));

        exit();

    }



    public function checking_pay_pwd(){

        $user       =   $this->check_user();

        $config     =   $this->set_config();

        if($this->request->post()){

            $fields         =   input('post.');

            if(md5(trim($fields['pass'])) != $user['m_pay_pwd']){

                return json(array('code' => 0, 'msg' => '交易密码错误'));

            }

            return json(array('code' => 1, 'msg' => '交易密码验证成功'));

        }

    }



    public function notice_list(){

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $list       =   Db::name('Notice')->order('id desc')->select();
        
        $uid  = $user["id"];
        
        $configMsg = 0;
        
        $time = time();
        
        $s_time = mktime(9, 0 , 0,date("m"),date("d"),date("Y"));
        
        $e_time = mktime(16, 0 , 0,date("m"),date("d"),date("Y"));
        

        if (($time < $s_time || $time > $e_time) && ($uid != 1309 && $uid != 2261 && $uid != 904 && $uid != 1129)) {

            $configMsg = 1;

        }

        foreach($list as &$v){

            $v['n_time']    =   date('Y-m-d',$v['n_time']);

            $v['n_img']     =   $v['n_img'] == ''?$config['w_logo']:$v['n_img'];

        }

        $this->assign(array('config'=>$config,'user'=>$user,'list'=>$list,'configMsg'=>$configMsg));

        return $this->fetch();

    }



    public function notice_con(){

        $config                 =   $this->set_config();

        $user                   =   $this->check_user();

        $id                     =   intval(input('id'));

        $detail                 =   Db::name('Notice')->where(array('id'=>$id))->find();

        if(!empty($detail)){

            $n_auth = $detail['n_auth']+1;

            Db::name('Notice')->where(array('id'=>$id))->update(array('n_auth'=>$n_auth));

            $detail['n_time']   =   date("Y-m-d H:i:s",$detail['n_time']);

        }

        $this->assign(array('config'=>$config,'user'=>$user,'detail'=>$detail));

        return $this->fetch();

    }



     /**
     * 二维码分享
     */
    public function share()
    {

        $config     =   $this->set_config();

        $user       =   $this->check_user();

        $path       =   url('Login/register', ['invite_code' => $user['m_account']]);

        $uid        =   intval($user['id']);

        $url        =   'http://' . $_SERVER['HTTP_HOST'] . $path;

        $this->assign(array('config' => $config, 'user' => $user, 'url' => $url));

        return $this->fetch();
    }
    /**
     * 快递查询 废弃
     */
    public function express()
    {

        $config     =   $this->set_config();
        // dump($config);
        $user       =   $this->check_user();

        // 首先得到用户id
        $uid        =   intval($user['id']);
        // 根据用户id查询这个用户所有的订单 待收货代表订单已经发出
        $orderWhere = array(
            'uid' => $uid,
            'o_status'  => 2
        );
        // dump($orderWhere);
        $orderInformation    = Db::name('order')->where($orderWhere)->all();
        // dump($orderInformation);
        // $name = "zhongtong";
        // $number = "547637748214";
        // dump($orderInformation);
        $allInfo=array();;
        foreach ($orderInformation as $info) {
            
            $name = $info['o_company'];
            $number = $info['o_company_num'];

            if($name && $number){
                $name = json_decode(plugin_action('Express/Express/getExpressCompany', [$info['o_company']]), true);
                $name=$name['showapi_res_body']['expressList'][0]['simpleName'];
                $expressInfo = plugin_action('Express/Express/getExpressInfo', [$name, $number]);
                $packageInfo = json_decode($expressInfo, true);
                array_push($allInfo,$packageInfo['showapi_res_body']['data']);
            }
          
        }

        $this->assign(array('config' => $config, 'allInfo' => $allInfo));

        return $this->fetch();
    }


    public function team(){

        $config          =   $this->set_config();

        $user            =   $this->check_user();

        $uid             =   $user['id'];

        $push_user       =   Db::name('User')->where("m_tid=$uid")->where(array('m_del'=>0))->select();

        $level_user      =   Db::name('User')->where("m_tid=$uid and m_level>=1")->where(array('m_del'=>0))->select();

        if(!empty($push_user)){

            foreach ($push_user as &$row){

                if($row['m_avatar'] == ''){

                    $row['m_avatar'] = $config['w_logo'];

                }else{

                    $m_avatar        = explode('.',$row['m_avatar']);

                    if(count($m_avatar)>2){

                        $row['m_avatar'] = $m_avatar[1].'.'.$m_avatar[2];

                    }

                }

            }

        }

        $team_user       =   Db::name('User')->where("m_line like '%,$uid,%'")->where(array('m_del'=>0))->select();

        $level_team      =   Db::name('User')->where("m_line like '%,$uid,%' and m_level>=1")->where(array('m_del'=>0))->select();

        if(!empty($push_user)){

            $status      = 1;

        }else{

            $status      = 0;

        }

        $this->assign(array('config'=>$config,'user'=>$user,'team_user'=>$team_user,'push_user'=>$push_user,'push_num'=>count($push_user),'team_num'=>count($team_user),'status'=>$status,'level_user'=>count($level_user),'level_team'=>count($level_team)));

        return $this->fetch();

    }





}