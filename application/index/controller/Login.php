<?php
namespace app\index\controller;
use think\Db;
use think\facade\Env;
use think\Session;
use think\Cookie;

header('Access-Control-Allow-Origin:*');
/**
 * 前台首页控制器
 * @package app\index\controller
 */
class Login extends Home
{


    public function login(){
        $config         = $this->set_config();
        $time = time();
        $date = date('Y-m-d',$time);
        if($this->request->isPost()){
            $fields     =   input('post.');
            $pass       =   md5(trim($fields['m_login_pwd']));
            $user       =   Db::name('User')->where(array('m_account'=>$fields['m_trade_code'],'m_login_pwd'=>$pass,'m_del'=>0,'m_reg_status'=>1))->find();
            if(empty($user)){
                return json(array('code'=>0,'msg'=>'账号密码有误，请检查后重试'));
            }
            if($user['m_lock']==1){
                return json(array('code'=>0,'msg'=>'账号已冻结,请联系管理员处理'));
            }
            $uid        = $user['id'];
            session('uid',$user['id']);
            session('memberUser',$user);
            cookie('z_uid',$user['id'],604800);
            add_wallet($user['id'],1);
            add_wallet($user['id'],3);
            $has_account= Db::name('Account')->where(array('a_uid'=>$uid,'a_bid'=>$uid))->find();
            if(empty($has_account)){
                $data   = array(
                    'a_uid'     =>  $uid,
                    'a_bid'     =>  $uid,
                    'a_time'    =>  time()
                );
                Db::name('Account')->insert($data);
            }
            $cont = Db::name('userReportCont')->where(array('uid'=>$uid,'addDate'=>$date))->find();
            if(empty($cont)){
                $contData = array(
                    'uid' => $uid,
                    'c_account' => $user['m_account'],
                    'c_nickname' => $user['m_nickname'],
                    'addDate' => $date
                );
                $contData['pid'] = 1;
                $contData['pname'] = '宁红一号';
                Db::name('userReportCont')->insert($contData);
                $contData['pid'] = 3;
                $contData['pname'] = '宁红二号';
                Db::name('userReportCont')->insert($contData);
            }
            SaveLastTime('User',$uid);
            AddUserIp($uid,1,'前台交易商登陆');
            return json(array('code'=>1,'msg'=>'登录成功'));
        }
        $this->assign(array('config'=>$config));
        return $this->fetch();
    }


    public function login_set(){
        $config         =   $this->set_config();
        $get_account    =   input('account');
        $account        =   isset($get_account)?$get_account:0;
        $user           =   Db::name('User')->field('id,m_name,m_account,m_avatar,m_nickname')->where(array('m_account'=>$account,'m_del'=>0))->find();
        $time = time();
        $date = date('Y-m-d',$time);
        if($user['m_avatar']){
            $m_avatar   = explode('.',$user['m_avatar']);
            if(count($m_avatar)>2){
                $user['m_avatar'] = $m_avatar[1].'.'.$m_avatar[2];
            }
        }else{
            $user['m_avatar'] = $config['w_logo'];
        }

        if($this->request->isPost()){
            $fields         = input('post.');
            $pass           = md5(trim($fields['m_login_pwd']));
            $has_user       = Db::name('User')->where(array('m_account'=>$fields['m_trade_code'],'m_login_pwd'=>$pass,'m_del'=>0))->find();
            if(empty($has_user)){
                return json(array('code' => 0, 'msg' => '密码有误，请检查后重试'));
            }
            if($has_user['m_lock']==1){
                return json(array('code' => 0, 'msg' => '账号已冻结,请联系客服处理'));
            }
            $uid            =  $has_user['id'];
            session('uid',$uid);
            $z_uid          =  cookie('z_uid');
            if(!$z_uid){
                cookie('z_uid',$uid,604800);
            }else{
                cookie('z_uid',$z_uid,604800);
            }
            SaveLastTime('User',$uid);
            AddUserIp($uid,1,'前台交易商登陆');
            $cont = Db::name('userReportCont')->where(array('uid'=>$uid,'addDate'=>$date))->find();
            if(empty($cont)){
                $contData = array(
                    'uid' => $uid,
                    'c_account' => $has_user['m_account'],
                    'c_nickname' => $has_user['m_nickname'],
                    'addDate' => $date
                );
                $contData['pid'] = 1;
                $contData['pname'] = '宁红一号';
                Db::name('userReportCont')->insert($contData);
                $contData['pid'] = 3;
                $contData['pname'] = '宁红二号';
                Db::name('userReportCont')->insert($contData);
            }
            return json(array('code' => 1, 'msg' => '登录成功'));
        }
        $this->assign(array('config'=>$config,'user'=>$user));
        return $this->fetch();
    }



    public function user_del(){
        if($this->request->isPost()){
            $id             = input('post.id');
            $z_uid          = cookie('z_uid');
            $has_account    = Db::name('Account')->where(array('a_uid'=>$z_uid,'a_bid'=>$id))->find();
            if(empty($has_account)){
                return json(array('code' => 0, 'msg' => '信息有误'));
            }
            $res            = Db::name('Account')->where(array('id'=>$has_account['id']))->delete();
            if($res){
                return json(array('code' => 1, 'msg' => '删除成功'));
            }else{
                return json(array('code' => 0, 'msg' => '删除失败'));
            }
        }
    }

    //注册验证
    public function sms_code()
    {
        if ($this->request->isPost()){
            $fields     =   input('post.');
            $v_phone    =   $fields['m_phone'];
            $type       =   $fields['type'];
            if (!$v_phone) {
                return json(array('code' => 0, 'msg' => '手机号填写有误'));
            }
            $user       =   Db::name('User')->where(array('m_phone'=>$v_phone,'m_del'=>0,'m_reg_status'=>1))->find();
            if($type==1){
                if(empty($user)) {
                    send_code($v_phone, 'reg');
                    return json(array('code' => 1, 'msg' => '验证码已发送至您的手机'));
                }else{
                    return json(array('code' => 0, 'msg' => '您输入的手机号已注册'));
                }
            }elseif($fields['type']==2){   //找回密码
                if(!empty($user)){
                    send_code($v_phone, 'fot');
                    return json(array('code' => 1, 'msg' => '验证码已发送至您的手机'));
                }else{
                    return json(array('code' => 0, 'msg' => '您输入的手机号未注册'));
                }
            }elseif ($fields['type']==3){  //修改手机号
                send_code($v_phone, 'set');
                return json(array('code' => 1, 'msg' => '验证码已发送至您的手机'));
            }
        }
    }

   public function register()
    {
        $config      = $this->set_config();
        $get_invite  = input('invite_code');
        $invite_code = isset($get_invite) ? $get_invite : 0;
        if ($this->request->isPost()) {
            $fields     =   input('post.');
            // 推荐交易商账户
            $has_push   =   Db::name('User')->where(array('m_account' => $fields['m_trade_code'], 'm_del' => 0))->find();
            // 如果不存在推荐交易商 则返回报错
            if (empty($has_push)) {
                return json(array('code' => 0, 'msg' => '推荐交易商不存在'));
            }
            // 如果推荐账户被锁定 返回报错
            if ($has_push['m_lock'] == 1) {
                return json(array('code' => 0, 'msg' => '推荐交易商账户已被锁定'));
            }
            // 检查手机号是否被注册过 并且是否没删除 0代表未被删除
            $has_phone  =   Db::name('User')->where(array('m_phone' => $fields['m_phone'], 'm_del' => 0))->find();
            if (!empty($has_phone)) {
                return json(array('code' => 0, 'msg' => '当前手机号已注册'));
            }
            // 手机号
            $phone    = session('phone');
            // 验证码 
            $verycode = session('verycode');
            if (!$phone || !$verycode) {
                return json(array('code' => 0, 'msg' => '请获取短信验证码'));
            }

            if ($phone != trim($fields['m_phone'])) {
                return json(array('code' => 0, 'msg' => '请输入验证的手机号'));
            }

            if ($verycode != trim($fields['m_code'])) {
                return json(array('code' => 0, 'msg' => '短信验证码错误'));
            }

            if (trim($fields['m_login_pwd']) != trim($fields['m_que_pwd'])) {
                return json(array('code' => 0, 'msg' => '两次密码不一致'));
            }
            if (trim($fields['m_pay_pwd']) != trim($fields['m_pay_pwd2'])) {
                return json(array('code' => 0, 'msg' => '两次支付密码不一致'));
            }
            if (!isset($fields['m_name'])) {
                return json(array('code' => 0, 'msg' => '请输入真实姓名'));
            }
            // 身份证信息验证
            $has_card   = Db::name('User')->where(array('m_car_id' => $fields['m_cardnum'], 'm_del' => 0, 'm_reg_status' => 1))->find();
            if (!empty($has_card)) {
                return json(array('code' => 0, 'msg' => '身份证号码已被注册'));
            }
            if (!$fields['m_car_img_1'] || !$fields['m_car_img_2']) {
                return json(array('code' => 0, 'msg' => '请按要求上传身份证照片'));
            }
            //上传正反面图片
            $m_car_img_1    = Base64GetImage($fields['m_car_img_1']);
            $m_car_img_2    = Base64GetImage($fields['m_car_img_2']);
            // 推荐人的id
            $m_tid          = $has_push['id'];
            // 推荐人的推荐树
            $m_line         = $has_push['m_line'];
            //生成账号
            $m_account      = $config['w_account_start'] . $fields['m_phone'];
            // 检查生成账号是否存在
            $has_account  =   Db::name('User')->where(array('m_account' => $m_account, 'm_del' => 0))->find();
            if (!empty($has_account)) {
                return json(array('code' => 0, 'msg' => '当前交易账号已存在'));
            }
            // 新增数据库信息
            $data         = array(
                'm_tid'             => $m_tid,
                'm_avatar'          => $config['w_logo'],
                'm_nickname'        => $fields['m_name'],
                'm_name'            => $fields['m_name'],
                'm_phone'           => $fields['m_phone'],
                'm_login_pwd'       => md5($fields['m_login_pwd']),
                'm_pay_pwd'         => md5($fields['m_pay_pwd']),
                'm_car_id'          => $fields['m_cardnum'],
                'm_account'         => $m_account,
                'm_level'           => 0,
                'm_sex'             => $fields['m_sex'],
                'm_car_img'         => $m_car_img_1 . ',' . $m_car_img_2,
                'm_reg_time'        => time(),
                'm_reg_status'      => 1,
            );
            //插入并得到插入后的id
            $member_id = Db::name('User')->insertGetId($data);
            // 如果推荐人存在 更新推荐人的数量
            if ($m_tid) {
                // 得到推荐人的信息
                $t_user         = getUserInfo($m_tid);
                // 初始化推荐人已经推荐的数量 默认为1 
                $m_push_num     = 1;
                // 更新推荐数量
                if (!empty($t_user)) {
                    $m_push_num = $t_user['m_push_num'] + 1;
                }
                // 进行数据库操作
                Db::name('User')->where(array('id' => $m_tid))->update(array('m_push_num' => $m_push_num));
            }
            // 更新新申请会员节点序列及所在层次
            if ($member_id) {
                Db::query('call register_recommentTree(:member_id,:recommend_id)', ['member_id' => $member_id,'recommend_id'=>$m_tid]);
                add_wallet($member_id, 1);
                add_wallet($member_id, 3);
                return json(array('code' => 1, 'msg' => '注册成功'));
            } else {
                return json(array('code' => 0, 'msg' => '发生未知错误，请联系在线客服处理'));
            }
        }
        // 设置数据
        $this->assign(array('config' => $config, 'invite_code' => $invite_code));
        return $this->fetch();
    }
    
    
    public function showdownload(){



        return $this->fetch();
    }

    public function account_list(){
        $config =   $this->set_config();
        $z_uid  =   cookie('z_uid');
        $list   =   Db::name('Account a')->join('w_user b','a.a_bid=b.id','left')->where(array('a_uid'=>$z_uid))->order('a_time asc')->select();
        foreach ($list as &$row){
            if($row['m_avatar']){
                $m_avatar   = explode('.',$row['m_avatar']);
                if(count($m_avatar)>2){
                    $row['m_avatar'] = $m_avatar[1].'.'.$m_avatar[2];
                }
            }else{
                $row['m_avatar'] = $config['w_logo'];
            }

        }
        $this->assign(array('config'=>$config,'list'=>$list));
        return $this->fetch();
    }

    public function account_add(){
        $config = $this->set_config();
        if($this->request->isPost()){
            $fields         =   input('post.');
            $pass           =   md5(trim($fields['m_login_pwd']));
            $user           =   Db::name('User')->where(array('m_account'=>$fields['m_trade_code'],'m_login_pwd'=>$pass,'m_del'=>0,'m_reg_status'=>1))->find();
            if(empty($user)){
                return json(array('code'=>0,'msg'=>'账号密码有误，请检查后重试'));
            }
            if($user['m_lock']==1){
                return json(array('code'=>0,'msg'=>'账号已冻结,请联系管理员处理'));
            }
            $uid            = $user['id'];
            $z_uid          = cookie('z_uid');
            if(!$z_uid){
                $z_uid      = $user['id'];
            }
            session('uid',$user['id']);
            $has_account    = Db::name('Account')->where(array('a_uid'=>$z_uid,'a_bid'=>$uid))->find();
            if(empty($has_account)){
                $data   = array(
                    'a_uid'     =>  $z_uid,
                    'a_bid'     =>  $uid,
                    'a_time'    =>  time()
                );
                $res = Db::name('Account')->insertGetId($data);
                if($res){
                    return json(array('code' => 1, 'msg' => '添加成功'));
                }else{
                    return json(array('code' => 0, 'msg' => '添加失败'));
                }
            }else{
                return json(array('code' => 0, 'msg' => '该账号已添加'));
            }
            //SaveLastTime('User',$uid);
            //AddUserIp($uid,1,'前台交易商登陆');
            return json(array('code'=>0,'msg'=>'添加成功'));
        }
        $this->assign(array('config'=>$config));
        return $this->fetch();
    }

    public function forget(){
        $config = $this->set_config();
        if($this->request->isPost()){
            $fields     =   input('post.');
            $has_phone  =   Db::name('User')->where(array('m_phone'=>$fields['m_phone'],'m_del'=>0))->find();
            if(empty($has_phone)){
                return json(array('code' => 0, 'msg' => '当前手机号未注册'));
            }
            if($has_phone['m_lock']==1){
                return json(array('code' => 0, 'msg' => '该手机号所绑定账号已被锁定'));
            }
            $phone    = session('phone');
            $verycode = session('verycode');
            if(!$phone || !$verycode){
                return json(array('code' => 0, 'msg' => '请获取短信验证码'));
            }
            if($phone != trim($fields['m_phone'])){
                return json(array('code' => 0, 'msg' => '请输入验证的手机号'));
            }
            if($verycode != trim($fields['m_code'])){
                return json(array('code' => 0, 'msg' => '短信验证码错误'));
            }
            if(md5(trim($fields['m_pass'])) == $has_phone['m_login_pwd']){
                echo json_encode(array('code' => 0, 'msg' => '新密码不能与原密码一致'));
                exit();
            }
            if(trim($fields['m_pass']) != trim($fields['m_que'])){
                return json(array('code' => 0, 'msg' => '两次密码不一致'));
            }
            $res = Db::name('User')->where(array('id'=>$has_phone['id']))->update(array('m_login_pwd'=>md5(trim($fields['m_pass']))));
            if($res){
                return json(array('code' => 1, 'msg' => '重置成功'));
            }else {
                return json(array('code' => 0, 'msg' => '重置失败'));
            }
        }
        $this->assign(array('config'=>$config));
        return $this->fetch();
    }

}