from attr import attr
import re
from excelData import excelData


class teaData(excelData):
    def __init__(self, Data):
        self.Data = Data
        self.attr = attr()

    def getInsertData(self):
        data = self.Data
        vals = []
        o_buy_num_count = 0
        excel_o_buy_num = 0
        for i in range(len(data)):
            if data[i]['产品名称'] == '':
                continue
            if data[i]['用户账号'] == '':
                continue

            data[i]['用户账号'] = str(data[i]['用户账号'])

            if '.' in data[i]['用户账号']:
                data[i]['用户账号'] = data[i]['用户账号'].split(".")[0]

            count = len(data[i]['用户账号'].split("/"))
            if count > 1:
                together = data[i]['用户账号'].split("/")
                excel_o_buy_num = excel_o_buy_num + count
                for j in range(len(together)):
                    gid = self.attr.getGid(data[i]['产品名称'], 1)
                    m_account = re.findall("\d+", together[j])

                    o_buy_num = int(m_account[1])
                    o_buy_num_count = o_buy_num_count + o_buy_num
                    uid, m_type = self.attr.getUid(m_account[0])
                    uid = uid
                    o_type = m_type
                    o_price = 186
                    # 等下确定
                    o_credit = 186
                    o_credit_1 = 0
                    o_credit_2 = 186 * o_buy_num
                    o_wot = 0
                    o_code = ""
                    o_info = "用户提货"
                    aid = self.attr.getAid(data[i]['收货地址'])
                    o_status = 0
                    o_company_num = ""
                    o_company_price = ""
                    o_company_code = ""
                    o_company = ""
                    o_addtime = ""
                    o_send_time = ""
                    o_take_time = ""
                    o_pay_time = ""
                    last_time = ""
                    o_exchange_type = 1
                    is_qs = 1
                    val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                           o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)

                    vals.append(val)
            else:

                gid = self.attr.getGid(data[i]['产品名称'], 1)
                o_buy_num = int(data[i]['发货数量'])
                excel_o_buy_num = excel_o_buy_num + o_buy_num
                o_buy_num_count = o_buy_num_count + o_buy_num
                uid, m_type = self.attr.getUid(data[i]['用户账号'])
                uid = uid
                o_type = m_type
                o_price = 186
                # 等下确定
                o_credit = 186
                o_credit_1 = 0
                o_credit_2 = 186*o_buy_num
                o_wot = 0
                o_code = ""
                o_info = "用户提货"
                aid = self.attr.getAid(data[i]['收货地址'])
                o_status = 0
                o_company_num = ""
                o_company_price = ""
                o_company_code = ""
                o_company = ""
                o_addtime = ""
                o_send_time = ""
                o_take_time = ""
                o_pay_time = ""
                last_time = ""
                o_exchange_type = 1
                is_qs = 1
                val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                       o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)
                vals.append(val)
        return o_buy_num_count, vals
