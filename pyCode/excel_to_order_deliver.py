import os
import pymysql
import xlrd
import re
from teaData import teaData
from redwineData import redwineData
from Excel import Excel
from Order_Deliver_Table import Order_Deliver_Table
keys = {
    'tea': ['产品名称', '用户账号', '收货姓名', '收货手机号', '收货地址', '发货数量', '商品单价', '物流单号'],

    'redWine': ['店铺名称', '原始单号', '用户账号', '收件人', '省', '市', '区', '地址', '手机', '固话', '网名（发货人姓名）', '发票抬头（发货人电话）', '发货条件', '应收合计', '邮费', '优惠金额',
                        'COD买家费用', '仓库名称', '物流公司', '买家备注', '客服备注', '商家编码', '货品数量', '货品价格', '货品总价', '货品优惠', '订单类别', '发票内容（支付方式）', '业务员', '客服备注']
}
excel_o_buy_num = 0
def insertData(key, data, excelPath):
    global excel_o_buy_num
    vals = []
    excel_o_buy_num = 0
    if key == keys['tea']:
        buynum, vals = getinsertData(data, 1)

    elif key != keys['tea']:
        buynum, vals = getinsertData(data, 2)
    
    orderDeliver = Order_Deliver_Table()
    orderDeliver.insert(vals)
    excel_o_buy_num = excel_o_buy_num+buynum


def getinsertData(data, orderType):
    vals = []
    buynum = 0
    if orderType == 1:
        tea = teaData(data)
        buynum, vals = tea.getInsertData()
    elif orderType == 2:
        redwine = redwineData(data)
        buynum,  vals = redwine.getInsertData()

    return buynum, vals


if __name__ == "__main__":
    # path = "C:/Users/Administrator/Desktop/红酒49384"
    # path = "C:/Users/Administrator/Desktop/绿茶6159"
    path = "C:/Users/Administrator/Desktop/红茶60816"
    for item in os.listdir(path):
        dirPath = path+'/'+item
        for fileName in os.listdir(dirPath):
            excelPath = dirPath+'/'+fileName
            if os.path.isdir(excelPath) == False and '~' not in excelPath:
                excel = Excel(excelPath)
                key, exceldata = excel.getExcelData()
                insertData(key, exceldata, excelPath)
                print(excel_o_buy_num)

            else:
                pass
