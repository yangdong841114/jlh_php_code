import xlrd
class Excel:
    def __init__(self, excelPath):
        self.excelPath = excelPath
        self.keys  = {
            'tea': ['产品名称', '用户账号', '收货姓名', '收货手机号', '收货地址', '发货数量', '商品单价', '物流单号'],

            'redWine': ['店铺名称', '原始单号', '用户账号', '收件人', '省', '市', '区', '地址', '手机', '固话', '网名（发货人姓名）', '发票抬头（发货人电话）', '发货条件', '应收合计', '邮费', '优惠金额',
                        'COD买家费用', '仓库名称', '物流公司', '买家备注', '客服备注', '商家编码', '货品数量', '货品价格', '货品总价', '货品优惠', '订单类别', '发票内容（支付方式）', '业务员', '客服备注']
        }
       


    def getExcelData(self):
        excelPath = self.excelPath
        print(excelPath)
        data = xlrd.open_workbook(excelPath)
        table = data.sheet_by_index(0)
        tableRows = table.nrows
        tableColumns = table.ncols
        # 定义一个空列表
        key = table.row_values(0)

        if key == self.keys['tea']:
            datas = self.getTableData(table, key)
            return key, datas
        else:
            datas = self.getTableData(table, key)
            return key, datas

    def getTableData(self, table, key):
        tableRows = table.nrows
        tableColumns = table.ncols
        datas = []
        for i in range(1, tableRows):
            # 定义一个空字典
            sheet_data = {}
            for j in range(tableColumns):
                # 获取单元格数据
                c_cell = table.cell_value(i, j)
                # 生成键值对
                sheet_data[key[j]] = c_cell

            datas.append(sheet_data)
        # 返回从excel中获取到的数据：以列表存字典的形式返回
        return datas
