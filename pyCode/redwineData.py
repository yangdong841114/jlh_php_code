
from attr import attr
from excelData import excelData


class redwineData(excelData):
    def __init__(self, Data):
        self.Data = Data
        self.attr = attr()

    def getInsertData(self):
        data = self.Data
        vals = []
        excel_o_buy_num = 0
        print(len(data))
        for i in range(len(data)):

            if data[i]['商家编码'] == 'ZYAGTJX':
                gid = self.attr.getGid("智域·阿根廷精选干红", 2)
            elif data[i]['商家编码'] == 'ZYADSZY':
                gid = self.attr.getGid("安弟斯之源干红葡萄酒", 2)

            if data[i]['货品数量'] == '':
                continue

            o_buy_num = int(data[i]['货品数量'])
            excel_o_buy_num = excel_o_buy_num + o_buy_num

            # m_account = '18'+str(data[i]['手机']).strip()
            m_account = str(data[i]['用户账号']).strip()
            uid, m_type = self.attr.getUid(m_account)
            if str(data[i]['手机']).strip() == '':
                uid = 999999

            if uid == 999999:
                uid = 999999

            uid = uid
            o_type = m_type
            o_price = 186
            # 等下确定
            o_credit = 186
            o_credit_1 = 0
            o_credit_2 = 186*o_buy_num
            o_wot = 0
            o_code = ""
            o_info = "用户提货"
            aid = self.attr.getAid(data[i]['地址'])
            o_status = 0
            o_company_num = ""
            o_company_price = ""
            o_company_code = ""
            o_company = ""
            o_addtime = ""
            o_send_time = ""
            o_take_time = ""
            o_pay_time = ""
            last_time = ""
            o_exchange_type = 3
            is_qs = 1
            val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                   o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)
            vals.append(val)

        return excel_o_buy_num, vals
