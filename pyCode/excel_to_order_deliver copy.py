import os
import pymysql
import xlrd
import re
conn = pymysql.connect(
    host='127.0.0.1',
    user='JLH',
    password='root',
    db='JLH',
    charset='utf8',
       # autocommit=True,    # 如果插入数据，， 是否自动提交? 和conn.commit()功能一致。
)

cur = conn.cursor()
keys = {
    'tea': ['产品名称', '用户账号', '收货姓名', '收货手机号', '收货地址', '发货数量', '商品单价', '物流单号'],

    'redWine': ['店铺名称', '原始单号', '用户账号', '收件人', '省', '市', '区', '地址', '手机', '固话', '网名（发货人姓名）', '发票抬头（发货人电话）', '发货条件', '应收合计', '邮费', '优惠金额',
                'COD买家费用', '仓库名称', '物流公司', '买家备注', '客服备注', '商家编码', '货品数量', '货品价格', '货品总价', '货品优惠', '订单类别', '发票内容（支付方式）', '业务员', '客服备注']
}

mutCount = 0
continueCount = 0
alldataCount = 0
o_buy_num_count = 0


def insertData(key, data, excelPath):
    global mutCount
    global continueCount
    global alldataCount
    global o_buy_num_count
    vals = []
    excel_o_buy_num = 0

    if key == keys['tea']:
        vals = getinsertData(data, 1)
    elif key != keys['tea']:
        vals = getinsertData(data, 2)

    sql = "INSERT INTO w_order_deliver(gid, o_buy_num,uid,o_type,o_price,o_credit,o_credit_1,o_credit_2,o_wot,o_code,o_info,aid,o_status,o_company_num,o_company_price,o_company_code,o_company,o_addtime,o_send_time,o_take_time,o_pay_time,last_time,o_exchange_type,is_qs) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s);"
    print(excelPath)
    print(excel_o_buy_num)
    try:
        alldataCount = alldataCount+len(vals)
        cur.executemany(sql, vals)
        conn.commit()

    except BaseException as e:
        return 0

    return 1


def getinsertData(data, orderType):
    vals = []
    if orderType == 1:
        for i in range(len(data)):
            if data[i]['产品名称'] == '':
                continueCount = continueCount + 1

                continue
            if data[i]['用户账号'] == '':
                continueCount = continueCount + 1

                continue

            data[i]['用户账号'] = str(data[i]['用户账号'])

            if '.' in data[i]['用户账号']:
                data[i]['用户账号'] = data[i]['用户账号'].split(".")[0]

            count = len(data[i]['用户账号'].split("/"))
            if count > 1:
                together = data[i]['用户账号'].split("/")

                mutCount = mutCount + count
                excel_o_buy_num = excel_o_buy_num + count
                for j in range(len(together)):
                    gid = getGid(data[i]['产品名称'], 1)
                    m_account = re.findall("\d+", together[j])

                    o_buy_num = int(m_account[1])
                    o_buy_num_count = o_buy_num_count + o_buy_num
                    uid, m_type = getUid(m_account[0])
                    uid = uid
                    o_type = m_type
                    o_price = 186
                    # 等下确定
                    o_credit = 186
                    o_credit_1 = 0
                    o_credit_2 = 186 * o_buy_num
                    o_wot = 0
                    o_code = ""
                    o_info = "用户提货"
                    aid = getAid(data[i]['收货地址'])
                    o_status = 0
                    o_company_num = ""
                    o_company_price = ""
                    o_company_code = ""
                    o_company = ""
                    o_addtime = ""
                    o_send_time = ""
                    o_take_time = ""
                    o_pay_time = ""
                    last_time = ""
                    o_exchange_type = 1
                    is_qs = 1
                    val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                           o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)

                    vals.append(val)
            else:
                gid = getGid(data[i]['产品名称'], 1)
                o_buy_num = int(data[i]['发货数量'])
                excel_o_buy_num = excel_o_buy_num + o_buy_num
                o_buy_num_count = o_buy_num_count + o_buy_num
                uid, m_type = getUid(data[i]['用户账号'])
                uid = uid
                o_type = m_type
                o_price = 186
                # 等下确定
                o_credit = 186
                o_credit_1 = 0
                o_credit_2 = 186*o_buy_num
                o_wot = 0
                o_code = ""
                o_info = "用户提货"
                aid = getAid(data[i]['收货地址'])
                o_status = 0
                o_company_num = ""
                o_company_price = ""
                o_company_code = ""
                o_company = ""
                o_addtime = ""
                o_send_time = ""
                o_take_time = ""
                o_pay_time = ""
                last_time = ""
                o_exchange_type = 1
                is_qs = 1
                val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                       o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)
                vals.append(val)
    elif orderType == 1:
        # 这里是红酒的
        for i in range(len(data)):
            # print("插入数据成功")

            if data[i]['商家编码'] == 'ZYAGTJX':
                gid = getGid("智域·阿根廷精选干红", 2)
            elif data[i]['商家编码'] == 'ZYADSZY':
                gid = getGid("安弟斯之源干红葡萄酒", 2)

            if data[i]['货品数量'] == '':

                continue
            o_buy_num = int(data[i]['货品数量'])
            excel_o_buy_num = excel_o_buy_num + o_buy_num
            o_buy_num_count = o_buy_num + o_buy_num_count
            # print(data[i]['用户账号'])
            m_account = '18'+str(data[i]['手机']).strip()
            uid, m_type = getUid(m_account)
            if str(data[i]['手机']).strip() == '':

                uid = 999999

            # print(uid)
            if uid == 999999:

                # print(excelPath)
                # print(m_account)
                uid = 999999
                # exit()

            uid = uid
            o_type = m_type
            o_price = 186
            # 等下确定
            o_credit = 186
            o_credit_1 = 0
            o_credit_2 = 186*o_buy_num
            o_wot = 0
            o_code = ""
            o_info = "用户提货"
            aid = getAid(data[i]['地址'])
            o_status = 0
            o_company_num = ""
            o_company_price = ""
            o_company_code = ""
            o_company = ""
            o_addtime = ""
            o_send_time = ""
            o_take_time = ""
            o_pay_time = ""
            last_time = ""
            o_exchange_type = 3
            is_qs = 1
            val = (gid, o_buy_num, uid, o_type, o_price, o_credit, o_credit_1, o_credit_2, o_wot, o_code, o_info, aid, o_status, o_company_num,
                   o_company_price, o_company_code, o_company, o_addtime, o_send_time, o_take_time, o_pay_time, last_time, o_exchange_type, is_qs)
            vals.append(val)

    return vals


def getGid(self, name, gType):
    cur = self.cur
    if gType == 1:
        name = name.split("-")[0].strip()

        sql = "select * from w_product where p_title = '" + name + "';"
    else:
        sql = "select * from w_goods where g_title = '" + name + "';"

    try:
        cur.execute(sql)
        product = cur.fetchall()
        return product[0][0]
    except BaseException as e:
        return 999999


def getUid(self, m_account):
    cur = self.cur
    if '-' in m_account:
        m_account = m_account.split("-")[0].strip()
    else:
        m_account = m_account.strip()

    sql = "select * from w_user where m_account like '" + m_account + "';"

    try:
        cur.execute(sql)
        user = cur.fetchall()
        # print(m_account)
        return user[0][0], user[0][7]
    except BaseException as e:
        # print(m_account)
        return 999999, 999999


def getAid(self, a_detail):
    cur = self.cur
    a_detail = a_detail.split(" ")[-1].strip()

    sql = "select * from w_address where a_detail like '" + a_detail + "';"

    try:
        cur.execute(sql)
        address = cur.fetchall()
        return address[0][0]
    except BaseException as e:
        return 999999


def getExcelData(excelPath):

    data = xlrd.open_workbook(excelPath)
    table = data.sheet_by_index(0)
    tableRows = table.nrows
    tableColumns = table.ncols

    # 定义一个空列表
    key = table.row_values(0)
    # print(key)

    if key == keys['tea']:
        datas = getTableData(table, key)
        return key, datas
    else:
        datas = getTableData(table, key)
        return key, datas


def getTableData(table, key):
    tableRows = table.nrows
    tableColumns = table.ncols
    datas = []
    for i in range(1, tableRows):
        # 定义一个空字典
        sheet_data = {}
        for j in range(tableColumns):
            # 获取单元格数据
            c_cell = table.cell_value(i, j)
            # 生成键值对
            sheet_data[key[j]] = c_cell

        datas.append(sheet_data)
    # 返回从excel中获取到的数据：以列表存字典的形式返回
    return datas


if __name__ == "__main__":
    # path = "C:/Users/Administrator/Desktop/红酒49384"
    # path = "C:/Users/Administrator/Desktop/绿茶6159"
    path = "C:/Users/Administrator/Desktop/红茶60816"
    # path = "C:/Users/Administrator/Desktop/wine"
    allRow = 0
    for item in os.listdir(path):
        dirPath = path+'/'+item
        for fileName in os.listdir(dirPath):
            excelPath = dirPath+'/'+fileName
            if os.path.isdir(excelPath) == False and '~' not in excelPath:

                key, exceldata = getExcelData(excelPath)
                insertData(key, exceldata, excelPath)

                allRow = allRow + len(exceldata)-1
            else:
                pass

    # print(allRow)
    print(alldataCount)
    print(mutCount)
    print(continueCount)
    print(o_buy_num_count)
    # print(fileName,os.path.isdir(dirPath+'/'+fileName))
