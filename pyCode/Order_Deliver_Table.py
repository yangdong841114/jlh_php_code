import pymysql


class Order_Deliver_Table:
    def __init__(self):
        self.conn = pymysql.connect(
            host='127.0.0.1',
            user='JLH',
            password='root',
            db='JLH',
            charset='utf8',
            # autocommit=True,    # 如果插入数据，， 是否自动提交? 和conn.commit()功能一致。
        )
        self.cur = self.conn.cursor()

    def insert(self,vals):
        cur = self.cur
        sql = "INSERT INTO w_order(gid, o_buy_num,uid,o_type,o_price,o_credit,o_credit_1,o_credit_2,o_wot,o_code,o_info,aid,o_status,o_company_num,o_company_price,o_company_code,o_company,o_addtime,o_send_time,o_take_time,o_pay_time,last_time,o_exchange_type,is_qs) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s);"
        try:
            cur.executemany(sql, vals)
            conn.commit()
            return 1
        except BaseException as e:
            return 0
