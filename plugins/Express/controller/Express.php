<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace plugins\Express\controller;

use app\common\controller\Common;


require_once  dirname(dirname(__FILE__)) . '/PHPExpress/Util/Autoloader.php';

/**
 * Excel控制器
 * @package plugins\Express\controller
 */
class Express extends Common
{


    /**
     * 根据快递名字，快递单号，用户电话来获取快递信息
     * @param string $name 快递名 如：中通快递
     * @param string $number 快递单号
     * @param string $phone 客户电话
     * @author 张云辉 <275885445@qq.com>
     */
    public function getExpressInfo($name, $number)
    {
        $showapi_appid = "390333";
        $showapi_sign = "301b41c73b2c41d4aaa26f468ef7c86e";

        $url = "http://route.showapi.com/64-19";
        $req = new \ShowapiRequest($url, $showapi_appid, $showapi_sign);
        $req->addTextPara("com", $name);
        $req->addTextPara("nu", $number);
        $response = $req->post();

        // print_r($response->getContent());
        return $response->getContent();
    }

    /**
     * 查询快递的建议名字
     * @param string $name 快递名 如：中通快递
     * @param string $number 快递单号
     * @param string $phone 客户电话
     * @author 张云辉 <275885445@qq.com>
     */
    public function getExpressCompany($name)
    {
        
        $showapi_appid = "390333";
        $showapi_sign = "301b41c73b2c41d4aaa26f468ef7c86e";

        $url = "http://route.showapi.com/64-20";
        $req = new \ShowapiRequest($url, $showapi_appid, $showapi_sign);
        $req->addTextPara("expName", $name);
        $req->addTextPara("maxSize", "");
        $req->addTextPara("page", "");
        $response = $req->post();

        // print_r($response->getContent());
        return $response->getContent();
    }
    /**
     * getExpressInfo的升级版
     * @param string $express_name 快递名 如：中通快递
     * @param string $express_num 快递单号
     * @author 张云辉 <275885445@qq.com>
     */
    public function getExpressData($express_name,$express_num)
    {



        $name                     = isset($express_name)? $express_name  : 0;

        $number                     = isset($express_num)? $express_num  : 0;


        $allInfo = array();
        

        if ($name && $number) {
            $temp['expressName'] = $name ;

            $temp['expressNum'] = $number  ;

            $name = json_decode(plugin_action('Express/Express/getExpressCompany', [$name]), true);
  
            $name = $name['showapi_res_body']['expressList'][0]['simpleName'];
            $expressInfo = plugin_action('Express/Express/getExpressInfo', [$name, $number]);
            $packageInfo = json_decode($expressInfo, true);
            $temp['data'] =  $packageInfo['showapi_res_body']['data']  ;


            array_push($allInfo, $temp);
        }
        return  $allInfo;

    }
}
